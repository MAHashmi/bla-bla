﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hussmann.Models;
using UnitCoolerDataModel;
using Hussmann.Common.Enumeration;
using Hussmann.Common.Helper;
using CondenserDataModel;
using CompressorDataModel;

namespace Hussmann.Common
{
    public class DashboardNewsFeed
    {
        #region Lists
        public List<DashboardCommonNewsFeedViewModel> CommonNewsFeed = new List<DashboardCommonNewsFeedViewModel>();
        #endregion

        #region Objects
        private RulestreamCoilDBEntities UnitCoolerDBContext = new RulestreamCoilDBEntities();
        private RulestreamCondenserDBEntities CondenserDBContext = new RulestreamCondenserDBEntities();
        private RulestreamCompressorDBEntities ConpressorDBCOntext = new RulestreamCompressorDBEntities();
        #endregion

        // merge data of four different databases  into common list
        public List<DashboardCommonNewsFeedViewModel> GetCombinedListofDashboardNewsFeed(string userType, string userName)
        {
            try
            {
                // For unit cooler dashboard news feed
                GetUnitCoolerDashboardNewsFeed(CommonNewsFeed, userType, userName);

                //For Case news feed
                GetCaseDashboardNewsFeed(CommonNewsFeed, userType, userName);

                //For Compressor news feed
                GetCompressorDashboardNewsFeed(CommonNewsFeed, userType, userName);

                //For Condenser news feed
                GetCondenserDashboardNewsFeed(CommonNewsFeed, userType, userName);

                //Sort CommonNewsFeed on the basis of LastModifiedDate
                CommonNewsFeed = CommonNewsFeed.OrderByDescending(a => a.LastModifiedOn).Select(x => new DashboardCommonNewsFeedViewModel() {
                    ModelID = x.ModelID,
                    StateID = x.StateID,
                    ModelNumber = x.ModelNumber,
                    ActionBy = x.ActionBy,
                    CreatedBy = x.CreatedBy,
                    LastModifiedOn = x.LastModifiedOn,
                    ChillerType = x.ChillerType
                }).Take(10).ToList();
                //return Object

            }
            catch(Exception ex)
            {
                Logger.PrintError(ex);
            }
            return CommonNewsFeed;
        }

        private void GetCondenserDashboardNewsFeed(List<DashboardCommonNewsFeedViewModel> commonNewsFeed, string userType, string userName)
        {
            try
            {
                List<DashboardCommonNewsFeedViewModel> newsFeedData = CondenserDBContext.spGetCondenserDasboardNewsFeed(userType, userName).Select(x => new DashboardCommonNewsFeedViewModel()
                {
                    ModelID = x.ModelID,
                    StateID = x.StateID,
                    ModelNumber = x.ModelNumber,
                    ActionBy = x.ActionBy,
                    CreatedBy = x.CreatedBy,
                    LastModifiedOn = x.LastModifiedOn,
                    ChillerType = enChillerType.Condenser.ToString()

                }).ToList();
                commonNewsFeed.AddRange(newsFeedData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetCompressorDashboardNewsFeed(List<DashboardCommonNewsFeedViewModel> commonNewsFeed, string userType, string userName)
        {
            try
            {
                List<DashboardCommonNewsFeedViewModel> newsFeedData = ConpressorDBCOntext.spGetCompressorDasboardNewsFeed(userType,userName).Select(x => new DashboardCommonNewsFeedViewModel()
                {
                    ModelID = x.ModelID,
                    StateID = x.StateID,
                    ModelNumber = x.ModelNumber,
                    ActionBy = x.ActionBy,
                    CreatedBy = x.CreatedBy,
                    LastModifiedOn = x.LastModifiedOn,
                    ChillerType = enChillerType.Compressor.ToString()

                }).ToList();
                commonNewsFeed.AddRange(newsFeedData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // For getting case dashboard news feed data
        private void GetCaseDashboardNewsFeed(List<DashboardCommonNewsFeedViewModel> commonNewsFeed, string userType, string userName)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // For getting unit cooler dashboard news feed data
        private void GetUnitCoolerDashboardNewsFeed(List<DashboardCommonNewsFeedViewModel> commonNewsFeed,string userType, string userName)
        {
            try
            {

                List<DashboardCommonNewsFeedViewModel> newsFeedData = UnitCoolerDBContext.spGetUnitCoolerDasboardNewsFeed(userType, userName).Select(x => new DashboardCommonNewsFeedViewModel()
                {
                    ModelID = x.ModelID,
                    StateID = x.StateID,
                    ModelNumber = x.ModelNumber,
                    ActionBy = x.ActionBy,
                    CreatedBy = x.CreatedBy,
                    LastModifiedOn = x.LastModifiedOn,
                    ChillerType = enChillerType.UnitCooler.ToString()

                }).ToList();
                commonNewsFeed.AddRange(newsFeedData);

            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }
        }
    }
}