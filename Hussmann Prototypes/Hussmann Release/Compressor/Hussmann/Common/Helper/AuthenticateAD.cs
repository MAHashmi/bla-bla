﻿using Hussmann.Models;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.Protocols;
using System.Security;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Hussmann.Common.Enumeration;
using System;
using System.DirectoryServices;
using System.Text.RegularExpressions;
using System.Text;
using Hussmann.Common.Helper;
using UnitCoolerDataModel;

namespace Hussmann.Attribute
{
    public class AuthorizeADAttribute : AuthorizeAttribute
    {
        private RulestreamCoilDBEntities UnitCoolerDBContext = new RulestreamCoilDBEntities();
        private Role[] _roles;
        public AuthorizeADAttribute(params Role[] roles)
        {
            _roles = roles;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            var routeData = httpContext.Request.RequestContext.RouteData;
            string currentAction = routeData.GetRequiredString("action");
            string userType = Authorization.GetUserType(httpContext.ApplicationInstance.Context);
            string userName = httpContext.User.Identity.Name.Split('\\')[1];

            var isAuthorized = base.AuthorizeCore(httpContext);

            // Data access validation
            if (currentAction == Convert.ToString(enAction.EditUnitCooler))
            {
                var ModelID = (httpContext.Request.RequestContext.RouteData.Values["ModelID"] as string)
             ??
             (httpContext.Request["ModelID"] as string);
                int modelID = Convert.ToInt32(ModelID);

                var model = UnitCoolerDBContext.
                CoilModels.Where
                (x => x.ModelID == modelID).Select(x => x).FirstOrDefault();

                // if model does not exsist then return false
                if (model == null)
                {
                    return false;
                }

                if (userType == Convert.ToString(enUserType.DataFeeder))
                {
                    if (userName != model.CreatedBy && model.StateID != (int)enState.Approved)
                    {
                        return false;
                    }
                }
                else if (userType == Convert.ToString(enUserType.Reader))
                {
                    if (model.StateID != (int)enState.Approved)
                    {
                        return false;
                    }
                }
            }
            else if (currentAction == Convert.ToString(enAction.AddUnitCooler))
            {
                if (userType == Convert.ToString(enUserType.Reader))
                {
                    return false;
                }
            }



            if (!isAuthorized) return false;


            if (Authorization.IsCookieNull())
            {
                Authorization.SetDataToCookies(httpContext.ApplicationInstance.Context);
            }


            if (Authorization.IsCookieNull())
            {
                return false;
            }
            else
            {
                return Authorization.IsAuthenticate(httpContext.ApplicationInstance.Context, _roles);
            }


        }

        protected override void HandleUnauthorizedRequest(
        AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(
                 new RouteValueDictionary
                 {
                  { "action", "UnAuthorized" },
                  { "controller", "UnitCooler" },
                  });
            }
            else
                base.HandleUnauthorizedRequest(filterContext);
        }
        /// <summary>
        /// Used to get the current login in user group from AD
        /// </summary>

        public string GetAdGroupForUser(SearchResult searchResult)
        {

            var userGroups = new List<string>();

            DirectoryEntry personEntry = searchResult.GetDirectoryEntry();
            PropertyValueCollection groups = personEntry.Properties["memberOf"];

            foreach (object group in groups)
            {
                string groupName = Regex.Replace(group.ToString(), @"^CN=(.*?)(?<!\\),.*", "$1");
                userGroups.Add(groupName);//name convention
            }

            if (userGroups.Count > 0)
            {

                //Get the user rules in json
                string file = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["UserGroupsRulesPath"]);
                string Server = ConfigurationManager.AppSettings["Server"];
                file = file.Replace("{0}", Server);

                //deserialize JSON from file  
                string userRuleJson = System.IO.File.ReadAllText(file);
                JObject jsonObj = JObject.Parse(userRuleJson);
                List<string> keys = jsonObj.Properties().Select(p => p.Name).ToList();



                var result = userGroups.Where(x => keys.Contains(x)).ToList();



                if (result.Count > 0)
                {
                    return result.FirstOrDefault().ToString(); //assumption user belong to single group
                }
                else
                {
                    return string.Empty;//user cannot belong to any group
                }
            }
            else
            {
                return string.Empty; //No group associated to current user
            }
        }

        /// <summary>
        /// Common method use to return the provided property name from active directory
        /// </summary>
        /// <param name="searchResult"></param>
        /// <param name="PropertyName"></param>
        /// <returns>Provide property value </returns>
        public string GetProperty(SearchResult searchResult, string PropertyName)
        {
            if (searchResult.Properties.Contains(PropertyName))
            {
                return searchResult.Properties[PropertyName][0].ToString();
            }
            else
            {
                return string.Empty;
            }
        }



        /// <summary>
        /// Use to get the user data against login user
        /// </summary>
        /// <param name="loginUserName"></param>
        /// <returns>AD data present against login user</returns>
        public UserProfile GetUserData(string loginUserName)
        {
            UserProfile usrProfile = new UserProfile();
            usrProfile = null;
            try
            {
                DirectoryEntry de = GetDirectoryObject();
                DirectorySearcher deSearch = new DirectorySearcher();
                deSearch.SearchRoot = de;
                deSearch.Filter = "(&(objectClass=user)(SAMAccountName=" + loginUserName + "))"; //this is required to access AD 
                deSearch.SearchScope = System.DirectoryServices.SearchScope.Subtree;
                SearchResult results = deSearch.FindOne();

                //User not present in the directory
                if (results == null)
                {
                    return usrProfile; //NotAuthorized
                }

                string userGroup = GetAdGroupForUser(results);

                if (string.IsNullOrEmpty(userGroup))
                {
                    return usrProfile; //NotAuthorized
                }

                //Get the user rules in json
                string file = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["UserGroupsRulesPath"]);
                string Server = ConfigurationManager.AppSettings["Server"];
                file = file.Replace("{0}", Server);
                //deserialize JSON from file  
                string userRuleJson = System.IO.File.ReadAllText(file);
                var rules = JObject.Parse(userRuleJson).SelectToken(userGroup).ToObject<GroupResponsibilities>();

                if (rules == null)
                {
                    return usrProfile;
                }
                usrProfile = new UserProfile();
                usrProfile.UserName = loginUserName;
                usrProfile.GroupName = userGroup;
                usrProfile.GroupResponsibilities = rules;

            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return usrProfile;
        }
        /// <summary>
        /// Use to get the active directory against provided credential 
        /// </summary>        
        private DirectoryEntry GetDirectoryObject()
        {

            string activeDirectoryServerDomain = ConfigurationManager.AppSettings["ADServer"];
            string username = ConfigurationManager.AppSettings["ADUserName"];
            string password = ConfigurationManager.AppSettings["ADPassword"];

            DirectoryEntry oDE;
            //oDE = new DirectoryEntry("LDAP://192.168.1.101", "administrator", "password", AuthenticationTypes.Secure);
            oDE = new DirectoryEntry("LDAP://" + activeDirectoryServerDomain, username + "@" + activeDirectoryServerDomain, password, AuthenticationTypes.Secure);
            return oDE;
        }

    }
    public static class Authorization
    {
        /// <summary>
        /// Get the Group name for the login user 
        /// </summary>        
        public static string GetUserGroup(HttpContext context)
        {
            if (Authorization.IsCookieNull())
            {
                Authorization.SetDataToCookies(context);
            }
            var cookieValue = GetValueFromCookies();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            UserProfile user = JsonConvert.DeserializeObject<UserProfile>(cookieValue);
            return user.GroupName;
        }



        public static bool IsAuthenticate(HttpContext context, params Role[] roles)
        {

            if (Authorization.IsCookieNull())
            {
                Authorization.SetDataToCookies(context);
            }
            var cookieValue = GetValueFromCookies();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            UserProfile user = JsonConvert.DeserializeObject<UserProfile>(cookieValue);

            //Convent User Given roles to string              
            var userRules = roles.Select(a => a.ToString()).ToArray();

            var groupResponsibilities = user.GroupResponsibilities;

            //Check User have access against given rights
            var type = groupResponsibilities.GetType();
            var haveAccess = type.GetProperties().Select(x => new SelectListItem { Text = x.Name, Value = x.GetValue(groupResponsibilities, null).ToString() })
                .Where(x => userRules.Contains(x.Text) && x.Value.ToLower() == "true").ToList();

            return haveAccess.Count() == roles.Count();
        }

        /// <summary>
        ///  used to return cookie from current cookie
        /// </summary>
        public static string GetValueFromCookies()
        {
            var cookieName = ConfigurationManager.AppSettings["AppCookie"];
            var accessCookie = HttpContext.Current.Request.Cookies[cookieName];//user present in cookie                
            return Encoding.UTF8.GetString(MachineKey.Unprotect(Convert.FromBase64String(accessCookie.Value)));
        }

        /// <summary>
        ///  Used for checking that correct is null
        /// </summary>        
        public static bool IsCookieNull()
        {
            var cookieName = ConfigurationManager.AppSettings["AppCookie"];
            return HttpContext.Current.Request.Cookies[cookieName] == null;
        }
        public static void SetDataToCookies(HttpContext context)
        {

            var cookieName = ConfigurationManager.AppSettings["AppCookie"];
            AuthorizeADAttribute autherize = new AuthorizeADAttribute();
            UserProfile userData = new UserProfile();

            bool isAuthenticated = Convert.ToBoolean(ConfigurationManager.AppSettings["IsAuthenticated"]);

            if (isAuthenticated)
            {
                userData = autherize.GetUserData(context.User.Identity.Name.Split('\\')[1]);
            }
            else
            {

                userData.UserName = context.User.Identity.Name.Split('\\')[1];
                userData.GroupName = ConfigurationManager.AppSettings["IsAuthenticated"];

                //Get the user rules in json
                string file = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["UserGroupsRulesPath"]);
                //deserialize JSON from file  
                string userRuleJson = System.IO.File.ReadAllText(file);
                var rules = JObject.Parse(userRuleJson).SelectToken(ConfigurationManager.AppSettings["DefaultGroup"]).ToObject<GroupResponsibilities>();
                userData.GroupResponsibilities = rules;
            }




            //if model is null no need to add in cookies
            if (userData != null)
            {
                //Get roles and assign to principal and assign model 
                var serializer = new JavaScriptSerializer();
                var serializerUserData = serializer.Serialize(userData);
                var encryptUserData = Convert.ToBase64String(MachineKey.Protect(System.Text.Encoding.UTF8.GetBytes(serializerUserData)));
                var cookie = new HttpCookie(cookieName, encryptUserData);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        public static void SetDataToCookies(string userName)
        {
            try
            {
                var cookieName = ConfigurationManager.AppSettings["AppCookie"];
                AuthorizeADAttribute autherize = new AuthorizeADAttribute();
                UserProfile userData = new UserProfile();

                // Flag to run with or without the AD setup 
                bool isAuthenticated = Convert.ToBoolean(ConfigurationManager.AppSettings["IsAuthenticated"]);
                if (isAuthenticated)
                {
                    userData = autherize.GetUserData(userName);
                }
                else //run based on web config provided data
                {
                    userData.UserName = userName;
                    userData.GroupName = ConfigurationManager.AppSettings["DefaultGroup"];

                    //Get the user rules in json
                    string file = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["UserGroupsRulesPath"]);
                    string Server = ConfigurationManager.AppSettings["Server"];
                    file = file.Replace("{0}", Server);
                    //deserialize JSON from file  
                    string userRuleJson = System.IO.File.ReadAllText(file);
                    var rules = JObject.Parse(userRuleJson).SelectToken(ConfigurationManager.AppSettings["DefaultGroup"]).ToObject<GroupResponsibilities>();
                    userData.GroupResponsibilities = rules;
                }



                //if model is null no need to add in cookies
                if (userData != null)
                {
                    //Get roles and assign to principal and assign model 
                    var serializer = new JavaScriptSerializer();
                    var serializerUserData = serializer.Serialize(userData);
                    var encryptUserData = Convert.ToBase64String(MachineKey.Protect(System.Text.Encoding.UTF8.GetBytes(serializerUserData)));
                    var cookie = new HttpCookie(cookieName, encryptUserData);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }

        }

        /// <summary>
        /// Used to return true and false against provide roles
        /// </summary>
        public static bool CheckResponsibility(HttpContext context, params Role[] roles)
        {

            if (Authorization.IsCookieNull())
            {
                Authorization.SetDataToCookies(context);
            }
            var cookieValue = GetValueFromCookies();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            UserProfile user = JsonConvert.DeserializeObject<UserProfile>(cookieValue);

            //Convent User Given roles to string              
            var userRules = roles.Select(a => a.ToString()).ToArray();

            var groupResponsibilities = user.GroupResponsibilities;

            //Check User have access against given rights
            var type = groupResponsibilities.GetType();
            var haveAccess = type.GetProperties().Select(x => new SelectListItem { Text = x.Name, Value = x.GetValue(groupResponsibilities, null).ToString() })
                .Where(x => userRules.Contains(x.Text) && x.Value.ToLower() == "true").ToList();

            return haveAccess.Count() == userRules.Count();
        }
        /// <summary>
        ///  Used to return the User type from current cookie
        /// </summary>

        public static string GetUserType(HttpContext context)
        {
            if (Authorization.IsCookieNull())
            {
                Authorization.SetDataToCookies(context);
            }
            var cookieValue = GetValueFromCookies();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            UserProfile user = JsonConvert.DeserializeObject<UserProfile>(cookieValue);

            string userType = "";
            if (user.GroupName != null)
            {
                userType = user.GroupName.Split('_')[3];
            }
            return userType;
        }

    }






}