﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Common.Helper
{
    public class WizardHelper
    {

        public static string GetClassNameForAlternatingRow(int  value)
        {
            try
            {
                if(value % 2 == 0)
                {
                    return "odd";
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}