﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using log4net.Config;
using System.Diagnostics;
using System.Reflection;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
[assembly: log4net.Config.Repository()]

namespace Hussmann.Common.Helper
{
    public static class Logger
    {
        #region DATA_MEMBERS
        private static readonly ILog logger = LogManager.GetLogger(typeof(Logger));
        #endregion

        #region FUNCTIONS
        public static void PrintInfo(string info)
        {
            try
            {
                if (logger.IsInfoEnabled)
                {
                    logger.Info("@@Info:" + " " + info + "\n");
                }

            }
            catch (Exception exception)
            {
                if (logger.IsErrorEnabled)
                {
                    logger.Error("@@LogError:" + " " + exception.StackTrace.ToString() + "\n");
                }
            }

        }

        public static void PrintWarning(string warning)
        {
            try
            {
                if (logger.IsInfoEnabled)
                {
                    logger.Info("@@Warning:" + " " + warning + "\n");
                }

            }
            catch (Exception exception)
            {
                if (logger.IsErrorEnabled)
                {
                    logger.Error("@@LogError:" + " " + exception.StackTrace.ToString() + "\n");
                }
            }

        }

        public static void PrintFatal(string fatal)
        {
            try
            {
                if (logger.IsInfoEnabled)
                {
                    logger.Fatal("@@FATAL:" + " " + fatal + "\n");
                }

            }
            catch (Exception exception)
            {
                if (logger.IsErrorEnabled)
                {
                    logger.Error("@@LogError:" + " " + exception.StackTrace.ToString() + "\n");
                }
            }

        }


        public static void PrintInfo(string line, params object[] list)
        {
            try
            {
                string category = "@@Info: ";
                if (logger.IsInfoEnabled)
                {
                    logger.InfoFormat(category +" " + line, list);
                }

            }
            catch (Exception exception)
            {
                if (logger.IsErrorEnabled)
                {
                    logger.Error("@@LogError:" + " " + exception.StackTrace.ToString() + "\n");
                }
            }

        }

        public static void PrintDebug(string debug)
        {

            try
            {
                if (logger.IsDebugEnabled)
                {
                    logger.Debug("@@Debug:" + " " + debug + "\n");
                }

            }
            catch (Exception exception)
            {
                if (logger.IsErrorEnabled)
                {
                    logger.Error("@@LogError:" + " " + exception.StackTrace.ToString() + "\n");
                }
            }
        }

        public static void PrintError(Exception error)
        {
            try
            {
                if (logger.IsErrorEnabled)
                {
                    logger.Error("@@Error Message:" + " " + error.Message.ToString() + "\n");
                    logger.Error("@@Error Trace:" + " " + error.StackTrace.ToString() + "\n");
                }

            }
            catch (Exception exception)
            {
                if (logger.IsErrorEnabled)
                {
                    logger.Error("@@LogError:" + " " + exception.StackTrace.ToString() + "\n");
                }
            }
        }

        public static void PrintLoggingDetails()
        {
            try
            {
                if (logger.IsInfoEnabled)
                    logger.Info("@@ IsInfoEnabled = TRUE.");
                if (logger.IsWarnEnabled)
                    logger.Warn("@@ IsWarnEnabled = TRUE.");
                if (logger.IsFatalEnabled)
                    logger.Fatal("@@ IsFatalEnabled = TRUE.");
                if (logger.IsDebugEnabled)
                    logger.Debug("@@ IsDebugEnabled = TRUE.");
                if (logger.IsErrorEnabled)
                    logger.Error("@@ IsErrorEnabled = TRUE.");

            }
            catch (Exception exception)
            {
                if (logger.IsErrorEnabled)
                {
                    logger.Error("@@LogError:" + " " + exception.StackTrace.ToString() + "\n");
                }
            }

        }

        #endregion
    }
}