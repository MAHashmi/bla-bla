﻿using CompressorDataModel;
using Hussmann.Controllers;
using Hussmann.Models.Compressor;
using Hussmann.Models.Compressor.Add;
using Hussmann.Models.Compressor.Export;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Common.Helper
{
    public class CompressorExcelExport : CompressorController
    {
        IWorkbook workBook = new HSSFWorkbook();
        RulestreamCompressorDBEntities compressorDBContext = new RulestreamCompressorDBEntities();

        public IWorkbook GenerateExcelFile(int modelID)
        {
            try
            {
                //var compressor = compressorDBContext.CompressorModels.Where(x => x.CompModel_ID == modelID).FirstOrDefault();

                GetCompressorDetails(modelID);
                GetRLADetails(modelID);
                GetBOMDetails(modelID);
                GetCoefficients(modelID);
            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }

            return workBook;
        }
        private void GetCompressorDetails(int modelID)//(int modelID, CompressorModel compressor)
        {
            ISheet sheet1 = workBook.CreateSheet("Details");
            var dbCompDetails = compressorDBContext.spGetCompressorDetailsToExport(modelID).FirstOrDefault();
            if (dbCompDetails != null)
            {
                sheet1 = WriteInSheet(sheet1, dbCompDetails, 1);
            }
            else
            {
                sheet1.CreateRow(1);
            }
        }

        private void GetRLADetails(int modelID)
        {
            ISheet sheet2 = workBook.CreateSheet("RLADetails");
            var dbRLADetails = compressorDBContext.spGetRLADetailsToExport(modelID).ToList();
            if (dbRLADetails.Count > 0)
            {
                int rowIndex = 1;
                //ISheet sheet5 = workBook.CreateSheet("Coefficients");
                foreach (var row in dbRLADetails)
                {
                    sheet2 = WriteInSheet(sheet2, row, rowIndex);
                    rowIndex++;
                }
            }
            else
            {
                sheet2.CreateRow(1);
            }
            
        }

        private void GetBOMDetails(int modelID)//(CompRLA rlaDetails, CompressorModel compressor)
        {
            ExportBOMDetails bomDetails = new ExportBOMDetails();
            ExportBOMKitDetails bomKitDetails = new ExportBOMKitDetails();

            ISheet sheet3 = workBook.CreateSheet("BOMDetails");
            ISheet sheet4 = workBook.CreateSheet("BOMKitDetails");

            var dbBOMDetails = compressorDBContext.spGetBOMDetailsToExport(modelID).FirstOrDefault();
            if (dbBOMDetails != null)
            {
                bomDetails.CompressorType = dbBOMDetails.CompressorType;
                bomDetails.CompressorVendor = dbBOMDetails.CompressorVendor;
                bomDetails.HussmannTempApplication = dbBOMDetails.HussmannTempApplication;
                bomDetails.VendorBOMCode = dbBOMDetails.VendorBOMCode;
                bomDetails.SuctionLineSize = dbBOMDetails.SuctionLineSize;
                bomDetails.DischargeLineSize = dbBOMDetails.DiscLineSize;

                bomKitDetails.CompressorKitProtocol = dbBOMDetails.CompressorKitProtocol;
                bomKitDetails.CompressorKitHSeries = dbBOMDetails.CompressorKitHSeries;
                bomKitDetails.CompressorKitRack = dbBOMDetails.CompressorKitRack;

                //ISheet sheet3 = workBook.CreateSheet("BOMDetails");
                sheet3 = WriteInSheet(sheet3, bomDetails, 1);

                //ISheet sheet4 = workBook.CreateSheet("BOMKitDetails");
                sheet4 = WriteInSheet(sheet4, bomKitDetails, 1);
            }
            else
            {
                sheet3.CreateRow(1);
                sheet4.CreateRow(1);
            }
        }

        private void GetCoefficients(int modelID)
        {
            var dbCoefficients = compressorDBContext.spGetCoefficientsToExport(modelID).ToList();
            ISheet sheet5 = workBook.CreateSheet("Coefficients");
            if (dbCoefficients.Count > 0)
            {
                int rowIndex = 1;
                //ISheet sheet5 = workBook.CreateSheet("Coefficients");
                foreach(var row in dbCoefficients)
                {
                    sheet5 = WriteInSheet(sheet5, row, rowIndex);
                    rowIndex++;
                }
            }
            else
            {
                sheet5.CreateRow(1);
            }
        }

        public static ISheet WriteInSheet(ISheet oSheet, object o, int row)
        {
            IRow Row = null, Row2 = null;
            int Cell_No = 0;

            if (row == 1)
            {
                Row = oSheet.CreateRow(row - 1);
                Row2 = oSheet.CreateRow(row);
            }
            else
            {
                Row2 = oSheet.CreateRow(row);
            }

            foreach (var Prop2 in o.GetType().GetProperties())
            {
                if (!(Prop2.PropertyType.ToString().Contains("List")) && !(Prop2.Name.Contains("ID")))
                {
                    if (!(o.GetType().ToString().Contains("Price") && Prop2.PropertyType.ToString().Contains("Int")))
                    {
                        if (!(Prop2.Name.Equals("CoilModel")))
                        {
                            ICell cell;
                            if (row == 1)
                            {
                                cell = Row.CreateCell(Cell_No);
                                cell.SetCellValue(Prop2.Name);
                                oSheet.AutoSizeColumn(Cell_No);
                            }
                            cell = Row2.CreateCell(Cell_No);
                            if (Prop2.GetValue(o, null) != null)
                            {
                                cell.SetCellValue(Prop2.GetValue(o, null).ToString());
                                oSheet.AutoSizeColumn(Cell_No);
                            }
                            Cell_No++;
                        }
                    }
                }
            }
            return oSheet;
        }
    }
}