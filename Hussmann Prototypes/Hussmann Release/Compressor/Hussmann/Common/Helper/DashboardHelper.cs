﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hussmann.Common.Enumeration;

namespace Hussmann.Common.Helper
{
    public class DashboardHelper
    {
        #region Constants
        // Chiller Images
        public const string UNIT_COOLER_IMAGE = "../content/img/unit_fan.png";
        public const string CASE_IMAGE = "../content/img/unit_case.png";
        public const string COMPRESSOR_IMAGE = "../content/img/unit_compressor.png";
        public const string CONDENSER_IMAGE = "../content/img/unit_accessory.png";

        // Chiller Headings of Right portion on Dashboard
        public const string UNIT_COOLER_HEADING = "Unit Cooler";
        public const string CASE_HEADING = "Case";
        public const string COMPRESSOR_HEADING = "Compressor";
        public const string CONDENSER_HEADING = "Condenser";

        // Icons Span Class
        public const string EDIT_ICON_SPAN_CLASS = "label btn btn-primary btn-icon btn-circle btn-sm";
        public const string APPROVED_ICON_SPAN_CLASS = "label btn btn-green btn-icon btn-circle btn-sm";
        public const string DELETED_ICON_SPAN_CLASS = "label btn btn-danger btn-icon btn-circle btn-sm";

        // Icons Image Class
        public const string EDIT_ICON_IMAGE_CLASS = "fa fa-pencil";
        public const string APPROVED_ICON_IMAGE_CLASS = "fa fa-plus";
        public const string DELETED_ICON_IMAGE_CLASS = "fa fa-trash-o";

        // Dashboard News Feed Chiller heading (Unit Cooler)
        public const string UNIT_COOLER_HEADING_ADDED = "New Unit Cooler";
        public const string UNIT_COOLER_HEADING_EDITED = "Unit Cooler Edited";
        public const string UNIT_COOLER_HEADING_DELETED = "Unit Cooler Deleted";
        public const string UNIT_COOLER_HEADING_SUBMITTED = "Unit Cooler Submitted";
        public const string UNIT_COOLER_HEADING_REJECTED = "Unit Cooler Rejected";

        // Dashboard News Feed Chiller heading (Case)
        public const string CASE_HEADING_ADDED = "New Case";
        public const string CASE_HEADING_EDITED = "Case Edited";
        public const string CASE_HEADING_DELETED = "Case Deleted";
        public const string CASE_HEADING_SUBMITTED = "Case Submitted";
        public const string CASE_HEADING_REJECTED = "Case Rejected";

        // Dashboard News Feed Chiller heading (Compressor)
        public const string COMPRESSOR_HEADING_ADDED = "New Compressor";
        public const string COMPRESSOR_HEADING_EDITED = "Compressor Edited";
        public const string COMPRESSOR_HEADING_DELETED = "Compressor Deleted";
        public const string COMPRESSOR_HEADING_SUBMITTED = "Compressor Submitted";
        public const string COMPRESSOR_HEADING_REJECTED = "Compressor Rejected";

        // Dashboard News Feed Chiller heading (Condenser)
        public const string CONDENSER_HEADING_ADDED = "New Condenser";
        public const string CONDENSER_HEADING_EDITED = "Condenser Edited";
        public const string CONDENSER_HEADING_DELETED = "Condenser Deleted";
        public const string CONDENSER_HEADING_SUBMITTED = "Condenser Submitted";
        public const string CONDENSER_HEADING_REJECTED = "Condenser Rejected";

        // State text
        public const string ADDED = "added";
        public const string EDITED = "edited";
        public const string DELETED = "deleted";
        public const string SUBMITTED = "submitted";
        public const string REJECTED = "rejected";

        #endregion
        #region Function
        public static string GetNameForChillerItem(string chillerType)
        {
            try
            {
                if(chillerType == enChillerType.UnitCooler.ToString())
                {
                    return UNIT_COOLER_HEADING;
                }
                else if (chillerType == enChillerType.Case.ToString())
                {
                    return CASE_HEADING;
                }
                else if(chillerType == enChillerType.Compressor.ToString())
                {
                    return COMPRESSOR_HEADING;
                }
                else if(chillerType == enChillerType.Condenser.ToString())
                {
                    return CONDENSER_HEADING;
                }
                else
                {
                    return null;
                }

            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        // return image path according to chiller type
        public static string GetImageForChillerItem(string chillerType)
        {
            try
            {
                if (chillerType == enChillerType.UnitCooler.ToString())
                {
                    return UNIT_COOLER_IMAGE;
                }
                else if (chillerType == enChillerType.Case.ToString())
                {
                    return CASE_IMAGE;
                }
                else if (chillerType == enChillerType.Compressor.ToString())
                {
                    return COMPRESSOR_IMAGE;
                }
                else if (chillerType == enChillerType.Condenser.ToString())
                {
                    return CONDENSER_IMAGE;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //return css class for edit ,added and deleted icons class on dashboard News Feed which is above chiller image
        public static string GetIconSpanClass(int stateID)
        {
            try
            {
                if (stateID == Convert.ToInt32((int)enState.Approved))
                {
                    return APPROVED_ICON_SPAN_CLASS;
                }
                else if (stateID == Convert.ToInt32((int)enState.Draft) || stateID == Convert.ToInt32((int)enState.Submitted))
                {
                    return EDIT_ICON_SPAN_CLASS;
                }
                else if (stateID == Convert.ToInt32((int)enState.Deleted) || stateID == Convert.ToInt32((int)enState.Rejected))
                {
                    return DELETED_ICON_SPAN_CLASS;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //return css class for edit ,added and deleted icons image on dashboard News Feed which is above chiller image
        public static string GetIconImageClass(int stateID)
        {
            try
            {
                if (stateID == Convert.ToInt32(enState.Approved))
                {
                    return APPROVED_ICON_IMAGE_CLASS;
                }
                else if (stateID == Convert.ToInt32((int)enState.Draft) || stateID == Convert.ToInt32((int)enState.Submitted))
                {
                    return EDIT_ICON_IMAGE_CLASS;
                }
                else if (stateID == Convert.ToInt32((int)enState.Deleted) || stateID == Convert.ToInt32((int)enState.Rejected))
                {
                    return DELETED_ICON_IMAGE_CLASS;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Get Headings for Dashboard News Feed on Chiller type and State
        public static string GetNewsFeedChillerHeading(int stateID, string chillerType)
        {
            try
            {
                if (stateID == Convert.ToInt32(enState.Approved))
                {
                    if (chillerType == enChillerType.UnitCooler.ToString())
                    {
                        return UNIT_COOLER_HEADING_ADDED;
                    }
                    else if (chillerType == enChillerType.Case.ToString())
                    {
                        return CASE_HEADING_ADDED;
                    }
                    else if (chillerType == enChillerType.Compressor.ToString())
                    {
                        return COMPRESSOR_HEADING_ADDED;
                    }
                    else if (chillerType == enChillerType.Condenser.ToString())
                    {
                        return CONDENSER_HEADING_ADDED;
                    }
                    else
                    {
                        return null;
                    }
                }
                else if (stateID == Convert.ToInt32(enState.Draft))
                {
                    if (chillerType == enChillerType.UnitCooler.ToString())
                    {
                        return UNIT_COOLER_HEADING_EDITED;
                    }
                    else if (chillerType == enChillerType.Case.ToString())
                    {
                        return CASE_HEADING_EDITED;
                    }
                    else if (chillerType == enChillerType.Compressor.ToString())
                    {
                        return COMPRESSOR_HEADING_EDITED;
                    }
                    else if (chillerType == enChillerType.Condenser.ToString())
                    {
                        return CONDENSER_HEADING_EDITED;
                    }
                    else
                    {
                        return null;
                    }
                }
                else if (stateID == Convert.ToInt32(enState.Deleted))
                {
                    if (chillerType == enChillerType.UnitCooler.ToString())
                    {
                        return UNIT_COOLER_HEADING_DELETED;
                    }
                    else if (chillerType == enChillerType.Case.ToString())
                    {
                        return CASE_HEADING_DELETED;
                    }
                    else if (chillerType == enChillerType.Compressor.ToString())
                    {
                        return COMPRESSOR_HEADING_DELETED;
                    }
                    else if (chillerType == enChillerType.Condenser.ToString())
                    {
                        return CONDENSER_HEADING_DELETED;
                    }
                    else
                    {
                        return null;
                    }
                }
                else if (stateID == Convert.ToInt32(enState.Submitted))
                {
                    if (chillerType == enChillerType.UnitCooler.ToString())
                    {
                        return UNIT_COOLER_HEADING_SUBMITTED;
                    }
                    else if (chillerType == enChillerType.Case.ToString())
                    {
                        return CASE_HEADING_SUBMITTED;
                    }
                    else if (chillerType == enChillerType.Compressor.ToString())
                    {
                        return COMPRESSOR_HEADING_SUBMITTED;
                    }
                    else if (chillerType == enChillerType.Condenser.ToString())
                    {
                        return CONDENSER_HEADING_SUBMITTED;
                    }
                    else
                    {
                        return null;
                    }
                }
                else if (stateID == Convert.ToInt32(enState.Rejected))
                {
                    if (chillerType == enChillerType.UnitCooler.ToString())
                    {
                        return UNIT_COOLER_HEADING_REJECTED;
                    }
                    else if (chillerType == enChillerType.Case.ToString())
                    {
                        return CASE_HEADING_REJECTED;
                    }
                    else if (chillerType == enChillerType.Compressor.ToString())
                    {
                        return COMPRESSOR_HEADING_REJECTED;
                    }
                    else if (chillerType == enChillerType.Condenser.ToString())
                    {
                        return CONDENSER_HEADING_REJECTED;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetStateActionForDisplayText(int stateID)
        {
            try
            {
                if (stateID == Convert.ToInt32(enState.Approved))
                {
                    return ADDED;
                }
                else if (stateID == Convert.ToInt32(enState.Draft))
                {
                    return EDITED;
                }
                else if (stateID == Convert.ToInt32(enState.Deleted))
                {
                    return DELETED;
                }
                else if(stateID == Convert.ToInt32(enState.Submitted))
                {
                    return SUBMITTED;
                }
                else if(stateID == Convert.ToInt32(enState.Rejected))
                {
                    return REJECTED;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
    }