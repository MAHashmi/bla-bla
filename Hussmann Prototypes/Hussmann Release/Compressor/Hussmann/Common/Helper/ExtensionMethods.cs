﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Dynamic;
using System.Linq;
using System.Web;
using Hussmann.Models.UnitCooler.Add;
using System.Reflection;

namespace Hussmann.Common.Helper
{
    public static class ExtensionMethods
    {

        public static List<YearModel> GetListOfYear()
        {
            try
            {
                int currentYear = DateTime.Today.Year;
                int index = 2; // to start with index 1
                List<YearModel> YearList = new List<YearModel>()
                {
                        new YearModel(){ YearID = index-1, Year = currentYear-1  },
                        new YearModel(){ YearID = index++, Year = currentYear++  },
                        new YearModel(){ YearID = index++, Year = currentYear++  },
                        new YearModel(){ YearID = index++, Year = currentYear++  },
                        new YearModel(){ YearID = index++, Year = currentYear++  },
                        new YearModel(){ YearID = index++, Year = currentYear++  }
                };

                return YearList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetYearID(string year)
        {
            int startYear = 2016;

            int id = Convert.ToInt32(year) - startYear;
            return id;
        }

        public static string GetYearFromYearID(int yearID)
        {
            int startYear = 2016;

            return Convert.ToString(startYear + yearID);
        }

        //copy from https://github.com/aspnet/EntityFrameworkCore/issues/2344
        /// <summary>
        /// Used to run the sql command for the table variable parameters
        /// </summary>        
        public static IEnumerable<dynamic> CollectionFromSql(this DbContext dbContext, string Sql, Dictionary<string, object> Parameters)
        {
            using (var cmd = dbContext.Database.Connection.CreateCommand())
            {
                cmd.CommandText = Sql;
                if (cmd.Connection.State != ConnectionState.Open)
                    cmd.Connection.Open();

                foreach (KeyValuePair<string, object> param in Parameters)
                {
                    DbParameter dbParameter = cmd.CreateParameter();
                    dbParameter.ParameterName = param.Key;
                    dbParameter.Value = param.Value;
                    cmd.Parameters.Add(dbParameter);
                }
                using (var dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        var dataRow = GetDataRow(dataReader);
                        yield return dataRow;
                    }
                }
            }
        }

        private static dynamic GetDataRow(DbDataReader dataReader)
        {
            var dataRow = new ExpandoObject() as IDictionary<string, object>;
            for (var fieldCount = 0; fieldCount < dataReader.FieldCount; fieldCount++)
                dataRow.Add(dataReader.GetName(fieldCount), dataReader[fieldCount]);
            return dataRow;
        }

        /// <summary>
        /// Convert provided list to data table
        /// </summary>
        
        public static DataTable ToDataTable<T>(this List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to data table rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check data table
            return dataTable;
        }


    }
}
