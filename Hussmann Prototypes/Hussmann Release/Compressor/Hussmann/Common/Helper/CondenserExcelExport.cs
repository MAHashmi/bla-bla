﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using CondenserDataModel;
using Hussmann.Controllers;
using Hussmann.Models.Condenser.Add;
using Newtonsoft.Json;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.IO;
using System.Threading.Tasks;
using System.Threading;

namespace Hussmann.Common.Helper
{
    public class CondenserExcelExport : CondenserController
    {
        private RulestreamCondenserDBEntities CondenserDBContext = new RulestreamCondenserDBEntities();
        IWorkbook workBook = new HSSFWorkbook();
        public IWorkbook GenerateExcelFile(int ModelID)
        {
            try
            {
                var CondenserModel = CondenserDBContext.
                CondenserModels.Where
                (x => x.ModelID == ModelID).Select(x => x).FirstOrDefault();

                //Create Excel File (xls)

                string[] SheetNames = { "Details", "Accessories", "Configurations", "Coil Accessories Price", "Coil Option Pricing" };

                //Sheet for details Tab
                 GetCondenserModelDetails(ModelID, CondenserModel);
                 GetCondenserTypeData(ModelID, CondenserModel);
                GetPricingDetails(ModelID);
                GetCondenserCapacity(ModelID, CondenserModel);
                GetMiscellaneousData(ModelID);
                GetControlPanelData(ModelID);
               // workBook

            }
            catch(Exception exception)
            {
                Logger.PrintError(exception);
            }
          
            return workBook;

        }
        // Details Tab Data
        public void GetCondenserModelDetails(int ModelID, CondenserModel CondenserModel)
        {
            AddCondenserDetailsViewModel condenserDetailsData = new AddCondenserDetailsViewModel();
            condenserDetailsData = SetDataForDetailsTab(new AddCondenserDetailsViewModel(), CondenserModel);

            condenserDetailsData.BrandDetailsName = CondenserDBContext.CondenserBrand_lkp.Where(x => x.BrandID == CondenserModel.MFGID).Select(x => x.CondenserBrandName).FirstOrDefault();//UnitCoolerDBContext.CoilFamilies.Where(x => x.CoilFamilyID == CoilModel.ModelFamilyID).Select(x => x.CoilFamilyName).SingleOrDefault();
            condenserDetailsData.CondenserTypesName = CondenserDBContext.CondenserTypes.Where(x => x.CondenserTypeID == CondenserModel.CondenserTypeID).Select(x => x.CondenserType1).FirstOrDefault();
            condenserDetailsData.ModelFamilyName = CondenserDBContext.CondenserFamilies.Where(x => x.CondenserFamilyID == CondenserModel.ModelFamilyID).Select(x => x.CondenserFamilyName).FirstOrDefault();
            condenserDetailsData.FanMotorsName = CondenserDBContext.CondenserFanMotors.Where(x => x.CondFanMotorID == CondenserModel.CondFanMotorID).Select(x => x.CondenserFanMotorType).FirstOrDefault();
            condenserDetailsData.ModelSuffixesName = CondenserDBContext.ModelSuffixes.Where(x => x.ModelSuffixID == CondenserModel.ModelSuffixID).Select(x => x.ModelSuffix1).FirstOrDefault();
            condenserDetailsData.GeometryTypesName = CondenserDBContext.CondenserGeometries.Where(x => x.GeometryID == CondenserModel.GeometryID).Select(x => x.GeometryType).FirstOrDefault();

            ISheet sheet1 = workBook.CreateSheet("Details");
            sheet1 = WriteInSheet(sheet1, condenserDetailsData, 1);
        }
        public void GetPricingDetails(int ModelID)
        {

            ISheet sheet1 = workBook.CreateSheet("Condenser Pricing");
            sheet1 = WriteInSheet(sheet1, SetCondenserPricingDetails(ModelID),1);

            int i = 1;
          
            ISheet sheet5 = workBook.CreateSheet("Receiver Details & Pricing");
            foreach (var row in SetReceiverPricingData(ModelID))
            {
                sheet5 = WriteInSheet(sheet5, row, i);
                i++;
            }
            i = 1;
            ISheet sheet2 = workBook.CreateSheet("Leg Pricing");
            foreach (var row in SetLegPricingGridData(ModelID))
            {
                sheet2 = WriteInSheet(sheet2,row,i);
                i++;
            }
            i = 1;
            ISheet sheet3 = workBook.CreateSheet("ControlPanel Pricing");
            foreach (var row in SetControlPanelPriceData(ModelID))
            {
                sheet3 = WriteInSheet(sheet3, row, i);
                i++;
            }
            i = 1;
            ISheet sheet4 = workBook.CreateSheet("Miscellaneous Options Pricing");
            foreach (var row in SetMiscellaneousOptionsPricingData(ModelID))
            {
              sheet4 = WriteInSheet(sheet4, row, i);
                i++;
            }
         

        }

        private CondenserPriceDetails SetCondenserPricingDetails(int modelID)
        {
            CondenserPriceDetails condenserPricingData = new CondenserPriceDetails();
            CondenserPricing cp = CondenserDBContext.CondenserPricing.Where(a => a.CondenserModel.ModelID == modelID).FirstOrDefault();
            if (cp != null)
            {
                condenserPricingData.ModelID = cp.CondenserModel.ModelID;
                condenserPricingData.ModelSuffix = cp.ModelSuffix;
                condenserPricingData.ModelSuffixID = cp.ModelSuffix1.ModelSuffixID;
                condenserPricingData.PolyesterFinCoating = cp.PolyesterFinCoating;
                condenserPricingData.EpoxyElectroFinCoatedCoil = cp.EpoxyElectroFinCoatedCoil;
                condenserPricingData.BaseListPrice = cp.BaseListPrice;
                condenserPricingData.CondenserTypeID = cp.CondenserType.CondenserTypeID;
                condenserPricingData.CopperFin = cp.CopperFin;
                condenserPricingData.CondenserPricingID = cp.CondenserPricingID;
                condenserPricingData.CondenserType = cp.CondenserType.CondenserType1;

            }
            else
            {
                CondenserModel cm = CondenserDBContext.CondenserModels.Find(modelID);
                condenserPricingData.CondenserType = cm.CondenserType.CondenserType1;
                condenserPricingData.ModelSuffix = cm.ModelSuffix.ModelSuffix1;
                condenserPricingData.CondenserTypeID = cm.CondenserTypeID;
                condenserPricingData.ModelSuffixID = cm.ModelSuffixID;
                condenserPricingData.ModelID = cm.ModelID;
            }
            return condenserPricingData;

        }

        private List<LegPriceDetails> SetLegPricingGridData(int modelID)
        {
            List<LegPrice> lstLegPrice = CondenserDBContext.LegPrice.Where(a => a.CondenserModel.ModelID == modelID && a.IsSelected == true).ToList();
            List<LegPriceDetails> lstLegPriceDetails = new List<LegPriceDetails>();
            if (lstLegPrice.Count > 0)
            {
                foreach (var lp in lstLegPrice)
                {
                    lstLegPriceDetails.Add(
                        new LegPriceDetails
                        {
                            GeometryID = lp.CondenserGeometry.GeometryID,
                            LegPrice = lp.LegPrice1,
                            LegPriceID = lp.LegPriceID,
                            LegTypeID = lp.LegType.LegTypeID,
                            LegType = lp.LegType.LegType1,
                            ModelID = lp.CondenserModel.ModelID
                        }

                    );

                }

            }


            return lstLegPriceDetails;
        }

        private List<ControlPanelPriceDetails> SetControlPanelPriceData(int modelID)
        {
            List<ControlPanelPrice> lstControlPanelPrice = CondenserDBContext.ControlPanelPrice.Where(a => a.CondenserModel.ModelID == modelID).ToList();
            List<ControlPanelPriceDetails> lstControlPanelPriceDetails = new List<ControlPanelPriceDetails>();
            if (lstControlPanelPrice.Count > 0)
            {
                foreach (var lp in lstControlPanelPrice)
                {
                    lstControlPanelPriceDetails.Add(
                        new ControlPanelPriceDetails
                        {
                            GeometryID = lp.CondenserGeometry.GeometryID,
                            GeometryType = lp.CondenserGeometry.GeometryType,
                            ListPrice = lp.ListPrice,
                            ModelID = lp.CondenserModel.ModelID,
                            PriceID = lp.PriceID
                        }

                    );

                }

            }


            return lstControlPanelPriceDetails;

        }
        private List<MiscellaneousPriceDetails> SetMiscellaneousOptionsPricingData(int modelID)
        {
            List<MiscellaneousOptionsPricing> lstMiscellaneousOptionsPricing = CondenserDBContext.MiscellaneousOptionsPricing.Where(a => a.CondenserModel.ModelID == modelID && a.IsSelected == true).ToList();
            List<MiscellaneousPriceDetails> lstMiscellaneousPriceDetails = new List<MiscellaneousPriceDetails>();
            if (lstMiscellaneousOptionsPricing.Count > 0)
            {
                foreach (var lp in lstMiscellaneousOptionsPricing)
                {
                    lstMiscellaneousPriceDetails.Add(
                        new MiscellaneousPriceDetails
                        {
                            GeometryID = lp.CondenserGeometry.GeometryID,
                            MiscellaneousPrice = lp.MiscellaneousPrice,
                            MiscellaneousPricingID = lp.MiscellaneousPricingID,
                            MiscellenousOptionsID = lp.MiscellaneousOptions.MiscellaneousOptionsID,
                            MiscellenousOptionsName = lp.MiscellaneousOptions.MiscellaneousOptions1,
                            ModelID = lp.CondenserModel.ModelID
                        }

                    );

                }

            }


            return lstMiscellaneousPriceDetails;

        }
        private List<ReceiverPriceDetails> SetReceiverPricingData(int modelID)
        {
            List<ReceiverPriceDetails> lstReceiverPriceData = new List<ReceiverPriceDetails>();
            List<Receiver> lstReceiver = CondenserDBContext.Receiver.Where(a => a.CondenserModel.ModelID == modelID && a.IsSelected == true).ToList();
            if (lstReceiver.Count > 0)
            {
                foreach (var lr in lstReceiver)
                {
                    lstReceiverPriceData.Add(
                     new ReceiverPriceDetails
                     {
                         ModelID = lr.CondenserModel.ModelID,
                         HeatTapeInsulPrice = lr.HeatTapeInsulPrice,
                         //  ReceiverCapacitorId = lr.ReceiverCapacity.RcvrCapacityID_,
                         ReceiverID = lr.ReceiverID,
                         ReceiverPrice = lr.ReceiverPrice,
                         ReceiverQty = lr.ReceiverQty,
                         ReceiverTypeID = lr.ReceiverType.ReceiverTypeID,
                         GeometryID = lr.CondenserGeometry.GeometryID,
                         ReceiverType = lr.ReceiverType.ReceiverSize

                     }
                     );



                }
            }
            return lstReceiverPriceData;

        }


        // Condenser Type Tab Data
        public void GetCondenserTypeData(int ModelID, CondenserModel CondenserModel)
        {
            AddCondenserTypeViewModel addCondenserType = new AddCondenserTypeViewModel();
            addCondenserType = SetDataForCondenserTypeTab(addCondenserType, CondenserModel);
       

            ISheet sheet1 = workBook.CreateSheet("Leg Details");
            var legData = addCondenserType.LegDetails;

            int i = 1;
            foreach(var row in addCondenserType.LegDetails)
            {
                    sheet1 = WriteInSheet(sheet1, row, i);
                    i++;
            }
            ISheet sheet6 = workBook.CreateSheet("Receiver Type Data");
            sheet6 = WriteInReceiverType(sheet6, SetCondenserReceiverTypeGridData(ModelID));


        }
        private List<ReceiverTypeDetails> SetCondenserReceiverTypeGridData(int modelID)
        {
            List<ReceiverTypeDetails> lstRecieverNewData = new List<ReceiverTypeDetails>();
            var lstReceiverTypeData = CondenserDBContext.spGetCondenserReceiverTypeData(modelID);
            foreach (var data in lstReceiverTypeData)
            {
                lstRecieverNewData.Add(
                    new ReceiverTypeDetails
                    {
                        ModelID = data.ModelID,
                        RcvrCapacityID = data.RcvrCapacityID_,
                        ReceiverCapacity = data.ReceiverCapacity,
                        ReceiverSize = data.ReceiverSize,
                        ReceiverTypeID = data.ReceiverTypeID,
                        RefrigerantType = data.RefrigerantType,
                        RefrigerantType_ID = data.RefrigerantType_ID

                    }

                    );

            }

            return lstRecieverNewData;
        }
        public static ISheet WriteInSheet(ISheet oSheet, object o, int row)
        {
            IRow Row = null, Row2 = null;
            int Cell_No = 0;

            if (row == 1)
            {
                Row = oSheet.CreateRow(row - 1);
                Row2 = oSheet.CreateRow(row);
            }
            else
            {
                Row2 = oSheet.CreateRow(row);
            }

            foreach (var Prop2 in o.GetType().GetProperties())
            {
                if (!(Prop2.PropertyType.ToString().Contains("List")) && !(Prop2.Name.Contains("ID")))
                {
                    if (!(o.GetType().ToString().Contains("Price") && Prop2.PropertyType.ToString().Contains("Int")))
                    {
                        if (!(Prop2.Name.Equals("CoilModel")))
                        {
                            ICell cell;
                            if (row == 1)
                            {
                                cell = Row.CreateCell(Cell_No);
                                cell.SetCellValue(Prop2.Name);
                                oSheet.AutoSizeColumn(Cell_No);
                            }
                            cell = Row2.CreateCell(Cell_No);
                            if (Prop2.GetValue(o, null) != null)
                            {
                                cell.SetCellValue(Prop2.GetValue(o, null).ToString());
                                oSheet.AutoSizeColumn(Cell_No);
                            }
                            Cell_No++;
                        }
                    }
                }
            }
            return oSheet;
        }

        public static ISheet WriteInReceiverType(ISheet oSheet, List<ReceiverTypeDetails> lstReceiverTypeDetails)
        {
            Dictionary<int, string> colheaders = new Dictionary<int, string>();
            colheaders = lstReceiverTypeDetails.Select(i => new { i.RefrigerantType_ID, i.RefrigerantType }).Distinct().ToDictionary(a => a.RefrigerantType_ID, a => a.RefrigerantType);

            Dictionary<int, string> rowHeaders = lstReceiverTypeDetails.Select(i => new { i.ReceiverTypeID, i.ReceiverSize }).Distinct().ToDictionary(a => a.ReceiverTypeID, a => a.ReceiverSize);
            rowHeaders.Add(0, "Refrigerant Type");
            IRow Row = null;
            int rowCount = 0;
            int Cell_No = 0;
            if (rowCount == 0)
            {
                //Print Headers
                Row = oSheet.CreateRow(rowCount);
                foreach (var head in rowHeaders.OrderBy(a => a.Key))
                {
                    ICell cell = Row.CreateCell(Cell_No);
                    cell.SetCellValue(head.Value);
                    oSheet.AutoSizeColumn(Cell_No);
                    Cell_No++;
                }
                rowCount++;

            }

            foreach (var col in colheaders)
            {
                Cell_No = 0;
                Row = oSheet.CreateRow(rowCount);
                foreach (var row in rowHeaders.OrderBy(a => a.Key))
                {

                    if (row.Key == 0)
                    {
                        ICell cell = Row.CreateCell(Cell_No);
                        cell.SetCellValue(col.Value);
                        oSheet.AutoSizeColumn(Cell_No);

                    }
                    else
                    {
                        ICell cell = Row.CreateCell(Cell_No);
                        decimal? capacity = lstReceiverTypeDetails.Where(a => a.ReceiverTypeID == row.Key && a.RefrigerantType_ID == col.Key).FirstOrDefault().ReceiverCapacity;
                        cell.SetCellValue(capacity == null ? "0" : capacity.ToString());
                        oSheet.AutoSizeColumn(Cell_No);
                    }

                    Cell_No++;
                }
                rowCount++;
            }


            return oSheet;
        }

        public void GetCondenserCapacity(int ModelID, CondenserModel CondenserModel)
        {
            try
            {
                
                List<CondenserCapacityDetails> condenserCapacityData = (CondenserDBContext.CondenserCapacity_.Where(x => x.CondenserModel.ModelID == CondenserModel.ModelID).Select(x => new CondenserCapacityDetails
                {
                    RefregerantTypeName = x.RefrigerantType.RefrigerantDescr.Substring(0, 5),
                    CondCapacityID_ = x.CondCapacityID_,
                    ModelID = x.CondenserModel.ModelID,
                    RefrigerantTypeID = x.RefrigerantType.RefrigerantType_ID,
                    Capacity = x.Capacity
                }).ToList());

                ISheet sheet1 = workBook.CreateSheet("Condenser Capacity");

                int i = 1;
                foreach (var row in condenserCapacityData)
                {
                    sheet1 = WriteInSheet(sheet1, row, i);
                    i++;
                }
            }
            catch(Exception exception)
            {
                Logger.PrintError(exception);
            }
        }

        private void GetMiscellaneousData(int ModelID)
        {
            try
            {

                List<CondenserPropertiesDetails> condenserPropertiesData = CondenserDBContext.CondenserProperties.Where(x => x.CondenserModel.ModelID == ModelID).Select(x => new CondenserPropertiesDetails
                {
                    CondPropID = x.CondPropID,
                    ModelID = x.CondenserModel.ModelID,
                    VoltageTypeID = x.VoltageType_lkp.VoltageTypeID,
                    VoltageTypeName = x.VoltageType_lkp.VoltageType,
                    CondenserAmps = x.CondenserAmps,
                    MCA = x.MCA,
                    MOPD = x.MOPD
                }).ToList();

                ISheet sheet1 = workBook.CreateSheet("Condenser Properties");

                int i = 1;
                foreach (var row in condenserPropertiesData)
                {
                    sheet1 = WriteInSheet(sheet1, row, i);
                    i++;
                }

            }
            catch(Exception exception)
            {
                Logger.PrintError(exception);
            }
        }

        private void GetControlPanelData(int ModelID)
        {
            try
            {

                ControlPanelDetailsViewModel controlPanel = CondenserDBContext.ControlPanelDetails.Where(x => x.CondenserModel.ModelID == ModelID).Select(x => new ControlPanelDetailsViewModel {
                    ControlPanelDetailID = x.ControlPanelDetailID,
                    Controls = x.Controls.ControlType,
                    ControlManufacturer = x.ControlManufacturer.ControlManufacturer1,
                    ControlPanelAirSensor = x.ControlPanelAirSensor.AirSensorOptions,
                    ControlFusesandBreakers = x.ControlFusesandBreakers.ControlFusesandBreakers1,
                    ControlTypeofApplication = x.ControlTypeofApplication.ControlTypeofApplication1,
                    ControlVoltage = x.ControlVoltage.ControlVoltage1
                }).FirstOrDefault();

                ISheet sheet1 = workBook.CreateSheet("Control Panel");
                sheet1 = WriteInSheet(sheet1, controlPanel, 1);


               List<ControlPanelOptionsViewModel> controlOptions = CondenserDBContext.ControlPanelDetailsAndOptions.Join(CondenserDBContext.ControlPanelOptions,       // Target table
              (CPAD => CPAD.ControlPanelOptions.ID),   // ID of clients column
              (CPO => CPO.ID), // Corresponding ID for orders to join on
              ((CPAD, CPO) => new ControlPanelOptionsViewModel
              {
                  ID = CPO.ID,
                  ControlPanelOptionsName = CPO.ControlPanelOptions1,
                  ListPriceAdder = CPO.ListPriceAdder,
                  BaaNFeature = CPO.BaaNFeature,
                  BaaNOption = CPO.BaaNOption,
                  ControlPanelDetailsID = CPAD.ControlPanelDetails.ControlPanelDetailID

              })).Where(x => x.ControlPanelDetailsID == controlPanel.ControlPanelDetailID).ToList();

                ISheet controlPanelsheet = workBook.CreateSheet("Control Panel Options");
                

                int i = 1;
                foreach (var row in controlOptions)
                {
                    controlPanelsheet = WriteInSheet(controlPanelsheet, row, i);
                    i++;
                }


            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
        }
    }
}