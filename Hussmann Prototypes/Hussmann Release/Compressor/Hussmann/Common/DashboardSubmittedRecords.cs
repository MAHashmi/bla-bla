﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hussmann.Models;
using UnitCoolerDataModel;
using Hussmann.Common.Enumeration;
using Hussmann.Common.Helper;
using CondenserDataModel;
using CompressorDataModel;

namespace Hussmann.Common
{
    public class DashboardSubmittedRecords
    {
            #region Lists
            public List<DashboardCommonSubmittedRecordsViewModel> CommonSubmittedRecords = new List<DashboardCommonSubmittedRecordsViewModel>();
            #endregion

            #region Objects
            private RulestreamCoilDBEntities UnitCoolerDBContext = new RulestreamCoilDBEntities();
            private RulestreamCondenserDBEntities CondenserDBContext = new RulestreamCondenserDBEntities();
            private RulestreamCompressorDBEntities CompressorDBContext = new RulestreamCompressorDBEntities();
            #endregion

            #region Properties
            #endregion


            #region Functions

            // merge data of four different databases  into common list for dashboard saved records
            public List<DashboardCommonSubmittedRecordsViewModel> GetCombinedListofDashboardSumittedRecords(string userType, string userName)
            {
                try
                {

                    // For unit cooler dashboard saved Records
                    GetUnitCoolerDashboardSubmittedRecords(CommonSubmittedRecords, userType, userName);

                    //For Case saved Records
                    GetCaseDashboardSubmittedRecords(CommonSubmittedRecords, userType, userName);

                    //For Compressor saved Records
                    GetCompressorDashboardSubmittedRecords(CommonSubmittedRecords, userType, userName);

                    //For Condenser saved Records
                    GetCondenserDashboardSubmittedRecords(CommonSubmittedRecords, userType, userName);

                    //Sort CommonSavedRecords on the basis of LastModifiedDate
                    CommonSubmittedRecords = CommonSubmittedRecords.OrderByDescending(a => a.LastModifiedOn).Select(x => new DashboardCommonSubmittedRecordsViewModel()
                    {
                        ModelID = x.ModelID,
                        StateID = x.StateID,
                        ModelNumber = x.ModelNumber,
                        ActionBy = x.ActionBy,
                        CreatedBy = x.CreatedBy,
                        LastModifiedOn = x.LastModifiedOn,
                        ChillerType = x.ChillerType
                    }).Take(10).ToList();

                }
                catch (Exception exception)
                {
                    Logger.PrintError(exception);
                }
                //return Object
                return CommonSubmittedRecords;
            }

            private void GetCondenserDashboardSubmittedRecords(List<DashboardCommonSubmittedRecordsViewModel> CommonSubmittedRecords, string userType, string userName)
            {
                List<DashboardCommonSubmittedRecordsViewModel> submittedRecordsData = CondenserDBContext.spGetCondenserSubmittedRecord(userType, userName).Select(x => new DashboardCommonSubmittedRecordsViewModel()
                {
                    ModelID = x.ModelID,
                    StateID = x.StateID,
                    ModelNumber = x.ModelNumber,
                    ActionBy = x.ActionBy,
                    CreatedBy = x.CreatedBy,
                    LastModifiedOn = x.LastModifiedOn,
                    ChillerType = enChillerType.Condenser.ToString()

                }).ToList();

                CommonSubmittedRecords.AddRange(submittedRecordsData);
            }

            private void GetCompressorDashboardSubmittedRecords(List<DashboardCommonSubmittedRecordsViewModel> CommonSubmittedRecords, string userType, string userName)
            {
                List<DashboardCommonSubmittedRecordsViewModel> submittedRecordsData = CompressorDBContext.spGetCompressorSubmittedRecord(userType, userName).Select(x => new DashboardCommonSubmittedRecordsViewModel()
                {
                    ModelID = x.ModelID,
                    StateID = x.StateID,
                    ModelNumber = x.ModelNumber,
                    ActionBy = x.ActionBy,
                    CreatedBy = x.CreatedBy,
                    LastModifiedOn = x.LastModifiedOn,
                    ChillerType = enChillerType.Compressor.ToString()

                }).ToList();

                CommonSubmittedRecords.AddRange(submittedRecordsData);

        }

            private void GetCaseDashboardSubmittedRecords(List<DashboardCommonSubmittedRecordsViewModel> CommonSubmittedRecords, string userType, string userName)
            {

            }

            private void GetUnitCoolerDashboardSubmittedRecords(List<DashboardCommonSubmittedRecordsViewModel> CommonSubmittedRecords, string userType, string userName)
            {
                try
                {
                    List<DashboardCommonSubmittedRecordsViewModel> submittedRecordsData = UnitCoolerDBContext.spGetUnitCoolerSubmittedRecord(userType, userName).Select(x => new DashboardCommonSubmittedRecordsViewModel()
                    {
                        ModelID = x.ModelID,
                        StateID = x.StateID,
                        ModelNumber = x.ModelNumber,
                        ActionBy = x.ActionBy,
                        CreatedBy = x.CreatedBy,
                        LastModifiedOn = x.LastModifiedOn,
                        ChillerType = enChillerType.UnitCooler.ToString()

                    }).ToList();

                    CommonSubmittedRecords.AddRange(submittedRecordsData);
                }
                catch (Exception exception)
                {
                    Logger.PrintError(exception);
                }
            }

            #endregion
    }
}