﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hussmann.Models;
using UnitCoolerDataModel;
using Hussmann.Common.Enumeration;
using Hussmann.Common.Helper;
using CondenserDataModel;
using CompressorDataModel;

namespace Hussmann.Common
{
    public class DashboardSavedRecords
    {
            #region Lists
            public List<DashboardCommonSavedRecordsViewModel> CommonSavedRecords = new List<DashboardCommonSavedRecordsViewModel>();
            #endregion

            #region Objects
            private RulestreamCoilDBEntities UnitCoolerDBContext = new RulestreamCoilDBEntities();
            private RulestreamCondenserDBEntities CondenserDBContext = new RulestreamCondenserDBEntities();
            private RulestreamCompressorDBEntities CompressorDBContext = new RulestreamCompressorDBEntities(); 
            #endregion

            #region Properties
            #endregion


            #region Functions

            // merge data of four different databases  into common list for dashboard saved records
            public List<DashboardCommonSavedRecordsViewModel> GetCombinedListofDashboardSavedRecords(string userType, string userName)
            {
                try
                {

                    // For unit cooler dashboard saved Records
                    GetUnitCoolerDashboardSavedRecords(CommonSavedRecords, userType, userName);

                    //For Case saved Records
                    GetCaseDashboardSavedRecords(CommonSavedRecords, userType, userName);

                    //For Compressor saved Records
                    GetCompressorDashboardSavedRecords(CommonSavedRecords, userType, userName);

                    //For Condenser saved Records
                    GetCondenserDashboardSavedRecords(CommonSavedRecords, userType, userName);

                    //Sort CommonSavedRecords on the basis of LastModifiedDate
                    CommonSavedRecords = CommonSavedRecords.OrderByDescending(a => a.LastModifiedOn).Select(x => new DashboardCommonSavedRecordsViewModel()
                    {
                        ModelID = x.ModelID,
                        StateID = x.StateID,
                        ModelNumber = x.ModelNumber,
                        ActionBy = x.ActionBy,
                        CreatedBy = x.CreatedBy,
                        LastModifiedOn = x.LastModifiedOn,
                        ChillerType = x.ChillerType
                    }).Take(10).ToList();

                }
                catch (Exception exception)
                {
                    Logger.PrintError(exception);
                }
                //return Object
                return CommonSavedRecords;
            }

            private void GetCondenserDashboardSavedRecords(List<DashboardCommonSavedRecordsViewModel> commonSavedRecords, string userType, string userName)
            {
                List<DashboardCommonSavedRecordsViewModel> savedRecordsData = CondenserDBContext.spGetCondenserSavedRecord(userType, userName).Select(x => new DashboardCommonSavedRecordsViewModel()
                {
                    ModelID = x.ModelID,
                    StateID = x.StateID,
                    ModelNumber = x.ModelNumber,
                    ActionBy = x.ActionBy,
                    CreatedBy = x.CreatedBy,
                    LastModifiedOn = x.LastModifiedOn,
                    ChillerType = enChillerType.Condenser.ToString()

                }).ToList();

                commonSavedRecords.AddRange(savedRecordsData);
            }

            private void GetCompressorDashboardSavedRecords(List<DashboardCommonSavedRecordsViewModel> commonSavedRecords, string userType, string userName)
            {
                List<DashboardCommonSavedRecordsViewModel> savedRecordsData = CompressorDBContext.spGetCompressorSavedRecord(userType, userName).Select(x => new DashboardCommonSavedRecordsViewModel()
                {
                    ModelID = x.ModelID,
                    StateID = x.StateID,
                    ModelNumber = x.ModelNumber,
                    ActionBy = x.ActionBy,
                    CreatedBy = x.CreatedBy,
                    LastModifiedOn = x.LastModifiedOn,
                    ChillerType = enChillerType.Compressor.ToString()

                }).ToList();

                commonSavedRecords.AddRange(savedRecordsData);

        }

            private void GetCaseDashboardSavedRecords(List<DashboardCommonSavedRecordsViewModel> commonSavedRecords, string userType, string userName)
            {

            }

            private void GetUnitCoolerDashboardSavedRecords(List<DashboardCommonSavedRecordsViewModel> commonSavedRecords, string userType, string userName)
            {
                try
                {
                    List<DashboardCommonSavedRecordsViewModel> savedRecordsData = UnitCoolerDBContext.spGetUnitCoolerSavedRecord(userType, userName).Select(x => new DashboardCommonSavedRecordsViewModel()
                    {
                        ModelID = x.ModelID,
                        StateID = x.StateID,
                        ModelNumber = x.ModelNumber,
                        ActionBy = x.ActionBy,
                        CreatedBy = x.CreatedBy,
                        LastModifiedOn = x.LastModifiedOn,
                        ChillerType = enChillerType.UnitCooler.ToString()

                    }).ToList();

                    commonSavedRecords.AddRange(savedRecordsData);
                }
                catch (Exception exception)
                {
                    Logger.PrintError(exception);
                }
            }

            #endregion

    }
}