﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Hussmann.Common.Enumeration
{

    public enum enState
    {
        Draft = 1,
        Submitted = 2,
        Rejected = 3,
        Approved = 4,
        Deleted = 5
    }

    public enum enUserType
    {
        Reader = 1,
        DataFeeder = 2,
        Approver = 3,
        Super = 4
    }

    public enum enChillerType
    {
        UnitCooler = 1,
        Case = 2,
        Compressor = 3,
        Condenser = 4
    }

    //These enums are define to check the login user roles
    public enum Role
    {
        CanReadContent,
        CanSearchContent,
        CanExportResult,
        CanAddRecord,
        CanUpdateRecord,
        CanApprovedRecord,
        CanUpdateAprrovedRecord,
        CanActivateRecord,
        CanAccessCaseUI,
        CanAccessCoilUI,
        CanAccessCompressorUI,
        CanAccessCondenserUI,
        CanRejectedRecord
    }

    public enum enTabs
    {
        DetailsTab = 1,
        AccessoriesTab = 2,
        ConfigurationsTab = 3,
        PricingTab = 4
    }

    public enum enCondenserTabs
    {
        CondenserDetailsTab = 1,
        CondenserTypeTab = 2,
        CondenserCapacityTab = 3,
        MiscellaneousTab = 4,
        ControlPanelTab = 5,
        ReceiverTypeTab = 6,
        PricingTab = 7
    }

    public enum enMode
    {
        Add,
        Edit
    }

    public enum enAction
    {
        AddUnitCooler,
        EditUnitCooler,
        Search,
        Index
    }

    public enum enController
    {
        UnitCooler,
        Home
    }

    public enum enSortExpression
    {
        ModelNumber = 0,
        CoilBrandName = 1,
        CoilFamilyName = 2,
        NumberofFans = 3,
        StateDescription = 4,
        Version = 5,
        LastModifiedOn = 6,
        ActionBy = 7,
        ActiveInactiveStatus = 8
    }

    public enum enCondenserSortExpression
    {
        ModelNumber = 0,
        CondenserBrandName = 1,
        CondenserFamilyName = 2,
        ModelSuffixID = 3,
        StateDescription = 4,
        Version = 5,
        LastModifiedOn = 6,
        ActionBy = 7
    }

    public enum enCondenserTabPartialView
    {
        _CondenserDetails = 0,
        _CondenserType = 1,
        _CondenserCapacity = 2,
        _Miscellaneous = 3,
        _ControlPanel = 4,
        _ReceiverType = 5,
        _Pricing = 6
    }

    public enum enCondenserController
    {
        Condenser,
        Home
    }
    public enum enCondenserAction
    {
        SearchCondenser,
        EditCondenser,
        AddCondenser,
        Home
    }

    public enum enCompressorSortExpression
    {
        ModelNumber = 0,
        CompTypeDescr = 1,
        CompSeries = 2,
        //RefrigerantType = 3,
        CompVendor = 3,
        StateDescription = 4,
        Version = 5,
        LastModifiedOn = 6,
        ActionBy = 7
    }

    public enum enCompressorTabPartialView
    {
        _CompressorDetails = 0,
        _RLADetails = 1,
        _BOMDetails = 2,
        _Coefficients = 3
    }

    public enum enCompressorController
    {
        Compressor,
        Home
    }
    public enum enCompressorAction
    {
        SearchCompressor,
        EditCompressor,
        AddCompressor,
        Home
    }
}