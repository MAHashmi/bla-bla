﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hussmann.Common.Enumeration;
using Hussmann.Common.Helper;
using Hussmann.Models;
using UnitCoolerDataModel;
using CondenserDataModel;
using CompressorDataModel;

namespace Hussmann.Common
{
    public class DashboardRejectedRecords
    {
        #region Lists
        public List<DashboardCommonRejectedRecordsViewModel> CommonRejectedRecords = new List<DashboardCommonRejectedRecordsViewModel>();
        #endregion

        #region Objects
        private RulestreamCoilDBEntities UnitCoolerDBContext = new RulestreamCoilDBEntities();
        private RulestreamCondenserDBEntities CondenserDBContext = new RulestreamCondenserDBEntities();
        private RulestreamCompressorDBEntities CompressorDBContext = new RulestreamCompressorDBEntities();
        #endregion

        #region Properties
        #endregion


        #region Functions

        // merge data of four different databases  into common list for dashboard saved records
        public List<DashboardCommonRejectedRecordsViewModel> GetCombinedListofDashboardRejectedRecords(string userType, string userName)
        {
            try
            {

                // For unit cooler dashboard saved Records
                GetUnitCoolerDashboardRejectedRecords(CommonRejectedRecords, userType, userName);

                //For Case saved Records
                GetCaseDashboardRejectedRecords(CommonRejectedRecords, userType, userName);

                //For Compressor saved Records
                GetCompressorDashboardRejectedRecords(CommonRejectedRecords, userType, userName);

                //For Condenser saved Records
                GetCondenserDashboardRejectedRecords(CommonRejectedRecords, userType, userName);

                //Sort CommonSavedRecords on the basis of LastModifiedDate
                CommonRejectedRecords = CommonRejectedRecords.OrderByDescending(a => a.LastModifiedOn).Select(x => new DashboardCommonRejectedRecordsViewModel()
                {
                    ModelID = x.ModelID,
                    StateID = x.StateID,
                    ModelNumber = x.ModelNumber,
                    ActionBy = x.ActionBy,
                    CreatedBy = x.CreatedBy,
                    LastModifiedOn = x.LastModifiedOn,
                    ChillerType = x.ChillerType
                }).Take(10).ToList();
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            //return Object
            return CommonRejectedRecords;
        }

        // Get rejected data from condenser database
        private void GetCondenserDashboardRejectedRecords(List<DashboardCommonRejectedRecordsViewModel> commonRejectedRecords, string userType, string userName)
        {
            List<DashboardCommonRejectedRecordsViewModel> rejectedRecordsData = CondenserDBContext.spGetCondenserRejectedRecord(userType, userName).Select(x => new DashboardCommonRejectedRecordsViewModel()
            {
                ModelID = x.ModelID,
                StateID = x.StateID,
                ModelNumber = x.ModelNumber,
                ActionBy = x.ActionBy,
                CreatedBy = x.CreatedBy,
                LastModifiedOn = x.LastModifiedOn,
                ChillerType = enChillerType.Condenser.ToString()

            }).ToList();

            commonRejectedRecords.AddRange(rejectedRecordsData);
        }

        // Get rejected data from compressor database
        private void GetCompressorDashboardRejectedRecords(List<DashboardCommonRejectedRecordsViewModel> commonRejectedRecords, string userType, string userName)
        {
            List<DashboardCommonRejectedRecordsViewModel> rejectedRecordsData = CompressorDBContext.spGetCompressorRejectedRecord(userType, userName).Select(x => new DashboardCommonRejectedRecordsViewModel()
            {
                ModelID = x.ModelID,
                StateID = x.StateID,
                ModelNumber = x.ModelNumber,
                ActionBy = x.ActionBy,
                CreatedBy = x.CreatedBy,
                LastModifiedOn = x.LastModifiedOn,
                ChillerType = enChillerType.Compressor.ToString()

            }).ToList();

            commonRejectedRecords.AddRange(rejectedRecordsData);

        }

        // Get rejected data from case database
        private void GetCaseDashboardRejectedRecords(List<DashboardCommonRejectedRecordsViewModel> commonRejectedRecords, string userType, string userName)
        {

        }

        // Get rejected data from unit cooler database
        private void GetUnitCoolerDashboardRejectedRecords(List<DashboardCommonRejectedRecordsViewModel> commonRejectedRecords, string userType, string userName)
        {
            try
            {
                List<DashboardCommonRejectedRecordsViewModel> rejectedRecordsData = UnitCoolerDBContext.spGetUnitCoolerRejectedRecord(userType, userName).Select(x => new DashboardCommonRejectedRecordsViewModel()
                {
                    ModelID = x.ModelID,
                    StateID = x.StateID,
                    ModelNumber = x.ModelNumber,
                    ActionBy = x.ActionBy,
                    CreatedBy = x.CreatedBy,
                    LastModifiedOn = x.LastModifiedOn,
                    ChillerType = enChillerType.UnitCooler.ToString()

                }).ToList();

                commonRejectedRecords.AddRange(rejectedRecordsData);
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
        }

        #endregion
    }
}