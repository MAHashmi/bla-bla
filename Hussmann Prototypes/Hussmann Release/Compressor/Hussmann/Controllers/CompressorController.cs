﻿using Hussmann.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Hussmann.Common.Enumeration;
using Hussmann.Models.Condenser.Add;
using CondenserDataModel;
using Hussmann.Common.Helper;
using System.Data;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Hussmann.Models.Compressor.RefrigerantType;
using System.Web.Script.Serialization;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.IO;
using Hussmann.Models.Compressor;
using CompressorDataModel;
using Hussmann.Models.Compressor.Add;

namespace Hussmann.Controllers
{
    public class CompressorController : BaseController
    {
        private RulestreamCompressorDBEntities CompressorDBContext;
        //private RulestreamCondenserDBEntities CondenserDBContext = new RulestreamCondenserDBEntities();
        public bool IsDraftMode;
        public enUserType userType;
        public string userName;
        public static string mode;

        public CompressorController()
        {
            ViewBag.ShowPopupAlert = false;
            CompressorDBContext = new RulestreamCompressorDBEntities();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            userName = filterContext.HttpContext.User.Identity.Name.Split('\\')[1];
            //  userType = Authorization.GetUserType(HttpContext.ApplicationInstance.Context);
            Enum.TryParse(Authorization.GetUserType(HttpContext.ApplicationInstance.Context), out enUserType currEnum);
            userType = currEnum;
            
        }

        [HttpGet]
        public ActionResult SearchCompressor(string modelNumber, int? compressorTypeId, int? compressorSeriesId, int? refrigerantTypeId,
            int? compressorVendorId, int? statusId, bool? pinned = true)
        {
            Logger.PrintInfo("Action:Search ~~~~~~ Search Screen");
            ViewBag.ModelNumber = modelNumber;
            ViewBag.TypeId = compressorTypeId;
            ViewBag.SeriesId = compressorSeriesId;
            ViewBag.RefrigerantTypeId = refrigerantTypeId;
            ViewBag.VendorId = compressorVendorId;
            ViewBag.StatusId = statusId;
            ViewBag.PinnedTab = pinned;

            ViewBag.CompTypeList = new SelectList(CompressorDBContext.CompTypes.OrderBy(x => x.CompTypeDescr), "CompType_ID", "CompTypeDescr", compressorTypeId);
            ViewBag.CompSeriesList = new SelectList(CompressorDBContext.CompSeries.OrderBy(x => x.CompSeries), "CompSeries_ID", "CompSeries", compressorSeriesId);
            ViewBag.RefrigerantTypeList = new SelectList(CompressorDBContext.RefrigerantTypes, "RefrigerantType_ID", "RefrigerantType1", refrigerantTypeId);
            ViewBag.CompVendorList = new SelectList(CompressorDBContext.CompVendors, "CompVendor_ID", "CompVendor1", compressorVendorId);
            ViewBag.StatusList = new SelectList(CompressorDBContext.States, "StateID", "Description", statusId);

            if (userType == enUserType.Reader)
            {
                ViewBag.EditOrView = "View";
                ViewBag.IconImage = "fa fa-eye";
            }
            else
            {
                ViewBag.EditOrView = "Edit";
                ViewBag.IconImage = "fa fa-pencil";
            }
            ViewBag.userType = userType;
            ViewBag.userName = userName;

            return View();
        }

        public ActionResult GetCompressorData(string modelNumber, int? compressorTypeId, int? compressorSeriesId, int? refrigerantTypeId,
            int? compressorVendorId, int? statusId)
        {
            JsonResult result = new JsonResult();

            string search = Request.Form.GetValues("search[value]")[0];
            string draw = Request.Form.GetValues("draw")[0];
            string order = Request.Form.GetValues("order[0][column]")[0];
            string orderDir = Request.Form.GetValues("order[0][dir]")[0];
            int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
            int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);

       
            if (modelNumber == "null" || modelNumber == "Null")
            {
                modelNumber = null;
            }

            if (orderDir == "desc")
            {
                orderDir = "Descending";
            }
            else if (orderDir == "asc")
            {
                orderDir = "Ascending";
            }
            //Total record count.
            int? totalRecords = CompressorDBContext.spGetRowsCountForSearch(modelNumber, compressorTypeId, compressorSeriesId, refrigerantTypeId,
                compressorVendorId, statusId, userName, userType.ToString()).SingleOrDefault();
            int? recFilter = totalRecords;
             List<spSearchCompressor_Result> searchResult = new List<spSearchCompressor_Result>();

            string sortExpression = GetSortExpression(order);

            //Search
            if (!string.IsNullOrEmpty(search) &&
                   !string.IsNullOrWhiteSpace(search))
            {
                // Apply search
                searchResult = CompressorDBContext.spSearchCompressor(modelNumber, compressorTypeId, compressorSeriesId, refrigerantTypeId,
                    compressorVendorId, statusId, userName, userType.ToString(), 0, totalRecords, sortExpression, orderDir).ToList();
                searchResult = searchResult.Where(p => p.ModelNumber.ToString().ToLower().Contains(search.ToLower()) ||
                    p.CompSeries.ToLower().Contains(search.ToLower()) ||
                    (!string.IsNullOrEmpty(p.CompTypeDescr) && p.CompTypeDescr.ToLower().Contains(search.ToLower())) ||
                    (!string.IsNullOrEmpty(p.CompVendor) && p.CompVendor.ToString().ToLower().Contains(search.ToLower())) ||
                    p.StateDescription.ToLower().Contains(search.ToLower()) ||
                    p.Version.ToString().ToLower().Contains(search.ToLower()) ||
                    p.LastModifiedOn.ToString().ToLower().Contains(search.ToLower()) ||
                    p.ActionBy.ToLower().Contains(search.ToLower())).ToList();
                recFilter = searchResult.Count;
                searchResult = searchResult.Skip(startRec).Take(pageSize).ToList();
            }
            else
            {
                searchResult = CompressorDBContext.spSearchCompressor(modelNumber, compressorTypeId, compressorSeriesId, refrigerantTypeId,
                    compressorVendorId, statusId, userName, userType.ToString(), startRec + 1, startRec + pageSize, sortExpression, orderDir).ToList();
            }

            //Initialize Json
            result = this.Json(new
            {
                draw = Convert.ToInt32(draw),
                recordsTotal = totalRecords,
                recordsFiltered = recFilter,
                data = searchResult
            }, JsonRequestBehavior.AllowGet);
            ViewBag.Page = 1;
            return result;
        }

        private string GetSortExpression(string order)
        {
            if (order != null)
            {
                try
                {
                    int columnIndex = int.Parse(order);
                    return Convert.ToString((enCompressorSortExpression)columnIndex);
                }
                catch (FormatException fx)
                {
                    Logger.PrintError(fx);
                    return Convert.ToString(enCompressorSortExpression.LastModifiedOn);
                }
            }
            return Convert.ToString(enCompressorSortExpression.LastModifiedOn);
        }

        // GET: UnitCondenser
        public ActionResult Index()
        {
            return View();
        }

        public CompressorModel GetCompressorModel(AddCompressorViewModel addCompressor)
        {
            Logger.PrintInfo("Function:GetCompressorModel ~~~~~~ Returns Compressor Model object");
            var CompressorModelInfo = CompressorDBContext.CompressorModels.AsNoTracking().Where(
                x => x.CompModel_ID == addCompressor.CurrentCompressorModelID).Select(x => x).FirstOrDefault();
            try
            {
                enState state = (enState)CompressorModelInfo.StateID;

                if (state == enState.Approved && addCompressor.IsEditedAfterApprove == true)
                {
                    if (userType != enUserType.Reader) /*(userType == Convert.ToString(enUserType.DataFeeder) && CoilModelInfo.CreatedBy == userName)*/
                    {
                        Logger.PrintInfo("Creating new version of " + CompressorModelInfo.ModelNumber + " with ModelID : " + CompressorModelInfo.CompModel_ID);
                        //  Create new compressor model with same data but new version
                        int ModelID = CompressorDBContext.spCreateCompressorModelWithNewVersion(CompressorModelInfo.CompModel_ID, userName).FirstOrDefault().Value;

                        addCompressor.CurrentCompressorModelID = ModelID;
                        Logger.PrintInfo("New Version created of " + CompressorModelInfo.ModelNumber + " with ModelID :" + ModelID);
                        addCompressor.IsEditedAfterApprove = false;

                        CompressorModelInfo = CompressorDBContext.CompressorModels.AsNoTracking().Where(x => x.CompModel_ID == ModelID).FirstOrDefault();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return CompressorModelInfo;
        }


        // get: Save refrigerant type
        [HttpPost]
        public ActionResult SaveRefrigerantType(RefrigerantTypeViewModel RefrigerantType)
        {
            var RefrigerantTypeDB = CompressorDBContext.RefrigerantTypes.Where(x => x.RefrigerantType_ID == RefrigerantType.RefrigerantType_ID).SingleOrDefault();
            if (RefrigerantTypeDB != null)
            {
                RefrigerantTypeDB.RefrigerantType1 = RefrigerantType.RefrigerantType1;
                RefrigerantTypeDB.RefrigerantDescr = RefrigerantType.RefrigerantDescr;
                RefrigerantTypeDB.Active = RefrigerantType.Active;
                CompressorDBContext.SaveChanges();
            }
            else
            {
                CompressorDataModel.RefrigerantType newRefrigerantType = new CompressorDataModel.RefrigerantType()
                {
                    RefrigerantDescr = RefrigerantType.RefrigerantDescr,
                    RefrigerantType1 = RefrigerantType.RefrigerantType1,
                    Active = RefrigerantType.Active
                };
                CompressorDBContext.RefrigerantTypes.Add(newRefrigerantType);
                CompressorDBContext.SaveChanges();
            }
            return RedirectToAction("ManageRefrigerantType");
        }

        public ActionResult AddCompressor(string Name)
        {
           if (Name == null)
              Logger.PrintInfo("Action:AddCompressor ~~~~~~ Add Compressor");
          // int StateID = Convert.ToInt32((int)enState.Draft);
           mode = Convert.ToString(enMode.Add);
           AddCompressorViewModel addCompressor = new AddCompressorViewModel();
            try
            {
                // For Compressor Details Tab
                AddCompressorDetailsViewModel addCompressorDetails = new AddCompressorDetailsViewModel();

                addCompressorDetails = SetDefaultListsDataForDetailsTab(addCompressorDetails);

                addCompressor.CompressorDetailsViewModel = addCompressorDetails;

            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }

            return View(addCompressor);
        }


        [HttpPost]
        [AuthorizeAD(Role.CanAddRecord)]
        public ActionResult AddCompressor(AddCompressorViewModel addCompressor)
        {
            Logger.PrintInfo("Action:AddCompressor Post ~~~~~~ Add Compressor");

            addCompressor.CompressorDetailsViewModel = SetDefaultListsDataForDetailsTab(addCompressor.CompressorDetailsViewModel);

            if (ModelState.IsValid)
            {
                addCompressor.CurrentCompressorModelID = InsertCompressorDetails(addCompressor);

                IsDraftMode = true;


                return RedirectToAction("EditCompressor", "Compressor", new { ModelID = addCompressor.CurrentCompressorModelID, RedirectTo = Convert.ToString(enAction.Index) });
            }
            return View(addCompressor);
        }

        //Call By Method: AddCompressor
        private int InsertCompressorDetails(AddCompressorViewModel addCompressor)
        {
            var modelID = 0;
            try
            {
              
                if (addCompressor.CompressorDetailsViewModel.CapReduct_ID == 0)
                    addCompressor.CompressorDetailsViewModel.CapReduct_ID = null;

                modelID = CompressorDBContext.spCreateCompressorModel(addCompressor.CompressorDetailsViewModel.ModelNumber.Trim(),
                                                                      addCompressor.CompressorDetailsViewModel.NomHorsepower.Trim(),
                                                                      addCompressor.CompressorDetailsViewModel.RealHorsePower,
                                                                      addCompressor.CompressorDetailsViewModel.CompSeries_ID,
                                                                      addCompressor.CompressorDetailsViewModel.CapReduct_ID,
                                                                      userName
                                                                         ).FirstOrDefault().Value;

            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);

            }
            return modelID;
        }

        private AddCompressorDetailsViewModel SetDefaultListsDataForDetailsTab(AddCompressorDetailsViewModel compressorDetails)
        {
            try
            {
                compressorDetails.CompSeries = CompressorDBContext.CompSeries.OrderBy(x => x.CompSeries).ToList();
                compressorDetails.CapacityReduction = CompressorDBContext.CapacityReductions.OrderBy(x => x.CapReductType).ToList();
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return compressorDetails;
        }

        public ActionResult GetCapacityReductionByCompressorSeries(int CompSeriesID)
        {
            try
            {

                var capacityReductionOptions = CompressorDBContext.spGetCapacityReductionByCompressorSeries(CompSeriesID).ToList();
                var capacityReductions = new SelectList(capacityReductionOptions, "CapReduct_ID", "CapReductType");
                return Json(capacityReductions);
            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }
            return null;
        }

        private AddBOMDetailsViewModel SetDataForBOMDetailsTab(AddBOMDetailsViewModel BOMDetails, CompressorModel compressor)
        {
            try
            {
                BOMDetails.CompTypeList = CompressorDBContext.CompTypes.OrderBy(x => x.CompTypeDescr).ToList();
                BOMDetails.CompVendorList = CompressorDBContext.CompVendors.OrderBy(x => x.CompVendor1).ToList();
                BOMDetails.HussmannTempAppList = CompressorDBContext.HussmannTempApps.OrderBy(x => x.HussTempApp).ToList();
                BOMDetails.VendorBOMCodeList = CompressorDBContext.VendorBOMCodes.OrderBy(x => x.VendorBOMDescr).ToList();
                BOMDetails.SuctLineSizes = CompressorDBContext.SuctLineSizes.OrderBy(x => x.SuctLineSize1).ToList();
                BOMDetails.DiscLineSizes = CompressorDBContext.DiscLineSizes.OrderBy(x => x.DiscLineSize1).ToList();

                List<CompBOM> lstBOMDetail = (from bom in CompressorDBContext.CompBOMs
                                              join rla in CompressorDBContext.CompRLAs
                                              on bom.CompRLA_ID equals rla.CompRLA_ID
                                              where rla.CompModel_ID == compressor.CompModel_ID
                                              select bom).ToList();
                if (lstBOMDetail.Count() != 0)
                {
                    var bomDetails = lstBOMDetail.FirstOrDefault();
                    BOMDetails.CompType_ID = bomDetails.CompType_ID;
                    BOMDetails.CompVendor_ID = CompressorDBContext.CompressorModels.Where(x => x.CompModel_ID == compressor.CompModel_ID).FirstOrDefault().CompVendor_ID == null ? 0 : CompressorDBContext.CompressorModels.Where(x => x.CompModel_ID == compressor.CompModel_ID).Select(x => x.CompVendor_ID).FirstOrDefault().Value; 
                    BOMDetails.HussTempApp_ID = bomDetails.HussTempApp_ID== null ? 0: bomDetails.HussTempApp_ID;
                    BOMDetails.VendorBOMCode_ID = bomDetails.VendorBOMCode_ID;
                    BOMDetails.SuctLine = bomDetails.SuctLine;
                    BOMDetails.DiscLine = bomDetails.DiscLine;
                    BOMDetails.CompKitProtocol = bomDetails.CompKitProtocol;
                    BOMDetails.CompKitHSeries = bomDetails.CompKitHSeries;
                    BOMDetails.CompKitRack = bomDetails.CompKitRack;

                    //set saved Compressor Vendor value to readonly textbox. eg: Copeland (Discus)
                    ViewBag.CompVendor = BOMDetails.CompVendorList.Where(x => x.CompVendor_ID == BOMDetails.CompVendor_ID)
                        .Select(x => x.CompVendor1).FirstOrDefault();

                    //set saved Hussmann Temp App value to readonly textbox. eg: D
                    ViewBag.HussmannTempApp = BOMDetails.HussmannTempAppList.Where(x => x.HussTempApp_ID == BOMDetails.HussTempApp_ID)
                        .Select(x => x.HussTempApp).FirstOrDefault();
                }

            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return BOMDetails;
        }

        public JsonResult IsModelExists([Bind(Prefix = "CompressorDetailsViewModel.ModelNumber")]string ModelNumber)
        {
            //check if any of the ModelNumber matches the ModelNumber 
           if (mode == Convert.ToString(enMode.Add))
            {
                return Json(!CompressorDBContext.CompressorModels.Any(x => x.ModelNumber == ModelNumber.Trim()), JsonRequestBehavior.AllowGet);
            }
            else if (mode == Convert.ToString(enMode.Edit))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        //
        public JsonResult IsRefrigerantTypeExists([Bind(Prefix = "RefrigerantType1")]string RefrigerantTypeName, [Bind(Prefix = "RefrigerantType_ID")]int RefrigerantTypeID)
        {
            if (RefrigerantTypeID != 0 && CompressorDBContext.RefrigerantTypes.Any(x => x.RefrigerantType_ID == RefrigerantTypeID && x.RefrigerantType1 == RefrigerantTypeName))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            //check if any of the RefrigerantType matches the RefrigerantType  
            return Json(!CompressorDBContext.RefrigerantTypes.Any(x => x.RefrigerantType1 == RefrigerantTypeName), JsonRequestBehavior.AllowGet);
        }

        [AuthorizeAD()]
        //[AuthorizeAD]
        public ActionResult EditCompressor(int ModelID, string RedirectTo, string queryStringParameters)
        {
            Logger.PrintInfo("Action:EditCompressor~~~~~~ Edit Compressor");
            mode = Convert.ToString(enMode.Edit);
           // int StateID = Convert.ToInt32((int)enState.Draft);

            var CompressorModel = CompressorDBContext.
            CompressorModels.Where
            (x => x.CompModel_ID == ModelID).Select(x => x).FirstOrDefault();


            AddCompressorViewModel addCompressor = new AddCompressorViewModel();
            AddCompressorDetailsViewModel addCompressorDetails = new AddCompressorDetailsViewModel();
            addCompressorDetails = SetDefaultListsDataForDetailsTab(addCompressorDetails);
            addCompressorDetails = SetDataForDetailsTab(addCompressorDetails, CompressorModel);


            addCompressor.CompressorDetailsViewModel = addCompressorDetails;
            addCompressor.RequestedTabName = "_CompressorDetails";
            addCompressor.CurrentTabName = "_CompressorDetails";
            addCompressor.CurrentCompressorModelID = CompressorModel.CompModel_ID;
            addCompressor.CurrentCompressorState = Enum.GetName(typeof(enState), CompressorModel.StateID);
            addCompressor.CurrentCompressorVersion = CompressorModel.Version;
            addCompressor.CurrentStateID = (int)CompressorModel.StateID;
            addCompressor.RedirectTo = RedirectTo;
            if (queryStringParameters != null)
            {
                queryStringParameters = queryStringParameters.Replace("_", "&");
                addCompressor.SearchQuery = queryStringParameters;
            }
            else
            {
                addCompressor.SearchQuery = "";
            }

            if (userType == enUserType.Reader || (userType == enUserType.DataFeeder && CompressorModel.CreatedBy != userName))
            {
                if (userType == enUserType.DataFeeder && CompressorModel.StateID == (int)enState.Approved)
                {
                    addCompressor.AllowEdit = true;
                }
                else
                {
                    if (CompressorModel.StateID == 5)
                    {
                        return RedirectToAction("Error", "UnitCooler");
                    }
                    else
                    {
                        addCompressor.AllowEdit = false;
                    }
                }
            }
            else
            {
                addCompressor.AllowEdit = true;// should be true
            }

            addCompressor.IsCondenserCapacityGridValid = true;
            addCompressor.IsMiscellaneousGridValid = true;
            addCompressor = SetControlVisibility(addCompressor, HttpContext.ApplicationInstance.Context);

            return View(addCompressor);
        }

        [HttpPost]
        public ActionResult EditCompressor(AddCompressorViewModel addCompressor)
        {
            Logger.PrintInfo("Action:EditCompressor POST~~~~~~ Edit Coompressor");
            var CompressorModelInfo = GetCompressorModel(addCompressor);
            if (CompressorModelInfo == null)
            {
                Logger.PrintInfo("Alert:CompressorModelInfo is null in EditCompressor POST Controller");
            }
            
                bool allowEdit = AllowEditing(CompressorModelInfo, userType.ToString());
                addCompressor.AllowEdit = allowEdit;

                if (allowEdit)
                {
                    switch ((enCompressorTabPartialView)Enum.Parse(typeof(enCompressorTabPartialView), addCompressor.CurrentTabName, true))
                    {
                        case enCompressorTabPartialView._CompressorDetails:
                            SaveCompressorDetailsTabData(addCompressor);
                            break;
                        case enCompressorTabPartialView._RLADetails:
                            SaveRLADetailsTabData(addCompressor);
                            break;
                        case enCompressorTabPartialView._BOMDetails:
                            SaveBOMDetailsTabData(addCompressor);
                            break;
                        case enCompressorTabPartialView._Coefficients:
                            SaveCoefficientsTabData(addCompressor);
                            break;
                    }

                }
            
            var Compressor = CompressorDBContext.
            CompressorModels.AsNoTracking().Where
            (x => x.CompModel_ID == addCompressor.CurrentCompressorModelID).Select(x => x).FirstOrDefault();

            addCompressor.CurrentStateID = Convert.ToInt32(Compressor.StateID);
            addCompressor.CurrentCompressorVersion = Compressor.Version;
            addCompressor.CurrentCompressorState = Enum.GetName(typeof(enState), Compressor.StateID);

            switch ((enCompressorTabPartialView)Enum.Parse(typeof(enCompressorTabPartialView), addCompressor.RequestedTabName, true))
            {
                case enCompressorTabPartialView._CompressorDetails:
                    AddCompressorDetailsViewModel addCompressorDetails = new AddCompressorDetailsViewModel();
                    addCompressor.CompressorDetailsViewModel = SetDefaultListsDataForDetailsTab(addCompressorDetails);
                    addCompressor.CompressorDetailsViewModel = SetDataForDetailsTab(addCompressorDetails, Compressor);
                    break;
                case enCompressorTabPartialView._RLADetails:
                    AddRLADetailsViewModel addRLADetails = new AddRLADetailsViewModel();
                    addCompressor.RLADetails = SetDataForRLADetailsTab(addRLADetails, Compressor);
                    break;
                case enCompressorTabPartialView._BOMDetails:
                    var compRLA = CompressorDBContext.CompRLAs.Where(x => x.CompModel_ID == Compressor.CompModel_ID).FirstOrDefault();
                    //Prevent user to insert data in BOM Details if RLA Details is empty 
                    //due to dependency of BOMDetails on RLADetails ID
                    if (compRLA == null)
                    {
                        ViewBag.ShowPopupAlert = true;
                    }
                    AddBOMDetailsViewModel addBOMDetails = new AddBOMDetailsViewModel();
                    addCompressor.BOMDetails = SetDataForBOMDetailsTab(addBOMDetails, Compressor);
                    break;
                case enCompressorTabPartialView._Coefficients:
                    AddCoefficientsViewModel coefficients = new AddCoefficientsViewModel();
                    addCompressor.Coefficients = SetDataForCoefficientsTab(coefficients, Compressor);
                    break;
             }
            addCompressor.CurrentTabName = addCompressor.RequestedTabName;
            addCompressor = SetControlVisibility(addCompressor, HttpContext.ApplicationInstance.Context);


            return View(addCompressor);
        }

        private void SaveBOMDetailsTabData(AddCompressorViewModel addCompressor)
        {
            try
            {
                if (addCompressor.BOMDetails.CompVendor_ID == 0)
                    addCompressor.BOMDetails.CompVendor_ID = null;
                if (addCompressor.BOMDetails.HussTempApp_ID == 0)
                    addCompressor.BOMDetails.HussTempApp_ID = null;

                CompressorDBContext.spInsertUpdateBOMDetailsTabData(
                    addCompressor.CurrentCompressorModelID
                    , addCompressor.BOMDetails.CompVendor_ID
                    , addCompressor.BOMDetails.HussTempApp_ID
                    , addCompressor.BOMDetails.VendorBOMCode_ID
                    , addCompressor.BOMDetails.CompType_ID
                    , addCompressor.BOMDetails.DiscLine
                    , addCompressor.BOMDetails.SuctLine
                    , addCompressor.BOMDetails.CompKitProtocol
                    , addCompressor.BOMDetails.CompKitHSeries
                    , addCompressor.BOMDetails.CompKitRack
                    );
            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }
        }

        private void SaveRLADetailsTabData(AddCompressorViewModel addCompressor)
        {
            try
            {
                var paramModelID = new SqlParameter("param_ModelID", SqlDbType.Int);
                paramModelID.Value = addCompressor.CurrentCompressorModelID;

                DataTable RLADetailsGrid = (DataTable)JsonConvert.DeserializeObject(addCompressor.RLADetails.RLADetails, (typeof(DataTable)));
                var param_RLADetailsGrid = new SqlParameter("param_RLADetailsUDT", SqlDbType.Structured);
                param_RLADetailsGrid.Value = RLADetailsGrid;
                param_RLADetailsGrid.TypeName = "RLADetailsUDT";
                CompressorDBContext.Database.ExecuteSqlCommand("exec spUpdateRLADetailsTabData @param_ModelID,@param_RLADetailsUDT", paramModelID, param_RLADetailsGrid);

            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }
        }

        private void SaveCoefficientsTabData(AddCompressorViewModel addCompressor)
        {
            if (!string.IsNullOrEmpty(addCompressor.Coefficients.CompressorCoefficients))
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    FloatParseHandling = FloatParseHandling.Decimal
                };

                DataTable coefficientsGridData = JsonConvert.DeserializeObject<CoefficientTable>(addCompressor.Coefficients.CompressorCoefficients, settings);

                var dbHertz = CompressorDBContext.Hertzs.ToList();
                Dictionary<int, string> hertz = new Dictionary<int, string>();
                foreach (var item in dbHertz)
                {
                    hertz.Add(item.Hertz_ID, item.Hertz_Type + "/" + item.Phase);
                }

                if (coefficientsGridData.Rows.Count > 0)
                {
                    foreach (DataRow row in coefficientsGridData.Rows)
                    {
                        var hertzKey = hertz.FirstOrDefault(x => x.Value == row["Hertz"].ToString()).Key;
                        row["Hertz"] = hertzKey.ToString();
                    }
                }

                var paramCoefficientGrid = new SqlParameter("param_CoefficientsUDT", SqlDbType.Structured);
                paramCoefficientGrid.Value = coefficientsGridData;
                paramCoefficientGrid.TypeName = "CoefficientsUDT";

                var paramModelID = new SqlParameter("param_ModelID", SqlDbType.Int);
                paramModelID.Value = addCompressor.CurrentCompressorModelID;

                var paramReturnGasID = new SqlParameter("param_ReturnGasID", SqlDbType.Int);
                paramReturnGasID.Value = addCompressor.Coefficients.ReturnGasID;
                try
                {
                    CompressorDBContext.Database.ExecuteSqlCommand("exec spInsertUpdateCoefficientsTabData @param_ModelID, @param_ReturnGasID, @param_CoefficientsUDT", paramModelID, paramReturnGasID, paramCoefficientGrid);
                }
                catch (Exception ex)
                {
                    Logger.PrintError(ex);
                }
            }
        }

        private AddRLADetailsViewModel SetDataForRLADetailsTab(AddRLADetailsViewModel addRLADetails, CompressorModel compressor)
        {
            try
            {
                var RLADetails = (from r in CompressorDBContext.CompRLAs
                                  where r.CompModel_ID == compressor.CompModel_ID
                                  select new
                                  {
                                      Voltage_ID = r.Voltage_ID,
                                      CompModel_ID = compressor.CompModel_ID,
                                      RLA = r.RLA,
                                      VendorVoltageCode_ID = r.VendorVoltageCode_ID,
                                      WiringSize = r.WiringSize,
                                      NEMA = r.NEMA.Trim(),
                                      DP = r.DP,
                                      Breaker = r.Breaker

                                  }).ToList();
                                           
                if (RLADetails != null)
                {
                    addRLADetails.RLADetails = JsonConvert.SerializeObject(RLADetails);
                    
                }

                addRLADetails.VendorVoltageCodes = CompressorDBContext.VendorVoltageCodes.OrderBy(x => x.VendorVoltageCodeType).ToList();
                addRLADetails.VoltageDetails = CompressorDBContext.spGetVoltageDetails().ToList();
                addRLADetails.listNEMA = CompressorDBContext.NEMAs.OrderBy(x => x.NEMAID).ToList();
                addRLADetails.listDP = CompressorDBContext.DPs.OrderBy(x => x.DP1).ToList();
                addRLADetails.listBreaker = CompressorDBContext.Breakers.OrderBy(x => x.Breaker1).ToList();
                addRLADetails.listWiringSize = CompressorDBContext.WiringSizes.OrderBy(x => x.WiringSizeID).ToList();
                addRLADetails.VendorVoltageCode = JsonConvert.SerializeObject(addRLADetails.VendorVoltageCodes.Select(a => new { label=a.VendorVoltageCodeType, id=a.VendorVoltageCode_ID }));
                addRLADetails.Voltages = JsonConvert.SerializeObject(CompressorDBContext.Voltages.Select(b=>new { id=b.Voltage_ID,label=b.VoltageType}));
                addRLADetails.WiringSizeID = JsonConvert.SerializeObject(addRLADetails.listWiringSize.Select(a => new { label=a.WiringSize1, id=a.WiringSize1 }));
                addRLADetails.NEMAID = JsonConvert.SerializeObject(addRLADetails.listNEMA.Select(a => new { id=a.NEMA1, label=a.NEMA1 }));
                addRLADetails.DPID = JsonConvert.SerializeObject(addRLADetails.listDP.Select(a => new { id=a.DP1, label=a.DP1 }));
                addRLADetails.BreakerID = JsonConvert.SerializeObject(addRLADetails.listBreaker.Select(a => new { id=a.Breaker1, label=a.Breaker1 }));

            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return addRLADetails;
        }

        private AddCoefficientsViewModel SetDataForCoefficientsTab(AddCoefficientsViewModel addCoefficients, CompressorModel compressor)
        {
            try
            {
                var coefficientsData = CompressorDBContext.Coefficients.Where(x => x.CompModel_ID == compressor.CompModel_ID).Select(x => new
                {
                    x.C0,
                    x.C1,
                    x.C2,
                    x.C3,
                    x.C4,
                    x.C5,
                    x.C6,
                    x.C7,
                    x.C8,
                    x.C9,
                    x.W0,
                    x.W1,
                    x.W2,
                    x.W3,
                    x.W4,
                    x.W5,
                    x.W6,
                    x.W7,
                    x.W8,
                    x.W9,
                    x.SuctMin,
                    x.SuctMax,
                    x.MaxCondensingTemp,
                    hertz = CompressorDBContext.Hertzs.Where(h => h.Hertz_ID == x.Hertz_ID).Select(h => h.Hertz_Type + "/" + h.Phase).FirstOrDefault(),
                    x.RefrigerantType_ID,
                    refrigerantType = CompressorDBContext.RefrigerantTypes.Where(r => r.RefrigerantType_ID == x.RefrigerantType_ID).Select(r => r.RefrigerantType1).FirstOrDefault()
                }).ToList();

                if (coefficientsData.Count > 0)
                {
                    addCoefficients.CompressorCoefficients = JsonConvert.SerializeObject(coefficientsData);
                    addCoefficients.isCoefficientsDataExists = true;
                }

                addCoefficients.ReturnGasTemperatures = CompressorDBContext.ReturnGas.Where(x => x.Active == true).ToList();
                addCoefficients.RefrigerantTypes = CompressorDBContext.RefrigerantTypes.Where(x => x.Active == true).ToList();
            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }
            return addCoefficients;
        }

        public bool AllowEditing(CompressorModel compressorModelInfo, string userType)
        {
            bool IsAllow = false;
            try
            {
                if (compressorModelInfo == null)
                {
                    Logger.PrintInfo("CompressorModelInfo is null in AllowEditing function");
                }
                var loggedInUserName = User.Identity.Name.Split('\\')[1];
                if (userType != Convert.ToString(enUserType.Reader) || (userType == Convert.ToString(enUserType.DataFeeder) && compressorModelInfo.CreatedBy == loggedInUserName))
                {
                    IsAllow = true;
                }
                else
                {
                    IsAllow = false;
                }
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return IsAllow;

        }

        protected AddCompressorDetailsViewModel SetDataForDetailsTab(AddCompressorDetailsViewModel addCompressorDetails, CompressorModel CompressorModel)
        {
            try
            {
                addCompressorDetails.ModelNumber = CompressorModel.ModelNumber.Trim();
                addCompressorDetails.NomHorsepower = CompressorModel.NomHorsepower.Trim();
                addCompressorDetails.RealHorsePower = CompressorModel.RealHorsePower;
                addCompressorDetails.CompSeries_ID = CompressorModel.CompSeries_ID;
                addCompressorDetails.CapReduct_ID = CompressorModel.CapReduct_ID;
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return addCompressorDetails;
        }

        private void SaveCompressorDetailsTabData(AddCompressorViewModel addCompressor)
        {
            try
            {
                if (addCompressor.CompressorDetailsViewModel.CapReduct_ID == 0)
                    addCompressor.CompressorDetailsViewModel.CapReduct_ID = null;

                CompressorDBContext.spUpdateCompressorDetailsTabData(
                                                        addCompressor.CurrentCompressorModelID,
                                                        addCompressor.CompressorDetailsViewModel.ModelNumber.Trim(),
                                                        addCompressor.CompressorDetailsViewModel.NomHorsepower.Trim(),
                                                        addCompressor.CompressorDetailsViewModel.RealHorsePower,
                                                        addCompressor.CompressorDetailsViewModel.CompSeries_ID,
                                                        addCompressor.CompressorDetailsViewModel.CapReduct_ID,
                                                        userName
                                                       );

            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);

            }
        }

        public ActionResult ChangeCompressorState(int modelId, int? state)
        {
            try
            {
                CompressorDBContext.spChangeCompressorState(modelId, state, User.Identity.Name.Split('\\')[1]);
            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }

            if (state == null)
            {
                return RedirectToAction("Search", "Compressor");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public bool ChangeCompressorStateFromSearch(int modelId, int? state) //Call From search screen and Tab screen
        {
            CompressorDBContext.spChangeCompressorState(modelId, state, User.Identity.Name.Split('\\')[1]);

            if (state == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private AddCompressorViewModel SetControlVisibility(AddCompressorViewModel addCompressor, HttpContext context)
        {
            Enum.TryParse(addCompressor.CurrentStateID.ToString(), out enState currEnum);
           
            if (Hussmann.Attribute.Authorization.GetUserType(context) != Convert.ToString(enUserType.Reader))
            {
                if (currEnum == enState.Draft)
                {
                    if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord))
                    {
                        addCompressor.CanApproveRecord = true;
                    }
                    else
                    {
                        addCompressor.CanSubmitRecord = true;
                    }
                }
                else if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord) && currEnum == enState.Submitted)
                {
                    addCompressor.CanApproveRecord = true;
                    addCompressor.CanRejectRecord = true;
                    addCompressor.CanDeleteRecord = true;
                }
                else if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord) && currEnum == enState.Rejected)
                {
                    addCompressor.CanDeleteRecord = true;
                    addCompressor.CanMoveToDraft = true;
                }
                else if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord) && currEnum == enState.Approved)
                {
                    addCompressor.CanDeleteRecord = true;
                    addCompressor.CanMoveToDraft = true;
                }
                else if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord) && currEnum == enState.Deleted)
                {
                    addCompressor.CanApproveRecord = true;
                    addCompressor.CanMoveToDraft = true;
                }
            }
            return addCompressor;
        }

        public void DownloadCompressorExcelFile(int modelID)
        {
            try
            {
                var compressorModel = CompressorDBContext.CompressorModels.Where(x => x.CompModel_ID == modelID)
                    .Select(x => x).FirstOrDefault();

                CompressorExcelExport exportCompressorData = new CompressorExcelExport();
                IWorkbook workBook = exportCompressorData.GenerateExcelFile(modelID);

                using (var exportData = new MemoryStream())
                {
                    Response.Clear();
                    workBook.Write(exportData);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", compressorModel.ModelNumber + "_v" + compressorModel.Version + ".xls"));
                    Response.BinaryWrite(exportData.GetBuffer());
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }

        }

        //GET: Add/Edit RefrigerantType
        public ActionResult ManageRefrigerantType(int? refrigerantTypeID)
        {
            try
            {
                if (refrigerantTypeID != null)
                {
                    var dbRefrigerantType = CompressorDBContext.RefrigerantTypes.Where(x => x.RefrigerantType_ID == refrigerantTypeID).FirstOrDefault();
                    RefrigerantTypeViewModel refrigerantType = new RefrigerantTypeViewModel()
                    {
                        RefrigerantType_ID = dbRefrigerantType.RefrigerantType_ID,
                        RefrigerantType1 = dbRefrigerantType.RefrigerantType1,
                        RefrigerantDescr = dbRefrigerantType.RefrigerantDescr,
                        Active = dbRefrigerantType.Active
          
                    };

                    return PartialView("AddRefrigerantType", refrigerantType);
                }

                List<RefrigerantTypeViewModel> refrigerantTypes = new List<RefrigerantTypeViewModel>();
                var dbRefrigerantTypes = CompressorDBContext.RefrigerantTypes.Select(x => new { x.RefrigerantType_ID, x.RefrigerantType1, x.RefrigerantDescr, x.Active }).ToList();
                foreach (var item in dbRefrigerantTypes)
                {
                    RefrigerantTypeViewModel refrigerantType = new RefrigerantTypeViewModel()
                    {
                        RefrigerantType_ID = item.RefrigerantType_ID,
                        RefrigerantType1 = item.RefrigerantType1,
                        RefrigerantDescr = item.RefrigerantDescr,
                        Active = item.Active
                   
                    };
                    refrigerantTypes.Add(refrigerantType);
                }
                return View(refrigerantTypes);
            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }

            return View();
        }

        public ActionResult GetHertz()
        {
            try
            {
                var dbHertz = CompressorDBContext.Hertzs.ToList();
                List<string> listHertz = new List<string>();
                foreach (var item in dbHertz)
                {
                    listHertz.Add(item.Hertz_Type + "/" + item.Phase);
                }
                listHertz.Sort();
                return Json(listHertz);
            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }
            return null;
        }

    }
}
