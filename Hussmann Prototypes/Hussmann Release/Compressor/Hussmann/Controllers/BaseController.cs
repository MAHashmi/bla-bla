﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hussmann.Attribute;
using Hussmann.Common.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Hussmann.Controllers
{
    public class BaseController : Controller
    {

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //Get the user rules in json
            try
            {
                string file = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["MenuPath"]);
                string Server = ConfigurationManager.AppSettings["Server"];
                file = file.Replace("{0}", Server);

                string groupName = Authorization.GetUserGroup(filterContext.HttpContext.ApplicationInstance.Context);
                //deserialize JSON from file  
                string userRuleJson = System.IO.File.ReadAllText(file);
                var rules = JObject.Parse(userRuleJson).SelectToken(groupName).ToObject<object>();
                string json = JsonConvert.SerializeObject(rules);
                ViewBag.menu = json;
                ViewBag.serverName = ConfigurationManager.AppSettings["Server"];

                base.OnActionExecuting(filterContext);
            }
            catch(Exception exception)
            {
                Logger.PrintError(exception);
            }
        }
    
    }
}