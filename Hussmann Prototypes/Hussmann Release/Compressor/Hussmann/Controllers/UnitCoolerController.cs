﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UnitCoolerDataModel;
using Hussmann.Models.UnitCooler;
using Hussmann.Models.UnitCooler.Add;
using Hussmann.Common.Helper;
using Hussmann.Common.Enumeration;
using Hussmann.Attribute;
using Newtonsoft.Json;
using System.Web.UI;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.IO;

namespace Hussmann.Controllers
{
    public class UnitCoolerController : BaseController
    {
        private RulestreamCoilDBEntities UnitCoolerDBContext = new RulestreamCoilDBEntities();
        public int CurrentCoilModelID;
        public bool IsDraftMode;
        public static string userName;
        public static string mode;
        public string userType;
        public int? TotalRecords;

        public string ModelNumber;
        public int? BrandID;
        public int? FamilyId;
        public int? StatusId;
        public int currentpage;
        public int previouspage;


        // GET: UnitCooler
        [AuthorizeAD(Role.CanSearchContent)]
        [HttpGet]
        public ActionResult Search(string modelNumber, int? brandId, int? familyId, int? statusId, bool? pinned = true )
        {

            Logger.PrintInfo("Action:Search ~~~~~~ Search Screen");
            userType = Authorization.GetUserType(HttpContext.ApplicationInstance.Context);
            userName = User.Identity.Name.Split('\\')[1];
            ViewBag.ModelNumberParm = modelNumber;
            ViewBag.BrandID = brandId;
            ViewBag.FamilyId = familyId;
            ViewBag.StatusId = statusId;
            ViewBag.PinnedTab = pinned;

            var CoilBrand =
            UnitCoolerDBContext.CoilBrand_lkp
            .Select(s => new
            {
                BrandID = s.BrandID,
                CoilBrandName = s.CoilBrandName + " " + s.CoilSubBrandName
            })
            .ToList();
            ViewBag.BrandList = new SelectList(CoilBrand, "BrandID", "CoilBrandName", brandId);
            ViewBag.FamilyList = new SelectList(UnitCoolerDBContext.CoilFamilies, "CoilFamilyID", "CoilFamilyName", familyId);
            ViewBag.StatusList = new SelectList(UnitCoolerDBContext.States, "StateID", "Description", statusId);
            if (userType == Convert.ToString(enUserType.Reader))
            {
                ViewBag.EditOrView = "View";
                ViewBag.IconImage = "fa fa-eye";
            }
            else
            {
                ViewBag.EditOrView = "Edit";
                ViewBag.IconImage = "fa fa-pencil";
            }
            ViewBag.userType = userType;
            ViewBag.userName = userName;
            ViewBag.page_Count = 0;
            //Initialize Total No of Records
            TotalRecords = UnitCoolerDBContext.spGetNoOFRows(modelNumber, brandId, familyId, statusId, userName, userType).SingleOrDefault();

            // var serachResult = UnitCoolerDBContext.spSearchUnitCooler(modelNumber, brandId, familyId, statusId, userName, userType).ToList();
            return View();
        }

        public ActionResult GetData(string modelNumber, int? BrandID, int? FamilyId, int? StatusId, string Page)
        {
            JsonResult result = new JsonResult();

            string search = Request.Form.GetValues("search[value]")[0];
            string draw = Request.Form.GetValues("draw")[0];
            string order = Request.Form.GetValues("order[0][column]")[0];
            string orderDir = Request.Form.GetValues("order[0][dir]")[0];
            int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
            int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);

            //Initialize User Name And UserType 
            userType = Authorization.GetUserType(HttpContext.ApplicationInstance.Context);
            userName = User.Identity.Name.Split('\\')[1];


            if (modelNumber == "null" || modelNumber == "Null")
            {
                modelNumber = null;
            }

            if (orderDir == "desc")
            {
                orderDir = "Descending";
            }
            else if (orderDir == "asc")
            {
                orderDir = "Ascending";
            }
            // Total record count.   
            int? totalRecords = UnitCoolerDBContext.spGetNoOFRows(modelNumber, BrandID, FamilyId, StatusId, userName, userType).SingleOrDefault(); ;
            int? recFilter = totalRecords;
            List<spSearchUnitCooler_Result> searchResult = new List<spSearchUnitCooler_Result>();

            //Search 
            if (!string.IsNullOrEmpty(search) &&
                   !string.IsNullOrWhiteSpace(search))
            {
                // Apply search
                string sortExpression = GetSortExpression(order);
                searchResult = UnitCoolerDBContext.spSearchUnitCooler(modelNumber, BrandID, FamilyId, StatusId, userName, userType, 0, totalRecords, sortExpression, orderDir).ToList();
                searchResult = searchResult.Where(p => p.ModelNumber.ToString().ToLower().Contains(search.ToLower()) ||
                    p.CoilBrandName.ToLower().Contains(search.ToLower()) ||
                    p.CoilFamilyName.ToLower().Contains(search.ToLower()) ||
                    p.NumberofFans.ToString().ToLower().Contains(search.ToLower()) ||
                    p.StateDescription.ToLower().Contains(search.ToLower()) ||
                    p.Version.ToString().ToLower().Contains(search.ToLower()) ||
                    p.LastModifiedOn.ToString().ToLower().Contains(search.ToLower()) ||
                    p.ActionBy.ToLower().Contains(search.ToLower()) ||
                    p.ActiveInactiveStatus.ToLower().Contains(search.ToLower())
                    ).ToList();
                recFilter = searchResult.Count;
                searchResult = searchResult.Skip(startRec).Take(pageSize).ToList();
            }
            else
            {
                string sortExpression = GetSortExpression(order);
                searchResult = UnitCoolerDBContext.spSearchUnitCooler(modelNumber, BrandID, FamilyId, StatusId, userName, userType, startRec + 1, startRec + pageSize, sortExpression, orderDir).ToList();

            }

            //Initialize Json
            result = this.Json(new
            {
                draw = Convert.ToInt32(draw),
                recordsTotal = totalRecords,
                recordsFiltered = recFilter,
                data = searchResult
            }, JsonRequestBehavior.AllowGet);
            ViewBag.Page = 1;
            return result;
        }
        //Sorting function
        private string GetSortExpression(string order)
        {
            try
            {
                switch (order)
                {
                    case "0":
                        // Setting.   
                        return Convert.ToString(enSortExpression.ModelNumber);
                    case "1":
                        // Setting.  
                        return Convert.ToString(enSortExpression.CoilBrandName);
                    case "2":
                        // Setting. 
                        return Convert.ToString(enSortExpression.CoilFamilyName);
                    case "3":
                        // Setting.   
                        return Convert.ToString(enSortExpression.NumberofFans);
                    case "4":
                        // Setting.  
                        return Convert.ToString(enSortExpression.StateDescription);
                    case "5":
                        // Setting. 
                        return Convert.ToString(enSortExpression.Version);
                    case "6":
                        // Setting. 
                        return Convert.ToString(enSortExpression.LastModifiedOn);
                    case "7":
                        // Setting. 
                        return Convert.ToString(enSortExpression.ActionBy);
                    case "8":
                        // Setting. 
                        return Convert.ToString(enSortExpression.ActiveInactiveStatus);

                    default:
                        // Setting.  
                        return Convert.ToString(enSortExpression.LastModifiedOn);
                }
            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }
            return Convert.ToString(enSortExpression.LastModifiedOn);
        }

        // GET: UnitCooler
        [AuthorizeAD(Role.CanAddRecord)]
        public ActionResult AddUnitCooler()
        {
            Logger.PrintInfo("Action:AddUnitCooler ~~~~~~ Add Unit Cooler");
            int StateID = Convert.ToInt32((int)enState.Draft);
            userName = User.Identity.Name.Split('\\')[1];
            mode = Convert.ToString(enMode.Add);
            AddUnitCoolerViewModel addUnitCooler = new AddUnitCoolerViewModel();
            try
            {

                // For Unit cooler Details Tab
                AddUnitCoolerDetailsViewModel addUnitCoolerDetails = new AddUnitCoolerDetailsViewModel();


                addUnitCoolerDetails = SetDefaultListsDataForDetailsTab(addUnitCoolerDetails);

                addUnitCooler.UnitCoolerDetailsData = addUnitCoolerDetails;


            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }
            return View(addUnitCooler);

        }

        [HttpPost]
        [AuthorizeAD(Role.CanAddRecord)]
        public ActionResult AddUnitCooler(AddUnitCoolerViewModel addUnitCoolerDetails)
        {
            Logger.PrintInfo("Action:AddUnitCooler Post ~~~~~~ Add Unit Cooler");
            addUnitCoolerDetails.UnitCoolerDetailsData = SetDefaultListsDataForDetailsTab(addUnitCoolerDetails.UnitCoolerDetailsData);

            if (ModelState.IsValid)
            {
                addUnitCoolerDetails.CurrentCoilModelID = InsertCoilDataForDetails(addUnitCoolerDetails);
                IsDraftMode = true;

                return RedirectToAction("EditUnitCooler", "UnitCooler", new { ModelID = addUnitCoolerDetails.CurrentCoilModelID, RedirectTo = Convert.ToString(enAction.Index) });
            }
            return View(addUnitCoolerDetails);
        }

        private int InsertCoilDataForDetails(AddUnitCoolerViewModel addUnitCoolerDetails)
        {
            var modelID = 0;
            try
            {
                CoilModel coilModel = new CoilModel();

                userName = User.Identity.Name.Split('\\')[1];

                modelID = UnitCoolerDBContext.spCreateCoilModel(Convert.ToInt32(addUnitCoolerDetails.UnitCoolerDetailsData.BrandID),
                                                       Convert.ToInt32(addUnitCoolerDetails.UnitCoolerDetailsData.FamilyID),
                                                       addUnitCoolerDetails.UnitCoolerDetailsData.ModelNumber,
                                                       addUnitCoolerDetails.UnitCoolerDetailsData.CapacityPerDegreeTDNeg20,
                                                       addUnitCoolerDetails.UnitCoolerDetailsData.CapacityPerDegreeTDPos25,
                                                       userName,
                                                       ExtensionMethods.GetYearFromYearID(Convert.ToInt32(addUnitCoolerDetails.UnitCoolerDetailsData.YearID)),
                                                       ExtensionMethods.GetYearFromYearID(Convert.ToInt32(addUnitCoolerDetails.UnitCoolerDetailsData.MakeID)),
                                                       addUnitCoolerDetails.UnitCoolerDetailsData.NumberOfFans
                                                       ).FirstOrDefault().Value;
                
            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
             
            }
            return modelID;
        }
        [AuthorizeAD()]
        //[AuthorizeAD]
        public ActionResult EditUnitCooler(int ModelID, string RedirectTo , string queryStringParameters)
        {
            Logger.PrintInfo("Action:EditUnitCooler~~~~~~ Edit Unit Cooler");
            userType = Authorization.GetUserType(HttpContext.ApplicationInstance.Context);
            mode = Convert.ToString(enMode.Edit);
            userName = User.Identity.Name.Split('\\')[1];
            int StateID = Convert.ToInt32((int)enState.Draft);

            var CoilModel = UnitCoolerDBContext.
            CoilModels.Where
            (x => x.ModelID == ModelID).Select(x => x).FirstOrDefault();

    

            AddUnitCoolerViewModel addUnitCooler = new AddUnitCoolerViewModel();
            // For Unit cooler Details Tab
            AddUnitCoolerDetailsViewModel addUnitCoolerDetails = new AddUnitCoolerDetailsViewModel();

            addUnitCoolerDetails = SetDefaultListsDataForDetailsTab(addUnitCoolerDetails); // Populate Default List Data

            addUnitCoolerDetails = SetDataForDetailsTab(addUnitCoolerDetails, CoilModel); // Populate Saved Data


            addUnitCooler.UnitCoolerDetailsData = addUnitCoolerDetails;
            addUnitCooler.CurrentTab = Convert.ToInt32((int)enTabs.DetailsTab);
            addUnitCooler.RequestedTab = Convert.ToInt32((int)enTabs.DetailsTab);
            addUnitCooler.CurrentCoilModelID = CoilModel.ModelID;
            addUnitCooler.CurrentStatusID = Convert.ToInt32(CoilModel.StateID);
            addUnitCooler.IsAccessoryGridValid = true;
            addUnitCooler.IsPricingGridValid = true;
            addUnitCooler.IsConfigrationTabDataValid = true;
            addUnitCooler.CurrentCoilState = Enum.GetName(typeof(enState), CoilModel.StateID);
            addUnitCooler.CurrentCoilVersion = CoilModel.Version;
            addUnitCooler.RedirectTo = RedirectTo;
            if(queryStringParameters != null)
            {
                queryStringParameters = queryStringParameters.Replace("_", "&");
                addUnitCooler.SearchQuery = queryStringParameters;
            }
            else
            {
                addUnitCooler.SearchQuery = "";
            }

            if (userType == Convert.ToString(enUserType.Reader) || (userType == Convert.ToString(enUserType.DataFeeder) && CoilModel.CreatedBy != userName))
            {
                if(userType == Convert.ToString(enUserType.DataFeeder) && CoilModel.StateID == (int)enState.Approved)
                {
                    addUnitCooler.AllowEdit = true;
                }
                else
                {
                    addUnitCooler.AllowEdit = false;
                }
            }
            else
            {
                addUnitCooler.AllowEdit = true;// should be true
            }

            addUnitCooler = SetControlVisibility(addUnitCooler, HttpContext.ApplicationInstance.Context);

            return View(addUnitCooler);
        }

        [HttpPost]
        public ActionResult EditUnitCooler(AddUnitCoolerViewModel addUnitCooler)
        {
            Logger.PrintInfo("Action:EditUnitCooler POST~~~~~~ Edit Unit Cooler");
            var CoilModelInfo = GetCoilModel(addUnitCooler);
            if(CoilModelInfo == null)
            {
                Logger.PrintInfo("Alert:CoilModelInfo is null in EditUnitCooler POST Controller");
            }
            userType = Authorization.GetUserType(HttpContext.ApplicationInstance.Context);

            bool allowEdit = AllowEditing(CoilModelInfo, userType);

            if (allowEdit == true)
            {
                switch (addUnitCooler.CurrentTab)
                {
                    case (int)enTabs.DetailsTab:
                        SaveDetailsTabData(addUnitCooler);
                        break;
                    case (int)enTabs.AccessoriesTab:
                        SaveAccessoriesTabData(addUnitCooler);
                        break;
                    case (int)enTabs.ConfigurationsTab:
                        SaveConfigurationTabData(addUnitCooler);
                        break;
                    case (int)enTabs.PricingTab:
                        SavePricingTabData(addUnitCooler);
                        break;
                }
            }


            // Using AsNotracking for getting updated result from database
            var CoilModel = UnitCoolerDBContext.
            CoilModels.AsNoTracking().Where
            (x => x.ModelID == addUnitCooler.CurrentCoilModelID).Select(x => x).FirstOrDefault();

            addUnitCooler.CurrentStatusID = Convert.ToInt32(CoilModel.StateID);
            addUnitCooler.CurrentCoilVersion = CoilModel.Version;
            addUnitCooler.CurrentCoilState = Enum.GetName(typeof(enState), CoilModel.StateID);

            switch (addUnitCooler.RequestedTab)
            {
                case (int)enTabs.DetailsTab:
                    AddUnitCoolerDetailsViewModel addUnitCoolerDetails = new AddUnitCoolerDetailsViewModel();
                    addUnitCoolerDetails = SetDefaultListsDataForDetailsTab(addUnitCoolerDetails);// Populate Default List Data
                    addUnitCoolerDetails = SetDataForDetailsTab(addUnitCoolerDetails, CoilModel); // Populate Saved Data
                    addUnitCooler.UnitCoolerDetailsData = addUnitCoolerDetails;
                    break;
                case (int)enTabs.AccessoriesTab:
                    AddUnitCoolerAccessoriesViewModel addUnitCoolerAccessories = new AddUnitCoolerAccessoriesViewModel();
                    addUnitCoolerAccessories = SetDataForAccessoriesTab(addUnitCoolerAccessories, CoilModel);
                    addUnitCooler.UnitCoolerAccessoriesData = addUnitCoolerAccessories;
                    break;
                case (int)enTabs.ConfigurationsTab:
                    AddUnitCoolerConfigurationViewModel addUnitCoolerConfiguration = new AddUnitCoolerConfigurationViewModel();
                    addUnitCoolerConfiguration = SetDataForConfigurationTab(addUnitCoolerConfiguration, CoilModel);
                    addUnitCooler.UnitCoolerConfigurationData = addUnitCoolerConfiguration;
                    break;
                case (int)enTabs.PricingTab:
                    AddUnitCoolerPricingViewModel addUnitCoolerPricingData = new AddUnitCoolerPricingViewModel();
                    addUnitCoolerPricingData = SetCoilPricingGridData(false, addUnitCoolerPricingData, CoilModel.ModelID, CoilModel.NumberofFans, CoilModel.ModelFamilyID, CoilModel.SelectedFan);
                    addUnitCooler.UnitCoolerPricingData = addUnitCoolerPricingData;
                    break;
            }
            addUnitCooler.CurrentTab = addUnitCooler.RequestedTab;
            addUnitCooler = SetControlVisibility(addUnitCooler, HttpContext.ApplicationInstance.Context);

            return View(addUnitCooler);
        }

        private void SavePricingTabData(AddUnitCoolerViewModel addUnitCooler)
        {
            try
            {
                Logger.PrintInfo("Function:SavePricingTabData ~~~~~~ Save Pricing Tab Data");
                #region Option Pricing Grid
                string data;
                DataTable AccessoryPricesListData;
                if (addUnitCooler.UnitCoolerPricingData.CoilAccessoryList == null)
                {
                    AccessoryPricesListData = null;
                }
                else
                {
                    data = JsonConvert.SerializeObject(addUnitCooler.UnitCoolerPricingData.CoilAccessoryList);
                    AccessoryPricesListData = (DataTable)JsonConvert.DeserializeObject(data, (typeof(DataTable)));
                }


                DataTable coilGridData = (DataTable)JsonConvert.DeserializeObject(addUnitCooler.UnitCoolerPricingData.CoilOptionPricing, (typeof(DataTable)));


                var dataTableCoilGrid = new SqlParameter("param_CoilOptionPricingUDT", SqlDbType.Structured);
                dataTableCoilGrid.Value = coilGridData;
                dataTableCoilGrid.TypeName = "CoilOptionsPricingUDT";

                var dataTableAccsPriceList = new SqlParameter("param_CoilAccsListPriceUDT", SqlDbType.Structured);
                dataTableAccsPriceList.Value = AccessoryPricesListData;
                dataTableAccsPriceList.TypeName = "CoilAccsListPriceUDT";


                var modelID = new SqlParameter("param_ModelId", SqlDbType.Int);
                modelID.Value = addUnitCooler.CurrentCoilModelID;

                var familyID = new SqlParameter("param_FamilyId", SqlDbType.Int);
                familyID.Value = addUnitCooler.UnitCoolerPricingData.FamilyID;

                var selectedFan = new SqlParameter("param_SelectedFan", SqlDbType.Int);
                selectedFan.Value = addUnitCooler.UnitCoolerPricingData.SelectedFan;


                UnitCoolerDBContext.Database.ExecuteSqlCommand("exec dbo.spUpdateCoilOptionPricing @param_ModelId,@param_FamilyId,@param_SelectedFan,@param_CoilOptionPricingUDT,@param_CoilAccsListPriceUDT", modelID, familyID, selectedFan, dataTableCoilGrid, dataTableAccsPriceList);
                #endregion
                
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
        
        }


        private AddUnitCoolerPricingViewModel SetCoilPricingGridData(bool ExcelChecker, AddUnitCoolerPricingViewModel addUnitCoolerPricingData, int ModelID, int? numberOfFans, int FamilyID, int? SelectedFan)
        {
            try
            {
                decimal priceSum;
    
                var CoilOptionPricingData = UnitCoolerDBContext.CoilOptionsPricings.Where(x => x.CoilFamilyID == FamilyID && x.ModelID == ModelID).Select(x => new
                {
                    x.NumberFans,
                    x.PSCMotorDeduct,
                    x.PaintCost,
                    x.InsulDrainPanCost,
                    x.SSNonInsulDrainPanCost,
                    x.SSInsulDrainPanCost,
                    x.SSHousingNonInsulDrainPanCost,
                    x.SSHousingInsulDrainPanCost,
                    x.ElectroFinCost,
                    x.CopperFinCost,
                    x.ReverseAirFlowCost,
                    x.StdLongThrowAdaptersCost,
                    x.SSLongThrowAdaptersCost,
                    x.AirFilterBonnetCost
                }).ToList();

                if (CoilOptionPricingData.Count() == 0)
                {
                    CoilOptionPricingData = UnitCoolerDBContext.CoilOptionsPricings.Where(x => x.CoilFamilyID == FamilyID).Select(x => new
                    {
                        x.NumberFans,
                        x.PSCMotorDeduct,
                        x.PaintCost,
                        x.InsulDrainPanCost,
                        x.SSNonInsulDrainPanCost,
                        x.SSInsulDrainPanCost,
                        x.SSHousingNonInsulDrainPanCost,
                        x.SSHousingInsulDrainPanCost,
                        x.ElectroFinCost,
                        x.CopperFinCost,
                        x.ReverseAirFlowCost,
                        x.StdLongThrowAdaptersCost,
                        x.SSLongThrowAdaptersCost,
                        x.AirFilterBonnetCost
                    }).ToList();
                }
                //Get data only for excel sheet
                if (ExcelChecker == true)
                {
                    List<CoilOptionsPricing> CoilOptionPricingList = new List<CoilOptionsPricing>();

                    foreach (var item in CoilOptionPricingData)
                    {
                        CoilOptionsPricing coilOptionsPricing = new CoilOptionsPricing()
                        {
                            NumberFans = item.NumberFans,
                            PSCMotorDeduct = item.PSCMotorDeduct,
                            PaintCost = item.PaintCost,
                            InsulDrainPanCost = item.InsulDrainPanCost,
                            SSNonInsulDrainPanCost = item.SSNonInsulDrainPanCost,
                            SSInsulDrainPanCost = item.SSInsulDrainPanCost,
                            SSHousingNonInsulDrainPanCost = item.SSHousingNonInsulDrainPanCost,
                            SSHousingInsulDrainPanCost = item.SSHousingInsulDrainPanCost,
                            ElectroFinCost = item.ElectroFinCost,
                            CopperFinCost = item.CopperFinCost,
                            ReverseAirFlowCost = item.ReverseAirFlowCost,
                            StdLongThrowAdaptersCost = item.StdLongThrowAdaptersCost,
                            SSLongThrowAdaptersCost = item.SSLongThrowAdaptersCost,
                            AirFilterBonnetCost = item.AirFilterBonnetCost
                        };
                        CoilOptionPricingList.Add(coilOptionsPricing);
                    }
                    addUnitCoolerPricingData.coilOptionsPricing = CoilOptionPricingList;
                }

                addUnitCoolerPricingData.CoilOptionPricing = JsonConvert.SerializeObject(CoilOptionPricingData);
                addUnitCoolerPricingData.PricingDataExsist = true;
                addUnitCoolerPricingData.NumberOfFans = Convert.ToInt32(numberOfFans);
                addUnitCoolerPricingData.FamilyID = FamilyID;

                List<AccessoriesPriceDetails> CoilAccessoryPricingList = new List<AccessoriesPriceDetails>();

                CoilAccessoryPricingList = UnitCoolerDBContext.CoilAccessoryPricings.
                    Where(x => x.ModelID == ModelID && x.CoilFamilyID == FamilyID).
                    Select(x => new AccessoriesPriceDetails
                    {
                        CoilAccessoryPricingID = x.CoilAccessoryPricingID,
                        CoilAccsListPrice = x.CoilAccsListPrice,
                        CoilFamilyID = x.CoilFamilyID,
                        ModelID = x.ModelID,
                        CoilAccsPartDescription = (UnitCoolerDBContext.CoilAccessoryID_lkp.Where(y => y.CoilAccsID == x.CoilAccsID).Select(z => z.CoilAccessoryName)).FirstOrDefault()
                    }).ToList();

                priceSum = Convert.ToDecimal(CoilAccessoryPricingList.Sum(x => x.CoilAccsListPrice));
                addUnitCoolerPricingData.CoilAccessoryList = CoilAccessoryPricingList;
                addUnitCoolerPricingData.AccessoryPrice = priceSum;
                addUnitCoolerPricingData.SelectedFan = Convert.ToInt32((SelectedFan == null ? 1 : SelectedFan));

            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return addUnitCoolerPricingData;
        }

        // Update Details Tab Data on Tab change or save button click
        private void SaveDetailsTabData(AddUnitCoolerViewModel addUnitCooler)
        {
            try
            {
                Logger.PrintInfo("Function:SaveDetailsTabData ~~~~~~ Save Details Tab Data");
                var actionBy = User.Identity.Name.Split('\\')[1];
                if (actionBy == null)
                {
                    Logger.PrintInfo("ActionBy null in details tab data saving");
                }

                // Sp call for updating Details Tab Data
                UnitCoolerDBContext.spUpdateDetailsTabData(addUnitCooler.CurrentCoilModelID,
                    Convert.ToInt32(addUnitCooler.UnitCoolerDetailsData.BrandID),
                    Convert.ToInt32(addUnitCooler.UnitCoolerDetailsData.FamilyID),
                    addUnitCooler.UnitCoolerDetailsData.ModelNumber,
                    addUnitCooler.UnitCoolerDetailsData.CapacityPerDegreeTDNeg20,
                    addUnitCooler.UnitCoolerDetailsData.CapacityPerDegreeTDPos25,
                    actionBy,
                    ExtensionMethods.GetYearFromYearID(Convert.ToInt32(addUnitCooler.UnitCoolerDetailsData.YearID)),
                    ExtensionMethods.GetYearFromYearID(Convert.ToInt32(addUnitCooler.UnitCoolerDetailsData.MakeID)),
                    addUnitCooler.UnitCoolerDetailsData.NumberOfFans
                    );
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
        }

        private void SaveConfigurationTabData(AddUnitCoolerViewModel addUnitCooler)
        {
            try
            {
                Logger.PrintInfo("Function:SaveConfigurationTabData ~~~~~~ Save Configuration Tab Data");

                //data for Coil Motor Grid
                dynamic coilMotorAmps = (List<object>)JsonConvert.DeserializeObject(addUnitCooler.UnitCoolerConfigurationData.CoilMotorAmpsData, (typeof(List<object>)));
                JavaScriptSerializer ser = new JavaScriptSerializer();
                if (addUnitCooler.UnitCoolerConfigurationData.ColumnHeaderCoilMotorAmps == null)
                {
                    addUnitCooler.UnitCoolerConfigurationData.ColumnHeaderCoilMotorAmps = "";
                }

                    var coilMotor = ser.Deserialize<List<MotorAmpsHeader>>(addUnitCooler.UnitCoolerConfigurationData.ColumnHeaderCoilMotorAmps);
                

                List<MotorAmps> motorAmps = new List<MotorAmps>();
                //Form the data for MotorAmps table
                if (coilMotor != null)
                {
                    int counter = 0; //used for Motor Type ID 
                    int VolyageTypeID = 0;
                    foreach (var voltage in coilMotorAmps)
                    {
                        foreach (var amps in ((Newtonsoft.Json.Linq.JContainer)voltage))
                        {
                            if (counter == 0)  //First row will be a voltage ID
                            {
                                VolyageTypeID = Convert.ToInt32(((Newtonsoft.Json.Linq.JValue)amps).Value);
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(((Newtonsoft.Json.Linq.JValue)amps).Value)) && counter != 0)
                            {
                                motorAmps.Add(new MotorAmps
                                {
                                    MotorType = Convert.ToInt32(coilMotor[counter].data),
                                    VolyageTypeID = VolyageTypeID,
                                    MotorAmpsValue = Convert.ToDecimal(((Newtonsoft.Json.Linq.JValue)amps).Value)
                                });
                            }
                            counter++;
                        }
                        counter = 0;
                    }
                }

                DataTable dtMotorAmps = motorAmps.ToDataTable();

                DataTable dtDefrostConfig = new DataTable();
                if (addUnitCooler.UnitCoolerConfigurationData.DefrostConfig != null || addUnitCooler.UnitCoolerConfigurationData.DefrostConfig.Count > 0)
                {
                    dtDefrostConfig = addUnitCooler.UnitCoolerConfigurationData.DefrostConfig.Where(x => x.IsChecked == true)
                        .Select(x => new { DefrostConfigID = x.DefrostConfigID }).ToList()
                       .ToDataTable();
                }

                DataTable dtDrainHeaterAmps;
                if (addUnitCooler.UnitCoolerConfigurationData.CoilDrainHeaterAmpsData == null || addUnitCooler.UnitCoolerConfigurationData.CoilDrainHeaterAmpsData == "[]")
                {
                    dtDrainHeaterAmps = null;
                }
                else
                {
                    dtDrainHeaterAmps = (DataTable)JsonConvert.DeserializeObject(addUnitCooler.UnitCoolerConfigurationData.CoilDrainHeaterAmpsData, (typeof(DataTable)));
                }                
                DataTable dtDefrostAmps;
                if (addUnitCooler.UnitCoolerConfigurationData.CoilDefrostAmpsData == null || addUnitCooler.UnitCoolerConfigurationData.CoilDefrostAmpsData == "[]")
                {
                    dtDefrostAmps = null;
                }
                else
                {
                    dtDefrostAmps = (DataTable)JsonConvert.DeserializeObject(addUnitCooler.UnitCoolerConfigurationData.CoilDefrostAmpsData, (typeof(DataTable)));
                }

                var drainHtrAmpsPrm = new SqlParameter("param_CoilDrainHtrAmpsUDT", SqlDbType.Structured);
                drainHtrAmpsPrm.Value = dtDrainHeaterAmps;
                drainHtrAmpsPrm.TypeName = "CoilDrainHtrAmpsUDT";

                var defrostAmpsPrm = new SqlParameter("param_CoilDefrostAmpsUDT", SqlDbType.Structured);
                defrostAmpsPrm.Value = dtDefrostAmps;
                defrostAmpsPrm.TypeName = "CoilDefrostAmpsUDT";

                var motorAmpsPrm = new SqlParameter("param_CoilMotorAmpsUDT", SqlDbType.Structured);
                motorAmpsPrm.Value = dtMotorAmps;
                motorAmpsPrm.TypeName = "CoilMotorAmpsUDT";

                var defrostConfigurationPrm = new SqlParameter("param_DefrostConfigurationsUDT", SqlDbType.Structured);
                defrostConfigurationPrm.Value = dtDefrostConfig;
                defrostConfigurationPrm.TypeName = "DefrostConfigurationsUDT";

                var modelId = new SqlParameter("parm_ModelId", SqlDbType.Int);
                modelId.Value = addUnitCooler.CurrentCoilModelID;

                UnitCoolerDBContext.Database.ExecuteSqlCommand("exec dbo.spInserorUpdateConfiguration @parm_ModelId,@param_CoilDrainHtrAmpsUDT,@param_CoilDefrostAmpsUDT,@param_CoilMotorAmpsUDT,@param_DefrostConfigurationsUDT"
                    , modelId, drainHtrAmpsPrm, defrostAmpsPrm, motorAmpsPrm, defrostConfigurationPrm);


            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
        }

        private void SaveAccessoriesTabData(AddUnitCoolerViewModel addUnitCooler)
        {
            try
            {
                Logger.PrintInfo("Function:SaveAccessoriesTabData ~~~~~~ Save Accessories Tab Data");
                DataTable dt = (DataTable)JsonConvert.DeserializeObject(addUnitCooler.UnitCoolerAccessoriesData.CoilAccessoryData, (typeof(DataTable)));

                var coilAccessory = new SqlParameter("param_CoilAccessoryPricingUDT", SqlDbType.Structured);
                coilAccessory.Value = dt;
                coilAccessory.TypeName = "CoilAccessoryPricingUDT";

                var modelId = new SqlParameter("parm_ModelId", SqlDbType.Int);
                modelId.Value = addUnitCooler.CurrentCoilModelID;

                UnitCoolerDBContext.Database.ExecuteSqlCommand("exec dbo.spInserorUpdateAccessory @parm_ModelId,@param_CoilAccessoryPricingUDT", modelId, coilAccessory);
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
        }

        private AddUnitCoolerDetailsViewModel SetDefaultListsDataForDetailsTab(AddUnitCoolerDetailsViewModel addUnitCoolerDetails)
        {
            try
            {
                addUnitCoolerDetails.FamilyDetails = UnitCoolerDBContext.CoilFamilies.ToList();
                addUnitCoolerDetails.BrandDetails = UnitCoolerDBContext.CoilBrand_lkp.ToList().Select(x => new CoilBrand_lkp { BrandID = x.BrandID, CoilBrandName = x.CoilBrandName + ' ' + x.CoilSubBrandName }).ToList();
                List<YearModel> YearList = ExtensionMethods.GetListOfYear();
                addUnitCoolerDetails.YearDetails = YearList;
                addUnitCoolerDetails.MakeDetails = YearList;
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return addUnitCoolerDetails;
        }

        private AddUnitCoolerAccessoriesViewModel SetDataForAccessoriesTab(AddUnitCoolerAccessoriesViewModel addUnitCoolerAccessories, CoilModel coildModel)
        {
            try
            {
                //Load Data Based on Model ID otherwise Load data based on the family selected from details tab
                var coilAccessoryPricing = UnitCoolerDBContext.CoilAccessoryPricings.AsNoTracking().Where(x => x.ModelID == coildModel.ModelID).ToList();
                string accessoryData = string.Empty;
                if (coilAccessoryPricing.Count > 0)
                {

                    accessoryData = JsonConvert.SerializeObject(UnitCoolerDBContext.spGetCoilAccessory(coildModel.ModelFamilyID, coildModel.ModelID).Select(x => new { ModelId = x.ModelID, CoilAccsID = x.CoilAccsID, CoilAccsPartNumber = x.CoilAccsPartNumber, CoilVendorID = x.CoilVendorID, CoilAccsMinBTU = x.CoilAccsMinBTU, CoilAccsMaxBTU = x.CoilAccsMaxBTU, CoilAccsPartDescription = x.CoilAccsPartDescription, CoilAccessoryPricingID = x.CoilAccessoryPricingID, CoilFamilyID = x.CoilFamilyID }).ToList());
                }
                else
                {
                    accessoryData = JsonConvert.SerializeObject(UnitCoolerDBContext.spGetCoilAccessory(coildModel.ModelFamilyID, null).Select(x => new { ModelId = x.ModelID, CoilAccsID = x.CoilAccsID, CoilAccsPartNumber = x.CoilAccsPartNumber, CoilVendorID = x.CoilVendorID, CoilAccsMinBTU = x.CoilAccsMinBTU, CoilAccsMaxBTU = x.CoilAccsMaxBTU, CoilAccsPartDescription = x.CoilAccsPartDescription, CoilAccessoryPricingID = x.CoilAccessoryPricingID, CoilFamilyID = x.CoilFamilyID }).ToList());
                }
                addUnitCoolerAccessories.CoilAccessoryData = accessoryData;
                addUnitCoolerAccessories.CoilVendor = JsonConvert.SerializeObject(UnitCoolerDBContext.CoilVendors.Select(x => new { id = x.CoilVendorID, label = x.CoilVendorName }).ToList());
                addUnitCoolerAccessories.CoilAccessory = JsonConvert.SerializeObject(UnitCoolerDBContext.CoilAccessoryID_lkp.Select(x => new { id = x.CoilAccsID, label = x.CoilAccessoryName }).ToList());

            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return addUnitCoolerAccessories;
        }


        private AddUnitCoolerDetailsViewModel SetDataForDetailsTab(AddUnitCoolerDetailsViewModel addUnitCoolerDetails, CoilModel CoilModel)
        {
            try
            {
                addUnitCoolerDetails.BrandID = Convert.ToString(CoilModel.MFGID);
                addUnitCoolerDetails.FamilyID = Convert.ToString(CoilModel.ModelFamilyID);
                addUnitCoolerDetails.YearID = Convert.ToString(ExtensionMethods.GetYearID(CoilModel.CoilModelYear));
                addUnitCoolerDetails.MakeID = Convert.ToString(ExtensionMethods.GetYearID(CoilModel.CoilMakeYear));
                addUnitCoolerDetails.ModelNumber = CoilModel.ModelNumber;
                addUnitCoolerDetails.CapacityPerDegreeTDNeg20 = CoilModel.CapacityPerDegreeTDNeg20;
                addUnitCoolerDetails.CapacityPerDegreeTDPos25 = CoilModel.CapacityPerDegreeTDPos25;
                addUnitCoolerDetails.NumberOfFans = Convert.ToInt32(CoilModel.NumberofFans);

            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return addUnitCoolerDetails;
        }


        private AddUnitCoolerViewModel SetControlVisibility(AddUnitCoolerViewModel addUnitCoolerModel, HttpContext context)
        {

            if (Hussmann.Attribute.Authorization.GetUserType(context) != Convert.ToString(enUserType.Reader))
            {
                if (addUnitCoolerModel.CurrentStatusID == Convert.ToInt32(enState.Draft))
                {
                    if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord) &&
                        Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanRejectedRecord))
                    {
                        addUnitCoolerModel.CanApproveRecord = true;
                        addUnitCoolerModel.CanRejectRecord = true;
                    }
                    else if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord))
                    {
                        addUnitCoolerModel.CanApproveRecord = true;
                    }
                    else
                    {
                        addUnitCoolerModel.CanSubmitRecord = true;
                    }
                }
                else if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord) && addUnitCoolerModel.CurrentStatusID ==
                Convert.ToInt32(enState.Submitted))
                {
                    addUnitCoolerModel.CanApproveRecord = true;
                    addUnitCoolerModel.CanRejectRecord = true;
                    addUnitCoolerModel.CanDeleteRecord = true;
                }
                else if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord) && addUnitCoolerModel.CurrentStatusID ==
                Convert.ToInt32(enState.Rejected))
                {
                    addUnitCoolerModel.CanDeleteRecord = true;
                    addUnitCoolerModel.CanMoveToDraft = true;
                }
                else if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord) && addUnitCoolerModel.CurrentStatusID ==
                Convert.ToInt32(enState.Approved))
                {
                    addUnitCoolerModel.CanDeleteRecord = true;
                    addUnitCoolerModel.CanMoveToDraft = true;
                }
                else if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord) && addUnitCoolerModel.CurrentStatusID ==
                Convert.ToInt32(enState.Deleted))
                {
                    addUnitCoolerModel.CanApproveRecord = true;
                    addUnitCoolerModel.CanMoveToDraft = true;
                }
            }
            return addUnitCoolerModel;
        }

        public JsonResult IsModelExists([Bind(Prefix = "UnitCoolerDetailsData.ModelNumber")]string ModelNumber)
        {
            //check if any of the ModelNumber matches the ModelNumber  
            if (mode == Convert.ToString(enMode.Add))
            {
                return Json(!UnitCoolerDBContext.CoilModels.Any(x => x.ModelNumber == ModelNumber.Trim()), JsonRequestBehavior.AllowGet);
            }
            else if (mode == Convert.ToString(enMode.Edit))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult ChangeSampleStatus(int modelId, int? state) //Call From search screen and Tab screen
        {
            UnitCoolerDBContext.spChangeUnitCoolerStatus(modelId, state, User.Identity.Name.Split('\\')[1]);

            if (state == null)
            {
                return RedirectToAction("Search", "UnitCooler");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public CoilModel GetCoilModel(AddUnitCoolerViewModel addUnitCooler)
        {
            Logger.PrintInfo("Function:GetCoilModel ~~~~~~ Returns coil Model object");
            var CoilModelInfo = UnitCoolerDBContext.
            CoilModels.AsNoTracking().Where
            (x => x.ModelID == addUnitCooler.CurrentCoilModelID).Select(x => x).FirstOrDefault();
            try
            {
                var loggedInUserName = User.Identity.Name.Split('\\')[1];
                userType = Authorization.GetUserType(HttpContext.ApplicationInstance.Context);

                if (CoilModelInfo.StateID == Convert.ToInt32(enState.Approved) && addUnitCooler.IsEditedAfterApprove == true)
                {
                    if (userType != Convert.ToString(enUserType.Reader)) /*(userType == Convert.ToString(enUserType.DataFeeder) && CoilModelInfo.CreatedBy == userName)*/
                    {
                        Logger.PrintInfo("Creating new version of " + CoilModelInfo.ModelNumber + " with MOdelID : " + CoilModelInfo.ModelID);
                        // Create new coil model with same data but new version
                        var modelID = UnitCoolerDBContext.spCreateCoilModelWithNewVersion(CoilModelInfo.ModelID, loggedInUserName
                                                        ).FirstOrDefault().Value;

                        addUnitCooler.CurrentCoilModelID = modelID;
                        Logger.PrintInfo("New Version created of " + CoilModelInfo.ModelNumber + " with MOdelID :" + modelID);
                        addUnitCooler.IsEditedAfterApprove = false;

                        CoilModelInfo =  UnitCoolerDBContext.
                        CoilModels.AsNoTracking().Where
                        (x => x.ModelID == modelID).Select(x => x).FirstOrDefault();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return CoilModelInfo;
        }

        public bool AllowEditing(CoilModel CoilModelInfo, string userType)
        {
            bool IsAllow = false;
            try
            {
                if (CoilModelInfo == null)
                {
                    Logger.PrintInfo("CoilModel is null in AllowEditing function");
                }
                var loggedInUserName = User.Identity.Name.Split('\\')[1];
                if (userType != Convert.ToString(enUserType.Reader) || (userType == Convert.ToString(enUserType.DataFeeder) && CoilModelInfo.CreatedBy == loggedInUserName))
                {
                    IsAllow = true;
                }
                else
                {
                    IsAllow = false;
                }
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return IsAllow;

        }
        //For Downloading Excel File
        public void DownloadExcelFile(int ModelID)
        {
            var CoilModel = UnitCoolerDBContext.
            CoilModels.Where
            (x => x.ModelID == ModelID).Select(x => x).FirstOrDefault();

            //Getting value for Details Tab
            AddUnitCoolerDetailsViewModel addUnitCoolerDetails = new AddUnitCoolerDetailsViewModel();
            addUnitCoolerDetails = SetDataForDetailsTab(new AddUnitCoolerDetailsViewModel(), CoilModel); // Populate Saved Data

            addUnitCoolerDetails.Coilfamily = UnitCoolerDBContext.CoilFamilies.Where(x => x.CoilFamilyID == CoilModel.ModelFamilyID).Select(x => x.CoilFamilyName).SingleOrDefault();
            addUnitCoolerDetails.CoilBrand = UnitCoolerDBContext.CoilBrand_lkp.Where(x => x.BrandID == CoilModel.MFGID).Select(x => x.CoilBrandName).SingleOrDefault();
            addUnitCoolerDetails.ModelYear = CoilModel.CoilModelYear;
            addUnitCoolerDetails.MakeYear = CoilModel.CoilMakeYear;
            addUnitCoolerDetails.Version = CoilModel.Version;
            addUnitCoolerDetails.LastModifiedOn = CoilModel.LastModifiedOn;

            //Getting value for accessories Tab
            var AccessoriesDetails = UnitCoolerDBContext.spGetCoilAccessory(CoilModel.ModelFamilyID, CoilModel.ModelID).Select(x => new { CoilAccessoryName = x.CoilAccessoryName, CoilAccsPartNumber = x.CoilAccsPartNumber, CoilVendorName = x.CoilVendorName, CoilAccsMinBTU = x.CoilAccsMinBTU, CoilAccsMaxBTU = x.CoilAccsMaxBTU, CoilAccsPartDescription = x.CoilAccsPartDescription }).ToList();

            //getting value for Configuration tab
            var ConfigurationHtrTab = UnitCoolerDBContext.spGetCoilDrainHtrAmps(CoilModel.ModelID).ToList();
            var ConfigurationDefTab = UnitCoolerDBContext.spGetCoilDefrostAmps(CoilModel.ModelID).ToList();

            //Getting Pricing tab
            AddUnitCoolerPricingViewModel addUnitCoolerPricingData = new AddUnitCoolerPricingViewModel();
            addUnitCoolerPricingData = SetCoilPricingGridData(true, addUnitCoolerPricingData, CoilModel.ModelID, CoilModel.NumberofFans, CoilModel.ModelFamilyID, CoilModel.SelectedFan);

            //Create Excel File (xls)
            IWorkbook wb = new HSSFWorkbook();

            int row = 1;
            string[] SheetNames = { "Details", "Accessories", "Configurations", "Coil Accessories Price", "Coil Option Pricing" };

            //Sheet for details Tab
            ISheet sheet1 = wb.CreateSheet(SheetNames[0]);
            sheet1 = WriteInSheet(sheet1, addUnitCoolerDetails, 1);

            //Sheet for Accessory Tab 
            sheet1 = wb.CreateSheet(SheetNames[1]);
            foreach (var item in AccessoriesDetails)
            {
                sheet1 = WriteInSheet(sheet1, item, row);
                row++;
            }

            //for Configuration Tab
            //DefrostAmps
            sheet1 = wb.CreateSheet(SheetNames[2] + " Defrost Amps");
            row = 1;
            foreach (var item in ConfigurationDefTab)
            {
                sheet1 = WriteInSheet(sheet1, item, row);
                row++;
            }

            //DrainHtrAmps
            sheet1 = wb.CreateSheet(SheetNames[2] + " Heater Amps");
            row = 1;
            foreach (var item in ConfigurationHtrTab)
            {
                sheet1 = WriteInSheet(sheet1, item, row);
                row++;
            }

            //Sheet for Pricing tab
            sheet1 = wb.CreateSheet(SheetNames[3]);
            row = 1;
            foreach (var item in addUnitCoolerPricingData.CoilAccessoryList)
            {
                sheet1 = WriteInSheet(sheet1, item, row);
                row++;
            }

            //Sheet 2 for Pricing tab
            sheet1 = wb.CreateSheet(SheetNames[4]);
            row = 1;
            foreach (var item in addUnitCoolerPricingData.coilOptionsPricing)
            {
                sheet1 = WriteInSheet(sheet1, item, row);
                row++;
            }

            //Download File From Web
            using (var exportData = new MemoryStream())
            {
                Response.Clear();
                wb.Write(exportData);
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", CoilModel.ModelNumber + " (" + CoilModel.Version + ") " + ".xlsx"));
                Response.BinaryWrite(exportData.GetBuffer());
                Response.End();
            }

        }

        //Function to write in excel sheet
        public static ISheet WriteInSheet(ISheet oSheet, object o, int row)
        {
            IRow Row = null, Row2 = null;
            int Cell_No = 0;

            if (row == 1)
            {
                Row = oSheet.CreateRow(row - 1);
                Row2 = oSheet.CreateRow(row);
            }
            else
            {
                Row2 = oSheet.CreateRow(row);
            }

            foreach (var Prop2 in o.GetType().GetProperties())
            {
                if (!(Prop2.PropertyType.ToString().Contains("List")) && !(Prop2.Name.Contains("ID")))
                {
                    if (!(o.GetType().ToString().Contains("Price") && Prop2.PropertyType.ToString().Contains("Int")))
                    {
                        if (!(Prop2.Name.Equals("CoilModel")))
                        {
                            ICell cell;
                            if (row == 1)
                            {
                                cell = Row.CreateCell(Cell_No);
                                cell.SetCellValue(Prop2.Name);
                                oSheet.AutoSizeColumn(Cell_No);
                            }
                            cell = Row2.CreateCell(Cell_No);
                            if (Prop2.GetValue(o, null) != null)
                            {
                                cell.SetCellValue(Prop2.GetValue(o, null).ToString());
                                oSheet.AutoSizeColumn(Cell_No);
                            }
                            Cell_No++;
                        }
                    }
                }
            }
            return oSheet;
        }


        public ActionResult UnAuthorized()
        {
            return View();
        }
        public ActionResult UnderConstruction()
        {
            return View();
        }

        private AddUnitCoolerConfigurationViewModel SetDataForConfigurationTab(AddUnitCoolerConfigurationViewModel addUnitCoolerConfiguration, CoilModel coildModel)
        {
            try
            {
                List<int> SelectedDefrostConfigs = UnitCoolerDBContext.DefrostConfigurations.Where(x => x.ModelID == coildModel.ModelID).Select(x => x.DefrostConfigID).ToList();
                addUnitCoolerConfiguration.DefrostConfig = UnitCoolerDBContext.DefrostConfig_lkp.Select(x => new DefrostConfigViewModel { DefrostConfigID = x.DefrostConfigID, DefrostConfigCode = x.DefrostConfigCode, DefrostConfigName = x.DefrostConfigName, IsChecked = SelectedDefrostConfigs.Contains(x.DefrostConfigID) }).ToList();
                addUnitCoolerConfiguration.CoilDrainHeaterAmpsData = JsonConvert.SerializeObject(UnitCoolerDBContext.CoilDrainHtrAmps.Where(x => x.ModelID == coildModel.ModelID)
                    .Select(x => new { x.Type_ID, x.ModelID, x.VoltageTypeID, x.DrainHtrAmps }).ToList());
                addUnitCoolerConfiguration.VoltageTypeList = JsonConvert.SerializeObject(UnitCoolerDBContext.VoltageType_lkp
                                        .Select(x => new { id = x.VoltageTypeID, label = x.VoltageName }).ToList());
                addUnitCoolerConfiguration.CoilDefrostAmpsData = JsonConvert.SerializeObject(UnitCoolerDBContext.CoilDefrostAmps.Where(x => x.ModelID == coildModel.ModelID)
                    .Select(x => new { x.Type_ID, x.ModelID, x.VoltageTypeID, x.DefrostAmps }).ToList());

                var coilMotorAmpsCount = UnitCoolerDBContext.CoilMotorAmps.Where(x => x.ModelID == coildModel.ModelID).Count();
                if (coilMotorAmpsCount > 0)
                {
                    //Coil Motor IDs against selected model ID
                    var coilMotorIds = UnitCoolerDBContext.CoilMotorAmps.Where(x => x.ModelID == coildModel.ModelID).Select(x => x.MotorTypeID).Distinct().ToList();

                    //Get Column header for Coil Motor Amps Grid
                    addUnitCoolerConfiguration.ColumnHeaderCoilMotorAmps = JsonConvert.SerializeObject(UnitCoolerDBContext.CoilMotorType_lkp.Where(x => coilMotorIds.Contains(x.Type_ID))
                        .Select(x => new { x.CoilMotorTypeName, x.Type_ID }).ToList());
                    addUnitCoolerConfiguration.CoilMotorAmpsData = JsonConvert.SerializeObject(UnitCoolerDBContext.CollectionFromSql(" exec spGetCoilMotorAmps @param_ModelID",
                                   new Dictionary<string, object> { { "@param_ModelID", coildModel.ModelID } }).ToList());
                }
                else
                {
                    addUnitCoolerConfiguration.CoilMotorAmpsData = JsonConvert.SerializeObject(UnitCoolerDBContext.VoltageType_lkp
                                        .Select(x => new { VoltageTypeID = x.VoltageTypeID }).ToList());
                }

                //Used to Display Motor Type on Motor Amps pop up
                addUnitCoolerConfiguration.MotorType = UnitCoolerDBContext.CoilMotorType_lkp.ToList();
                addUnitCoolerConfiguration.IsDrainHeaterDrainAmpsValid = true;
                addUnitCoolerConfiguration.IsDrainHeaterVoltageTypeValid = true;
                addUnitCoolerConfiguration.IsDefrostAmpsDrainAmpsValid = true;
                addUnitCoolerConfiguration.IsDefrostAmpsVoltageTypeValid = true;
                addUnitCoolerConfiguration.IsMotorAmpsGridValid = true;
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return addUnitCoolerConfiguration;
        }
        public bool ChangeSampleStatusWithSearchState(int modelId, int? state) //Call From search screen and Tab screen
        {
            UnitCoolerDBContext.spChangeUnitCoolerStatus(modelId, state, User.Identity.Name.Split('\\')[1]);

            if (state == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        [HandleError]
        public ActionResult Error()
        {
            return View();
        }
    }
}