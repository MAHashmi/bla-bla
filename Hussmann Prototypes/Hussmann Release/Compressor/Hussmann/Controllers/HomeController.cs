﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hussmann.Attribute;
using Hussmann.Common;
using Hussmann.Common.Helper;
using Hussmann.Models;
using UnitCoolerDataModel;

namespace Hussmann.Controllers
{
    public class HomeController : BaseController
    {
        private RulestreamCoilDBEntities UnitCoolerDBContext = new RulestreamCoilDBEntities();
        public string userType;
        public string userName ;

        //[OutputCache(NoStore = true, Location = System.Web.UI.OutputCacheLocation.Client, Duration = 0)]
        public ActionResult Index()
        {

            //Logger.PrintInfo("Action:Index ~~~~~~ DashBoard");

            // Setting logged in user name
            userName = User.Identity.Name.Split('\\')[1];

            
            userType = Authorization.GetUserType(HttpContext.ApplicationInstance.Context);

            // Creating object to send all data to View (News Feed, Saved Records, Submitted Records, Rejected Records)
            DashboardViewModel dashboardViewModel = new DashboardViewModel();

            // Separate object For calling methods which returns combined data of each database
            DashboardNewsFeed dashboardNewsFeed = new DashboardNewsFeed();
            DashboardSavedRecords dashboardSavedRecords = new DashboardSavedRecords();
            DashboardSubmittedRecords dashboardSubmittedRecords = new DashboardSubmittedRecords();
            DashboardRejectedRecords dashboardRejectedRecords = new DashboardRejectedRecords();


            // Coping data for sending to View
            dashboardViewModel.CombinedNewsFeed = dashboardNewsFeed.GetCombinedListofDashboardNewsFeed(userType, userName);
            dashboardViewModel.CombinedSavedRecords = dashboardSavedRecords.GetCombinedListofDashboardSavedRecords(userType, userName);
            dashboardViewModel.CombinedSubmittedRecords = dashboardSubmittedRecords.GetCombinedListofDashboardSumittedRecords(userType, userName);
            dashboardViewModel.CombinedRejectedRecords = dashboardRejectedRecords.GetCombinedListofDashboardRejectedRecords(userType, userName);

            return View(dashboardViewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}