﻿using Hussmann.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Hussmann.Common.Enumeration;
using Hussmann.Models.Condenser.Add;
using CondenserDataModel;
using Hussmann.Common.Helper;
using System.Data;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Hussmann.Models.Condenser.RefrigerantType;
using System.Web.Script.Serialization;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.IO;

namespace Hussmann.Controllers
{
    public class CondenserController : BaseController
    {
        private RulestreamCondenserDBEntities CondenserDBContext = new RulestreamCondenserDBEntities();
        public int CurrentCondenserID;
        public bool IsDraftMode;
        public string userType;
        public string userName;
        public static string mode;
        [HttpGet]
        public ActionResult SearchCondenser(string modelNumber, int? condenserBrandId, int? condenserFamilyId, int? statusId, bool? pinned = true)
        {
            Logger.PrintInfo("Action:Search ~~~~~~ Search Screen");
            userType = Authorization.GetUserType(HttpContext.ApplicationInstance.Context);
            userName = User.Identity.Name.Split('\\')[1];
            ViewBag.ModelNumberParm = modelNumber;
            ViewBag.BrandID = condenserBrandId;
            ViewBag.FamilyId = condenserFamilyId;
            ViewBag.StatusId = statusId;
            ViewBag.PinnedTab = pinned;

            ViewBag.PinnedTab = pinned;

            var CondenserBrand = CondenserDBContext.CondenserBrand_lkp
            .Select(s => new
            {
                BrandID = s.BrandID,
                CondenserBrandName = s.CondenserBrandName + " " + s.CondenserSubBrandName
            }).ToList();

            var ModelSuffixes = CondenserDBContext.ModelSuffixes
            .Select(s => new
            {
                ModelSuffixID = s.ModelSuffixID,
                ModelSuffix = s.ModelSuffix1
            }).ToList();

            ViewBag.BrandList = new SelectList(CondenserBrand, "BrandID", "CondenserBrandName", condenserBrandId);
            ViewBag.FamilyList = new SelectList(CondenserDBContext.CondenserFamilies, "CondenserFamilyID", "CondenserFamilyName", condenserFamilyId);
            ViewBag.StatusList = new SelectList(CondenserDBContext.States, "StateID", "Description", statusId);

            if (userType == Convert.ToString(enUserType.Reader))
            {
                ViewBag.EditOrView = "View";
                ViewBag.IconImage = "fa fa-eye";
            }
            else
            {
                ViewBag.EditOrView = "Edit";
                ViewBag.IconImage = "fa fa-pencil";
            }
            ViewBag.userType = userType;
            ViewBag.userName = userName;

            return View();
        }

        public ActionResult GetCondenserData(string modelNumber, int? condenserBrandId, int? condenserFamilyId, int? StatusId, string Page)
        {
            JsonResult result = new JsonResult();

            string search = Request.Form.GetValues("search[value]")[0];
            string draw = Request.Form.GetValues("draw")[0];
            string order = Request.Form.GetValues("order[0][column]")[0];
            string orderDir = Request.Form.GetValues("order[0][dir]")[0];
            int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
            int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);

            //Initialize User Name And UserType 
            userType = Authorization.GetUserType(HttpContext.ApplicationInstance.Context);
            userName = User.Identity.Name.Split('\\')[1];


            if (modelNumber == "null" || modelNumber == "Null")
            {
                modelNumber = null;
            }

            if (orderDir == "desc")
            {
                orderDir = "Descending";
            }
            else if (orderDir == "asc")
            {
                orderDir = "Ascending";
            }
            //Total record count.
            int? totalRecords = CondenserDBContext.spGetRowsCountForSearch(modelNumber, condenserBrandId, condenserFamilyId, StatusId, userName, userType).SingleOrDefault(); ;
            int? recFilter = totalRecords;
            List<spSearchCondenser_Result> searchResult = new List<spSearchCondenser_Result>();

            //Search
            if (!string.IsNullOrEmpty(search) &&
                   !string.IsNullOrWhiteSpace(search))
            {
                // Apply search
                string sortExpression = GetSortExpression(order);
                searchResult = CondenserDBContext.spSearchCondenser(modelNumber, condenserBrandId, condenserFamilyId, StatusId, userName, userType, 0, totalRecords, sortExpression, orderDir).ToList();
                searchResult = searchResult.Where(p => p.ModelNumber.ToString().ToLower().Contains(search.ToLower()) ||
                    p.CondenserBrandName.ToLower().Contains(search.ToLower()) ||
                    p.CondenserFamilyName.ToLower().Contains(search.ToLower()) ||
                    p.ModelSuffixID.ToString().ToLower().Contains(search.ToLower()) ||
                    p.StateDescription.ToLower().Contains(search.ToLower()) ||
                    p.Version.ToString().ToLower().Contains(search.ToLower()) ||
                    p.LastModifiedOn.ToString().ToLower().Contains(search.ToLower()) ||
                    p.ActionBy.ToLower().Contains(search.ToLower())).ToList();
                recFilter = searchResult.Count;
                searchResult = searchResult.Skip(startRec).Take(pageSize).ToList();
            }
            else
            {
                string sortExpression = GetSortExpression(order);
                searchResult = CondenserDBContext.spSearchCondenser(modelNumber, condenserBrandId, condenserFamilyId, StatusId, userName, userType, startRec + 1, startRec + pageSize, sortExpression, orderDir).ToList();

            }

            //Initialize Json
            result = this.Json(new
            {
                draw = Convert.ToInt32(draw),
                recordsTotal = totalRecords,
                recordsFiltered = recFilter,
                data = searchResult
            }, JsonRequestBehavior.AllowGet);
            ViewBag.Page = 1;
            return result;
        }

        private string GetSortExpression(string order)
        {
            try
            {
                switch (order)
                {
                    case "0":
                        // Setting.   
                        return Convert.ToString(enCondenserSortExpression.ModelNumber);
                    case "1":
                        // Setting.  
                        return Convert.ToString(enCondenserSortExpression.CondenserBrandName);
                    case "2":
                        // Setting. 
                        return Convert.ToString(enCondenserSortExpression.CondenserFamilyName);
                    case "3":
                        // Setting.   
                        return Convert.ToString(enCondenserSortExpression.ModelSuffixID);
                    case "4":
                        // Setting.  
                        return Convert.ToString(enCondenserSortExpression.StateDescription);
                    case "5":
                        // Setting. 
                        return Convert.ToString(enCondenserSortExpression.Version);
                    case "6":
                        // Setting. 
                        return Convert.ToString(enCondenserSortExpression.LastModifiedOn);
                    case "7":
                        // Setting. 
                        return Convert.ToString(enCondenserSortExpression.ActionBy);

                    default:
                        // Setting.  
                        return Convert.ToString(enCondenserSortExpression.LastModifiedOn);
                }
            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }
            return Convert.ToString(enCondenserSortExpression.LastModifiedOn);
        }
        // GET: UnitCondenser
        public ActionResult Index()
        {
            return View();
        }

        // GET: Add or Edit Refrigerant Type
        public ActionResult AddRefrigerantType(int? ModelID)
        {
            if (ModelID != null)
            {
                var RefrigerantTypeDB = CondenserDBContext.RefrigerantType.Where(m => m.RefrigerantType_ID == ModelID).Select(x => new { x.RefrigerantType_ID, x.RefrigerantType1, x.RefrigerantDescr, x.Active, x.Obsolete }).SingleOrDefault();
                RefrigerantTypeViewModel RefrigerantType = new RefrigerantTypeViewModel()
                {
                    RefrigerantType_ID = RefrigerantTypeDB.RefrigerantType_ID,
                    RefrigerantType1 = RefrigerantTypeDB.RefrigerantType1,
                    RefrigerantDescr = RefrigerantTypeDB.RefrigerantDescr,
                    Active = RefrigerantTypeDB.Active,
                    Obsolete = RefrigerantTypeDB.Obsolete
                };

                return PartialView("EditRefrigerantType", RefrigerantType);
            }
            List<RefrigerantTypeViewModel> RefrigerantTypes = new List<RefrigerantTypeViewModel>();
            foreach (var item in CondenserDBContext.RefrigerantType.Select(x => new { x.RefrigerantType_ID, x.RefrigerantType1, x.RefrigerantDescr, x.Active, x.Obsolete }).ToList())
            {
                RefrigerantTypeViewModel RefrigerantType = new RefrigerantTypeViewModel()
                {
                    RefrigerantType_ID = item.RefrigerantType_ID,
                    RefrigerantType1 = item.RefrigerantType1,
                    RefrigerantDescr = item.RefrigerantDescr,
                    Active = item.Active,
                    Obsolete = item.Obsolete
                };
                RefrigerantTypes.Add(RefrigerantType);
            }
            return View(RefrigerantTypes);
        }

        public CondenserModel GetCondenserModel(AddCondenserViewModel addCondenser)
        {
            Logger.PrintInfo("Function:GetCondenserModel ~~~~~~ Returns Condenser Model object");
            var CondenserModelInfo = CondenserDBContext.
            CondenserModels.AsNoTracking().Where
            (x => x.ModelID == addCondenser.CurrentCondenserModelID).Select(x => x).FirstOrDefault();
            try
            {
                var loggedInUserName = User.Identity.Name.Split('\\')[1];
                userType = Authorization.GetUserType(HttpContext.ApplicationInstance.Context);

                if (CondenserModelInfo.StateID == Convert.ToInt32(enState.Approved) && addCondenser.IsEditedAfterApprove == true)
                {
                    if (userType != Convert.ToString(enUserType.Reader)) /*(userType == Convert.ToString(enUserType.DataFeeder) && CoilModelInfo.CreatedBy == userName)*/
                    {
                        Logger.PrintInfo("Creating new version of " + CondenserModelInfo.ModelNumber + " with MOdelID : " + CondenserModelInfo.ModelID);
                        //  Create new condenser model with same data but new version
                        int ModelID = CondenserDBContext.spCreateCondenserModelWithNewVersion(CondenserModelInfo.ModelID, loggedInUserName).FirstOrDefault().Value;

                         addCondenser.CurrentCondenserModelID = ModelID;
                         Logger.PrintInfo("New Version created of " + CondenserModelInfo.ModelNumber + " with MOdelID :" + ModelID);
                         addCondenser.IsEditedAfterApprove = false;

                        CondenserModelInfo = CondenserDBContext.CondenserModels.AsNoTracking().Where(x => x.ModelID == ModelID).Select(x => x).FirstOrDefault();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return CondenserModelInfo;
        }


        // get: Save refrigerant type
        public ActionResult SaveRefrigerantType(RefrigerantTypeViewModel RefrigerantType)
        {
            var RefrigerantTypeDB = CondenserDBContext.RefrigerantType.Where(x => x.RefrigerantType_ID == RefrigerantType.RefrigerantType_ID).SingleOrDefault();
            if (RefrigerantTypeDB != null)
            {
                RefrigerantTypeDB.RefrigerantType1 = RefrigerantType.RefrigerantType1;
                RefrigerantTypeDB.RefrigerantDescr = RefrigerantType.RefrigerantDescr;
                RefrigerantTypeDB.Active = RefrigerantType.Active;
                RefrigerantTypeDB.Obsolete = !RefrigerantType.Active;
                CondenserDBContext.SaveChanges();
            }
            else
            {
                RefrigerantType NewRefrigerantType = new RefrigerantType()
                {
                    RefrigerantDescr = RefrigerantType.RefrigerantDescr,
                    RefrigerantType1 = RefrigerantType.RefrigerantType1,
                    Active = RefrigerantType.Active,
                    Obsolete = !RefrigerantType.Active
                };
                CondenserDBContext.RefrigerantType.Add(NewRefrigerantType);
                CondenserDBContext.SaveChanges();
            }
            return RedirectToAction("AddRefrigerantType");
        }
        // GET: Add Refrigerant Type
        public ActionResult AddCondenser(string Name)
        {
            if (Name == null)
                Logger.PrintInfo("Action:AddCondenser ~~~~~~ Add Condenser");
            int StateID = Convert.ToInt32((int)enState.Draft);
            string userName = User.Identity.Name.Split('\\')[1];
            mode = Convert.ToString(enMode.Add);
            TempData["Mode"] = Convert.ToString(enMode.Add);
            AddCondenserViewModel addCondenser = new AddCondenserViewModel();
            try
            {
                // For Unit cooler Details Tab
                AddCondenserDetailsViewModel addCondenserDetails = new AddCondenserDetailsViewModel();

                addCondenserDetails = SetDefaultListsDataForDetailsTab(addCondenserDetails);

                addCondenser.CondenserDetailsData = addCondenserDetails;

            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);
            }

            return View(addCondenser);
        }


        [HttpPost]
        [AuthorizeAD(Role.CanAddRecord)]
        public ActionResult AddCondenser(AddCondenserViewModel addCondenser)
        {
            Logger.PrintInfo("Action:AddCondenser Post ~~~~~~ Add Condenser");

            addCondenser.CondenserDetailsData = SetDefaultListsDataForDetailsTab(addCondenser.CondenserDetailsData);

            if (ModelState.IsValid)
            {
                addCondenser.CurrentCondenserModelID = InsertCondenserDetails(addCondenser);

                IsDraftMode = true;


                return RedirectToAction("EditCondenser", "Condenser", new { ModelID = addCondenser.CurrentCondenserModelID, RedirectTo = Convert.ToString(enAction.Index) });
            }
            return View(addCondenser);
        }

        private int InsertCondenserDetails(AddCondenserViewModel addCondenser)
        {
            var modelID = 0;
            try
            {
                string currentUserName = User.Identity.Name.Split('\\')[1];

                modelID = CondenserDBContext.spCreateCondenserModel(addCondenser.CondenserDetailsData.BrandID,
                                                       addCondenser.CondenserDetailsData.ModelFamilyID,
                                                       addCondenser.CondenserDetailsData.ModelNumber,
                                                       addCondenser.CondenserDetailsData.CubicFeetPerMinute,
                                                       addCondenser.CondenserDetailsData.Title24Compliant,
                                                       addCondenser.CondenserDetailsData.R404SummerCharge,
                                                       addCondenser.CondenserDetailsData.R404WinterCharge,
                                                       addCondenser.CondenserDetailsData.SoundDecibals10,

                                                       addCondenser.CondenserDetailsData.UnitWeight,
                                                       addCondenser.CondenserDetailsData.Length,
                                                       addCondenser.CondenserDetailsData.Width,
                                                       addCondenser.CondenserDetailsData.Height,
                                                       addCondenser.CondenserDetailsData.CondenserTypeID,
                                                       addCondenser.CondenserDetailsData.ModelSuffixID,
                                                       addCondenser.CondenserDetailsData.GeometryID,
                                                       addCondenser.CondenserDetailsData.CondFanMotorID,
                                                       currentUserName
                                                       ).FirstOrDefault().Value;

            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);

            }
            return modelID;
        }

        private AddCondenserDetailsViewModel SetDefaultListsDataForDetailsTab(AddCondenserDetailsViewModel condenserDetails)
        {
            try
            {
                condenserDetails.CondenserTypes = CondenserDBContext.CondenserTypes.OrderBy(x => x.CondenserType1).ToList();
                condenserDetails.FanMotors = CondenserDBContext.CondenserFanMotors.OrderBy(x => x.CondenserFanMotorType).ToList();
                condenserDetails.GeometryTypes = CondenserDBContext.CondenserGeometries.OrderBy(x => x.GeometryType).ToList();
                condenserDetails.ModelFamily = CondenserDBContext.CondenserFamilies.OrderBy(x => x.CondenserFamilyName).ToList();
                condenserDetails.BrandDetails = CondenserDBContext.CondenserBrand_lkp.OrderBy(x => x.CondenserBrandName).ToList();
                condenserDetails.ModelSuffixes = CondenserDBContext.ModelSuffixes.OrderBy(x => x.ModelSuffix1).ToList();
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return condenserDetails;
        }

        public JsonResult IsModelExists([Bind(Prefix = "CondenserDetailsData.ModelNumber")]string ModelNumber)
        {
            //check if any of the ModelNumber matches the ModelNumber 
          
           if (mode == Convert.ToString(enMode.Add))
            {
                return Json(!CondenserDBContext.CondenserModels.Any(x => x.ModelNumber == ModelNumber.Trim()), JsonRequestBehavior.AllowGet);
            }
            else if (mode == Convert.ToString(enMode.Edit))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        //
        public JsonResult IsRefrigerantTypeExists([Bind(Prefix = "RefrigerantType1")]string RefrigerantTypeName, [Bind(Prefix = "RefrigerantType_ID")]int RefrigerantTypeID)
        {
            if (RefrigerantTypeID != 0 && CondenserDBContext.RefrigerantType.Any(x => x.RefrigerantType_ID == RefrigerantTypeID && x.RefrigerantType1 == RefrigerantTypeName))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            //check if any of the RefrigerantType matches the RefrigerantType  
            return Json(!CondenserDBContext.RefrigerantType.Any(x => x.RefrigerantType1 == RefrigerantTypeName), JsonRequestBehavior.AllowGet);
        }

        [AuthorizeAD()]
        //[AuthorizeAD]
        public ActionResult EditCondenser(int ModelID, string RedirectTo, string queryStringParameters)
        {
            Logger.PrintInfo("Action:EditUnitCooler~~~~~~ Edit Unit Cooler");
            userType = Authorization.GetUserType(HttpContext.ApplicationInstance.Context);
            mode = Convert.ToString(enMode.Edit);
            userName = User.Identity.Name.Split('\\')[1];
            int StateID = Convert.ToInt32((int)enState.Draft);

            var CondenserModel = CondenserDBContext.
            CondenserModels.Where
            (x => x.ModelID == ModelID).Select(x => x).FirstOrDefault();


            AddCondenserViewModel addCondenser = new AddCondenserViewModel();
            AddCondenserDetailsViewModel addCondenserDetails = new AddCondenserDetailsViewModel();
            AddCondenserPricingViewModel addCondenserPricingViewModel = new AddCondenserPricingViewModel();
            addCondenserDetails = SetDefaultListsDataForDetailsTab(addCondenserDetails);
            addCondenserDetails = SetDataForDetailsTab(addCondenserDetails, CondenserModel);


            addCondenser.CondenserDetailsData = addCondenserDetails;
            addCondenser.RequestedTabName = "_CondenserDetails";
            addCondenser.CurrentTabName = "_CondenserDetails";
            addCondenser.CurrentCondenserModelID = CondenserModel.ModelID;
            addCondenser.CurrentCoilState = Enum.GetName(typeof(enState), CondenserModel.StateID);
            addCondenser.CurrentCoilVersion = CondenserModel.Version;
            addCondenser.CurrentStateID = (int)CondenserModel.StateID;
            addCondenser.RedirectTo = RedirectTo;
            if (queryStringParameters != null)
            {
                queryStringParameters = queryStringParameters.Replace("_", "&");
                addCondenser.SearchQuery = queryStringParameters;
            }
            else
            {
                addCondenser.SearchQuery = "";
            }

            if (userType == Convert.ToString(enUserType.Reader) || (userType == Convert.ToString(enUserType.DataFeeder) && CondenserModel.CreatedBy != userName))
            {
                if (userType == Convert.ToString(enUserType.DataFeeder) && CondenserModel.StateID == (int)enState.Approved)
                {
                    addCondenser.AllowEdit = true;
                }
                else
                {
                    if (CondenserModel.StateID == 5)
                    {
                        return RedirectToAction("Error", "UnitCooler");
                    }
                    else
                    {
                        addCondenser.AllowEdit = false;
                    }
                }
            }
            else
            {
                addCondenser.AllowEdit = true;// should be true
            }
            
            addCondenser.IsCondenserCapacityGridValid = true;
            addCondenser.IsMiscellaneousGridValid = true;
            addCondenser = SetControlVisibility(addCondenser, HttpContext.ApplicationInstance.Context);

            return View(addCondenser);
        }

        [HttpPost]
       public ActionResult EditCondenser(AddCondenserViewModel addCondensor)
        {
            Logger.PrintInfo("Action:EditCondenser POST~~~~~~ Edit Condenser");
            var CondenserModelInfo = GetCondenserModel(addCondensor);
            if (CondenserModelInfo == null)
            {
                Logger.PrintInfo("Alert:CondenserModelInfo is null in EditCondenser POST Controller");
            }
            userType = Authorization.GetUserType(HttpContext.ApplicationInstance.Context);

            bool allowEdit = AllowEditing(CondenserModelInfo, userType);
            addCondensor.AllowEdit = allowEdit;

            if (allowEdit == true)
            {
                switch ((enCondenserTabPartialView)Enum.Parse(typeof(enCondenserTabPartialView), addCondensor.CurrentTabName, true))
                {
                    case enCondenserTabPartialView._CondenserDetails:
                        SaveCondenserDetailsTabData(addCondensor);
                        break;
                    case enCondenserTabPartialView._CondenserType:
                        SaveCondenserTypeTabData(addCondensor);
                        break;
                    case enCondenserTabPartialView._CondenserCapacity:
                        SaveCondenserCapacityTabData(addCondensor);
                        break;
                    case enCondenserTabPartialView._Miscellaneous:
                        SaveMiscellaneousTabData(addCondensor);
                        break;
                    case enCondenserTabPartialView._ControlPanel:
                        addCondensor.ControlPanel.ModelID = addCondensor.CurrentCondenserModelID;
                        SaveControlPanelTabData(addCondensor.ControlPanel);
                        break;
                    case enCondenserTabPartialView._ReceiverType:
                        SaveReceiverTypeTabData(addCondensor);
                        break;
                    case enCondenserTabPartialView._Pricing:
                        SavePricingTabData(addCondensor);
                        break;

                }

            }

            var Condenser = CondenserDBContext.
            CondenserModels.AsNoTracking().Where
            (x => x.ModelID == addCondensor.CurrentCondenserModelID).Select(x => x).FirstOrDefault();

            addCondensor.CurrentStateID = Convert.ToInt32(Condenser.StateID);
            addCondensor.CurrentCoilVersion = Condenser.Version;
            addCondensor.CurrentCoilState = Enum.GetName(typeof(enState), Condenser.StateID);



            switch ((enCondenserTabPartialView)Enum.Parse(typeof(enCondenserTabPartialView), addCondensor.RequestedTabName, true))
            {

                case enCondenserTabPartialView._CondenserDetails:
                    AddCondenserDetailsViewModel addCondenserDetails = new AddCondenserDetailsViewModel();
                    addCondensor.CondenserDetailsData = SetDefaultListsDataForDetailsTab(addCondenserDetails);
                    addCondensor.CondenserDetailsData = SetDataForDetailsTab(addCondenserDetails, Condenser);
                    break;
                case enCondenserTabPartialView._CondenserType:
                    AddCondenserTypeViewModel addCondenserType = new AddCondenserTypeViewModel();
                    addCondensor.CondenserTypeData = SetDataForCondenserTypeTab(addCondenserType, Condenser);
                    break;
                case enCondenserTabPartialView._CondenserCapacity:
                    AddCondenserCapacityViewModel addCondenserCapacity = new AddCondenserCapacityViewModel();
                    addCondensor.CondenserCapacityData = SetDataForCondenserCapacityTab(addCondenserCapacity, Condenser);
                    break;
                case enCondenserTabPartialView._Miscellaneous:
                    AddMiscellaneousViewModel addMiscellaneous = new AddMiscellaneousViewModel();
                    addCondensor.MiscellaneousData = SetDataForMiscellaneousTab(addMiscellaneous, Condenser);
                    break;
                case enCondenserTabPartialView._ControlPanel:
                    AddControlPanelViewModel addControlPanel = new AddControlPanelViewModel();
                    addCondensor.ControlPanel = SetDefaultListsAndDataForControlPanelTab(addControlPanel, Condenser.ModelID);

                    break;
                case enCondenserTabPartialView._ReceiverType:
                    AddCondenserReceiverTypeViewModel addCondenserReceiverTypeViewModel = new AddCondenserReceiverTypeViewModel();
                    addCondensor.CondenserReceiverTypeData = new AddCondenserReceiverTypeViewModel();
                    addCondensor.CondenserReceiverTypeData.lstReceiverType = new List<ReceiverTypeDetails>();
                    addCondensor.CondenserReceiverTypeData.lstReceiverType = SetCondenserReceiverTypeGridData(addCondenserReceiverTypeViewModel, Condenser.ModelID);
                    break;
                case enCondenserTabPartialView._Pricing:
                    AddCondenserPricingViewModel addCondenserPricingViewModel = new AddCondenserPricingViewModel();
                    addCondensor.CondenserPricingData = SetCondenserPricingGridData(addCondenserPricingViewModel, Condenser.ModelID);

                    break;

            }
            addCondensor.CurrentTabName = addCondensor.RequestedTabName;
            addCondensor = SetControlVisibility(addCondensor, HttpContext.ApplicationInstance.Context);


            return View(addCondensor);
        }
         public bool AllowEditing(CondenserModel CondenserModelInfo, string userType)
        {
            bool IsAllow = false;
            try
            {
                if (CondenserModelInfo == null)
                {
                    Logger.PrintInfo("CondenserModelInfo is null in AllowEditing function");
                }
                var loggedInUserName = User.Identity.Name.Split('\\')[1];
                if (userType != Convert.ToString(enUserType.Reader) || (userType == Convert.ToString(enUserType.DataFeeder) && CondenserModelInfo.CreatedBy == loggedInUserName))
                {
                    IsAllow = true;
                }
                else
                {
                    IsAllow = false;
                }
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return IsAllow;

        }
        
        private List<ReceiverTypeDetails> SetCondenserReceiverTypeGridData(AddCondenserReceiverTypeViewModel acrtv, int modelID)
        {
            List<ReceiverTypeDetails> lstRecieverNewData = new List<ReceiverTypeDetails>();
            var lstReceiverTypeData = CondenserDBContext.spGetCondenserReceiverTypeData(modelID);
            foreach(var data in lstReceiverTypeData)
            {
                lstRecieverNewData.Add(
                    new ReceiverTypeDetails
                    {
                       ModelID=data.ModelID,
                       RcvrCapacityID=data.RcvrCapacityID_,
                       ReceiverCapacity=data.ReceiverCapacity,
                       ReceiverSize=data.ReceiverSize,
                       ReceiverTypeID=data.ReceiverTypeID,
                       RefrigerantType=data.RefrigerantType,
                       RefrigerantType_ID=data.RefrigerantType_ID

                    }

                    );

            }
            acrtv.lstReceiverType = lstRecieverNewData;
            return lstRecieverNewData;

        }
        
        private AddCondenserPricingViewModel SetCondenserPricingGridData(AddCondenserPricingViewModel addCondenserPricingViewModel, int modelID)
        {
            addCondenserPricingViewModel.condenserPricing = SetCondenserPricingDetails(modelID);
            addCondenserPricingViewModel.lstLegPricing = SetLegPricingGridData(modelID);
            addCondenserPricingViewModel.legPricing = Newtonsoft.Json.JsonConvert.SerializeObject(addCondenserPricingViewModel.lstLegPricing);
            addCondenserPricingViewModel.geometryType = addCondenserPricingViewModel.condenserPricing.CondenserType;
            addCondenserPricingViewModel.lstControlPanelPricing = SetControlPanelPriceData(modelID);
            addCondenserPricingViewModel.controlPanelPricing = Newtonsoft.Json.JsonConvert.SerializeObject(addCondenserPricingViewModel.lstControlPanelPricing);

            addCondenserPricingViewModel.lstMiscellaneousPricing = SetMiscellaneousOptionsPricingData(modelID);
            addCondenserPricingViewModel.miscellaneousPricing = Newtonsoft.Json.JsonConvert.SerializeObject(addCondenserPricingViewModel.lstMiscellaneousPricing);
            addCondenserPricingViewModel.lstReceiverPricing = SetReceiverPricingData(modelID);
            addCondenserPricingViewModel.receiverPricing = Newtonsoft.Json.JsonConvert.SerializeObject(addCondenserPricingViewModel.lstReceiverPricing);

            return addCondenserPricingViewModel;
        }

        private CondenserPriceDetails SetCondenserPricingDetails(int modelID)
        {
            CondenserPriceDetails condenserPricingData = new CondenserPriceDetails();
            CondenserPricing cp = CondenserDBContext.CondenserPricing.Where(a => a.CondenserModel.ModelID == modelID).FirstOrDefault();
            if (cp != null)
            {
                condenserPricingData.ModelID = cp.CondenserModel.ModelID;
                condenserPricingData.ModelSuffix = cp.ModelSuffix;
                condenserPricingData.ModelSuffixID = cp.ModelSuffix1.ModelSuffixID;
                condenserPricingData.PolyesterFinCoating = cp.PolyesterFinCoating;
                condenserPricingData.EpoxyElectroFinCoatedCoil = cp.EpoxyElectroFinCoatedCoil;
                condenserPricingData.BaseListPrice = cp.BaseListPrice;
                condenserPricingData.CondenserTypeID = cp.CondenserType.CondenserTypeID;
                condenserPricingData.CopperFin = cp.CopperFin;
                condenserPricingData.CondenserPricingID = cp.CondenserPricingID;
                condenserPricingData.CondenserType = cp.CondenserType.CondenserType1;

            }
            else
            {
                CondenserModel cm = CondenserDBContext.CondenserModels.Find(modelID);
                condenserPricingData.CondenserType = cm.CondenserType.CondenserType1;
                condenserPricingData.ModelSuffix = cm.ModelSuffix.ModelSuffix1;
                condenserPricingData.CondenserTypeID = cm.CondenserTypeID;
                condenserPricingData.ModelSuffixID = cm.ModelSuffixID;
                condenserPricingData.ModelID = cm.ModelID;
            }
            return condenserPricingData;

        }

        private List<LegPriceDetails> SetLegPricingGridData(int modelID)
        {
            List<LegPrice> lstLegPrice = CondenserDBContext.LegPrice.Where(a => a.CondenserModel.ModelID == modelID && a.IsSelected==true).ToList();
            List<LegPriceDetails> lstLegPriceDetails = new List<LegPriceDetails>();
            if (lstLegPrice.Count > 0)
            {
                foreach (var lp in lstLegPrice)
                {
                    lstLegPriceDetails.Add(
                        new LegPriceDetails
                        {
                            GeometryID = lp.CondenserGeometry.GeometryID,
                            LegPrice = lp.LegPrice1,
                            LegPriceID = lp.LegPriceID,
                            LegTypeID = lp.LegType.LegTypeID,
                            LegType = lp.LegType.LegType1,
                            ModelID = lp.CondenserModel.ModelID
                        }

                    );

                }

            }


            return lstLegPriceDetails;
        }

        private List<ControlPanelPriceDetails> SetControlPanelPriceData(int modelID)
        {
            List<ControlPanelPrice> lstControlPanelPrice = CondenserDBContext.ControlPanelPrice.Where(a => a.CondenserModel.ModelID == modelID).ToList();
            List<ControlPanelPriceDetails> lstControlPanelPriceDetails = new List<ControlPanelPriceDetails>();
            if (lstControlPanelPrice.Count > 0)
            {
                foreach (var lp in lstControlPanelPrice)
                {
                    lstControlPanelPriceDetails.Add(
                        new ControlPanelPriceDetails
                        {
                            GeometryID = lp.CondenserGeometry.GeometryID,
                            GeometryType = lp.CondenserGeometry.GeometryType,
                            ListPrice = lp.ListPrice,
                            ModelID = lp.CondenserModel.ModelID,
                            PriceID = lp.PriceID
                        }

                    );

                }

            }


            return lstControlPanelPriceDetails;

        }
        private List<MiscellaneousPriceDetails> SetMiscellaneousOptionsPricingData(int modelID)
        {
            List<MiscellaneousOptionsPricing> lstMiscellaneousOptionsPricing = CondenserDBContext.MiscellaneousOptionsPricing.Where(a => a.CondenserModel.ModelID == modelID && a.IsSelected==true).ToList();
            List<MiscellaneousPriceDetails> lstMiscellaneousPriceDetails = new List<MiscellaneousPriceDetails>();
            if (lstMiscellaneousOptionsPricing.Count > 0)
            {
                foreach (var lp in lstMiscellaneousOptionsPricing)
                {
                    lstMiscellaneousPriceDetails.Add(
                        new MiscellaneousPriceDetails
                        {
                            GeometryID = lp.CondenserGeometry.GeometryID,
                            MiscellaneousPrice = lp.MiscellaneousPrice,
                            MiscellaneousPricingID = lp.MiscellaneousPricingID,
                            MiscellenousOptionsID = lp.MiscellaneousOptions.MiscellaneousOptionsID,
                            MiscellenousOptionsName = lp.MiscellaneousOptions.MiscellaneousOptions1,
                            ModelID = lp.CondenserModel.ModelID
                        }

                    );

                }

            }


            return lstMiscellaneousPriceDetails;

        }
        private List<ReceiverPriceDetails> SetReceiverPricingData(int modelID)
        {
            List<ReceiverPriceDetails> lstReceiverPriceData = new List<ReceiverPriceDetails>();
            List<Receiver> lstReceiver = CondenserDBContext.Receiver.Where(a => a.CondenserModel.ModelID == modelID && a.IsSelected==true).ToList();
            if (lstReceiver.Count > 0)
            {
                foreach (var lr in lstReceiver)
                {
                    lstReceiverPriceData.Add(
                     new ReceiverPriceDetails
                     {
                         ModelID = lr.CondenserModel.ModelID,
                         HeatTapeInsulPrice = lr.HeatTapeInsulPrice,
                       //  ReceiverCapacitorId = lr.ReceiverCapacity.RcvrCapacityID_,
                         ReceiverID = lr.ReceiverID,
                         ReceiverPrice = lr.ReceiverPrice,
                         ReceiverQty = lr.ReceiverQty,
                         ReceiverTypeID = lr.ReceiverType.ReceiverTypeID,
                         GeometryID = lr.CondenserGeometry.GeometryID,
                         ReceiverType = lr.ReceiverType.ReceiverSize
                         
                     }
                     );



                }
            }
            return lstReceiverPriceData;

        }
        private void SavePricingTabData(AddCondenserViewModel addCondensor)
        {
            Logger.PrintInfo("Function:SavePricingTabData ~~~~~~ Save Condenser Pricing Tab Data");
            try
            {
                addCondensor.CondenserPricingData.lstMiscellaneousPricing = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MiscellaneousPriceDetails>>(addCondensor.CondenserPricingData.miscellaneousPricing);
                addCondensor.CondenserPricingData.lstLegPricing = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LegPriceDetails>>(addCondensor.CondenserPricingData.legPricing);
                addCondensor.CondenserPricingData.lstControlPanelPricing = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ControlPanelPriceDetails>>(addCondensor.CondenserPricingData.controlPanelPricing);
                addCondensor.CondenserPricingData.lstReceiverPricing = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ReceiverPriceDetails>>(addCondensor.CondenserPricingData.receiverPricing);

                string MiscellaneousPricingData;
                DataTable dtMiscellaneousPricing;
                if (addCondensor.CondenserPricingData.lstMiscellaneousPricing == null || addCondensor.CondenserPricingData.lstMiscellaneousPricing.Count() == 0)
                {
                    dtMiscellaneousPricing = null;
                }
                else
                {

                    MiscellaneousPricingData = JsonConvert.SerializeObject(addCondensor.CondenserPricingData.lstMiscellaneousPricing);
                    dtMiscellaneousPricing = (DataTable)JsonConvert.DeserializeObject(MiscellaneousPricingData, (typeof(DataTable)));
                    if (dtMiscellaneousPricing.Columns.Contains("MiscellenousOptionsName"))
                        dtMiscellaneousPricing.Columns.Remove("MiscellenousOptionsName");
                }

                string LegPricingData;
                DataTable dtLegPricing;
                if (addCondensor.CondenserPricingData.lstLegPricing == null || addCondensor.CondenserPricingData.lstLegPricing.Count() == 0)
                {
                    dtLegPricing = null;
                }
                else
                {

                    LegPricingData = JsonConvert.SerializeObject(addCondensor.CondenserPricingData.lstLegPricing);
                    dtLegPricing = (DataTable)JsonConvert.DeserializeObject(LegPricingData, (typeof(DataTable)));
                    if (dtLegPricing.Columns.Contains("LegType"))
                        dtLegPricing.Columns.Remove("LegType");
                }

                string ControlPanelPricingData;
                DataTable dtControlPanelPricing;
                if (addCondensor.CondenserPricingData.lstControlPanelPricing == null || addCondensor.CondenserPricingData.lstControlPanelPricing.Count() == 0)
                {
                    dtControlPanelPricing = null;
                }
                else
                {

                    ControlPanelPricingData = JsonConvert.SerializeObject(addCondensor.CondenserPricingData.lstControlPanelPricing);
                    dtControlPanelPricing = (DataTable)JsonConvert.DeserializeObject(ControlPanelPricingData, (typeof(DataTable)));
                    if (dtControlPanelPricing.Columns.Contains("GeometryType"))
                        dtControlPanelPricing.Columns.Remove("GeometryType");
                }

                string RecieverPricingData;
                DataTable dtRecieverPricing;
                if (addCondensor.CondenserPricingData.lstReceiverPricing == null || addCondensor.CondenserPricingData.lstReceiverPricing.Count() == 0)
                {
                    dtRecieverPricing = null;
                }
                else
                {


                    RecieverPricingData = JsonConvert.SerializeObject(addCondensor.CondenserPricingData.lstReceiverPricing);
                    dtRecieverPricing = (DataTable)JsonConvert.DeserializeObject(RecieverPricingData, (typeof(DataTable)));
                    if (dtRecieverPricing.Columns.Contains("ReceiverType"))
                        dtRecieverPricing.Columns.Remove("ReceiverType");
                }


                var dataTableLegsGrid = new SqlParameter("param_LegPriceUDT", SqlDbType.Structured);
                dataTableLegsGrid.Value = dtLegPricing;
                dataTableLegsGrid.TypeName = "LegPriceUDT";

                var dataTableMiscellaneousPricing = new SqlParameter("param_MiscellaneousOptionsPricingUDT", SqlDbType.Structured);
                dataTableMiscellaneousPricing.Value = dtMiscellaneousPricing;
                dataTableMiscellaneousPricing.TypeName = "MiscellaneousOptionsPricingUDT";

                var dataTableControlPanelGrid = new SqlParameter("param_ControlPanelPricingUDT", SqlDbType.Structured);
                dataTableControlPanelGrid.Value = dtControlPanelPricing;
                dataTableControlPanelGrid.TypeName = "ControlPanelPricingUDT";

                var dataTableReceiverGrid = new SqlParameter("param_ReceiverPricingUDT", SqlDbType.Structured);
                dataTableReceiverGrid.Value = dtRecieverPricing;
                dataTableReceiverGrid.TypeName = "ReceiverUDT";

                var modelID = new SqlParameter("param_ModelID", SqlDbType.Int);
                modelID.Value = addCondensor.CondenserPricingData.condenserPricing.ModelID;
                var modelSuffix = new SqlParameter("param_ModelSuffix", SqlDbType.NVarChar);
                modelSuffix.Value = addCondensor.CondenserPricingData.condenserPricing.ModelSuffix;
                var epoxyElectroFinCoatedCoil = new SqlParameter("param_EpoxyElectroFinCoatedCoil", SqlDbType.Money);
                epoxyElectroFinCoatedCoil.Value = addCondensor.CondenserPricingData.condenserPricing.EpoxyElectroFinCoatedCoil;
                if (epoxyElectroFinCoatedCoil.Value == null)
                {
                    epoxyElectroFinCoatedCoil.Value = DBNull.Value;
                }

                var polyesterFinCoating = new SqlParameter("param_PolyesterFinCoating", SqlDbType.Money);
                polyesterFinCoating.Value = addCondensor.CondenserPricingData.condenserPricing.PolyesterFinCoating;
                if (polyesterFinCoating.Value == null)
                {
                    polyesterFinCoating.Value = DBNull.Value;
                }
                var copperFin = new SqlParameter("param_CopperFin", SqlDbType.Money);
                copperFin.Value = addCondensor.CondenserPricingData.condenserPricing.CopperFin;
                if (copperFin.Value == null)
                {
                    copperFin.Value = DBNull.Value;
                }

                var modelSuffixID = new SqlParameter("param_ModelSuffixID", SqlDbType.Int);
                modelSuffixID.Value = addCondensor.CondenserPricingData.condenserPricing.ModelSuffixID;
                var baseListPrice = new SqlParameter("param_BaseListPrice", SqlDbType.Money);
                baseListPrice.Value = addCondensor.CondenserPricingData.condenserPricing.BaseListPrice;
                if (baseListPrice.Value == null)
                {
                    baseListPrice.Value = DBNull.Value;
                }

                var condenserTypeID = new SqlParameter("param_CondenserTypeID", SqlDbType.Int);
                condenserTypeID.Value = addCondensor.CondenserPricingData.condenserPricing.CondenserTypeID;
                var condenserPricingID = new SqlParameter("param_CondenserPricingID", SqlDbType.Int);
                condenserPricingID.Value = addCondensor.CondenserPricingData.condenserPricing.CondenserPricingID;
                if(dtLegPricing != null)
                {
                    if (dtLegPricing.Columns.Contains("LegType"))
                    {
                        dtLegPricing.Columns.Remove("LegType");
                    }
                }
                CondenserDBContext.Database.ExecuteSqlCommand("exec dbo.spInsertUpdatePricing @param_ModelID,@param_ModelSuffix,@param_EpoxyElectroFinCoatedCoil,@param_PolyesterFinCoating,@param_CopperFin,@param_ModelSuffixID,@param_BaseListPrice,@param_CondenserTypeID,@param_CondenserPricingID,@param_LegPriceUDT,@param_MiscellaneousOptionsPricingUDT,@param_ControlPanelPricingUDT,@param_ReceiverPricingUDT", modelID, modelSuffix, epoxyElectroFinCoatedCoil, polyesterFinCoating, copperFin, modelSuffixID, baseListPrice, condenserTypeID, condenserPricingID, dataTableLegsGrid, dataTableMiscellaneousPricing, dataTableControlPanelGrid, dataTableReceiverGrid);
            }
            catch(Exception ex)
            {
                Logger.PrintInfo(ex.ToString());
            }
        }

        private void SaveReceiverTypeTabData(AddCondenserViewModel addCondensor)
        {
            if (addCondensor.CondenserReceiverTypeData.receiverType != "[]")
            {

                Logger.PrintInfo("Function:SaveReceiverTypeTabData ~~~~~~ Save Condenser Receiver Type Tab Data");
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<CustomReceiverTypeDetails> lstCustomRType = serializer.Deserialize<List<CustomReceiverTypeDetails>>(addCondensor.CondenserReceiverTypeData.receiverType);
                List<ReceiverTypeDetails> lstRecieverNewData = new List<ReceiverTypeDetails>();
                var lstReceiverTypeData = CondenserDBContext.spGetCondenserReceiverTypeData(lstCustomRType.FirstOrDefault().ModelID);
                foreach (var data in lstReceiverTypeData)
                {
                    lstRecieverNewData.Add(
                        new ReceiverTypeDetails
                        {
                            ModelID = data.ModelID,
                            RcvrCapacityID = data.RcvrCapacityID_,
                            ReceiverCapacity = data.ReceiverCapacity,
                            ReceiverSize = data.ReceiverSize,
                            ReceiverTypeID = data.ReceiverTypeID,
                            RefrigerantType = data.RefrigerantType,
                            RefrigerantType_ID = data.RefrigerantType_ID

                        }

                        );

                }


                foreach (var c in lstCustomRType)
                {
                    foreach (var b in c.GridValues.ToList())
                    {
                        lstRecieverNewData.Where(a => a.RefrigerantType_ID == c.RefrigerantType_ID && a.ReceiverSize == b.key).FirstOrDefault().ReceiverCapacity = b.value;

                    }

                }


                string RecieverTypeData;
                DataTable dtRecieverType;

                RecieverTypeData = JsonConvert.SerializeObject(lstRecieverNewData);
                dtRecieverType = (DataTable)JsonConvert.DeserializeObject(RecieverTypeData, (typeof(DataTable)));
                if (dtRecieverType.Columns.Contains("RefrigerantType"))
                    dtRecieverType.Columns.Remove("RefrigerantType");
                if (dtRecieverType.Columns.Contains("ReceiverSize"))
                    dtRecieverType.Columns.Remove("ReceiverSize");


                var dataTableReceiverGrid = new SqlParameter("param_ReceiverCapacityUDT", SqlDbType.Structured);
                dataTableReceiverGrid.Value = dtRecieverType;
                dataTableReceiverGrid.TypeName = "ReceiverCapacityUDT";

                CondenserDBContext.Database.ExecuteSqlCommand("exec dbo.spUpdateReceiverCapacity @param_ReceiverCapacityUDT", dataTableReceiverGrid);
            }
        }

        protected AddCondenserDetailsViewModel SetDataForDetailsTab(AddCondenserDetailsViewModel addCondenserDetails, CondenserModel CondenserModel)
        {
            try
            {
                addCondenserDetails.ModelNumber = CondenserModel.ModelNumber;
                addCondenserDetails.BrandID = CondenserModel.MFGID;
                addCondenserDetails.ModelFamilyID = CondenserModel.ModelFamilyID;
                addCondenserDetails.CubicFeetPerMinute = CondenserModel.CubicFeetPerMinute;
                addCondenserDetails.R404SummerCharge = CondenserModel.R404SummerCharge;
                addCondenserDetails.R404WinterCharge = CondenserModel.R404WinterCharge;
                addCondenserDetails.SoundDecibals10 = CondenserModel.SoundDecibals10;
                addCondenserDetails.UnitWeight = CondenserModel.UnitWeight;
                addCondenserDetails.Length = CondenserModel.Length;
                addCondenserDetails.Width = CondenserModel.Width;
                addCondenserDetails.Height = CondenserModel.Height;
                addCondenserDetails.CondenserTypeID = CondenserModel.CondenserTypeID;
                addCondenserDetails.GeometryID = CondenserModel.GeometryID;
                addCondenserDetails.ModelSuffixID = CondenserModel.ModelSuffixID;
                addCondenserDetails.CondFanMotorID = CondenserModel.CondFanMotorID;
                addCondenserDetails.Title24Compliant = CondenserModel.Title24Compliant;

            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return addCondenserDetails;
        }

        private void SaveCondenserDetailsTabData(AddCondenserViewModel addCondenser)
        {
            try
            {
                string currentUserName = User.Identity.Name.Split('\\')[1];

                CondenserDBContext.spUpdateCondenserDetailsTabData(
                                                       addCondenser.CurrentCondenserModelID,
                                                       addCondenser.CondenserDetailsData.BrandID,
                                                       addCondenser.CondenserDetailsData.ModelFamilyID,
                                                       addCondenser.CondenserDetailsData.ModelNumber,
                                                       addCondenser.CondenserDetailsData.CubicFeetPerMinute,
                                                       addCondenser.CondenserDetailsData.Title24Compliant,
                                                       addCondenser.CondenserDetailsData.R404SummerCharge,
                                                       addCondenser.CondenserDetailsData.R404WinterCharge,
                                                       addCondenser.CondenserDetailsData.SoundDecibals10,
                                                       addCondenser.CondenserDetailsData.UnitWeight,
                                                       addCondenser.CondenserDetailsData.Length,
                                                       addCondenser.CondenserDetailsData.Width,
                                                       addCondenser.CondenserDetailsData.Height,
                                                       addCondenser.CondenserDetailsData.CondenserTypeID,
                                                       addCondenser.CondenserDetailsData.ModelSuffixID,
                                                       addCondenser.CondenserDetailsData.GeometryID,
                                                       addCondenser.CondenserDetailsData.CondFanMotorID,
                                                       currentUserName
                                                       );

            }
            catch (Exception ex)
            {
                Logger.PrintError(ex);

            }
        }

        protected AddCondenserTypeViewModel SetDataForCondenserTypeTab(AddCondenserTypeViewModel addCondenserType, CondenserModel condenser)
        {
            try
            {
                var GeometryDetails = CondenserDBContext.CondenserGeometries.Where(x => x.GeometryID == condenser.GeometryID).Select(x => x).FirstOrDefault();
                addCondenserType.GeometryID = GeometryDetails.GeometryID;
                addCondenserType.GeometryTypeName = GeometryDetails.GeometryType;

                var CondenserDetails = CondenserDBContext.CondenserTypes.Where(x => x.CondenserTypeID == condenser.CondenserTypeID).Select(x => x).FirstOrDefault();
                addCondenserType.CondenserTypeID = CondenserDetails.CondenserTypeID;
                addCondenserType.CondenserTypeName = CondenserDetails.CondenserType1;

                var LegDetails = CondenserDBContext.LegAvailabilityXref.Where(x => x.CondenserModel.ModelID == condenser.ModelID).OrderBy(x => x.LegType.LegType1).Select(x => new LegDetails
                {
                    ID = x.ID,
                    CondenserTypeID = x.CondenserType.CondenserTypeID,
                    LegTypeID = x.LegType.LegTypeID,
                    LegAvailable = x.LegAvailable,
                    Standard = x.Standard,
                    ModelID = x.CondenserModel.ModelID,
                    LegTypeName = x.LegType.LegType1
                }).ToList();
                addCondenserType.LegDetails = LegDetails;

                var ReceiverDetails = CondenserDBContext.Receiver.Where(x => x.CondenserModel.ModelID == condenser.ModelID).Select(x => new ReceiverDetails
                {
                    ReceiverID = x.ReceiverID,
                    GeometryID = x.CondenserGeometry.GeometryID,
                    ReceiverTypeID = x.ReceiverType.ReceiverTypeID,
                    ReceiverPrice = x.ReceiverPrice,
                    HeatTapeInsulPrice = x.HeatTapeInsulPrice,
                    ReceiverQty = x.ReceiverQty,
                    ModelID = x.CondenserModel.ModelID,
                    IsSelected = x.IsSelected
                }).ToList();
                addCondenserType.ReceiverDetails = ReceiverDetails;

                var ReceiverTypeList = CondenserDBContext.ReceiverType.Select(x => x).ToList();
                addCondenserType.ReceiverTypeList = ReceiverTypeList;

                var LegTypeList = CondenserDBContext.LegType.Select(x => x).ToList();
                addCondenserType.LegTypeList = LegTypeList;
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
            return addCondenserType;
        }

        private void SaveCondenserTypeTabData(AddCondenserViewModel addCondensor)
        {
            try
            {
                string LegAvaliabilityXrefData;
                DataTable dtLegAvaliabilityXref;
                if (addCondensor.CondenserTypeData.LegDetails == null)
                {
                    dtLegAvaliabilityXref = null;
                }
                else
                {
                    LegAvaliabilityXrefData = JsonConvert.SerializeObject(addCondensor.CondenserTypeData.LegDetails);
                    dtLegAvaliabilityXref = (DataTable)JsonConvert.DeserializeObject(LegAvaliabilityXrefData, (typeof(DataTable)));
                    dtLegAvaliabilityXref.Columns.Remove("LegTypeName");
                }

                string ReceiverData;
                DataTable dtReceiver;
                if (addCondensor.CondenserTypeData.ReceiverDetails == null)
                {
                    dtReceiver = null;
                }
                else
                {
                    ReceiverData = JsonConvert.SerializeObject(addCondensor.CondenserTypeData.ReceiverDetails);
                    dtReceiver = (DataTable)JsonConvert.DeserializeObject(ReceiverData, (typeof(DataTable)));
                }

                var dataTableLegAvaliabilityXref = new SqlParameter("param_LegAvailabilityXrefUDT", SqlDbType.Structured);
                dataTableLegAvaliabilityXref.Value = dtLegAvaliabilityXref;
                dataTableLegAvaliabilityXref.TypeName = "LegAvailabilityXrefUDT";

                var dataTableReceiver = new SqlParameter("param_ReceiverUDT", SqlDbType.Structured);
                dataTableReceiver.Value = dtReceiver;
                dataTableReceiver.TypeName = "ReceiverUDT";

                var modelID = new SqlParameter("param_ModelID", SqlDbType.Int);
                modelID.Value = addCondensor.CurrentCondenserModelID;

                CondenserDBContext.Database.ExecuteSqlCommand("exec dbo.spUpdateCondenserTypeTabData @param_ModelID,@param_LegAvailabilityXrefUDT,@param_ReceiverUDT", modelID, dataTableLegAvaliabilityXref, dataTableReceiver);
            }
            catch (Exception exception)
            {
                Logger.PrintError(exception);
            }
        }

        protected AddCondenserCapacityViewModel SetDataForCondenserCapacityTab(AddCondenserCapacityViewModel addCondenserCapacity, CondenserModel condenser)
        {
            try
            {
                // Insert Data for any new refregerantType or refregerantType which is not present in CondenserCapacity Table
                CondenserDBContext.spInsertCondenserCapacity(condenser.ModelID);
                string condenserCapacityData = JsonConvert.SerializeObject(CondenserDBContext.CondenserCapacity_.Where(x => x.CondenserModel.ModelID == condenser.ModelID).Select(x => new CondenserCapacityDetails
                {
                    RefregerantTypeName = x.RefrigerantType.RefrigerantDescr.Substring(0, 5),
                    CondCapacityID_ = x.CondCapacityID_,
                    ModelID = x.CondenserModel.ModelID,
                    RefrigerantTypeID = x.RefrigerantType.RefrigerantType_ID,
                    Capacity = x.Capacity
                }).ToList());
                addCondenserCapacity.CondenserCapacityGridData = condenserCapacityData;

                
            }
            catch(Exception exception)
            {
                Logger.PrintError(exception);
            }
            return addCondenserCapacity;
        }

        private void SaveCondenserCapacityTabData(AddCondenserViewModel addCondensor)
        {
            try
            {
                Logger.PrintInfo("Function:SaveCondenserCapacityTabData ~~~~~~ Save Condenser Capacity Tab Data");
                DataTable dt = (DataTable)JsonConvert.DeserializeObject(addCondensor.CondenserCapacityData.CondenserCapacityGridData, (typeof(DataTable)));
            
                var condenserCapacity = new SqlParameter("param_CondenserCapacityUDT", SqlDbType.Structured);
                condenserCapacity.Value = dt;
                condenserCapacity.TypeName = "CondenserCapacityUDT";

                var modelId = new SqlParameter("parm_ModelId", SqlDbType.Int);
                modelId.Value = addCondensor.CurrentCondenserModelID;

                CondenserDBContext.Database.ExecuteSqlCommand("exec dbo.spUpdateCondenserCapacity @parm_ModelId,@param_CondenserCapacityUDT", modelId, condenserCapacity);
            }
            catch(Exception exception)
            {
                Logger.PrintError(exception);
            }
        }

        private AddMiscellaneousViewModel SetDataForMiscellaneousTab(AddMiscellaneousViewModel addMiscellaneous, CondenserModel condenser)
        {
            try
            {
                addMiscellaneous.MiscellaneousOptionList = CondenserDBContext.MiscellaneousOptionsPricing.Where(x => x.CondenserModel.ModelID == condenser.ModelID).OrderBy(x => x.MiscellaneousOptions.MiscellaneousOptions1).Select(x => new MiscellaneousOptionDetails
                {
                    MiscellaneousPricingID = x.MiscellaneousPricingID,
                    MiscellenousOptionsID = x.MiscellaneousOptions.MiscellaneousOptionsID,
                    ModelID = x.CondenserModel.ModelID,
                    IsSelected = x.IsSelected,
                    MiscellenousOption = x.MiscellaneousOptions.MiscellaneousOptions1

                }).ToList();

                string condenserPropertiesData = JsonConvert.SerializeObject(CondenserDBContext.CondenserProperties.Where(x => x.CondenserModel.ModelID == condenser.ModelID).Select(x => new CondenserPropertiesDetails
                {
                    CondPropID = x.CondPropID,
                    ModelID = x.CondenserModel.ModelID,
                    VoltageTypeID = x.VoltageType_lkp.VoltageTypeID,
                    CondenserAmps = x.CondenserAmps,
                    MCA = x.MCA,
                    MOPD = x.MOPD
                }).ToList());

                addMiscellaneous.CondenserPropertiesData = condenserPropertiesData;

               string voltageTypeList =  JsonConvert.SerializeObject(CondenserDBContext.VoltageType_lkp.OrderBy(x => x.VoltageType).Select(x => new
               {
                   id = x.VoltageTypeID,
                   label = x.VoltageType
               }).ToList());
                addMiscellaneous.VoltageTypeList = voltageTypeList;


            }
            catch(Exception exception)
            {
                Logger.PrintError(exception);
            }
            return addMiscellaneous;
        }

        private void SaveMiscellaneousTabData(AddCondenserViewModel addCondensor)
        {
            try
            {
                Logger.PrintInfo("Function:SaveMiscellaneousTabData ~~~~~~ Save Miscellaneous Tab Data");

                // Miscellaneous Pop up data
                string miscellaneousData;
                DataTable dtMiscellaneous;
                if (addCondensor.MiscellaneousData.MiscellaneousOptionList == null)
                {
                    dtMiscellaneous = null;
                }
                else
                {
                    miscellaneousData = JsonConvert.SerializeObject(addCondensor.MiscellaneousData.MiscellaneousOptionList);
                    dtMiscellaneous = (DataTable)JsonConvert.DeserializeObject(miscellaneousData, (typeof(DataTable)));
                }

                DataTable condenserPropertiesDataTable;
                if (addCondensor.MiscellaneousData.CondenserPropertiesData == null || addCondensor.MiscellaneousData.CondenserPropertiesData == "[]")
                {
                    condenserPropertiesDataTable = null;
                }
                else
                {
                    condenserPropertiesDataTable = (DataTable)JsonConvert.DeserializeObject(addCondensor.MiscellaneousData.CondenserPropertiesData, (typeof(DataTable)));
                }


                var condenserProperties = new SqlParameter("param_CondenserPropertiesUDT", SqlDbType.Structured);
                condenserProperties.Value = condenserPropertiesDataTable;
                condenserProperties.TypeName = "CondenserPropertiesUDT";

                var miscellaneousDetails = new SqlParameter("param_MiscellaneousOptionsSelectionUDT", SqlDbType.Structured);
                miscellaneousDetails.Value = dtMiscellaneous;
                miscellaneousDetails.TypeName = "MiscellaneousOptionsSelectionUDT";

                var modelId = new SqlParameter("parm_ModelId", SqlDbType.Int);
                modelId.Value = addCondensor.CurrentCondenserModelID;

                CondenserDBContext.Database.ExecuteSqlCommand("exec dbo.spUpdateMiscellaneous @parm_ModelId,@param_CondenserPropertiesUDT,@param_MiscellaneousOptionsSelectionUDT", modelId, condenserProperties, miscellaneousDetails);


            }
            catch(Exception ex)
            {
                Logger.PrintError(ex);
            }
        }
        //Save ControlPanelData
        private void SaveControlPanelTabData(AddControlPanelViewModel controlPanel)
        {
            controlPanel.SelectedControlPanelOptions = JsonConvert.DeserializeObject<List<ControlPanelOptions>>(controlPanel.SelectedControlPanelOptionsList);
            DataTable SelectedControlPanelOptionsTable = ExtensionMethods.ToDataTable(controlPanel.SelectedControlPanelOptions.Select(m=> new { Id=m.ID}).ToList());
            //Calling Sp
            var modelId = new SqlParameter("param_ModelID", SqlDbType.Int);
            modelId.Value = controlPanel.ModelID;

            var ControlID = new SqlParameter("param_ControlID", SqlDbType.Int);
            ControlID.Value = controlPanel.ControlsID;

            var ControlMfgID = new SqlParameter("param_ControlMfgID", SqlDbType.Int);
            ControlMfgID.Value = controlPanel.ControlManufacturerID;

            var ControlAirSensorID = new SqlParameter("param_ControlAirSensorID", SqlDbType.Int);
            ControlAirSensorID.Value = controlPanel.ControlPanelAirSensorID;

            var ControlFuseAndBreakerID = new SqlParameter("param_ControlFuseAndBreakerID", SqlDbType.Int);
            ControlFuseAndBreakerID.Value = controlPanel.ControlFusesandBreakersID;

            var ControlTypeOfApplicationID = new SqlParameter("param_ControlTypeOfApplicationID", SqlDbType.Int);
            ControlTypeOfApplicationID.Value = controlPanel.ControlTypeofApplicationID;

            var ControlVoltageID = new SqlParameter("param_ControlVoltageID", SqlDbType.Int);
            ControlVoltageID.Value = controlPanel.ControlVoltageID;

            var ControlPanelOptionIdsUDT = new SqlParameter("param_ControlPanelOptionIdsUDT", SqlDbType.Structured);
            ControlPanelOptionIdsUDT.Value = SelectedControlPanelOptionsTable;
            ControlPanelOptionIdsUDT.TypeName = "ControlPanelOptionIds";

            CondenserDBContext.Database.ExecuteSqlCommand("exec dbo.spUpdateCondenserControlPanelTabData @param_ModelID,@param_ControlID, @param_ControlMfgID, @param_ControlAirSensorID, @param_ControlFuseAndBreakerID, @param_ControlTypeOfApplicationID, @param_ControlVoltageID, @param_ControlPanelOptionIdsUDT", modelId, ControlID,ControlMfgID, ControlAirSensorID, ControlFuseAndBreakerID, ControlTypeOfApplicationID, ControlVoltageID, ControlPanelOptionIdsUDT);
        }
        //Get ControlPanel Data
        private AddControlPanelViewModel SetDefaultListsAndDataForControlPanelTab(AddControlPanelViewModel addControlPanel, int modelID)
        {
            //set default list using lookups table
            addControlPanel.ControlFusesandBreakers = CondenserDBContext.ControlFusesandBreakers.OrderBy(x => x.ControlFusesandBreakers1).ToList();
            addControlPanel.ControlManufacturer = CondenserDBContext.ControlManufacturer.OrderBy(x => x.ControlManufacturer1).ToList();
            addControlPanel.ControlPanelAirSensor = CondenserDBContext.ControlPanelAirSensor.OrderBy(x => x.AirSensorOptions).ToList();
            addControlPanel.ControlPanelOptions = CondenserDBContext.ControlPanelOptions.OrderBy(x=> x.ControlPanelOptions1).ToList();
            addControlPanel.Controls = CondenserDBContext.Controls.OrderBy(x => x.ControlType).ToList();
            addControlPanel.ControlTypeofApplication = CondenserDBContext.ControlTypeofApplication.OrderBy(x => x.ControlTypeofApplication1).ToList();
            addControlPanel.ControlVoltage = CondenserDBContext.ControlVoltage.OrderBy(x => x.ControlVoltage1).ToList();
            addControlPanel.ControlPanelOptionList = JsonConvert.SerializeObject(addControlPanel.ControlPanelOptions.Select(m=> new { ID = m.ID, ControlPanelOptions1 =m.ControlPanelOptions1, ListPriceAdder=m.ListPriceAdder, BaaNFeature = m.BaaNFeature , BaaNOption = m.BaaNOption}).OrderBy(x=> x.ControlPanelOptions1));
            //initialize saved condenser model if exist
            var ControlPanelDetailsInDb = CondenserDBContext.ControlPanelDetails.Where(m => m.CondenserModel.ModelID == modelID).SingleOrDefault();
            if (ControlPanelDetailsInDb != null)
            {
                addControlPanel.ModelID = ControlPanelDetailsInDb.CondenserModel.ModelID;
                addControlPanel.ControlFusesandBreakersID = ControlPanelDetailsInDb.ControlFusesandBreakers.ControlFusesandBreakersID;
                addControlPanel.ControlManufacturerID = ControlPanelDetailsInDb.ControlManufacturer.ControlManufacturerID;
                addControlPanel.ControlPanelAirSensorID = ControlPanelDetailsInDb.ControlPanelAirSensor.AirSensorID;
                addControlPanel.ControlsID = ControlPanelDetailsInDb.Controls.ID;
                addControlPanel.ControlTypeofApplicationID = ControlPanelDetailsInDb.ControlTypeofApplication.ControlTypeofApplicationID;
                addControlPanel.SelectedControlPanelOptions = ControlPanelDetailsInDb.ControlPanelDetailsAndOptions.Select(m => m.ControlPanelOptions).ToList();
            }
            else
            {
                addControlPanel.SelectedControlPanelOptions.Add(addControlPanel.ControlPanelOptions[0]);
            }
            addControlPanel.SelectedControlPanelOptionsList = JsonConvert.SerializeObject(addControlPanel.SelectedControlPanelOptions.Select(m => new { ID = m.ID, ControlPanelOptions1 = m.ControlPanelOptions1, ListPriceAdder = m.ListPriceAdder, BaaNFeature = m.BaaNFeature, BaaNOption = m.BaaNOption }));
            return addControlPanel;
        }

    
        public ActionResult ChangeCondenserState(int modelId, int? state) 
        {
            CondenserDBContext.spChangeCondenserState(modelId, state, User.Identity.Name.Split('\\')[1]);

            if (state == null)
            {
                return RedirectToAction("Search", "Condenser");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public bool ChangeCondenserStateWithSearchState(int modelId, int? state) //Call From search screen and Tab screen
        {
            CondenserDBContext.spChangeCondenserState(modelId, state, User.Identity.Name.Split('\\')[1]);

            if (state == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private AddCondenserViewModel SetControlVisibility(AddCondenserViewModel addCondenser, HttpContext context)
        {

            if (Hussmann.Attribute.Authorization.GetUserType(context) != Convert.ToString(enUserType.Reader))
            {
                if (addCondenser.CurrentStateID == Convert.ToInt32(enState.Draft))
                {
                    if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord))
                    {
                        addCondenser.CanApproveRecord = true;
                    }
                    else
                    {
                        addCondenser.CanSubmitRecord = true;
                    }
                }
                else if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord) && addCondenser.CurrentStateID ==
                Convert.ToInt32(enState.Submitted))
                {
                    addCondenser.CanApproveRecord = true;
                    addCondenser.CanRejectRecord = true;
                    addCondenser.CanDeleteRecord = true;
                }
                else if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord) && addCondenser.CurrentStateID ==
                Convert.ToInt32(enState.Rejected))
                {
                    addCondenser.CanDeleteRecord = true;
                    addCondenser.CanMoveToDraft = true;
                }
                else if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord) && addCondenser.CurrentStateID ==
                Convert.ToInt32(enState.Approved))
                {
                    addCondenser.CanDeleteRecord = true;
                    addCondenser.CanMoveToDraft = true;
                }
                else if (Hussmann.Attribute.Authorization.CheckResponsibility(context, Role.CanApprovedRecord) && addCondenser.CurrentStateID ==
                Convert.ToInt32(enState.Deleted))
                {
                    addCondenser.CanApproveRecord = true;
                    addCondenser.CanMoveToDraft = true;
                }
            }
            return addCondenser;
        }

        public void DownloadCondenserExcelFile(int ModelID)
        {
            var CondenserModel = CondenserDBContext.
            CondenserModels.Where
            (x => x.ModelID == ModelID).Select(x => x).FirstOrDefault();

            CondenserExcelExport condenserFile = new CondenserExcelExport();
            IWorkbook workBook =  condenserFile.GenerateExcelFile(ModelID);

            using (var exportData = new MemoryStream())
            {
                Response.Clear();
                workBook.Write(exportData);
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", CondenserModel.ModelNumber + " (" + CondenserModel.Version + ") " + ".xlsx"));
                Response.BinaryWrite(exportData.GetBuffer());
                Response.End();
            }

        }

    }
}
