﻿//<--------- Edit Unit Cooler Details tab start -------------->
function OpenDesiredTab(tabIndex)
{

    var toIndex = tabIndex;

    $("#hdnRequestedTab").val(toIndex);
    var currentIndex = $("#hdnCurrentTab").val();
    var pricingGridValid = $("#hdnPricingGridValid").val();

    if ($("#details").valid())
    {
        if (currentIndex === 4)
        {
            
            var btn = $("#hdnButtonPreviousPrice");
            btn.click();

            var val = $("#hdnPricingGridValid").val();
            if (val === false || val === "False" || val === "false")
            {
                $("#hdnRequestedTab").val(currentIndex);
                    $('#wizard').bwizard('show', parseInt(currentIndex) - 1);
            }
            else
            {
                $("#details").submit();
            }

        }
        else if (currentIndex === 2)
        {
            var btn2 = $("#hdnButtonAccessory");
            btn2.click();

            var val2 = $("#hdnAccessoryGridValid").val();
            if (val2 === false || val2 === "False" || val2 === "false") {
                $("#hdnRequestedTab").val(currentIndex);
                $('#wizard').bwizard('show', parseInt(currentIndex) - 1);
                $("#hdnAccessoryGridValid").val(false);
            }
            else
            {
                $("#details").submit();
            }
        }
        else if (currentIndex === 3)
        {
            var btn3 = $("#hdnButtonConfiguration");
            btn3.click();

            var val3 = $("#hdnConfigurationGridDataValid").val();
            if (val3 === false || val3 === "False" || val3 === "false") {
                $("#hdnRequestedTab").val(currentIndex);
                $('#wizard').bwizard('show', parseInt(currentIndex) - 1);
            }
            else {
                $("#details").submit();
            }
        }
        else
        {
            setTimeout(function () {

                $("#details").submit();

            }, 1);

        }
       
    }
    else {
        $('#wizard').bwizard('show', parseInt(currentIndex)-1);
    }

}

function OpenDesiredCondenserTab(toTab)
{
    var Tab = {
        CondenserDetails: '_CondenserDetails',
        CondenserType: '_CondenserType',
        CondenserCapacity: '_CondenserCapacity',
        Miscellaneous: '_Miscellaneous',
        ControlPanle: '_ControlPanel',
        ReceiverType: '_ReceiverType',
        Pricing: '_Pricing'
    };

    $("#hdnCondenserRequestedTabName").val(toTab);
    var currentTab = $("#hdnCondenserCurrentTabName").val();

    //var form = $("#condenser")
    //    .removeData("validator")
    //    .removeData("unobtrusiveValidation");
    //$.validator.unobtrusive.parse(form);

    if ($("#condenser").valid())
    {
        if (currentTab === Tab.CondenserDetails)
        {
        
            $("#condenser").submit();
        }
        else if (currentTab === Tab.CondenserType )
        {
            $("#condenser").submit();
        }
        else if (currentTab === Tab.CondenserCapacity)
        {
            var btnHiddenCondenserCapacity = $("#hdnButtonCondenserCapacity");
            btnHiddenCondenserCapacity.click();

            var IsCondenserCapacityValid = $("#hdnCondenserCapacityGridValid").val();
            if (IsCondenserCapacityValid === false || IsCondenserCapacityValid === "False" || IsCondenserCapacityValid === "false") {
                $("#hdnCondenserRequestedTabName").val(currentTab);
                setTimeout(function () {
                    $('#_CondenserDetails').addClass('').removeClass('active');
                    document.getElementById(currentTab).classList.add('active');
                }, 10);
            }
            else {
                $("#condenser").submit();
            }
        }
        else if (currentTab === Tab.Miscellaneous)
        {
            var btnHiddenMiscellaneous = $("#hdnButtonMiscellaneous");
            btnHiddenMiscellaneous.click();

            var IsMiscellaneousValid = $("#hdnMiscellaneousGridValid").val();
            if (IsMiscellaneousValid === false || IsMiscellaneousValid === "False" || IsMiscellaneousValid  === "false") {
                $("#hdnCondenserRequestedTabName").val(currentTab);
                setTimeout(function () {
                    $('#_CondenserDetails').addClass('').removeClass('active');
                    document.getElementById(currentTab).classList.add('active');
                }, 10);
            }
            else {
                $("#condenser").submit();
            }
        }
        else if (currentTab === Tab.ControlPanle)
        {
            var controlPanelValid = SetValueInJson();
            if (controlPanelValid === true)
            {
                $("#condenser").submit();
            }
            else
            {
                setTimeout(function () {
                    $('#_CondenserDetails').addClass('').removeClass('active');
                    document.getElementById(currentTab).classList.add('active');
                }, 10);
            }
        }
        else if (currentTab === Tab.ReceiverType)
        {
            var btnReceiver = $('#hdnButtonReceiverTypeGrid');
            btnReceiver.click();
            $("#condenser").submit();
        }
        else if (currentTab === Tab.Pricing)
        {
            var btnPricing1 = $("#hdnButtonControlPanelPricing");
            btnPricing1.click();
            var btnPricing2 = $('#hdnButtonRecieverPricing');
            btnPricing2.click();
            var btnPricing3 = $('#btnMiscellaneousPricing');
            btnPricing3.click();
            var btnPricing4 = $('#btnLegPricingData');
            btnPricing4.click();

            $("#condenser").submit();
        }
    }
    
}

function PricingTabHold(index)
{
    $('#wizard').bwizard('show', index);
}

//global variable
var isValidate;

//  <--Open Compressor Tab -->
function OpenDesiredCompressorTab(toTab) {
    var Tab = {
        CompressorDetails: '_CompressorDetails',
        RLADetails: '_RLADetails',
        BOMDetails: '_BOMDetails',
        Coefficients: '_Coefficients'
    };
    if (toTab === "_RLADetailsBack")
    {
        toTab = "_RLADetails";

    }
    $("#hdnCompressorRequestedTabName").val(toTab);
    var currentTab = $("#hdnCompressorCurrentTabName").val();

    //var form = $("#condenser")
    //    .removeData("validator")
    //    .removeData("unobtrusiveValidation");
    //$.validator.unobtrusive.parse(form);

    if ($("#compressor").valid()) {
        if (currentTab === Tab.CompressorDetails) {

            $("#compressor").submit();
        }
        else if (currentTab === Tab.RLADetails) {
            var btn1 = $("#hdnBtnSetRLADetails");
            btn1.click();
            if (($('#hdnIsFieldValid').val() === true || $('#hdnIsFieldValid').val() === 'true') && $('#hdnIsRLAValid').val() === "false") {
                $('#lblRLAFieldsError').hide();
                $("#compressor").submit();
               
            }
            else if ($('#hdnIsRLAValid').val() === "true")
            {
                $("#lblRLAFieldsError").text("Invalid Entry in RLA");
                $('#lblRLAFieldsError').show();
            }
            else
            {
                $("#lblRLAFieldsError").text("Missing required fields (*)");
                $('#lblRLAFieldsError').show();
            }
      
        }
        else if (currentTab === Tab.BOMDetails) {
            isValidate = validateBOMDetails();
            if (isValidate) {
                $("#compressor").submit();
            }
            else {
                resetToCurrentTab(activeTab);
            }
        }
        else if (currentTab === Tab.Coefficients) {
            var btn = $("#hdnBtnSetCoefficients");
            btn.click();
            var IsCoefficientsGridValid = $("#hdnCoefficientsGridValid").val();
            if (IsCoefficientsGridValid == false || IsCoefficientsGridValid == "False" || IsCoefficientsGridValid == "false") {
                $("#hdnCompressorRequestedTabName").val(activeTab);
                setTimeout(function () {
                    $('#_CompressorDetails').addClass('').removeClass('active');
                    document.getElementById(activeTab).classList.add('active');
                }, 10);
            }
            else {
                $("#compressor").submit();
            }
        }
    }
    else {
        resetToCurrentTab(activeTab);
    }

}

function resetToCurrentTab(activeTab) {
    setTimeout(function () {
        $('#_CompressorDetails').addClass('').removeClass('active');
        document.getElementById(activeTab).classList.add('active');
        $("#hdnCompressorRequestedTabName").val(activeTab);
    }, 10);
}

//< -------- Edit Unit cooler Details tab end ---------------->

//<--------- Dashboard starts -------------->



//< -------- Dashboard end ---------------->

//Open RLADetails Tab if empty
function OpenRLADetailsTab()
{
    $("#hdnCompressorRequestedTabName").val("_RLADetails");
    var currentTab = $("#hdnCompressorCurrentTabName").val();

    if (currentTab === "_BOMDetails") {
        $("#compressor").submit();
    }
}


//--Validations --
//function validateRLADetails() {
//    if ($("#voltageType").val() === "" || $("#voltageType").val() === null) {
//        $("#validationVoltageType").show();
//        return false;
//    }
//    else {
//        $("#validationVoltageType").hide();
//        return true;
//    }
//}

function validateBOMDetails() {

    var compVendorValidate = true,
        tempRangeValidate = true;

    return true;
    //if ((!$("#txtCompVendor").attr("value")) && ($("#txtCompVendor").val() == "" || $("#txtCompVendor").val() == null)) {
    //    $("#validationCompVendor").show();
    //    compVendorValidate = false;
    //}
    //else {
    //    $("#validationCompVendor").hide();
    //    compVendorValidate = true;
    //}

    //if ((!$("#txtTempRange").attr("value")) && ($("#txtTempRange").val() == "" || $("#txtTempRange").val() == null)) {
    //    $("#validationTempRange").show();
    //    tempRangeValidate = false;
    //}
    //else {
    //    $("#validationTempRange").hide();
    //    tempRangeValidates = true;
    //}

    //if ((!compVendorValidate) ||  (!tempRangeValidate)) {
    //    return false;
    //}
    //else {
    //    return true;
    //}
}