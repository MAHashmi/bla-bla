﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class ControlPanelDetailsViewModel
    {
        public int ControlPanelDetailID { get; set; }
        public string Controls { get; set; }
        public string ControlManufacturer { get; set; }
        public string ControlPanelAirSensor { get; set; }
        public string ControlFusesandBreakers { get; set; }
        public string ControlTypeofApplication { get; set; }
        public string ControlVoltage { get; set; }

    }
}