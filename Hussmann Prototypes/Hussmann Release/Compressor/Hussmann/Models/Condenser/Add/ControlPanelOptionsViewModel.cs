﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class ControlPanelOptionsViewModel
    {
        public int ID { get; set; }
        public string ControlPanelOptionsName { get; set; }
        public Nullable<decimal> ListPriceAdder { get; set; }
        public string BaaNFeature { get; set; }
        public string BaaNOption { get; set; }
        public Nullable<int> ControlPanelDetailsID { get; set; }
    }
}