﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class MiscellaneousPriceDetails
    {
        public int MiscellaneousPricingID { get; set; }
       
        public Nullable<decimal> MiscellaneousPrice { get; set; }
        public Nullable<int> MiscellenousOptionsID { get; set; }
        public string MiscellenousOptionsName { get; set; }
        public Nullable<int> GeometryID { get; set; }
        public Nullable<int> ModelID { get; set; }
    }
}