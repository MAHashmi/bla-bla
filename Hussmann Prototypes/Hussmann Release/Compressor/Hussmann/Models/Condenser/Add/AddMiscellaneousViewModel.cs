﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CondenserDataModel;

namespace Hussmann.Models.Condenser.Add
{
    public class AddMiscellaneousViewModel
    {
        public List<MiscellaneousOptionDetails> MiscellaneousOptionList { get; set; }

        public string CondenserPropertiesData { get; set; }
        public string VoltageTypeList { get; set; }
    }
}