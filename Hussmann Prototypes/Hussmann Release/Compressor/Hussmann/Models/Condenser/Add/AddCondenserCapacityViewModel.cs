﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CondenserDataModel;

namespace Hussmann.Models.Condenser.Add
{
    public class AddCondenserCapacityViewModel
    {
        public string CondenserCapacityGridData { get; set; }
    }
}