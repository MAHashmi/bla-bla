﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class ReceiverTypeDetails
    {
        public int RcvrCapacityID { get; set; }
        public int ReceiverTypeID { get; set; }
        public int RefrigerantType_ID { get; set; }
        public Nullable<decimal> ReceiverCapacity { get; set; }
        public Nullable<int> ModelID { get; set; }
        public string RefrigerantType { get; set; }
        public string ReceiverSize { get; set; }


    }
    public class CustomReceiverTypeDetails
    {
        public int RefrigerantType_ID { get; set; }
        public string RefrigerantType { get; set; }
        public List<CustomDict> GridValues { get; set; }
        public int  ModelID { get; set; }

    }
    public class CustomDict
    {
        public string key { get; set; }
        public Nullable<decimal> value { get; set; }

    }
}