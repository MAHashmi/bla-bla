﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class AddCondenserViewModel
    {
        public AddCondenserDetailsViewModel CondenserDetailsData { get; set; }
        public AddCondenserTypeViewModel CondenserTypeData { get; set; }
        public AddCondenserCapacityViewModel CondenserCapacityData { get; set; }
        public AddMiscellaneousViewModel MiscellaneousData { get; set; }
        public AddCondenserPricingViewModel CondenserPricingData { get; set; }
        public AddCondenserReceiverTypeViewModel CondenserReceiverTypeData { get; set; }
        public AddControlPanelViewModel ControlPanel { get; set; }

        public int CurrentTab { get; set; }
        public int RequestedTab { get; set; }
        public int CurrentCondenserModelID { get; set; }
        public string CurrentCoilState { get; set; }
        public int CurrentCoilVersion { get; set; }
        public int CurrentStateID { get; set; }

        public string CurrentTabName { get; set; }
        public string RequestedTabName { get; set; }

        // Tab Validations
        public bool IsCondenserCapacityGridValid { get; set; }
        public bool IsMiscellaneousGridValid { get; set; }
        public bool AllowEdit { get; set; }

        // Actions Visibility
        public bool CanApproveRecord { get; set; }
        public bool CanSubmitRecord { get; set; }
        public bool CanRejectRecord { get; set; }
        public bool CanDeleteRecord { get; set; }
        public bool CanMoveToDraft { get; set; }
        public bool IsEditedAfterApprove { get; set; }

        // Redirection
        public string RedirectTo { get; set; }
        public string SearchQuery { get; set; }
    }
}