﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CondenserDataModel;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Hussmann.Models.Condenser.Add
{
    public class AddCondenserDetailsViewModel
    {
        public List<CondenserType> CondenserTypes { get; set; }
        public List<CondenserGeometry> GeometryTypes { get; set; }
        public List<CondenserFamily> ModelFamily { get; set; }
        public List<CondenserFanMotor> FanMotors { get; set; }
        public List<CondenserBrand_lkp> BrandDetails { get; set; }
        public List<ModelSuffix> ModelSuffixes { get; set; }

        [Required(ErrorMessage = "Model Number is required.")]
        [StringLength(15)]
        [Remote("IsModelExists", "Condenser", ErrorMessage = "Model Number already exists.")]
        public string ModelNumber { get; set; }

        public int BrandID { get; set; }
        public int ModelFamilyID { get; set; }

        [Required(ErrorMessage = "Cubic Feet Per Minute is required.")]
        [Range(typeof(int), "0", "99999", ErrorMessage = "Please enter values between 0 and 99999.")]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "Please enter values between 0 and 99999.")]
        public int CubicFeetPerMinute { get; set; }

        [Required(ErrorMessage = "R404 Summer Charge is required.")]
        [Range(typeof(int), "0", "9999", ErrorMessage = "Please enter values between 0 and 9999.")]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "Please enter values between 0 and 9999.")]
        public Nullable<int> R404SummerCharge { get; set; }

        [Required(ErrorMessage = "R404 Winter Charge is required.")]
        [Range(typeof(int), "0", "9999", ErrorMessage = "Please enter values between 0 and 9999.")]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "Please enter values between 0 and 9999.")]
        public Nullable<int> R404WinterCharge { get; set; }

        [Required(ErrorMessage = "Sound Decibals 10 is required.")]
        [Range(typeof(int), "0", "9999", ErrorMessage = "Please enter values between 0 and 9999.")]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "Please enter values between 0 and 9999.")]
        public Nullable<int> SoundDecibals10 { get; set; }

        [Required(ErrorMessage = "Unit Weight is required.")]
        [Range(typeof(int), "0", "9999", ErrorMessage = "Please enter values between 0 and 9999.")]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "Please enter values between 0 and 9999.")]
        public Nullable<int> UnitWeight { get; set; }

        [Required(ErrorMessage = "Length is required.")]
        [Range(typeof(int), "0", "9999", ErrorMessage = "Please enter values between 0 and 9999.")]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "Please enter values between 0 and 9999.")]
        public int Length { get; set; }

        [Required(ErrorMessage = "Width is required.")]
        [Range(typeof(int), "0", "9999", ErrorMessage = "Please enter values between 0 and 9999.")]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "Please enter values between 0 and 9999.")]
        public int Width { get; set; }

        [Required(ErrorMessage = "Height is required.")]
        [Range(typeof(int), "0", "9999", ErrorMessage = "Please enter values between 0 and 9999.")]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "Please enter values between 0 and 9999.")]
        public int Height { get; set; }

        public Nullable<int> CondenserTypeID { get; set; }
        
        public Nullable<int> ModelSuffixID { get; set; }
        public Nullable<int> GeometryID { get; set; }
        public Nullable<int> CondFanMotorID { get; set; }

        public string CondenserTypesName { get; set; }
        public string GeometryTypesName { get; set; }
        public string ModelFamilyName { get; set; }
        public string FanMotorsName { get; set; }
        public string BrandDetailsName { get; set; }
        public string ModelSuffixesName { get; set; }

        public bool Title24Compliant { get; set; }
        public System.DateTime LastModifiedOn { get; set; }
    }
}