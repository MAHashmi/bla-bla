﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class ControlPanelPriceDetails
    {
        public int PriceID { get; set; }
        public Nullable<int> GeometryID { get; set; }
        public string GeometryType { get; set; }
        public Nullable<decimal> ListPrice { get; set; }
        public Nullable<int> ModelID { get; set; }
    }
}