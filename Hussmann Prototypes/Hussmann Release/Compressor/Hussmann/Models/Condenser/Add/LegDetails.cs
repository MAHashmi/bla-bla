﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class LegDetails
    {/// <summary>
     /// Do not change the sequence of the properties
     /// </summary>
        public int ID { get; set; }
        public string LegTypeName { get; set; }
        public Nullable<int> CondenserTypeID { get; set; }
        public Nullable<int> LegTypeID { get; set; }
        public Nullable<bool> LegAvailable { get; set; }
        public Nullable<bool> Standard { get; set; }
        public Nullable<int> ModelID { get; set; }
    }
}