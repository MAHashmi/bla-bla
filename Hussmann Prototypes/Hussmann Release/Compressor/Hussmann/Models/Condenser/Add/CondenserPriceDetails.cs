﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class CondenserPriceDetails
    {
        public int CondenserPricingID { get; set; }
        public string ModelSuffix { get; set; }

        [Range(typeof(decimal), "1", "99999999", ErrorMessage = "Please enter numbers between 1 and 99999999.")]
        [RegularExpression(@"^([1-9]+)(\.)*([0-9]{0,4})$", ErrorMessage = "Fractional part cannot be more than 4 places.")]
        public Nullable<decimal> EpoxyElectroFinCoatedCoil { get; set; }

        [Range(typeof(decimal), "1", "99999999", ErrorMessage = "Please enter numbers between 1 and 99999999.")]
        [RegularExpression(@"^([1-9]+)(\.)*([0-9]{0,4})$", ErrorMessage = "Fractional part cannot be more than 4 places.")]
        public Nullable<decimal> PolyesterFinCoating { get; set; }

        [Range(typeof(decimal), "1", "99999999", ErrorMessage = "Please enter numbers between 1 and 99999999.")]
        [RegularExpression(@"^([1-9]+)(\.)*([0-9]{0,4})$", ErrorMessage = "Fractional part cannot be more than 4 places.")]
        public Nullable<decimal> CopperFin { get; set; }

        public Nullable<int> ModelSuffixID { get; set; }
        [Range(typeof(decimal), "1", "99999999", ErrorMessage = "Please enter numbers between 1 and 99999999.")]
        [RegularExpression(@"^([1-9]+)(\.)*([0-9]{0,4})$", ErrorMessage = "Fractional part cannot be more than 4 places.")]
        public Nullable<decimal> BaseListPrice { get; set; }
        public Nullable<int> CondenserTypeID { get; set; }
        public string CondenserType { get; set; }
        public Nullable<int> ModelID { get; set; }
    }
}