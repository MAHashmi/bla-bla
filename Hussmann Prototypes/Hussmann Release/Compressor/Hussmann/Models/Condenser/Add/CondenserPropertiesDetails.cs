﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class CondenserPropertiesDetails
    {
        public int CondPropID { get; set; }
        public int ModelID { get; set; }
        public int VoltageTypeID { get; set; }
        public string VoltageTypeName { get; set; }
        public Nullable<decimal> CondenserAmps { get; set; }
        public Nullable<decimal> MCA { get; set; }
        public Nullable<decimal>  MOPD { get; set; }
    }
}