﻿using CondenserDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class AddControlPanelViewModel
    {
        public List<ControlFusesandBreakers> ControlFusesandBreakers { get; set; }
        public List<ControlManufacturer> ControlManufacturer { get; set; }
        public List<ControlPanelAirSensor> ControlPanelAirSensor { get; set; }
        public List<Controls> Controls { get; set; }
        public List<ControlTypeofApplication> ControlTypeofApplication { get; set; }
        public List<ControlVoltage> ControlVoltage { get; set; }
        public List<ControlPanelOptions> ControlPanelOptions { get; set; }
        public string ControlPanelOptionList { get; set; }

        public int ControlFusesandBreakersID { get; set; }
        public int ControlManufacturerID { get; set; }
        public int ControlPanelAirSensorID { get; set; }
        public int ControlsID { get; set; }
        public int ControlTypeofApplicationID { get; set; }
        public int ControlVoltageID { get; set; }
        public int ModelID { get; set; }
        public List<ControlPanelOptions> SelectedControlPanelOptions = new List<ControlPanelOptions>();
        public string SelectedControlPanelOptionsList { get; set; }

    }
}