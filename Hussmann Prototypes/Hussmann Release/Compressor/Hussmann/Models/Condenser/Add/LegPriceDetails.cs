﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class LegPriceDetails
    {
        public int LegPriceID { get; set; }
   
        public Nullable<decimal> LegPrice { get; set; }
        public Nullable<int> LegTypeID { get; set; }
        public string LegType { get; set; }
        public Nullable<int> GeometryID { get; set; }
 
        public Nullable<int> ModelID { get; set; }
    }
}