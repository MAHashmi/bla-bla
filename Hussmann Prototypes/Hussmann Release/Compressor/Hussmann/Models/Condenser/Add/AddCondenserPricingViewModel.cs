﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class AddCondenserPricingViewModel
    {
        public CondenserPriceDetails condenserPricing { get; set; }
        public List<ControlPanelPriceDetails> lstControlPanelPricing { get; set; }
        public List<LegPriceDetails> lstLegPricing { get; set; }
        public List<ReceiverPriceDetails> lstReceiverPricing { get; set; }
        public List<MiscellaneousPriceDetails> lstMiscellaneousPricing { get; set; }
        public string controlPanelPricing { get; set; }
        public string legPricing { get; set; }
        public string miscellaneousPricing { get; set; }
        public string receiverPricing { get; set; }
        public string geometryType { get; set; }
      
    }
    
}