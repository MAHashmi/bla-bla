﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class ReceiverDetails
    {
    /// <summary>
    /// Do not change the sequence of the properties
    /// </summary>
        public int ReceiverID { get; set; }
        public int GeometryID { get; set; }
        public Nullable<int> ReceiverTypeID { get; set; }
        public Nullable<decimal> ReceiverPrice { get; set; }
        public Nullable<decimal> HeatTapeInsulPrice { get; set; }
        public Nullable<int> ReceiverQty { get; set; }
        public Nullable<int> ModelID { get; set; }
        public Nullable<bool> IsSelected { get; set; }
    }
}