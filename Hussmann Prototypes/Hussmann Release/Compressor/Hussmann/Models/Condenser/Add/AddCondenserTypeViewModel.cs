﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CondenserDataModel;

namespace Hussmann.Models.Condenser.Add
{
    public class AddCondenserTypeViewModel
    {
        public int CondenserTypeID { get; set; }
        public string CondenserTypeName { get; set; }

        public int GeometryID { get; set; }
        public string GeometryTypeName { get; set; }

        public List<LegDetails> LegDetails { get; set;}

        public List<ReceiverDetails> ReceiverDetails { get; set; }

        public List<ReceiverType> ReceiverTypeList { get; set; }

        public List<LegType> LegTypeList { get; set; }
    }
}