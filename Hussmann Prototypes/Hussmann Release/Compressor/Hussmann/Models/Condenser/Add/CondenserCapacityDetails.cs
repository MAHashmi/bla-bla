﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class CondenserCapacityDetails
    {
        public string RefregerantTypeName { get; set; }
        public int CondCapacityID_ { get; set; }
        public int ModelID { get; set; }
        public int RefrigerantTypeID { get; set;}
        public Nullable<decimal> Capacity { get; set; }
    }
}