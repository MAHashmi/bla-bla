﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CondenserDataModel;
namespace Hussmann.Models.Condenser.Add
{
    public class AddCondenserReceiverTypeViewModel
    {
       
        public List<ReceiverTypeDetails> lstReceiverType { get; set; }
        public List<CustomReceiverTypeDetails> lstCustomReceiverType { get; set; }
        public string receiverType { get; set; }
    }

}