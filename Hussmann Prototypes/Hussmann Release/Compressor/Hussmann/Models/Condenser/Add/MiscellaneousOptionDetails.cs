﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Condenser.Add
{
    public class MiscellaneousOptionDetails
    {
        public int MiscellaneousPricingID { get; set; }
        public int MiscellenousOptionsID { get; set; }
        public int ModelID { get; set; }
        public bool IsSelected { get; set; }
        public string MiscellenousOption { get; set; }
    }
}