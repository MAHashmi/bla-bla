﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hussmann.Models.Condenser.RefrigerantType
{
    public class RefrigerantTypeViewModel
    {
        public int RefrigerantType_ID { get; set; }

        [Display(Name = "Refrigerant Type")]
        [Required(ErrorMessage = "Refrigerant Type is required.")]
        [StringLength(1)]
        [Remote("IsRefrigerantTypeExists", "Condenser", ErrorMessage = "Refrigerant Type already exists." , AdditionalFields = "RefrigerantType_ID")]
        public string RefrigerantType1 { get; set; }

        [Required(ErrorMessage = "Refrigerant Description is required.")]
        [StringLength(100)]
        [Display(Name = "Refrigerant Description")]
        public string RefrigerantDescr { get; set; }

        [Required]
        public bool Active { get; set; } = true;
        [Required]
        public bool Obsolete { get; set; }
    }
}