﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Compressor
{
    internal class CoefficientTable : DataTable
    {
        public CoefficientTable()
        {
            Columns.Add("C0", typeof(decimal));
            Columns.Add("C1", typeof(decimal));
            Columns.Add("C2", typeof(decimal));
            Columns.Add("C3", typeof(decimal));
            Columns.Add("C4", typeof(decimal));
            Columns.Add("C5", typeof(decimal));
            Columns.Add("C6", typeof(decimal));
            Columns.Add("C7", typeof(decimal));
            Columns.Add("C8", typeof(decimal));
            Columns.Add("C9", typeof(decimal));
            Columns.Add("W0", typeof(decimal));
            Columns.Add("W1", typeof(decimal));
            Columns.Add("W2", typeof(decimal));
            Columns.Add("W3", typeof(decimal));
            Columns.Add("W4", typeof(decimal));
            Columns.Add("W5", typeof(decimal));
            Columns.Add("W6", typeof(decimal));
            Columns.Add("W7", typeof(decimal));
            Columns.Add("W8", typeof(decimal));
            Columns.Add("W9", typeof(decimal));
            Columns.Add("SuctMin", typeof(decimal));
            Columns.Add("SuctMax", typeof(decimal));
            Columns.Add("MaxCondensingTemp", typeof(decimal));
            Columns.Add("Hertz", typeof(string));
            Columns.Add("refrigerantTypeId", typeof(int));
            Columns.Add("ModelID", typeof(int));
        }
    }
}