﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Compressor.Add
{
    public class VendorTempRangeTempAppViewModel
    {
        public int VendorTempRange_ID { get; set; }
        public string VendorTempRangeType { get; set; }
        public string EconomizerSetting { get; set; }
        public Nullable<int> LiquidTemp { get; set; }
        public string HussTempApp { get; set; }
        public string HussTempAppDescr { get; set; }
        public int HussmanTempAppID { get; set; }
    }
}