﻿using Hussmann.Models.Compressor.Add;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Compressor
{
    public class AddCompressorViewModel
    {
        public AddCompressorDetailsViewModel CompressorDetailsViewModel { get; set; }

        public int CurrentTab { get; set; }
        public int RequestedTab { get; set; }
        public int CurrentCompressorModelID { get; set; }
        public string CurrentCompressorState { get; set; }
        public int CurrentCompressorVersion { get; set; }
        public int CurrentStateID { get; set; }

        public string CurrentTabName { get; set; }
        public string RequestedTabName { get; set; }
        public string Operation { get; set; }

        // Tab Validations
        public bool IsCondenserCapacityGridValid { get; set; }
        public bool IsCoefficientsGridValid { get; set; } = true;
        public bool IsMiscellaneousGridValid { get; set; }
        public bool AllowEdit { get; set; }

        // Actions Visibility
        public bool CanApproveRecord { get; set; }
        public bool CanSubmitRecord { get; set; }
        public bool CanRejectRecord { get; set; }
        public bool CanDeleteRecord { get; set; }
        public bool CanMoveToDraft { get; set; }
        public bool IsEditedAfterApprove { get; set; }

        // Redirection
        public string RedirectTo { get; set; }
        public string SearchQuery { get; set; }

        public AddRLADetailsViewModel RLADetails { get; set; }
        public AddBOMDetailsViewModel BOMDetails { get; set; }
        public AddCoefficientsViewModel Coefficients { get; set; }
    }
}