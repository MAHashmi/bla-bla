﻿using CompressorDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Compressor.Add
{
    public class AddCoefficientsViewModel
    {
        public string CompressorCoefficients { get; set; }
        public List<Coefficient> CompressorCoefficientsList { get; set; }
        public bool isCoefficientsDataExists { get; set; }
        public int ReturnGasID { get; set; }
        public List<ReturnGa> ReturnGasTemperatures { get; set; }
        public List<CompressorDataModel.RefrigerantType> RefrigerantTypes { get; set; }
    }
}