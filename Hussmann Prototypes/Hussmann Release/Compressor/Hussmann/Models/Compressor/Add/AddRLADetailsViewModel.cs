﻿using CompressorDataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Compressor.Add
{
    public class AddRLADetailsViewModel
    {
        public int CompRLA_ID { get; set; }
        public int VoltageType { get; set; }
        public int CompModelID { get; set; }
     
        public decimal RLA { get; set; }
        public int VendorVoltageCode_ID { get; set; }

        public string WiringSizeID { get; set; }
        public string RLADetails { get; set; }
        public string VendorVoltageCode { get; set; }
        public string Voltages { get; set; }
        public string NEMAID { get; set; }
        public string DPID { get; set; }
        public string BreakerID { get; set; }

        public List<VendorVoltageCode> VendorVoltageCodes { get; set; }
        public List<spGetVoltageDetails_Result> VoltageDetails { get; set; }
        public List<NEMA> listNEMA { get; set; }
        public List<DP> listDP { get; set; }
        public List<Breaker> listBreaker { get; set; }
        public List<WiringSize> listWiringSize { get; set; }

    }
}