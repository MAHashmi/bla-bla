﻿using CompressorDataModel;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hussmann.Models.Compressor
{
    public class AddCompressorDetailsViewModel
    {
        [Required(ErrorMessage = "Model Number is required.")]
        [StringLength(15)]
        [Remote("IsModelExists", "Compressor", ErrorMessage = "Model Number already exists.")]
        public string ModelNumber { get; set; }
        [Required]
        public int CompSeries_ID { get; set; }
        [StringLength(8)]
        [Required(ErrorMessage = "Normal HP is required.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Normal HP must be a whole number")]
        public string NomHorsepower { get; set; }
        [RegularExpression(@"^([0-9]{0,5})(\.)*([0-9]{0,9})$", ErrorMessage = "Input valid decimal number with 9 decimal places.")]
        [Required(ErrorMessage = "Real HP is required.")]
        public decimal RealHorsePower { get; set; }
        public int Version { get; set; }
        public Nullable<int> StateID { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime LastModifiedOn { get; set; }
        public string ActionBy { get; set; }
        public string Note { get; set; }
        public Nullable<int> CapReduct_ID { get; set; }
        public Nullable<int> CompType_ID { get; set; }
        public Nullable<int> RefrigerantType_ID { get; set; }
        public Nullable<int> CompVendor_ID { get; set; }
        

        public List<CompSery> CompSeries { get; set; }
        public List<CapacityReduction> CapacityReduction { get; set; }
    }
}