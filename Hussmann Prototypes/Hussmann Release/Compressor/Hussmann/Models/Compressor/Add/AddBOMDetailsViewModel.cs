﻿using CompressorDataModel;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Compressor.Add
{
    public class AddBOMDetailsViewModel
    {
        public int CompBOM_ID { get; set; }
        public int CompRLA_ID { get; set; }
        public int? HussTempApp_ID { get; set; }
        public int? VendorBOMCode_ID { get; set; }
        public int CompType_ID { get; set; }
        [StringLength(4)]
        [Required(ErrorMessage = "The field Discharge Line Size is required.")]
        public string DiscLine { get; set; }
        [StringLength(4)]
        [Required(ErrorMessage = "The field Suction Line Size Link is required.")]
        public string SuctLine { get; set; }
        [StringLength(40)]
        public string CompKitProtocol { get; set; }
        [StringLength(40)]
        public string CompKitHSeries { get; set; }
        [StringLength(40)]
        public string CompKitRack { get; set; }
        public int? CompVendor_ID { get; set; }
        public List<CompType> CompTypeList { get; set; }
        public List<CompVendor> CompVendorList { get; set; }
        public List<VendorBOMCode> VendorBOMCodeList { get; set; }
        public List<HussmannTempApp> HussmannTempAppList { get; set; }
        public List<SuctLineSize> SuctLineSizes { get; set; }
        public List<DiscLineSize> DiscLineSizes { get; set; }
    }
}