﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Compressor
{
    public class CompRLAVoltageTable 
    {
        public int? CompRLAVoltage_ID { get; set; }
        public int? CompRLA_ID { get; set; }
        public int Voltage_ID { get; set; }

    }
}