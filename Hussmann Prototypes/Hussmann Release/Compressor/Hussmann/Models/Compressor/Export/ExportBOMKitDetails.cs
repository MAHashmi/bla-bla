﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Compressor.Export
{
    public class ExportBOMKitDetails
    {
        public string CompressorKitProtocol { get; set; }
        public string CompressorKitHSeries { get; set; }
        public string CompressorKitRack { get; set; }
    }
}