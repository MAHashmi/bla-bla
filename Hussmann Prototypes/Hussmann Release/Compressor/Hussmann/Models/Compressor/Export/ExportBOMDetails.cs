﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.Compressor.Export
{
    public class ExportBOMDetails
    {
        public string CompressorType { get; set; }
        public string CompressorVendor { get; set; }
        public string HussmannTempApplication { get; set; }
        public string VendorBOMCode { get; set; }
        public string SuctionLineSize { get; set; }
        public string DischargeLineSize { get; set; }
    }
}