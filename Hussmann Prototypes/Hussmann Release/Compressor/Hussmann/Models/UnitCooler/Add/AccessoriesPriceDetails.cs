﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hussmann.Models.UnitCooler.Add
{
    public class AccessoriesPriceDetails
    {
        public int CoilAccessoryPricingID { get; set; }
        [RegularExpression(@"^(\d{1,16})(\.)*(\d{0,2})$", ErrorMessage = "Price can not have more than 16 whole numbers and 2 decimal places")]
        public Nullable<decimal> CoilAccsListPrice { get; set; }
        public Nullable<int> CoilFamilyID { get; set; }
        public Nullable<int> ModelID { get; set; }
        public string CoilAccsPartDescription { get; set; }

    }
}