﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.UnitCooler.Add
{
    public class DefrostConfigViewModel
    {
        public int DefrostConfigID { get; set; }
        public string DefrostConfigCode { get; set; }
        public string DefrostConfigName { get; set; }
        public bool IsChecked { get; set; }
    }
}