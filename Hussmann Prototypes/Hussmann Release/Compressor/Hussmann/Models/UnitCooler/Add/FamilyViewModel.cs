﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.UnitCooler.Add
{
    public class FamilyViewModel
    {
        public int CoilFamilyID { get; set; }
        public string CoilFamilyName { get; set; }
    }
}