﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.UnitCooler.Add
{
    public class YearViewModel
    {
        public int YearID { get; set; }
        public int Year { get; set; }

    }
}