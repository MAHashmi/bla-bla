﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UnitCoolerDataModel;
using Hussmann.Models.UnitCooler.Add;
using System.Web.Mvc;

namespace Hussmann.Models.UnitCooler
{
    public class AddUnitCoolerDetailsViewModel
    {
        public List<CoilFamily>  FamilyDetails { get; set; }
        public List<CoilBrand_lkp> BrandDetails { get; set; }
        public List<YearModel> YearDetails { get; set; }
        public List<YearModel> MakeDetails { get; set; }
        public List<VendorViewModel> VendorDetails { get; set; }

        [Required(ErrorMessage ="Model Number is required.")]
        [StringLength(15)]
        [Remote("IsModelExists", "UnitCooler", ErrorMessage ="Model Number already exists.")]
        public string ModelNumber { get; set; }

        public string BrandID { get; set; }
        public string FamilyID { get; set; }
        public string YearID { get; set; }
        public string MakeID { get; set; }

        [Required(ErrorMessage = "Please enter numbers from 1-10 only.")]
        [RegularExpression("(0?[1-9][0-9]*)", ErrorMessage = "Please enter numbers from 1-10 only.")]
        [Range(1, 10, ErrorMessage = "Please enter numbers from 1-10 only.")]
        public int NumberOfFans { get; set; }



        [Range(typeof(int), "0", "99999", ErrorMessage = "Please enter positive numbers between 0 and 99999.")]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "Please enter positive numbers between 0 and 99999.")]
        public Nullable<int> CapacityPerDegreeTDNeg20 { get; set; }


        [Range(typeof(int), "0", "99999", ErrorMessage = "Please enter positive numbers between 0 and 99999.")]
        [RegularExpression(@"([0-9]+)", ErrorMessage = "Please enter positive numbers between 0 and 99999.")]
        public Nullable<int> CapacityPerDegreeTDPos25 { get; set; }
        public string Coilfamily { get; set; }
        public string CoilBrand { get; set; }
        public string ModelYear { get; set; }
        public string MakeYear { get; set; }
        public int Version { get; set; }
        public System.DateTime LastModifiedOn { get; set; }
    }
}