﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.UnitCooler.Add
{
    public class BrandViewModel
    {
        public int BrandID { get; set; }
        public string CoilBrandName { get; set; }
        public string CoilSubBrandName { get; set; }
    }
}