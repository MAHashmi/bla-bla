﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hussmann.Models.UnitCooler.Add
{
    public class HeatExchangerViewModel
    {
        public int HeatExchgrPricingID { get; set; }
        [Required(ErrorMessage = "Conductor Unit HP required")]
        public decimal CondUnitHP { get; set; }
        [Required(ErrorMessage = "Freezer BTU required")]
        public int FreezerBTU { get; set; }
        [Required(ErrorMessage = "Cooler BTU required")]
        public int CoolerBTU { get; set; }
        [StringLength(15)]
        public string PartNumber { get; set; }
        [StringLength(15)]
        public string LiqConnSize { get; set; }
        [StringLength(15)]
        public string SuctConnSize { get; set; }
        public Nullable<int> ModelID { get; set; }
    }
}