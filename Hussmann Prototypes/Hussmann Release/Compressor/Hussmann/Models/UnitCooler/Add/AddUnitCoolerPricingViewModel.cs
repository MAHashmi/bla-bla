﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UnitCoolerDataModel;

namespace Hussmann.Models.UnitCooler.Add
{
    public class AddUnitCoolerPricingViewModel
    {
        public string CoilOptionPricing { get; set; }

        public bool PricingDataExsist { get; set; }

        public int NumberOfFans { get; set; }

        public int FamilyID { get; set; }

        public List<AccessoriesPriceDetails> CoilAccessoryList { get; set; }

        public decimal AccessoryPrice { get; set; }
        public List<CoilOptionsPricing> coilOptionsPricing { get; set; }

        public int SelectedFan { get; set; }
    }
}