﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.UnitCooler.Add
{
    public class CoilMotorTypeViewModel
    {
        public int Type_ID { get; set; }
        public string CoilMotorTypeName { get; set; }
    }
}