﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UnitCoolerDataModel;


namespace Hussmann.Models.UnitCooler.Add
{
    public class AddUnitCoolerAccessoriesViewModel
    {
        
        public int CoilAccessoryPricingID { get; set; }
        public Nullable<int> CoilAccsID { get; set; }
        public Nullable<int> CoilFamilyID { get; set; }
        public Nullable<int> CoilVendorID { get; set; }
        public string CoilAccsPartNumber { get; set; }
        public string CoilAccsPartDescription { get; set; }
        public Nullable<int> CoilAccsMinBTU { get; set; }
        public Nullable<int> CoilAccsMaxBTU { get; set; }

        public string CoilAccessoryData { get; set; }
        public string CoilAccessory { get; set; }

        public bool IsModified { get; set; }

        public string CoilVendor { get; set; }

    }


    public class UnitCoolerAccessories
    {
        public int CoilAccessoryPricingID { get; set; }
        public Nullable<int> CoilAccsID { get; set; }
        public Nullable<int> CoilFamilyID { get; set; }
        public Nullable<int> CoilVendorID { get; set; }
        public string CoilAccsPartNumber { get; set; }
        public string CoilAccsPartDescription { get; set; }
        public Nullable<int> CoilAccsMinBTU { get; set; }
        public Nullable<int> CoilAccsMaxBTU { get; set; }
        //public Nullable<decimal> CoilAccsShipLooseListPrice { get; set; }
        //public Nullable<decimal> CoilAccsListPrice { get; set; }
        public Nullable<int> ModelID { get; set; }
        

    }


}