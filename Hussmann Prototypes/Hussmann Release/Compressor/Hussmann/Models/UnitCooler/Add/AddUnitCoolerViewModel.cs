﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hussmann.Models.UnitCooler.Add;

namespace Hussmann.Models.UnitCooler
{
    public class AddUnitCoolerViewModel
    {
       public AddUnitCoolerDetailsViewModel UnitCoolerDetailsData { get; set; }

       public AddUnitCoolerAccessoriesViewModel UnitCoolerAccessoriesData { get; set; }

        public AddUnitCoolerConfigurationViewModel UnitCoolerConfigurationData { get; set; }

        public AddUnitCoolerPricingViewModel UnitCoolerPricingData { get; set; }

        public int CurrentTab { get; set; }
        public int RequestedTab { get; set; }
        public int CurrentCoilModelID { get; set; }
        public bool AllowEdit { get; set; }
        public bool IsPricingGridValid { get; set; }

        public int CurrentStatusID { get; set; }

        public bool CanApproveRecord { get; set; }

        public bool CanSubmitRecord { get; set; }

        public bool CanRejectRecord { get; set; }

        public bool CanDeleteRecord { get; set; }
        public bool CanMoveToDraft { get; set; }

        public bool IsEditedAfterApprove { get; set; }

        public bool IsAccessoryGridValid { get; set; }

        public string CurrentCoilState { get; set; }
        public int CurrentCoilVersion { get; set; } 
        public bool IsConfigrationTabDataValid { get; set; }

        public string RedirectTo { get; set; }

        public string SearchQuery { get; set;}

    }
}