﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UnitCoolerDataModel;

namespace Hussmann.Models.UnitCooler.Add
{
    public class AddUnitCoolerConfigurationViewModel
    {

        public string VoltageTypeList { get; set; }
        public string CoilDefrostAmpsData { get; set; }
        public string CoilDrainHeaterAmpsData { get; set; }

        public string CoilMotorAmpsData { get; set; }

        public string RowHeaderCoilMotorAmps { get; set; }

        public string ColumnHeaderCoilMotorAmps { get; set; }

        public string ColumnHeaderIds { get; set; }

        public List<CoilMotorType_lkp> MotorType { get; set; }

        public List<DefrostConfigViewModel> DefrostConfig { get; set; }

        public bool IsDrainHeaterDrainAmpsValid { get; set; }
        public bool IsDrainHeaterVoltageTypeValid { get; set; }
        public bool IsDefrostAmpsDrainAmpsValid { get; set; }
        public bool IsDefrostAmpsVoltageTypeValid { get; set; }
        public bool IsMotorAmpsGridValid { get; set; }


    }
}