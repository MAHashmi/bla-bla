﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models.UnitCooler.Add
{
    public class VendorViewModel
    {
        public int CoilVendorID { get; set; }
        public string CoilVendorName { get; set; }
    }
}