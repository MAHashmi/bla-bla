﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models
{

    public class UserProfile
    {
        public string UserName { get; set; }
        public string GroupName { get; set; }
        public GroupResponsibilities GroupResponsibilities { get; set; }
    }
    public class GroupResponsibilities
    {
        public bool CanReadContent { get; set; }
        public bool CanSearchContent { get; set; }
        public bool CanExportResult { get; set; }
        public bool CanAddRecord { get; set; }
        public bool CanUpdateRecord { get; set; }
        public bool CanApprovedRecord { get; set; }
        public bool CanUpdateAprrovedRecord { get; set; }
        public bool CanActivateRecord { get; set; }
        public bool CanAccessCaseUI { get; set; }
        public bool CanAccessCoilUI { get; set; }
        public bool CanAccessCompressorUI { get; set; }
        public bool CanAccessCondenserUI { get; set; }
        public bool CanRejectedRecord { get; set; }
    }
}