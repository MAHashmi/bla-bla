﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hussmann.Models
{
    public class DashboardCommonNewsFeedViewModel
    {
        public Nullable<int> ModelID { get; set; }
        public Nullable<int> StateID { get; set; }
        public System.DateTime LastModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ActionBy { get; set; }
        public string ModelNumber { get; set; }
        public string ChillerType { get; set; }
    }
}