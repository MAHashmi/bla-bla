﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hussmann.Common;
using UnitCoolerDataModel;

namespace Hussmann.Models
{
    public class DashboardViewModel
    {
        public List<DashboardCommonNewsFeedViewModel> CombinedNewsFeed { get; set; } // View Model for News Feeds
        public List<DashboardCommonSavedRecordsViewModel> CombinedSavedRecords { get; set; } // View Model for Recently Saved Records
        public List<DashboardCommonSubmittedRecordsViewModel> CombinedSubmittedRecords { get; set; } // View Model for Recently Submitted Records
        public List<DashboardCommonRejectedRecordsViewModel> CombinedRejectedRecords { get; set; }// View Model for Recently Rejected Records
    }
}