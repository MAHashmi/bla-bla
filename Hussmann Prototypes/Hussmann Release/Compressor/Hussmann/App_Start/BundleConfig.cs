﻿using System.Web;
using System.Web.Optimization;

namespace Hussmann
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        //"~/Scripts/jquery-{version}.js",
                        "~/scripts/datatables/jquery.datatables.js",
                         "~/scripts/datatables/datatables.bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/css/fonts.googleapis.css",
                      "~/Content/css/jquery-ui.min.css",
                      "~/Content/css/bootstrap.min.css",
                      "~/Content/plugins/font-awesome-4.2.0/css/font-awesome.min.css",
                      "~/Content/css/animate.min.css",
                      "~/Content/css/style.min.css",
                      "~/Content/css/style-responsive.min.css",
                      "~/Content/css/default.css",
                      "~/Content/css/datepicker.css",
                      "~/Content/css/datepicker3.css",
                      "~/Content/css/bwizard.min.css",

                      

                      //"~/Content/plugins/DataTables-1.9.4/css/data-table.css",
                      "~/Content/plugins/DataTables-1.9.4/css/dataTables.bootstrap.css",
                      "~/Content/plugins/DataTables-1.9.4/css/responsive.dataTables.min.css",
                      "~/Content/plugins/DataTables-1.9.4/css/rowReorder.dataTables.min.css",

                      //****Excel table script START HERE*******//
                      "~/Content/css/handsontable.css",
                      "~/Content/css/pikaday.css",
                      //****Excel table script END HERE*******//

                      //"~/Content/css/jquery-ui.css",

                      //"~/Content/css/data-table.css",


                      "~/Content/plugins/gritter/css/jquery.gritter.css"));
        }
    }
}
