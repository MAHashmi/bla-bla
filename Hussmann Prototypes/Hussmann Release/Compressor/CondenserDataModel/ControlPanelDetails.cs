//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CondenserDataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ControlPanelDetails
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ControlPanelDetails()
        {
            this.ControlPanelDetailsAndOptions = new HashSet<ControlPanelDetailsAndOptions>();
        }
    
        public int ControlPanelDetailID { get; set; }
    
        public virtual CondenserModel CondenserModel { get; set; }
        public virtual ControlFusesandBreakers ControlFusesandBreakers { get; set; }
        public virtual ControlManufacturer ControlManufacturer { get; set; }
        public virtual ControlPanelAirSensor ControlPanelAirSensor { get; set; }
        public virtual Controls Controls { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ControlPanelDetailsAndOptions> ControlPanelDetailsAndOptions { get; set; }
        public virtual ControlTypeofApplication ControlTypeofApplication { get; set; }
        public virtual ControlVoltage ControlVoltage { get; set; }
    }
}
