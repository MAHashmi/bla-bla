//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CondenserDataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ModelProperties
    {
        public int Model_ID { get; set; }
        public string Dimensions { get; set; }
        public string FPI { get; set; }
        public string Fans { get; set; }
        public Nullable<decimal> Flood_Charge { get; set; }
        public Nullable<decimal> Total_Weight { get; set; }
        public Nullable<decimal> Circuits { get; set; }
        public Nullable<decimal> Breaker_Size { get; set; }
    }
}
