//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CondenserDataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class CondenserGeometry
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CondenserGeometry()
        {
            this.ControlPanelPrice = new HashSet<ControlPanelPrice>();
            this.LegPrice = new HashSet<LegPrice>();
            this.MiscellaneousOptionsPricing = new HashSet<MiscellaneousOptionsPricing>();
            this.Receiver = new HashSet<Receiver>();
        }
    
        public int GeometryID { get; set; }
        public string GeometryType { get; set; }
        public Nullable<int> FansWide { get; set; }
        public Nullable<int> FansInline { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ControlPanelPrice> ControlPanelPrice { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LegPrice> LegPrice { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MiscellaneousOptionsPricing> MiscellaneousOptionsPricing { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Receiver> Receiver { get; set; }
    }
}
