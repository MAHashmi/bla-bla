//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CondenserDataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ModelSuffix
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ModelSuffix()
        {
            this.CondenserModels = new HashSet<CondenserModel>();
            this.CondenserPricing = new HashSet<CondenserPricing>();
        }
    
        public int ModelSuffixID { get; set; }
        public string ModelSuffix1 { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CondenserModel> CondenserModels { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CondenserPricing> CondenserPricing { get; set; }
    }
}
