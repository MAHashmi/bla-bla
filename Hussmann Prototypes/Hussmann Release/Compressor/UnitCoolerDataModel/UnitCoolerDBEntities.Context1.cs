﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UnitCoolerDataModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class RulestreamCoilDBEntities : DbContext
    {
        public RulestreamCoilDBEntities()
            : base("name=RulestreamCoilDBEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<CoilFamily> CoilFamilies { get; set; }
        public virtual DbSet<CoilBrand_lkp> CoilBrand_lkp { get; set; }
        public virtual DbSet<CoilVendor> CoilVendors { get; set; }
        public virtual DbSet<CoilModel> CoilModels { get; set; }
        public virtual DbSet<CoilMotorType_lkp> CoilMotorType_lkp { get; set; }
        public virtual DbSet<DefrostConfig_lkp> DefrostConfig_lkp { get; set; }
        public virtual DbSet<State> States { get; set; }
    
        public virtual ObjectResult<spGetUnitCoolerDasboardNewsFeed_Result> spGetUnitCoolerDasboardNewsFeed()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetUnitCoolerDasboardNewsFeed_Result>("spGetUnitCoolerDasboardNewsFeed");
        }
    
        public virtual ObjectResult<spGetUnitCoolerRejectedRecord_Result> spGetUnitCoolerRejectedRecord(string param_UserType, string param_UserName)
        {
            var param_UserTypeParameter = param_UserType != null ?
                new ObjectParameter("param_UserType", param_UserType) :
                new ObjectParameter("param_UserType", typeof(string));
    
            var param_UserNameParameter = param_UserName != null ?
                new ObjectParameter("param_UserName", param_UserName) :
                new ObjectParameter("param_UserName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetUnitCoolerRejectedRecord_Result>("spGetUnitCoolerRejectedRecord", param_UserTypeParameter, param_UserNameParameter);
        }
    
        public virtual ObjectResult<spGetUnitCoolerSavedRecord_Result> spGetUnitCoolerSavedRecord(string param_UserType, string param_UserName)
        {
            var param_UserTypeParameter = param_UserType != null ?
                new ObjectParameter("param_UserType", param_UserType) :
                new ObjectParameter("param_UserType", typeof(string));
    
            var param_UserNameParameter = param_UserName != null ?
                new ObjectParameter("param_UserName", param_UserName) :
                new ObjectParameter("param_UserName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetUnitCoolerSavedRecord_Result>("spGetUnitCoolerSavedRecord", param_UserTypeParameter, param_UserNameParameter);
        }
    
        public virtual ObjectResult<spGetUnitCoolerSubmittedRecord_Result> spGetUnitCoolerSubmittedRecord(string param_UserType, string param_UserName)
        {
            var param_UserTypeParameter = param_UserType != null ?
                new ObjectParameter("param_UserType", param_UserType) :
                new ObjectParameter("param_UserType", typeof(string));
    
            var param_UserNameParameter = param_UserName != null ?
                new ObjectParameter("param_UserName", param_UserName) :
                new ObjectParameter("param_UserName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetUnitCoolerSubmittedRecord_Result>("spGetUnitCoolerSubmittedRecord", param_UserTypeParameter, param_UserNameParameter);
        }
    
        public virtual int spSetNewCoilModel(Nullable<int> param_MFGID, Nullable<int> param_ModelFamilyID, string param_ModelNumber, Nullable<int> param_CapacityPerDegreeTDNeg20, Nullable<int> param_CapacityPerDegreeTDPos25, Nullable<int> param_NumberOfFans, Nullable<int> param_Version, Nullable<System.DateTime> param_LastModifiedOn, string param_Note, string param_CreatedBy, string param_ActionBy, Nullable<int> param_StateID, string param_CoilModelYear, string param_CoilMakeYear)
        {
            var param_MFGIDParameter = param_MFGID.HasValue ?
                new ObjectParameter("param_MFGID", param_MFGID) :
                new ObjectParameter("param_MFGID", typeof(int));
    
            var param_ModelFamilyIDParameter = param_ModelFamilyID.HasValue ?
                new ObjectParameter("param_ModelFamilyID", param_ModelFamilyID) :
                new ObjectParameter("param_ModelFamilyID", typeof(int));
    
            var param_ModelNumberParameter = param_ModelNumber != null ?
                new ObjectParameter("param_ModelNumber", param_ModelNumber) :
                new ObjectParameter("param_ModelNumber", typeof(string));
    
            var param_CapacityPerDegreeTDNeg20Parameter = param_CapacityPerDegreeTDNeg20.HasValue ?
                new ObjectParameter("param_CapacityPerDegreeTDNeg20", param_CapacityPerDegreeTDNeg20) :
                new ObjectParameter("param_CapacityPerDegreeTDNeg20", typeof(int));
    
            var param_CapacityPerDegreeTDPos25Parameter = param_CapacityPerDegreeTDPos25.HasValue ?
                new ObjectParameter("param_CapacityPerDegreeTDPos25", param_CapacityPerDegreeTDPos25) :
                new ObjectParameter("param_CapacityPerDegreeTDPos25", typeof(int));
    
            var param_NumberOfFansParameter = param_NumberOfFans.HasValue ?
                new ObjectParameter("param_NumberOfFans", param_NumberOfFans) :
                new ObjectParameter("param_NumberOfFans", typeof(int));
    
            var param_VersionParameter = param_Version.HasValue ?
                new ObjectParameter("param_Version", param_Version) :
                new ObjectParameter("param_Version", typeof(int));
    
            var param_LastModifiedOnParameter = param_LastModifiedOn.HasValue ?
                new ObjectParameter("param_LastModifiedOn", param_LastModifiedOn) :
                new ObjectParameter("param_LastModifiedOn", typeof(System.DateTime));
    
            var param_NoteParameter = param_Note != null ?
                new ObjectParameter("param_Note", param_Note) :
                new ObjectParameter("param_Note", typeof(string));
    
            var param_CreatedByParameter = param_CreatedBy != null ?
                new ObjectParameter("param_CreatedBy", param_CreatedBy) :
                new ObjectParameter("param_CreatedBy", typeof(string));
    
            var param_ActionByParameter = param_ActionBy != null ?
                new ObjectParameter("param_ActionBy", param_ActionBy) :
                new ObjectParameter("param_ActionBy", typeof(string));
    
            var param_StateIDParameter = param_StateID.HasValue ?
                new ObjectParameter("param_StateID", param_StateID) :
                new ObjectParameter("param_StateID", typeof(int));
    
            var param_CoilModelYearParameter = param_CoilModelYear != null ?
                new ObjectParameter("param_CoilModelYear", param_CoilModelYear) :
                new ObjectParameter("param_CoilModelYear", typeof(string));
    
            var param_CoilMakeYearParameter = param_CoilMakeYear != null ?
                new ObjectParameter("param_CoilMakeYear", param_CoilMakeYear) :
                new ObjectParameter("param_CoilMakeYear", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("spSetNewCoilModel", param_MFGIDParameter, param_ModelFamilyIDParameter, param_ModelNumberParameter, param_CapacityPerDegreeTDNeg20Parameter, param_CapacityPerDegreeTDPos25Parameter, param_NumberOfFansParameter, param_VersionParameter, param_LastModifiedOnParameter, param_NoteParameter, param_CreatedByParameter, param_ActionByParameter, param_StateIDParameter, param_CoilModelYearParameter, param_CoilMakeYearParameter);
        }
    
        public virtual ObjectResult<spSearchUnitCooler_Result> spSearchUnitCooler(string param_ModelNumber, string param_CoilBrandName, string param_CoilFamilyName, string param_StateDescription)
        {
            var param_ModelNumberParameter = param_ModelNumber != null ?
                new ObjectParameter("param_ModelNumber", param_ModelNumber) :
                new ObjectParameter("param_ModelNumber", typeof(string));
    
            var param_CoilBrandNameParameter = param_CoilBrandName != null ?
                new ObjectParameter("param_CoilBrandName", param_CoilBrandName) :
                new ObjectParameter("param_CoilBrandName", typeof(string));
    
            var param_CoilFamilyNameParameter = param_CoilFamilyName != null ?
                new ObjectParameter("param_CoilFamilyName", param_CoilFamilyName) :
                new ObjectParameter("param_CoilFamilyName", typeof(string));
    
            var param_StateDescriptionParameter = param_StateDescription != null ?
                new ObjectParameter("param_StateDescription", param_StateDescription) :
                new ObjectParameter("param_StateDescription", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spSearchUnitCooler_Result>("spSearchUnitCooler", param_ModelNumberParameter, param_CoilBrandNameParameter, param_CoilFamilyNameParameter, param_StateDescriptionParameter);
        }
    }
}
