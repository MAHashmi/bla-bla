//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UnitCoolerDataModel
{
    using System;
    
    public partial class spGetCoilAccessory_Result
    {
        public int CoilAccessoryPricingID { get; set; }
        public Nullable<int> CoilAccsID { get; set; }
        public Nullable<int> CoilFamilyID { get; set; }
        public Nullable<int> CoilVendorID { get; set; }
        public string CoilAccsPartNumber { get; set; }
        public string CoilAccsPartDescription { get; set; }
        public Nullable<int> CoilAccsMinBTU { get; set; }
        public Nullable<int> CoilAccsMaxBTU { get; set; }
        public Nullable<decimal> CoilAccsShipLooseListPrice { get; set; }
        public Nullable<decimal> CoilAccsListPrice { get; set; }
        public Nullable<int> ModelID { get; set; }
        public string CoilAccessoryName { get; set; }
        public string CoilVendorName { get; set; }
        public string CoilFamilyName { get; set; }
    }
}
