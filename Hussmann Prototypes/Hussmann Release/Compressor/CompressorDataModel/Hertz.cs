//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CompressorDataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Hertz
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Hertz()
        {
            this.Voltages = new HashSet<Voltage>();
            this.Coefficients = new HashSet<Coefficient>();
        }
    
        public int Hertz_ID { get; set; }
        public int Hertz_Type { get; set; }
        public int Phase { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Voltage> Voltages { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Coefficient> Coefficients { get; set; }
    }
}
