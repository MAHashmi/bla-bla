//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CompressorDataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class VendorBOMCode
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VendorBOMCode()
        {
            this.CompBOMs = new HashSet<CompBOM>();
        }
    
        public int VendorBOMCode_ID { get; set; }
        public string VendorBOMDescr { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompBOM> CompBOMs { get; set; }
    }
}
