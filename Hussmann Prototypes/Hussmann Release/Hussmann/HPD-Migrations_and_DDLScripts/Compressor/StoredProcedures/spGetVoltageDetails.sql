IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spGetVoltageDetails'))
BEGIN
    DROP PROCEDURE spGetVoltageDetails
END

GO
-- =============================================
-- Author:		MURTAZA
-- Create date: 20/6/2018
-- Description:	Get List of Voltage Details
-- =============================================
CREATE PROCEDURE spGetVoltageDetails
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Voltage_ID
	,VoltageType
	,VoltageTypeDescr
	,Hertz_Type
	,Phase
	from Voltage
	inner join Hertz 
	on Voltage.Hertz_ID = Hertz.Hertz_ID
END
GO
