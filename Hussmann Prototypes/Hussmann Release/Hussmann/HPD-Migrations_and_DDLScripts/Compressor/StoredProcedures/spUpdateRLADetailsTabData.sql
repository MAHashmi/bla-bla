IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spUpdateRLADetailsTabData'))
BEGIN
    DROP PROCEDURE spUpdateRLADetailsTabData
END

GO
-- =============================================
-- Author:		MURTAZA
-- Create date: 6/20/2018
-- Description:	Insert/Update RLA Details Tab data
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateRLADetailsTabData]
	@param_ModelID int = null,  
	@param_RLADetailsUDT [dbo].[RLADetailsUDT] Readonly
	
AS
BEGIN
		SET NOCOUNT ON;
		DECLARE
		@OldRLAId int = null,
		@NewRLAId int = null
		Select @OldRLAId=MAX(CompRLA_ID) FROM CompRLA WHERE CompModel_ID=@param_ModelID;
		create table #BOMDetailsTemp
		(
			[CompRLA_ID] [int] NULL,
			[HussTempApp_ID] [int] NULL,
			[VendorBOMCode_ID] [int] NULL,
			[CompType_ID] [int] NULL,
			[DiscLine] [nchar](2) NULL,
			[SuctLine] [nchar](2)  NULL,
			[CompKitProtocol] [nvarchar](20) NULL,
			[CompKitHSeries] [nvarchar](20) NULL,
			[CompKitRack] [nvarchar](20) NULL
		)
	IF EXISTS(Select top 1 * from CompBOM where CompRLA_ID = @OldRLAId)
		BEGIN
		Insert INTO #BOMDetailsTemp(
			CompRLA_ID,
			HussTempApp_ID,
			VendorBOMCode_ID,
			CompType_ID,
			DiscLine,
			SuctLine,
			CompKitProtocol,
			CompKitHSeries,
			CompKitRack)
		Select CompRLA_ID,HussTempApp_ID,VendorBOMCode_ID,CompType_ID,DiscLine,SuctLine,
		CompKitProtocol,CompKitHSeries,CompKitRack
		FROM CompBOM WHERE CompRLA_ID=@OldRLAId
		END

		DELETE FROM CompRLA WHERE CompModel_ID = @param_ModelID;

		INSERT INTO CompRLA (
		Voltage_ID, 
		CompModel_ID, 
		RLA, 
		VendorVoltageCode_ID, 
		NEMA, 
		DP, 
		Breaker, 
		WiringSize) select 
		Voltage_ID, 
		@param_ModelID, 
		RLA, 
		VendorVoltageCode_ID, 
		NEMA,
		DP, 
		Breaker, 
		WiringSize
		FROM @param_RLADetailsUDT; 
		SET @NewRLAId = SCOPE_IDENTITY()

	IF EXISTS(Select top 1 * from #BOMDetailsTemp)
		BEGIN
			update #BOMDetailsTemp SET CompRLA_ID=@NewRLAId;
			INSERT INTO CompBOM(
			CompRLA_ID,
			HussTempApp_ID,
			VendorBOMCode_ID,
			CompType_ID,
			DiscLine,
			SuctLine,
			CompKitProtocol,
			CompKitHSeries,
			CompKitRack)
		Select CompRLA_ID,HussTempApp_ID,VendorBOMCode_ID,CompType_ID,DiscLine,SuctLine,
		CompKitProtocol,CompKitHSeries,CompKitRack FROM #BOMDetailsTemp
		END
END


GO
