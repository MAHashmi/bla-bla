IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spGetBOMDetailsToExport'))
BEGIN
    DROP PROCEDURE spGetBOMDetailsToExport
END

GO
-- =============================================
-- Author:		MURTAZA
-- Create date: 17/July/2018
-- Description:	Get BOM Details Data to Export
-- =============================================
CREATE PROCEDURE spGetBOMDetailsToExport
	-- Add the parameters for the stored procedure here
	@param_ModelID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--IF EXISTS(select top 1 * from CompRLA where CompModel_ID = @param_ModelID)
	--BEGIN
	Declare @CompRLA_ID int;
	Select @CompRLA_ID = CompRLA_ID FROM CompRLA where CompModel_ID = @param_ModelID;
		
		--Check if CompRLA_ID exists for current model (@param_ModelID)
		--IF EXISTS(Select top 1 * from CompBOM where CompRLA_ID = @CompRLA_ID)
		--BEGIN
			SELECT CompTypeDescr	AS CompressorType,
			CompVendor				AS CompressorVendor,
			HussTempApp				AS HussmannTempApplication,
			VendorBOMDescr			AS VendorBOMCode,
			SuctLineSize			AS SuctionLineSize,
			DiscLineSize			AS DiscLineSize,
			CompKitProtocol			AS CompressorKitProtocol,
			CompKitHSeries			AS CompressorKitHSeries,
			CompKitRack				AS CompressorKitRack
			FROM CompBOM CB
			LEFT JOIN CompType CT ON CB.CompType_ID = CT.CompType_ID
			LEFT JOIN CompressorModel CM ON CM.CompModel_ID = @param_ModelID
			RIGHT JOIN CompVendors CV ON CV.CompVendor_ID = CM.CompVendor_ID
			LEFT JOIN HussmannTempApp HTA ON CB.HussTempApp_ID = HTA.HussTempApp_ID
			LEFT JOIN VendorBOMCode VBC ON CB.VendorBOMCode_ID = VBC.VendorBOMCode_ID
			LEFT JOIN SuctLineSize SL ON CB.SuctLine = SL.SuctLineSize
			LEFT JOIN DiscLineSize DL ON CB.DiscLine = DL.DiscLineSize
			WHERE CB.CompRLA_ID = @CompRLA_ID
		--END
	--END

END
GO
