IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spCreateCompressorModel'))
BEGIN
    DROP PROCEDURE spCreateCompressorModel
END

GO

-- =============================================
-- Author:		faris khan
-- Create date: 6/19/2018
-- Description:	Create Compressor Model
-- =============================================
CREATE PROCEDURE [dbo].[spCreateCompressorModel]

	@param_ModelNumber nvarchar(15),
	@param_NormalHP nvarchar(4),
	@param_RealHP decimal(9,1),
	@param_CompSeries_ID int,
	@param_CapReduct_ID int,
	@param_CreatedBy varchar(75)

	
AS
BEGIN
	
	DECLARE @Version int = 1,
			@StateID int = 1,
			@ModelID int,
			@CurrentDate datetime = GETDATE();

	Insert Into CompressorModel(
			
			ModelNumber,
			NomHorsepower,
			RealHorsePower,
			CompSeries_ID,
			CapReduct_ID,
			[Version],
			StateID,
			LastModifiedOn,
			Note,
			CreatedBy,
			ActionBy
			)
	select
		
			@param_ModelNumber,
			@param_NormalHP,
			@param_RealHP,
			@param_CompSeries_ID,
			@param_CapReduct_ID,
		 	@Version,
			@StateID,
		 	@CurrentDate,
		 	Null ,
		 	@param_CreatedBy ,
		 	@param_CreatedBy;

		
	SET @ModelID = SCOPE_IDENTITY();

	INSERT INTO [dbo].[CompressorModelActionHistory]
           ([ModelID]
           ,[StateID]
           ,[CreatedBy]
           ,[LastModifiedOn]
           ,[ActionBy]
           ,[XMLData])
     VALUES
           (@ModelID
           ,@StateID
           ,@param_CreatedBy
           ,@CurrentDate
           ,@param_CreatedBy
           ,(Select * from CompressorModel where CompModel_ID = @ModelID  For XML Path));
	
	

	
		select @ModelID as ModelID

END
