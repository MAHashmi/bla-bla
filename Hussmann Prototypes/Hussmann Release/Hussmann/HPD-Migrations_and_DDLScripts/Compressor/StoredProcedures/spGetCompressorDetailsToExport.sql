IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spGetCompressorDetailsToExport'))
BEGIN
    DROP PROCEDURE spGetCompressorDetailsToExport
END

GO
-- =============================================
-- Author:		MURTAZA
-- Create date: 17/July/2018
-- Description:	Get Compressor Details To Export
-- =============================================
CREATE PROCEDURE [dbo].[spGetCompressorDetailsToExport] 
	-- Add the parameters for the stored procedure here
	@param_ModelID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT ModelNumber		AS CompressorModel,
	CompSeries				AS CompressorSeries,
	NomHorsepower			AS NormalHorsePower,
	RealHorsePower			AS RealHorsePower,
	[Version]				AS [Version]
	FROM CompressorModel CM
	LEFT JOIN CompSeries CS ON CM.CompSeries_ID = CS.CompSeries_ID
	LEFT JOIN CapacityReduction CRed ON CM.CapReduct_ID = CRed.CapReduct_ID
	WHERE CompModel_ID = @param_ModelID
END
GO
