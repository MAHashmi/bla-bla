IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spChangeCompressorState'))
BEGIN
    DROP PROCEDURE spChangeCompressorState
END

GO

-- =============================================
-- Author:		Murtaza 
-- Create date: 19/6/2018
-- Description:	Change State of Compressor
-- =============================================
CREATE PROCEDURE [dbo].[spChangeCompressorState] 
	 @param_ModelId INT
	,@param_StateId INT = NULL
	,@param_ActionBy VARCHAR(75)
AS
BEGIN
	DECLARE @stateID INT
		,@CreatedBy VARCHAR(75)

	IF @param_StateId IS NULL
	BEGIN
		SELECT @stateID = StateID
		FROM CompressorModel
		WHERE CompModel_ID = @param_ModelId
	END

	IF @stateID = 5
		AND @param_StateId IS NULL --Check that in deleted state 
	BEGIN
		SELECT TOP 1 @stateID = StateID
		FROM --restore the second last recored if null then draft
			(
			SELECT TOP 2 *
			FROM [CompressorModelActionHistory]
			WHERE ModelID = @param_ModelId
			ORDER BY LastModifiedOn DESC
			) r
		WHERE ModelID = @param_ModelId
		ORDER BY LastModifiedOn

		IF @stateID IS NULL
			OR @stateID = 5
			SET @stateID = 1

		UPDATE CompressorModel
		SET StateID = @stateID
			,LastModifiedOn = GETDATE()
			,ActionBy = @param_ActionBy
			,@CreatedBy = CreatedBy
		WHERE CompModel_ID = @param_ModelId
	END
	ELSE
	BEGIN
		UPDATE CompressorModel
		SET @stateID = Case when @param_StateId is Null THEN 5
						ELSE @param_StateId END	
			,StateID = Case when @param_StateId is Null THEN 5
						ELSE @param_StateId END
			,LastModifiedOn = GETDATE()
			,ActionBy = @param_ActionBy
			,@CreatedBy = CreatedBy
		WHERE CompModel_ID = @param_ModelId
	END

	INSERT INTO [dbo].[CompressorModelActionHistory] (
		 [ModelID]
		,[StateID]
		,[CreatedBy]
		,[LastModifiedOn]
		,[ActionBy]
		,[XMLData]
		)
	VALUES (
		@param_ModelId
		,@stateID
		,@CreatedBy
		,GETDATE()
		,@param_ActionBy
		,(
			SELECT *
			FROM CompressorModel
			WHERE CompModel_ID = @param_ModelId
			FOR XML Path
			)
		)
END




GO


