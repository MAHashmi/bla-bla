IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spGetCompressorRejectedRecord'))
BEGIN
    DROP PROCEDURE spGetCompressorRejectedRecord
END

GO
-- =============================================
-- Author:		Jaffer Raza
-- Create date: 18/1/2018
-- Description:	Get top 10 recently rejected records of compressor
-- =============================================
CREATE PROCEDURE [dbo].[spGetCompressorRejectedRecord]
	@param_UserType varchar(20),
	@param_UserName varchar(75)
AS
BEGIN
	IF(@param_UserType = 'Super' OR @param_UserType = 'Approver')
	BEGIN
	SELECT TOP 10 
	CM.ModelID,
	CM.StateID,
	CM.LastModifiedOn,
	CM.CreatedBy,
	CM.ActionBy, 
	C.ModelNumber
	FROM CompressorModelActionHistory CM
	join CompressorModel C
	on C.CompModel_ID = CM.ModelID
	WHERE CM.StateID = 3 -- rejected state
	ORDER BY CM.LastModifiedOn DESC
	END

	IF(@param_UserType = 'DataFeeder')
	BEGIN
	SELECT TOP 10 
	CM.ModelID,
	CM.StateID,
	CM.LastModifiedOn,
	CM.CreatedBy,
	CM.ActionBy,
	C.ModelNumber 
	FROM CompressorModelActionHistory CM
	join CompressorModel C
	on C.CompModel_ID = CM.ModelID
	WHERE CM.StateID = 3 -- rejected state
	AND CM.CreatedBy = @param_UserName
	ORDER BY CM.LastModifiedOn DESC
	END

	ELSE
	BEGIN
	SELECT TOP 10 
	CM.ModelID,
	CM.StateID,
	CM.LastModifiedOn,
	CM.CreatedBy,
	CM.ActionBy,
	C.ModelNumber 
	FROM CompressorModelActionHistory CM
	join CompressorModel C
	on C.CompModel_ID = CM.ModelID
	WHERE 1=0
	END

END
GO