IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spGetCapacityReductionByCompressorSeries'))
BEGIN
    DROP PROCEDURE spGetCapacityReductionByCompressorSeries
END

-- =============================================
-- Author:		MURTAZA
-- Create date: 12/July/2018
-- Description:	Get CapacityReductions By Compressor Series
-- =============================================
CREATE PROCEDURE [dbo].[spGetCapacityReductionByCompressorSeries]
	-- Add the parameters for the stored procedure here
	@param_CompSeriesID INT = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT CRO.CapReduct_ID AS CapReduct_ID, CR.CapReductType AS CapReductType FROM
	CompSeries CS
	INNER JOIN CapacityReductOptions CRO ON CS.CompSeries_ID = CRO.CompSeries_ID
	AND CS.CompSeries_ID = @param_CompSeriesID
	INNER JOIN CapacityReduction CR ON CRO.CapReduct_ID = CR.CapReduct_ID
END

GO


