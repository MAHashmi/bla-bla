IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spGetRLADetailsToExport'))
BEGIN
    DROP PROCEDURE spGetRLADetailsToExport
END

GO
-- =============================================
-- Author:		MURTAZA
-- Create date: 17/July/2018
-- Description:	Get RLA Details to Export
-- =============================================
CREATE PROCEDURE [dbo].[spGetRLADetailsToExport]
	-- Add the parameters for the stored procedure here
	@param_ModelID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT RLA,
	VoltageType,
	CR.WiringSize			AS WiringSize,
	VVC.VendorVoltageCodeType,
	CR.NEMA				AS NEMA,
	CR.DP					AS DP,
	CR.Breaker				AS Breaker
	FROM CompRLA CR
	LEFT JOIN Voltage V ON CR.Voltage_ID = V.Voltage_ID
	LEFT JOIN VendorVoltageCode VVC ON CR.VendorVoltageCode_ID = VVC.VendorVoltageCode_ID
	  
	WHERE CompModel_ID = @param_ModelID
END
GO
