IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spSearchCompressor'))
BEGIN
    DROP PROCEDURE spSearchCompressor
END

GO

-- =============================================
-- Author:		Murtaza
-- Create date: 19/6/2018
-- Description:	Get Data Against given filter
-- =============================================
CREATE PROCEDURE [dbo].[spSearchCompressor]
	@param_ModelNumber varchar(30) = null,
	@param_CompressorTypeId int = null,
	@param_CompressorSeriesId int = null,
	@param_RefrigerantTypeId int = null,
	@param_CompressorVendorId int = null,
	@param_StatusId int  = null,
	@param_UserName varchar(75),
	@param_UserType varchar(20),
	@param_StartRecord int,
	@param_NoOFRecords int,
	@param_SortExpression varchar(20),
	@param_SortDirection varchar(20)
AS
BEGIN
select CompModel_ID
	,ModelNumber
	,CompTypeDescr
	,CompSeries
	,RefrigerantType
	,CompVendor
	,CONVERT(VARCHAR, LastModifiedOn, 120) as 'LastModifiedOn'
	,[Version]
	,ActionBy
	,CreatedBy
	,StateDescription

	from(
	select
	 CompModel_ID
	,ModelNumber
	,CompTypeDescr
	,CompSeries
	,RefrigerantType
	,CompVendor
	,LastModifiedOn
	,[Version]
	,ActionBy
	,CreatedBy
	,StateDescription
	,ROW_NUMBER() OVER (ORDER BY 
		CASE WHEN (@param_SortExpression = 'ModelNumber' AND @param_SortDirection='Ascending')
                    THEN ModelNumber
        END ASC,
        CASE WHEN (@param_SortExpression = 'ModelNumber' AND @param_SortDirection='Descending')
                   THEN ModelNumber
        END DESC,

        CASE WHEN (@param_SortExpression = 'CompTypeDescr' AND @param_SortDirection='Ascending')
                  THEN CompTypeDescr
        END ASC,
        CASE WHEN (@param_SortExpression = 'CompTypeDescr' AND @param_SortDirection='Descending')
                 THEN CompTypeDescr
        END DESC,

        CASE WHEN @param_SortExpression = 'CompSeries' AND @param_SortDirection='Ascending'
                THEN CompSeries
        END ASC,
        CASE WHEN @param_SortExpression = 'CompSeries' AND @param_SortDirection='Descending'
                THEN CompSeries
        END DESC,

        CASE WHEN @param_SortExpression = 'RefrigerantType' AND @param_SortDirection='Ascending'
                THEN RefrigerantType
        END ASC,
        CASE WHEN @param_SortExpression = 'RefrigerantType' AND @param_SortDirection='Descending'
               THEN RefrigerantType
        END DESC,

		CASE WHEN @param_SortExpression = 'CompVendor' AND @param_SortDirection='Ascending'
                THEN CompVendor
        END ASC,
        CASE WHEN @param_SortExpression = 'CompVendor' AND @param_SortDirection='Descending'
               THEN CompVendor
        END DESC,

		 CASE WHEN @param_SortExpression = 'StateDescription' AND @param_SortDirection='Ascending'
              THEN StateDescription
        END ASC,
        CASE WHEN @param_SortExpression = 'StateDescription' AND @param_SortDirection='Descending'
              THEN StateDescription
        END DESC,
		CASE WHEN @param_SortExpression = 'Version' AND @param_SortDirection='Ascending'
              THEN [Version]
        END ASC,
        CASE WHEN @param_SortExpression = 'Version' AND @param_SortDirection='Descending'
              THEN [Version]
        END DESC,
		CASE WHEN @param_SortExpression = 'LastModifiedOn' AND @param_SortDirection='Ascending'
              THEN LastModifiedOn
        END ASC,
        CASE WHEN @param_SortExpression = 'LastModifiedOn' AND @param_SortDirection='Descending'
              THEN LastModifiedOn
        END DESC,
		CASE WHEN @param_SortExpression = 'ActionBy' AND @param_SortDirection='Ascending'
              THEN ActionBy
        END ASC,
        CASE WHEN @param_SortExpression = 'ActionBy' AND @param_SortDirection='Descending'
              THEN ActionBy
        END DESC
		) AS RowNumber
	from(
SELECT CompModel_ID
	,ModelNumber
	,CompTypeDescr
	,CompSeries
	,RefrigerantType
	,CompVendor
	,LastModifiedOn
	,[Version]
	,ActionBy
	,CreatedBy
	,[Description] AS StateDescription

	
FROM (
	SELECT CompModel_ID
		,ModelNumber
		,B.CompType_ID
		,CompSeries_ID
		,RefrigerantType_ID
		,CompVendor_ID
		,LastModifiedOn
		,[Version]
		,ActionBy
		,CreatedBy
		,StateID
		,ROW_NUMBER() OVER (
			PARTITION BY ModelNumber ORDER BY [Version] DESC
			) AS rownum
	FROM CompressorModel LEFT JOIN 
	(select cr.CompModel_ID as RLACompModelID, cb.CompType_ID from CompRLA cr left join CompBOM cb
	on cr.CompRLA_ID = cb.CompRLA_ID) B on CompressorModel.CompModel_ID = B.RLACompModelID
	WHERE StateID = 4
	) AS CM
INNER JOIN [State] St ON St.StateID = CM.StateID
LEFT JOIN CompType CT ON CT.CompType_ID = CM.CompType_ID
INNER JOIN CompSeries CS ON CS.CompSeries_ID = CM.CompSeries_ID
LEFT JOIN RefrigerantType RT ON RT.RefrigerantType_ID = CM.RefrigerantType_ID
LEFT JOIN CompVendors CV ON CV.CompVendor_ID = CM.CompVendor_ID
WHERE rownum = 1
	AND (
		ModelNumber LIKE '%' + @param_ModelNumber + '%'
		OR @param_ModelNumber IS NULL
		)
	AND (
		CT.CompType_ID = @param_CompressorTypeId
		OR @param_CompressorTypeId IS NULL
		)
	AND (
		CS.CompSeries_ID = @param_CompressorSeriesId
		OR @param_CompressorSeriesId IS NULL
		)
	AND (
		RT.RefrigerantType_ID = @param_RefrigerantTypeId
		OR @param_RefrigerantTypeId IS NULL
		)
	AND (
		CV.CompVendor_ID = @param_CompressorVendorId
		OR @param_CompressorVendorId IS NULL
		)
	AND (
		St.StateID = @param_StatusId
		OR @param_StatusId IS NULL
		)
	AND (St.StateID = 4)
------------------ Latest Approved Versions END --------------------

UNION

SELECT CompModel_ID
	,ModelNumber
    ,CompTypeDescr
	,CompSeries
	,RefrigerantType
	,CompVendor
	,LastModifiedOn
	,[Version]
	,ActionBy
	,CreatedBy
	,[Description] AS StateDescription
FROM CompressorModel CM LEFT JOIN
(select cr.CompModel_ID as RLACompModelID, cb.CompType_ID from CompRLA cr left join CompBOM cb
	on cr.CompRLA_ID = cb.CompRLA_ID) B on CM.CompModel_ID = B.RLACompModelID
INNER JOIN [State] St ON St.StateID = CM.StateID
LEFT JOIN CompType CT ON CT.CompType_ID = B.CompType_ID
INNER JOIN CompSeries CS ON CS.CompSeries_ID = CM.CompSeries_ID
LEFT JOIN RefrigerantType RT ON RT.RefrigerantType_ID = CM.RefrigerantType_ID
LEFT JOIN CompVendors CV ON CV.CompVendor_ID = CM.CompVendor_ID
WHERE (
		ModelNumber LIKE '%' + @param_ModelNumber + '%'
		OR @param_ModelNumber IS NULL
		)
	AND (
		CT.CompType_ID = @param_CompressorTypeId
		OR @param_CompressorTypeId IS NULL
		)
	AND (
		CS.CompSeries_ID = @param_CompressorSeriesId
		OR @param_CompressorSeriesId IS NULL
		)
	AND (
		RT.RefrigerantType_ID = @param_RefrigerantTypeId
		OR @param_RefrigerantTypeId IS NULL
		)
	AND (
		CV.CompVendor_ID = @param_CompressorVendorId
		OR @param_CompressorVendorId IS NULL
		)
	AND (
		St.StateID = @param_StatusId
		OR @param_StatusId IS NULL
		)
	AND (St.StateID != 4)
	AND (
			(
			@param_UserType = 'DataFeeder'
			AND CreatedBy = @param_UserName
			)
			OR
						(
			@param_UserType = 'Reader'
			AND St.StateID = 0
			)
			OR
			(@param_UserType = 'Super' OR @param_UserType = 'Approver' )
	
		)
		) A)as B
		where
		B.RowNumber between @param_StartRecord and @param_NoOFRecords
END







GO


