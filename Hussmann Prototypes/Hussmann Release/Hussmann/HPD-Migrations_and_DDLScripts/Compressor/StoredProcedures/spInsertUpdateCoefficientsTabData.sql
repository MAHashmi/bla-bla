IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spInsertUpdateCoefficientsTabData'))
BEGIN
    DROP PROCEDURE spInsertUpdateCoefficientsTabData
END

-- =============================================
-- Author:		MURTAZA
-- Create date: 2/July/2018
-- Description:	Insert OR Update Coefficients of Compressor
-- =============================================
Create PROCEDURE spInsertUpdateCoefficientsTabData
	-- Add the parameters for the stored procedure here
	@param_ModelID int = null,
	@param_ReturnGasID int = null,
	@param_CoefficientsUDT CoefficientsUDT Readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(Select TOP 1 * from Coefficients WHERE CompModel_ID = @param_ModelID)
	BEGIN
		DELETE FROM Coefficients WHERE CompModel_ID = @param_ModelID;
	END

	Declare
		@vendorTempRange int = 1

	INSERT INTO Coefficients (
	VendorTempRange_ID,
	ReturnGas_ID,
	C0, C1, C2, C3, C4, C5, C6, C7, C8, C9,
	W0, W1, W2, W3, W4, W5, W6, W7, W8, W9,
	SuctMin,
	SuctMax,
	MaxCondensingTemp, 
	Hertz_ID,
	RefrigerantType_ID,
	CompModel_ID)
	SELECT 
	@vendorTempRange,
	@param_ReturnGasID,
	C0, C1, C2, C3, C4, C5, C6, C7, C8, C9,
	W0, W1, W2, W3, W4, W5, W6, W7, W8, W9,
	SuctMin,
	SuctMax,
	MaxCondensingTemp,
	CAST(Hertz AS INT),
	RefrigerantTypeID,
	@param_ModelID
	FROM @param_CoefficientsUDT
END

GO


