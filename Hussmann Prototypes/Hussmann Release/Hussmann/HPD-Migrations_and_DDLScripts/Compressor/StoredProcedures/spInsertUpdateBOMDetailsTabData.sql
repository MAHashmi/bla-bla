IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spInsertUpdateBOMDetailsTabData'))
BEGIN
    DROP PROCEDURE spInsertUpdateBOMDetailsTabData
END
GO
-- =============================================
-- Author:		MURTAZA
-- Create date: 6/26/2018
-- Description:	Insert/Update BOM Details Tab data
-- =============================================
CREATE PROCEDURE [dbo].[spInsertUpdateBOMDetailsTabData]  
	@param_ModelID int,
	@param_CompVendorID int,
	@param_HussTempAppID int null,
	@param_VendorBOMCodeID int null,
	@param_CompTypeID int,
	@param_DiscLine nchar(4),
	@param_SuctLine nchar(4),
	@param_CompKitProtocol nvarchar(40),
	@param_CompKitHSeries nvarchar(40),
	@param_CompKitRack nvarchar(40)
	
AS
BEGIN
	--Check if ModelID exists in CompRLA table
	IF EXISTS(select top 1 * from CompRLA where CompModel_ID = @param_ModelID)
	BEGIN
	Declare @CompRLA_ID int;
	Select @CompRLA_ID = MAX(CompRLA_ID) FROM CompRLA where CompModel_ID = @param_ModelID;
		
		--Check if CompRLA_ID exists for current model (@param_ModelID)
		IF EXISTS(Select top 1 * from CompBOM where CompRLA_ID = @CompRLA_ID)
		BEGIN
			Update CompBOM
			SET
			HussTempApp_ID = @param_HussTempAppID,
			VendorBOMCode_ID = @param_VendorBOMCodeID,
			CompType_ID = @param_CompTypeID,
			DiscLine = @param_DiscLine,
			SuctLine = @param_SuctLine,
			CompKitProtocol = @param_CompKitProtocol,
			CompKitHSeries = @param_CompKitHSeries,
			CompKitRack = @param_CompKitRack
			WHERE CompRLA_ID = @CompRLA_ID;
		END
		ELSE
		BEGIN
			INSERT INTO CompBOM (
			CompRLA_ID,
			HussTempApp_ID,
			VendorBOMCode_ID,
			CompType_ID,
			DiscLine,
			SuctLine,
			CompKitProtocol,
			CompKitHSeries,
			CompKitRack) VALUES (
			@CompRLA_ID,
			@param_HussTempAppID,
			@param_VendorBOMCodeID,
			@param_CompTypeID,
			@param_DiscLine,
			@param_SuctLine,
			@param_CompKitProtocol,
			@param_CompKitHSeries,
			@param_CompKitRack)
		END
		--Update Compressor Vendor ID in Compressor Model
		Update CompressorModel
		SET
		CompVendor_ID = @param_CompVendorID
		WHERE CompModel_ID = @param_ModelID;
	END

END

GO


