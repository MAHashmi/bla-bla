IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spCreateCompressorModelWithNewVersion'))
BEGIN
    DROP PROCEDURE spCreateCompressorModelWithNewVersion
END

GO
-- =============================================
-- Author:		MURTAZA
-- Create date: 18/July/2018
-- Description:	Create New Version of Current Model
-- =============================================
CREATE PROCEDURE [dbo].[spCreateCompressorModelWithNewVersion]
	-- Add the parameters for the stored procedure here
	@param_ModelID INT,
	@param_ActionBy VARCHAR(75)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--------------------- Variable Declaration -----------------------
	DECLARE
		@VersionIncrement int = 1,
		@StateID int = 1, --State: DRAFT
		@NewModelID int = null,
		@NewCompRLAID int = null,
		@CurrentDate datetime = GETDATE(), --ModifiedDate
		@PreviousModelCreatedBy VARCHAR(75) = (SELECT CreatedBy FROM CompressorModel 
												WHERE CompModel_ID = @param_ModelID),
		@OldCompRLAID int = (SELECT MAX(CompRLA_ID) FROM CompRLA WHERE CompModel_ID = @param_ModelID)

    ------------------ Insert Into Compressor Model ------------------
	INSERT INTO CompressorModel(
			ModelNumber,
			CompSeries_ID,
			NomHorsepower,
			RealHorsePower,
			[Version],
			StateID,
			CreatedBy,
			LastModifiedOn,
			ActionBy,
			Note,
			CompVendor_ID,
			CapReduct_ID
	)
	--OUTPUT INSERTED.CompModel_ID
	SELECT	ModelNumber,
			CompSeries_ID,
			NomHorsePower,
			RealHorsePower,
			[Version] + @VersionIncrement,
			@StateID,
			@param_ActionBy,
			@CurrentDate,
			@param_ActionBy,
			Note,
			CompVendor_ID,
			CapReduct_ID
			FROM CompressorModel WHERE CompModel_ID = @param_ModelID

	SET @NewModelID = SCOPE_IDENTITY()

	------------- Insert Into Compressor Model Action History --------------------
	INSERT INTO [dbo].[CompressorModelActionHistory]
           ([ModelID]
           ,[StateID]
           ,[CreatedBy]
           ,[LastModifiedOn]
           ,[ActionBy]
           ,[XMLData])
     VALUES
           (@NewModelID
           ,@StateID
           ,@PreviousModelCreatedBy
           ,@CurrentDate
           ,@param_ActionBy
           ,(Select *  from CompressorModel where CompModel_ID = @NewModelID For XML Path));

	------------------ Insert Into CompRLA  ------------------
	INSERT INTO [dbo].[CompRLA]
			(
			Voltage_ID,
			CompModel_ID,
			RLA,
			VendorVoltageCode_ID,
			NEMA,
			DP,
			Breaker,
			WiringSize
			)
		SELECT
			Voltage_ID,
			@NewModelID,
			RLA,
			VendorVoltageCode_ID,
			NEMA,
			DP,
			Breaker,
			WiringSize
			FROM CompRLA WHERE CompModel_ID = @param_ModelID;
		SET @NewCompRLAID = SCOPE_IDENTITY()


	------------------ Insert Into CompBOM ------------------
	INSERT INTO [dbo].[CompBOM]
			(
			CompRLA_ID,
			HussTempApp_ID,
			VendorBOMCode_ID,
			CompType_ID,
			DiscLine,
			SuctLine,
			CompKitProtocol,
			CompKitHSeries,
			CompKitRack
			)
		SELECT
			@NewCompRLAID,
			HussTempApp_ID,
			VendorBOMCode_ID,
			CompType_ID,
			DiscLine,
			SuctLine,
			CompKitProtocol,
			CompKitHSeries,
			CompKitRack
			FROM CompBOM WHERE CompRLA_ID = @OldCompRLAID

	------------------ Insert Into Coefficients ------------------
	INSERT INTO [dbo].[Coefficients]
			(
			CompModel_ID,
			RefrigerantType_ID,
			Hertz_ID,
			VendorTempRange_ID,
			ReturnGas_ID,
			SuctMin,
			SuctMax,
			MaxCondensingTemp,
			C0, C1, C2, C3, C4, C5, C6, C7, C8, C9,
			W0, W1, W2, W3, W4, W5, W6, W7, W8, W9
			)
		SELECT
			@NewModelID,
			RefrigerantType_ID,
			Hertz_ID,
			VendorTempRange_ID,
			ReturnGas_ID,
			SuctMin,
			SuctMax,
			MaxCondensingTemp,
			C0, C1, C2, C3, C4, C5, C6, C7, C8, C9,
			W0, W1, W2, W3, W4, W5, W6, W7, W8, W9
			FROM Coefficients WHERE CompModel_ID = @param_ModelID

	----------- Return Compressor Model ID of new version -----------
	 select @NewModelID as 'ModelID'
END


GO
