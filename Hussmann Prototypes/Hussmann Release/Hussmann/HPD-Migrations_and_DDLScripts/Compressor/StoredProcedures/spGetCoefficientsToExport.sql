IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spGetCoefficientsToExport'))
BEGIN
    DROP PROCEDURE spGetCoefficientsToExport
END

GO
-- =============================================
-- Author:		MURTAZA
-- Create date: 17/July/2018
-- Description:	Get Coefficients to Export
-- =============================================
CREATE PROCEDURE spGetCoefficientsToExport
	-- Add the parameters for the stored procedure here
	@param_ModelID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT ReturnGasTemp AS ReturnGasTemperature,
	RefrigerantType,
	C0, C1, C2, C3, C4, C5, C6, C7, C8, C9,
	W0, W1, W2, W3, W4, W5, W6, W7, W8, W9,
	SuctMin,
	SuctMax,
	MaxCondensingTemp,
	CAST(H.Hertz_Type AS VARCHAR) + '/' + CAST(H.Phase AS VARCHAR) AS Hertz
	FROM Coefficients C
	LEFT JOIN ReturnGas RG ON C.ReturnGas_ID = RG.ReturnGas_ID
	LEFT JOIN RefrigerantType RT ON C.RefrigerantType_ID = RT.RefrigerantType_ID
	LEFT JOIN Hertz H ON C.Hertz_ID = H.Hertz_ID
	WHERE CompModel_ID = @param_ModelID
END
GO
