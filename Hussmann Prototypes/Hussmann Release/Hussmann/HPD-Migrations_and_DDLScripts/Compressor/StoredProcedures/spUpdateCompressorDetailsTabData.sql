IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spUpdateCompressorDetailsTabData'))
BEGIN
    DROP PROCEDURE spUpdateCompressorDetailsTabData
END

GO
-- =============================================
-- Author:		Faris khan
-- Create date: 6/20/2018
-- Description:	Update Compressor Details Tab data
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateCompressorDetailsTabData]  
	@param_ModelID int,
	@param_ModelNumber nvarchar(15),
	@param_NormalHP nvarchar(4),
	@param_RealHP decimal(9,1),
	@param_CompSeries_ID int,
	@param_CapReduct_ID int = null,
	@param_ActionBy varchar(75)
	
AS
BEGIN

	UPDATE CompressorModel 
	SET
	 ModelNumber = @param_ModelNumber,
	 NomHorsepower = @param_NormalHP,
	 RealHorsePower = @param_RealHP,
	 CompSeries_ID = @param_CompSeries_ID,
	 CapReduct_ID = @param_CapReduct_ID,
	 ActionBy = @param_ActionBy
	 where CompModel_ID = @param_ModelID

END


GO
