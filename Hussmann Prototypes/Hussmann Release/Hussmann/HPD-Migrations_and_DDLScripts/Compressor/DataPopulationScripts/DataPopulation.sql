------ Populating states in state table ---------
IF NOT EXISTS(SELECT 1 FROM dbo.State WHERE Description='Draft')
BEGIN
Insert Into [State]([Description]) values ('Draft');
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.State WHERE Description='Submitted' )
BEGIN
Insert Into [State]([Description]) values ('Submitted');
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.State WHERE Description='Rejected' )
BEGIN
Insert Into [State]([Description]) values ('Rejected');
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.State WHERE Description='Approved' )
BEGIN
Insert Into [State]([Description]) values ('Approved');
END
GO

IF NOT EXISTS(SELECT 1 FROM dbo.State WHERE Description='Deleted')
BEGIN
Insert Into [State]([Description]) values ('Deleted');
END
GO

------ Populate given sizes in SuctLineSize table ---------

IF NOT EXISTS(SELECT 1 FROM dbo.SuctLineSize)
BEGIN
	INSERT INTO dbo.SuctLineSize VALUES('04');
	INSERT INTO dbo.SuctLineSize VALUES('05');
	INSERT INTO dbo.SuctLineSize VALUES('07');
	INSERT INTO dbo.SuctLineSize VALUES('09');
	INSERT INTO dbo.SuctLineSize VALUES('11');
	INSERT INTO dbo.SuctLineSize VALUES('13');
	INSERT INTO dbo.SuctLineSize VALUES('17');
	INSERT INTO dbo.SuctLineSize VALUES('25');
END
GO

------ Populate given sizes in DiscLineSize table ---------

IF NOT EXISTS(SELECT 1 FROM dbo.DiscLineSize)
BEGIN
	INSERT INTO dbo.DiscLineSize VALUES('04');
	INSERT INTO dbo.DiscLineSize VALUES('05');
	INSERT INTO dbo.DiscLineSize VALUES('07');
	INSERT INTO dbo.DiscLineSize VALUES('09');
	INSERT INTO dbo.DiscLineSize VALUES('11');
	INSERT INTO dbo.DiscLineSize VALUES('13');
	INSERT INTO dbo.DiscLineSize VALUES('17');
	INSERT INTO dbo.DiscLineSize VALUES('21');
END
GO

------ Populate Dummy sizes in NEMA table ---------
TRUNCATE TABLE NEMA;
GO
IF NOT EXISTS(SELECT 1 FROM dbo.NEMA)
BEGIN 
	INSERT INTO dbo.NEMA VALUES('00');
	INSERT INTO dbo.NEMA VALUES('0');
	INSERT INTO dbo.NEMA VALUES('1');
	INSERT INTO dbo.NEMA VALUES('2');
	INSERT INTO dbo.NEMA VALUES('3');
	INSERT INTO dbo.NEMA VALUES('4');
	INSERT INTO dbo.NEMA VALUES('5');
	INSERT INTO dbo.NEMA VALUES('6');
END
GO

------ Populate Dummy sizes in DP table ---------
TRUNCATE TABLE DP;
GO
IF NOT EXISTS(SELECT 1 FROM dbo.DP)
BEGIN
	INSERT INTO dbo.DP VALUES(20);
	INSERT INTO dbo.DP VALUES(30);
	INSERT INTO dbo.DP VALUES(40);
	INSERT INTO dbo.DP VALUES(50);
	INSERT INTO dbo.DP VALUES(60);
	INSERT INTO dbo.DP VALUES(75);
	INSERT INTO dbo.DP VALUES(90);
	INSERT INTO dbo.DP VALUES(115);
	INSERT INTO dbo.DP VALUES(150);
	INSERT INTO dbo.DP VALUES(220);
	INSERT INTO dbo.DP VALUES(353);

END
GO
------ Populate Dummy sizes in Breaker table ---------
TRUNCATE TABLE Breaker;
GO
IF NOT EXISTS(SELECT 1 FROM dbo.Breaker)
BEGIN
	INSERT INTO dbo.Breaker VALUES(15);
	INSERT INTO dbo.Breaker VALUES(20);
	INSERT INTO dbo.Breaker VALUES(30);
	INSERT INTO dbo.Breaker VALUES(40);
	INSERT INTO dbo.Breaker VALUES(50);
	INSERT INTO dbo.Breaker VALUES(60);
	INSERT INTO dbo.Breaker VALUES(70);
	INSERT INTO dbo.Breaker VALUES(80);
	INSERT INTO dbo.Breaker VALUES(90);
	INSERT INTO dbo.Breaker VALUES(100);
	INSERT INTO dbo.Breaker VALUES(110);
	INSERT INTO dbo.Breaker VALUES(125);
	INSERT INTO dbo.Breaker VALUES(150);
	INSERT INTO dbo.Breaker VALUES(175);
	INSERT INTO dbo.Breaker VALUES(200);
	INSERT INTO dbo.Breaker VALUES(225);
	INSERT INTO dbo.Breaker VALUES(250);
	INSERT INTO dbo.Breaker VALUES(300);
	INSERT INTO dbo.Breaker VALUES(350);
	INSERT INTO dbo.Breaker VALUES(400);
	INSERT INTO dbo.Breaker VALUES(500);
	INSERT INTO dbo.Breaker VALUES(600);
	INSERT INTO dbo.Breaker VALUES(700);
END
GO
------ Populate Dummy sizes in WiringSize table ---------

IF NOT EXISTS(SELECT 1 FROM dbo.WiringSize)
BEGIN
	INSERT INTO dbo.WiringSize VALUES(1);
	INSERT INTO dbo.WiringSize VALUES(2);
	INSERT INTO dbo.WiringSize VALUES(3);
	INSERT INTO dbo.WiringSize VALUES(4);
	INSERT INTO dbo.WiringSize VALUES(5);
	INSERT INTO dbo.WiringSize VALUES(6);
	INSERT INTO dbo.WiringSize VALUES(7);
	INSERT INTO dbo.WiringSize VALUES(8);
	INSERT INTO dbo.WiringSize VALUES(9);
	INSERT INTO dbo.WiringSize VALUES(10);
	INSERT INTO dbo.WiringSize VALUES(11);
	INSERT INTO dbo.WiringSize VALUES(12);
	INSERT INTO dbo.WiringSize VALUES(13);
	INSERT INTO dbo.WiringSize VALUES(14);
	INSERT INTO dbo.WiringSize VALUES(15);
END
GO