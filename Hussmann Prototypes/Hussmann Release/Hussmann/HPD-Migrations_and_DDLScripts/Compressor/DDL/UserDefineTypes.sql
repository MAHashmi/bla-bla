IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'CoefficientsUDT')
BEGIN
CREATE TYPE [dbo].[CoefficientsUDT] AS TABLE(
	[C0] [decimal](28, 22) NOT NULL,
	[C1] [decimal](28, 22) NOT NULL,
	[C2] [decimal](28, 22) NOT NULL,
	[C3] [decimal](28, 22) NOT NULL,
	[C4] [decimal](28, 22) NOT NULL,
	[C5] [decimal](28, 22) NOT NULL,
	[C6] [decimal](28, 22) NOT NULL,
	[C7] [decimal](28, 22) NOT NULL,
	[C8] [decimal](28, 22) NOT NULL,
	[C9] [decimal](28, 22) NOT NULL,
	[W0] [decimal](28, 22) NOT NULL,
	[W1] [decimal](28, 22) NOT NULL,
	[W2] [decimal](28, 22) NOT NULL,
	[W3] [decimal](28, 22) NOT NULL,
	[W4] [decimal](28, 22) NOT NULL,
	[W5] [decimal](28, 22) NOT NULL,
	[W6] [decimal](28, 22) NOT NULL,
	[W7] [decimal](28, 22) NOT NULL,
	[W8] [decimal](28, 22) NOT NULL,
	[W9] [decimal](28, 22) NOT NULL,
	[SuctMin] [decimal](28, 22) NOT NULL,
	[SuctMax] [decimal](28, 22) NOT NULL,
	[MaxCondensingTemp] [decimal](28, 22) NOT NULL,
	[Hertz] [varchar](2) NOT NULL,
	[RefrigerantTypeID] [int] NOT NULL,
	[ModelID] [int] NOT NULL
)
END
GO


IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'CompRLAVoltageUDT')
BEGIN
CREATE TYPE [dbo].[CompRLAVoltageUDT] AS TABLE(
[CompRLAVoltage_ID] [int],
[CompRLA_ID] [int],
[Voltage_ID] [int])
END
GO


IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'RLADetailsUDT')
BEGIN
CREATE TYPE [dbo].[RLADetailsUDT] AS TABLE(
	[Voltage_ID] [int] NULL,
	[CompModel_ID] [int] NULL,
	[RLA] [decimal](9, 1) NULL,
	[VendorVoltageCode_ID] [int] NULL,
	[NEMA] [nchar](5) NULL,
	[DP] [int] NULL,
	[Breaker] [int] NULL,
	[WiringSize] [nvarchar](10) NULL
)
END
GO


