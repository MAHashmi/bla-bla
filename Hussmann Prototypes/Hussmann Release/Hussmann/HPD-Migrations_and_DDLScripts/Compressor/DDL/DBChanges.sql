------------------------- For State -------------
If Not Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'State')
Begin
CREATE TABLE [dbo].[State](
	StateID int Identity(1,1) NOT NULL Primary Key,
	Description varchar(15) NOT NULL
)
End
Go

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompressorModel'
			And	Column_Name = 'Version')
Begin
	Alter Table CompressorModel
	Add Version int NOT NULL DEFAULT(1) -- Set default value of 1 only at the time of column creation into exsisting rows
End
GO
-------------------- For adding StateID as a foreign key in CondenserModel--------------------------------
If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompressorModel'
			And	Column_Name = 'StateID')
Begin
	ALTER TABLE CompressorModel
	ADD StateID integer CONSTRAINT fk FOREIGN KEY (StateID) REFERENCES [State](StateID) 
End
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompressorModel'
			And	Column_Name = 'CreatedBy')
Begin
	Alter Table CompressorModel
	Add CreatedBy varchar(75) Not NULL Default('System') -- Maximum legth of windows domain user is 256 but we are assuming that users do not usually set long names
End
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompressorModel'
			And	Column_Name = 'LastModifiedOn')
Begin
	Alter Table CompressorModel
	Add LastModifiedOn Datetime Not NULL Default(GETDATE()) -- Set default value of current date only at the time of column creation into exsisting rows
End
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompressorModel'
			And	Column_Name = 'ActionBy')
Begin
	Alter Table CompressorModel
	Add ActionBy varchar(75) Not NULL Default('System') -- Maximum legth of windows domain user is 256 but we are assuming that users do not usually set long names
End
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompressorModel'
			And	Column_Name = 'Note')
Begin
	Alter Table CompressorModel
	Add Note varchar(250) NULL
End
GO

------------------------- For maitaining Action History -------------
If Not Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompressorModelActionHistory')
Begin
CREATE TABLE [dbo].[CompressorModelActionHistory](
	ModelID int FOREIGN KEY REFERENCES [CompressorModel](CompModel_ID),
	StateID int FOREIGN KEY REFERENCES [State](StateID),
	CreatedBy varchar(75) not null,-- Maximum legth of windows domain user is 256 but we are assuming that users do not usually set long names
	LastModifiedOn Datetime not null,
	ActionBy varchar(75) not null,-- Maximum legth of windows domain user is 256 but we are assuming that users do not usually set long names
	XMLData varchar(max)
)
End
Go

------------------------- Add Column CompType_ID to CompressorModel --------------------------
If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompressorModel'
			And	Column_Name = 'CompType_ID')
BEGIN
	Alter table dbo.CompressorModel
	Add CompType_ID int null,
	constraint FK_CompType FOREIGN KEY(CompType_ID) references CompType(CompType_ID)
END
GO
------------------------- Add Column RefrigerantType_ID to CompressorModel --------------------------
If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompressorModel'
			And	Column_Name = 'RefrigerantType_ID')
BEGIN
	Alter table dbo.CompressorModel
	Add RefrigerantType_ID int null,
	constraint FK_RefrigerantType FOREIGN KEY(RefrigerantType_ID) references RefrigerantType(RefrigerantType_ID);
END
GO
------------------------- Add Column CompVendor_ID to CompressorModel --------------------------
If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompressorModel'
			And	Column_Name = 'CompVendor_ID')
BEGIN
	Alter table dbo.CompressorModel
	Add CompVendor_ID int null,
	constraint FK_CompVendor FOREIGN KEY(CompVendor_ID) references CompVendors(CompVendor_ID);
END
Go

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompressorModel'
			And	Column_Name = 'CapReduct_ID')
Begin
	Alter Table CompressorModel 
	Add CapReduct_ID integer CONSTRAINT [CapReduct_CompressorModel_fk] FOREIGN KEY([CapReduct_ID])
REFERENCES [dbo].[CapacityReduction] ([CapReduct_ID])End
GO

------------------------- DROP Column CompType_ID to CompressorModel --------------------------
If Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompressorModel'
			And	Column_Name = 'CompType_ID')
BEGIN
	Alter table dbo.CompressorModel drop constraint [FK_CompType]
	Alter table dbo.CompressorModel drop column [CompType_ID]
END
Go

-------------------------- Add Column Active in Table Refrigerant Type ----------------------------

If NOT Exists (Select 1 from Information_Schema.Columns
			Where Table_Name = 'RefrigerantType'
			And	Column_Name = 'Active')
BEGIN
Alter table RefrigerantType
add [Active] BIT NOT NULL
CONSTRAINT DF_ActiveTrue DEFAULT 1
END
GO

------------------------- Add Table SuctLineSize -------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'SuctLineSize'))
BEGIN
CREATE TABLE [dbo].[SuctLineSize] (
	SuctLineSizeID INT IDENTITY(1,1),
	SuctLineSize NVARCHAR(4) NOT NULL
	CONSTRAINT [PK_SuctLine] PRIMARY KEY (SuctLineSizeID)
)
END
GO
------------------------- Add Table DiscLineSize -------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'DiscLineSize'))
BEGIN
CREATE TABLE [dbo].[DiscLineSize] (
	DiscLineSizeID INT IDENTITY(1,1),
	DiscLineSize NVARCHAR(4) NOT NULL
	CONSTRAINT [PK_DiscLine] PRIMARY KEY (DiscLineSizeID)
)
END
GO
------------------------- Add Table NEMA -------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'NEMA'))
BEGIN
	CREATE TABLE NEMA (
	NEMAID INT IDENTITY(1,1),
	NEMA NVARCHAR(10) NOT NULL,
	CONSTRAINT [PK_NEMA] PRIMARY KEY (NEMAID)
	)
END
GO
------------------------- Add Table DP -------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'DP'))
BEGIN
	CREATE TABLE DP (
	DPID INT IDENTITY(1,1),
	DP INT NOT NULL,
	CONSTRAINT [PK_DP] PRIMARY KEY (DPID)
	)
END
GO
------------------------- Add Table Breaker -------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Breaker'))
BEGIN
	CREATE TABLE Breaker (
	BreakerID INT IDENTITY(1,1),
	Breaker INT NOT NULL,
	CONSTRAINT [PK_Breaker] PRIMARY KEY (BreakerID)
	)
END
GO
------------------------- Add Table WiringSize -------------------------

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'WiringSize'))
BEGIN
	CREATE TABLE WiringSize (
	WiringSizeID INT IDENTITY(1,1),
	WiringSize NVARCHAR(20) NOT NULL,
	CONSTRAINT [PK_WiringSize] PRIMARY KEY (WiringSizeID)
	)
END
GO
------------ Delete Duplicate Entries FROM CompRLA Table -------------

BEGIN
ALTER TABLE [dbo].[CompBOM]
DROP CONSTRAINT [CompBOM_CompRLA_fk]

ALTER TABLE [dbo].[CompBOM]  WITH CHECK ADD CONSTRAINT [CompBOM_CompRLA_fk] FOREIGN KEY([CompRLA_ID])
REFERENCES [dbo].[CompRLA] ([CompRLA_ID]) ON DELETE CASCADE

;WITH CTE AS (
	SELECT *, RN = ROW_NUMBER() OVER (PARTITION BY CompModel_ID order by CompRLA_ID) 
	from CompRLA)

Delete from CTE where RN > 1;
END
GO

If Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompRLA'
			And	Column_Name = 'Voltage_ID')
Begin

ALTER TABLE CompRLA ALTER COLUMN Voltage_ID INT NULL


End
Go


------------ Delete Duplicate Entries FROM CompBOM Table -------------

BEGIN
;WITH CTE AS (
	SELECT *, RN = ROW_NUMBER() OVER (PARTITION BY CompRLA_ID order by CompRLA_ID)
	from CompBOM)

DELETE from CTE WHERE RN > 1;
END
GO

If Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompBOM'
			And	Column_Name = 'HussTempApp_ID')
Begin

ALTER TABLE CompBOM ALTER COLUMN HussTempApp_ID INT NULL


End
Go


If Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompBOM'
			And	Column_Name = 'VendorBOMCode_ID')
Begin

ALTER TABLE CompBOM ALTER COLUMN VendorBOMCode_ID INT NULL


End
Go

-------------- Add Column Active in ReturnGas Table ---------------

If NOT Exists (Select 1 from Information_Schema.Columns
			Where Table_Name = 'ReturnGas'
			And	Column_Name = 'Active')
BEGIN
ALTER TABLE ReturnGas
Add [Active] bit not null 
CONSTRAINT Active_False DEFAULT 0
END
GO
--------------- Set ReturnGasTemp 65 to Active -----------------

UPDATE ReturnGas SET [Active] = 1 WHERE ReturnGasTemp = 65
GO

-------------- Table CompRLAVoltage ----------------------------


If Not Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CompRLAVoltage')
Begin
CREATE TABLE [dbo].[CompRLAVoltage](
	CompRLAVoltage_ID int Identity(1,1) NOT NULL Primary Key,
	CompRLA_ID INT NOT NULL,
	Voltage_ID INT NOT NULL,
	constraint FK_CompRLAVoltage_Voltage FOREIGN KEY(Voltage_ID) references Voltage(Voltage_ID),
	constraint FK_CompRLAVoltage_CompRLA FOREIGN KEY(CompRLA_ID) references CompRLA(CompRLA_ID)
)
End
Go


