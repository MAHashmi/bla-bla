DROP PROCEDURE [dbo].[spUpdateDetailsTabData] 
go


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jaffer Raza
-- Create date: 10/2/2018
-- Description:	Update Details Tab data
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateDetailsTabData]  
	@param_ModelID int,
	@param_MFGID int,
	@param_ModelFamilyID int,
	@param_ModelNumber nvarchar(15),
	@param_CapacityPerDegreeTDNeg20  int,
	@param_CapacityPerDegreeTDPos25 int,
	@param_ActionBy varchar(75),
	@param_CoilModelYear varchar(4),
	@param_CoilMakeYear varchar(4),
	@param_NumberOfFans int 
	
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE CoilModel 
	SET
	 MFGID = @param_MFGID,
	 ModelFamilyID = @param_ModelFamilyID,
	 ModelNumber = @param_ModelNumber,
	 CapacityPerDegreeTDNeg20 = @param_CapacityPerDegreeTDNeg20,
	 CapacityPerDegreeTDPos25 = @param_CapacityPerDegreeTDPos25,
	 LastModifiedOn = GETDATE(),
	 ActionBy =  @param_ActionBy,
	 CoilModelYear = @param_CoilModelYear,
	 CoilMakeYear = @param_CoilMakeYear,
	 NumberofFans = @param_NumberOfFans
	 where ModelID = @param_ModelID

	
END
GO