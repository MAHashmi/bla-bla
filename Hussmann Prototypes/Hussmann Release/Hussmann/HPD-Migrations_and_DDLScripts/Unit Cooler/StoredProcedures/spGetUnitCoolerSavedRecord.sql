DROP PROCEDURE [dbo].[spGetUnitCoolerSavedRecord]
go


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jaffer
-- Create date: 18/1/2018
-- Description:	Get top 10 recently saved records
-- =============================================
CREATE PROCEDURE [dbo].[spGetUnitCoolerSavedRecord]
	@param_UserType varchar(20),
	@param_UserName varchar(75)
AS
BEGIN
	IF(@param_UserType = 'Super' OR @param_UserType = 'Approver')
	BEGIN
	SELECT TOP 10 
	CM.ModelID,
	CM.StateID,
	CM.LastModifiedOn,
	CM.CreatedBy,
	CM.ActionBy, 
	C.ModelNumber
	FROM CoilModelActionHistory CM
	join CoilModel C
	on C.ModelID = CM.ModelID
	WHERE CM.StateID = 1 -- draft or saved state
	ORDER BY CM.LastModifiedOn DESC
	END

	IF(@param_UserType = 'DataFeeder')
	BEGIN
	SELECT TOP 10 
	CM.ModelID,
	CM.StateID,
	CM.LastModifiedOn,
	CM.CreatedBy,
	CM.ActionBy,
	C.ModelNumber 
	FROM CoilModelActionHistory CM
	join CoilModel C
	on C.ModelID = CM.ModelID
	WHERE CM.StateID = 1 -- draft or saved state
	AND CM.CreatedBy = @param_UserName
	ORDER BY CM.LastModifiedOn DESC
	END

	ELSE
	BEGIN
	SELECT TOP 10 
	CM.ModelID,
	CM.StateID,
	CM.LastModifiedOn,
	CM.CreatedBy,
	CM.ActionBy,
	C.ModelNumber 
	FROM CoilModelActionHistory CM
	join CoilModel C
	on C.ModelID = CM.ModelID
	WHERE 1=0
	END

END
GO