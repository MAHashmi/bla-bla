DROP PROCEDURE [dbo].[spGetCoilMotorAmps] 
go
-- =============================================
-- Author:		Asad
-- Create date: 6/2/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[spGetCoilMotorAmps]
@param_ModelID int
AS
BEGIN

DECLARE @parseModelID AS NVARCHAR(50) = convert(nvarchar(50), @param_ModelID) 

DECLARE @cols AS NVARCHAR(MAX),
    @query  AS NVARCHAR(MAX)

select @cols = STUFF((SELECT ',' + QUOTENAME(MotorTypeID) 
                    from CoilMotorAmps
					where ModelID = @param_ModelID
                    group by MotorTypeID
                    order by MotorTypeID
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

set @query = ' select VYL.VoltageTypeID,' + @cols + ' from (SELECT 
				VoltageTypeID,' + @cols + ' from 
             (
                select 
				   VoltageTypeID
				 ,motorTypeID
					,MotorAmps
                from CoilMotorAmps
					where ModelID = '+ @parseModelID +'
            ) x
            pivot 
            (
                MAX(MotorAmps) FOR motorTypeID IN (' + @cols + ')
            ) p ) m
			right join 
			VoltageType_lkp VYL on m.VoltageTypeID = VYL.VoltageTypeID
'	
execute(@query);

END





