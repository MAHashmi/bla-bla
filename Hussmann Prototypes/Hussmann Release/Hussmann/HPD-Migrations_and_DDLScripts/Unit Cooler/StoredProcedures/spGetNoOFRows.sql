DROP PROCEDURE [dbo].[spGetNoOFRows] 
go

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetNoOFRows] 
	@param_ModelNumber varchar(30) = null,
	@param_CoilBrandId int = null,
	@param_CoilFamilyId int = null ,
	@param_StatusId int  = null,
	@param_UserName varchar(75),
	@param_UserType varchar(20)
AS
BEGIN
select Count(*) as TotalRows from(
SELECT ModelID
	,ModelNumber
	,NumberofFans
	,LastModifiedOn
	,[Version]
	,ActionBy
	,CreatedBy
	,CoilFamilyName
	,CoilBrandName + ' ' + CASE 
		WHEN CoilSubBrandName IS NOT NULL
			THEN '-' + CoilSubBrandName
		ELSE ''
		END AS CoilBrandName
	,[Description] AS StateDescription

	
FROM (
	SELECT ModelID
		,ModelNumber
		,NumberofFans
		,LastModifiedOn
		,[Version]
		,ActionBy
		,CreatedBy
		,ModelFamilyID
		,MFGID
		,StateID
		,ROW_NUMBER() OVER (
			PARTITION BY ModelNumber ORDER BY [Version] DESC
			) AS rownum
	FROM CoilModel
	WHERE StateID = 4
	) AS CM
INNER JOIN [State] St ON cm.StateID = st.StateID
INNER JOIN CoilFamily CF ON CF.CoilFamilyID = cm.ModelFamilyID
INNER JOIN CoilBrand_lkp CB ON cb.BrandID = CM.MFGID
WHERE rownum = 1
	AND (
		ModelNumber LIKE '%' + @param_ModelNumber + '%'
		OR @param_ModelNumber IS NULL
		)
	AND (
		CB.BrandID = @param_CoilBrandId
		OR @param_CoilBrandId IS NULL
		)
	AND (
		CF.CoilFamilyID = @param_CoilFamilyId
		OR @param_CoilFamilyId IS NULL
		)
	AND (
		St.StateID = @param_StatusId
		OR @param_StatusId IS NULL
		)
	AND (St.StateID = 4)
------------------ Latest Approved Versions END --------------------

UNION

SELECT ModelID
	,ModelNumber
	,NumberofFans
	,LastModifiedOn
	,[Version]
	,ActionBy
	,CreatedBy
	,CoilFamilyName
	,CoilBrandName + ' ' + CASE 
		WHEN CoilSubBrandName IS NOT NULL
			THEN '-' + CoilSubBrandName
		ELSE ''
		END AS CoilBrandName
	,[Description] AS StateDescription
FROM CoilModel CM
INNER JOIN [State] St ON cm.StateID = st.StateID
INNER JOIN CoilFamily CF ON CF.CoilFamilyID = cm.ModelFamilyID
INNER JOIN CoilBrand_lkp CB ON cb.BrandID = CM.MFGID
WHERE (
		ModelNumber LIKE '%' + @param_ModelNumber + '%'
		OR @param_ModelNumber IS NULL
		)
	AND (
		CB.BrandID = @param_CoilBrandId
		OR @param_CoilBrandId IS NULL
		)
	AND (
		CF.CoilFamilyID = @param_CoilFamilyId
		OR @param_CoilFamilyId IS NULL
		)
	AND (
		St.StateID = @param_StatusId
		OR @param_StatusId IS NULL
		)
	AND (St.StateID != 4)
	AND (
			(
			@param_UserType = 'DataFeeder'
			AND CreatedBy = @param_UserName
			)
			OR
						(
			@param_UserType = 'Reader'
			AND St.StateID = 0
			)
			OR
			(@param_UserType = 'Super' OR @param_UserType = 'Approver' )
	
		)
		)A

END




