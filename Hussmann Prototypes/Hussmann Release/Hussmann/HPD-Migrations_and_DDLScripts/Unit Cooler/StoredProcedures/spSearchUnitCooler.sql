DROP PROCEDURE [dbo].[spSearchUnitCooler] 
go


/****** Object:  StoredProcedure [dbo].[spSearchUnitCooler]    Script Date: 2/12/2018 5:51:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
-- =============================================    
-- Author:  Asad    
-- Create date: 18/1/2018    
-- Description: Get Data Against given filter    
-- =============================================    
CREATE PROCEDURE [dbo].[spSearchUnitCooler]    
 @param_ModelNumber varchar(30) = null,    
 @param_CoilBrandId int = null,    
 @param_CoilFamilyId int = null ,    
 @param_StatusId int  = null,    
 @param_UserName varchar(75),    
 @param_UserType varchar(20),    
 @param_StartRecord int,    
 @param_NoOFRecords int,    
 @param_SortExpression varchar(20),    
 @param_SortDirection varchar(20)    
AS    
BEGIN    
select ModelID    
 ,ModelNumber    
 ,NumberofFans    
 ,CONVERT(VARCHAR, LastModifiedOn, 120) as 'LastModifiedOn'    
 ,[Version]    
 ,ActionBy    
 ,CreatedBy    
 ,CoilFamilyName    
 ,CoilBrandName    
 ,StateDescription
 ,ActiveInactiveStatus
    
 from(    
 select    
 ModelID    
 ,ModelNumber    
 ,NumberofFans    
 ,LastModifiedOn    
 ,[Version]    
 ,ActionBy    
 ,CreatedBy    
 ,CoilFamilyName    
 ,CoilBrandName    
 , StateDescription
 ,ActiveInactiveStatus
 ,ROW_NUMBER() OVER (ORDER BY     
  CASE WHEN (@param_SortExpression = 'ModelNumber' AND @param_SortDirection='Ascending')    
                    THEN ModelNumber    
        END ASC,    
        CASE WHEN (@param_SortExpression = 'ModelNumber' AND @param_SortDirection='Descending')    
                   THEN ModelNumber    
        END DESC,    
    
        CASE WHEN (@param_SortExpression = 'CoilBrandName' AND @param_SortDirection='Ascending')    
                  THEN CoilBrandName    
        END ASC,    
        CASE WHEN (@param_SortExpression = 'CoilBrandName' AND @param_SortDirection='Descending')    
                 THEN CoilBrandName    
        END DESC,    
    
        CASE WHEN @param_SortExpression = 'CoilFamilyName' AND @param_SortDirection='Ascending'    
                THEN CoilFamilyName    
        END ASC,    
        CASE WHEN @param_SortExpression = 'CoilFamilyName' AND @param_SortDirection='Descending'    
                THEN CoilFamilyName    
        END DESC,    
    
        CASE WHEN @param_SortExpression = 'NumberofFans' AND @param_SortDirection='Ascending'    
                THEN NumberofFans    
        END ASC,    
        CASE WHEN @param_SortExpression = 'NumberofFans' AND @param_SortDirection='Descending'    
               THEN NumberofFans    
        END DESC,    
   CASE WHEN @param_SortExpression = 'StateDescription' AND @param_SortDirection='Ascending'    
              THEN StateDescription    
        END ASC,    
        CASE WHEN @param_SortExpression = 'StateDescription' AND @param_SortDirection='Descending'    
              THEN StateDescription    
        END DESC,    
  CASE WHEN @param_SortExpression = 'Version' AND @param_SortDirection='Ascending'    
              THEN [Version]    
        END ASC,    
        CASE WHEN @param_SortExpression = 'Version' AND @param_SortDirection='Descending'    
              THEN [Version]    
        END DESC,    
  CASE WHEN @param_SortExpression = 'LastModifiedOn' AND @param_SortDirection='Ascending'    
              THEN LastModifiedOn    
        END ASC,    
        CASE WHEN @param_SortExpression = 'LastModifiedOn' AND @param_SortDirection='Descending'    
              THEN LastModifiedOn    
        END DESC,    
  CASE WHEN @param_SortExpression = 'ActionBy' AND @param_SortDirection='Ascending'    
              THEN ActionBy    
        END ASC,    
        CASE WHEN @param_SortExpression = 'ActionBy' AND @param_SortDirection='Descending'    
              THEN ActionBy    
        END DESC
		,    
  CASE WHEN @param_SortExpression = 'ActiveInactiveStatus' AND @param_SortDirection='Ascending'    
              THEN ActiveInactiveStatus    
        END ASC,    
        CASE WHEN @param_SortExpression = 'ActiveInactiveStatus' AND @param_SortDirection='Descending'    
              THEN ActiveInactiveStatus    
        END DESC
  ) AS RowNumber    
 from(    
SELECT ModelID    
 ,ModelNumber    
 ,NumberofFans    
 ,LastModifiedOn    
 ,[Version]    
 ,ActionBy    
 ,CreatedBy    
 ,CoilFamilyName    
 ,CoilBrandName + ' ' + CASE     
  WHEN CoilSubBrandName IS NOT NULL    
   THEN '-' + CoilSubBrandName    
  ELSE ''    
  END AS CoilBrandName    
 ,[Description] AS StateDescription    
 ,CASE WHEN [Description] = 'Deleted' THEN 'Activate' ELSE 'Inactivate' END as ActiveInactiveStatus   
     
FROM (    
 SELECT ModelID    
  ,ModelNumber    
  ,NumberofFans    
  ,LastModifiedOn    
  ,[Version]    
  ,ActionBy    
  ,CreatedBy    
  ,ModelFamilyID    
  ,MFGID    
  ,StateID    
  ,ROW_NUMBER() OVER (    
   PARTITION BY ModelNumber ORDER BY [Version] DESC    
   ) AS rownum    
 FROM CoilModel    
 WHERE StateID = 4    
 ) AS CM    
INNER JOIN [State] St ON cm.StateID = st.StateID    
INNER JOIN CoilFamily CF ON CF.CoilFamilyID = cm.ModelFamilyID    
INNER JOIN CoilBrand_lkp CB ON cb.BrandID = CM.MFGID    
WHERE rownum = 1    
 AND (    
  ModelNumber LIKE '%' + @param_ModelNumber + '%'    
  OR @param_ModelNumber IS NULL    
  )    
 AND (    
  CB.BrandID = @param_CoilBrandId    
  OR @param_CoilBrandId IS NULL    
  )    
 AND (    
  CF.CoilFamilyID = @param_CoilFamilyId    
  OR @param_CoilFamilyId IS NULL    
  )    
 AND (    
  St.StateID = @param_StatusId    
  OR @param_StatusId IS NULL    
  )    
 AND (St.StateID = 4)    
------------------ Latest Approved Versions END --------------------    
    
UNION    
    
SELECT ModelID    
 ,ModelNumber    
 ,NumberofFans    
 ,LastModifiedOn    
 ,[Version]    
 ,ActionBy    
 ,CreatedBy    
 ,CoilFamilyName    
 ,CoilBrandName + ' ' + CASE     
  WHEN CoilSubBrandName IS NOT NULL    
   THEN '-' + CoilSubBrandName    
  ELSE ''    
  END AS CoilBrandName    
 ,[Description] AS StateDescription
 ,CASE WHEN [Description] = 'Deleted' THEN 'Activate' ELSE 'Inactivate' END as ActiveInactiveStatus
FROM CoilModel CM    
INNER JOIN [State] St ON cm.StateID = st.StateID    
INNER JOIN CoilFamily CF ON CF.CoilFamilyID = cm.ModelFamilyID    
INNER JOIN CoilBrand_lkp CB ON cb.BrandID = CM.MFGID    
WHERE(    
  ModelNumber LIKE '%' + @param_ModelNumber + '%'    
  OR @param_ModelNumber IS NULL    
  )    
 AND (    
  CB.BrandID = @param_CoilBrandId    
  OR @param_CoilBrandId IS NULL    
  )    
 AND (    
  CF.CoilFamilyID = @param_CoilFamilyId    
  OR @param_CoilFamilyId IS NULL    
  )    
 AND (    
  St.StateID = @param_StatusId    
  OR @param_StatusId IS NULL    
  )    
 AND (St.StateID != 4)    
 AND (    
   (    
   @param_UserType = 'DataFeeder'    
   AND CreatedBy = @param_UserName    
   )    
   OR    
      (    
   @param_UserType = 'Reader'    
   AND St.StateID = 0    
   )    
   OR    
   (@param_UserType = 'Super' OR @param_UserType = 'Approver' )    
     
  )    
  ) A)as B    
  where    
  B.RowNumber between @param_StartRecord and @param_NoOFRecords    
END    
