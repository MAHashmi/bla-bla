Drop PROCEDURE [dbo].[spGetCoilDrainHtrAmps]

Create PROCEDURE [dbo].[spGetCoilDrainHtrAmps] @param_ModelId INT = NULL
AS
BEGIN
	SELECT
		CAP.Type_ID
       ,CAP.VoltageTypeID
	   ,CAP.ModelID
	   ,VA.VoltageName
	   ,CAP.DrainHtrAmps
		
		
	FROM CoilDrainHtrAmps CAP
	LEFT JOIN VoltageType_lkp VA ON CAP.VoltageTypeID = VA.VoltageTypeID
	WHERE(
			CAP.ModelID = @param_ModelId
			OR @param_ModelId is NULL
			)
END


