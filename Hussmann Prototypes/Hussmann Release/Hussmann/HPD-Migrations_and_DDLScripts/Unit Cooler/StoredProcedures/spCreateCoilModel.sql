IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[spSetNewCoilModel]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[spSetNewCoilModel]
END

IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[spCreateCoilModel]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[spCreateCoilModel]
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jaffer Raza
-- Create date: 6/2/2018
-- Description:	Create coil Model
-- =============================================
CREATE PROCEDURE [dbo].[spCreateCoilModel]

	@param_MFGID int,
	@param_ModelFamilyID int,
	@param_ModelNumber nvarchar(15),
	@param_CapacityPerDegreeTDNeg20  int,
	@param_CapacityPerDegreeTDPos25 int,
	@param_CreatedBy varchar(75),
	@param_CoilModelYear varchar(4),
	@param_CoilMakeYear varchar(4),
	@param_NumberOfFans int 

	
AS
BEGIN
	
	DECLARE @Version int = 1,
			@StateID int = 1,
			@SelectedFan int = 1,
			@ModelID int,
			@CurrentDate datetime = GETDATE(),
			@FansExsistInFamily int ;

	Insert Into CoilModel(
			MFGID,
			ModelFamilyID,
			ModelNumber,
			CapacityPerDegreeTDNeg20,
			CapacityPerDegreeTDPos25,
			NumberofFans,
			[Version],
			LastModifiedOn,
			Note,
			CreatedBy,
			ActionBy,
			StateID,
			CoilModelYear,
			CoilMakeYear,
			SelectedFan)
	select
		 	@param_MFGID,
		 	@param_ModelFamilyID,
		 	@param_ModelNumber,
		 	@param_CapacityPerDegreeTDNeg20,
		 	@param_CapacityPerDegreeTDPos25,
		 	@param_NumberOfFans ,
		 	@Version  ,
		 	@CurrentDate,
		 	Null ,
		 	@param_CreatedBy ,
		 	@param_CreatedBy ,
		 	@StateID ,
		 	@param_CoilModelYear,
		 	@param_CoilMakeYear,
			@SelectedFan;

		
	SET @ModelID = SCOPE_IDENTITY();

	INSERT INTO [dbo].[CoilModelActionHistory]
           ([ModelID]
           ,[StateID]
           ,[CreatedBy]
           ,[LastModifiedOn]
           ,[ActionBy]
           ,[XMLData])
     VALUES
           (@ModelID
           ,@StateID
           ,@param_CreatedBy
           ,@CurrentDate
           ,@param_CreatedBy
           ,(Select *  from CoilModel where ModelID = @ModelID  For XML Path));
	

	----- Select exsisting number of Fans exsist against the Family -------------------
	Set @FansExsistInFamily = (select Count(*) from CoilOptionsPricing where CoilFamilyID = @param_ModelFamilyID); 

	----- Table variable for new rows insertion in CoilOptionPricing --------
	DECLARE @CoilOptionPricing TABLE
			   ([CoilFamilyID] int
			   ,[NumberFans] int
			   ,[PSCMotorDeduct] int 
			   ,[PaintCost] decimal(18, 2)
			   ,[InsulDrainPanCost] decimal(18, 2)
			   ,[SSNonInsulDrainPanCost] decimal(18, 2)
			   ,[SSInsulDrainPanCost] decimal(18, 2)
			   ,[SSHousingNonInsulDrainPanCost] decimal(18, 2)
			   ,[SSHousingInsulDrainPanCost] decimal(18, 2)
			   ,[ElectroFinCost] decimal(18, 2)
			   ,[CopperFinCost] decimal(18, 2)
			   ,[ReverseAirFlowCost] decimal(18, 2)
			   ,[StdLongThrowAdaptersCost] decimal(18, 2)
			   ,[SSLongThrowAdaptersCost] decimal(18, 2)
			   ,[AirFilterBonnetCost] decimal(18, 2)
			   ,[ModelID] int 
	)

	----- Populate Table varaible  for CoilOptionPricing -------------------
	Insert Into @CoilOptionPricing
	select @param_ModelFamilyID
			   ,B.NumberOFFans
			   ,ISNULL( [PSCMotorDeduct],0)
			   ,ISNULL( [PaintCost],0)
			   ,ISNULL( [InsulDrainPanCost],0)
			   ,ISNULL( [SSNonInsulDrainPanCost],0)
			   ,ISNULL( [SSInsulDrainPanCost],0)
			   ,ISNULL( [SSHousingNonInsulDrainPanCost],0)
			   ,ISNULL( [SSHousingInsulDrainPanCost],0)
			   ,ISNULL( [ElectroFinCost],0)
			   ,ISNULL( [CopperFinCost],0)
			   ,ISNULL( [ReverseAirFlowCost],0)
			   ,ISNULL( [StdLongThrowAdaptersCost],0)
			   ,ISNULL( [SSLongThrowAdaptersCost],0)
			   ,ISNULL( [AirFilterBonnetCost],0)
			   ,@ModelID
	from 
	(
	SELECT  TOP (@param_NumberOfFans)
			ROW_NUMBER() OVER (ORDER BY object_id) as 'NumberOFFans'
	FROM    sys.objects 
	)B
	Left join CoilOptionsPricing
on B.NumberOFFans = CoilOptionsPricing.NumberFans
and (CoilFamilyID = @param_ModelFamilyID OR CoilFamilyID is NULL)
and ModelID is null

	------------- Insert Default rows in CoilOptionPricing on the basis of Number of Fans Selected --------------------
	Insert Into CoilOptionsPricing
	select * from @CoilOptionPricing
	

	select @ModelID as ModelID

END
GO