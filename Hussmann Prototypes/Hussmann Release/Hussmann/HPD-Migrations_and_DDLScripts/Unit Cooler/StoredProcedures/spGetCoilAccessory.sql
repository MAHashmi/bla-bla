DROP PROCEDURE [dbo].[spGetCoilAccessory] 
go
/****** Object:  StoredProcedure [dbo].[spGetCoilAccessory]    Script Date: 2/19/2018 10:14:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Asad
-- Create date: 15/02/2018
-- Description:	Get data against coil family ID
-- =============================================
CREATE PROCEDURE [dbo].[spGetCoilAccessory] @param_FamilyId INT = NULL
	,@param_ModelId INT = NULL
AS
BEGIN
	SELECT CAP.CoilAccessoryPricingID
		,CAP.CoilAccsID
		,CAP.CoilFamilyID
		,CAP.CoilVendorID
		,CAP.CoilAccsPartNumber
		,CAP.CoilAccsPartDescription
		,CAP.CoilAccsMinBTU
		,CAP.CoilAccsMaxBTU
		,CAP.CoilAccsShipLooseListPrice
		,CAP.CoilAccsListPrice
		,CAP.ModelID		
		,CA.CoilAccessoryName		
		,CV.CoilVendorName		
		,CF.CoilFamilyName
	FROM CoilAccessoryPricing CAP
	LEFT JOIN CoilAccessoryID_lkp CA ON CAP.CoilAccsID = CA.CoilAccsID
	LEFT JOIN CoilVendor CV ON CV.CoilVendorID = CAP.CoilVendorID
	LEFT JOIN CoilFamily CF ON CF.CoilFamilyID = cap.CoilFamilyID
	WHERE (
			CF.CoilFamilyID = @param_FamilyId
			OR @param_FamilyId IS NULL
			)
		AND (
			CAP.ModelID = @param_ModelId
			OR (CAP.ModelID IS NULL AND @param_ModelId is null)
			)
END


GO


