INSERT INTO [dbo].[DefrostConfig_lkp]
           ([DefrostConfigCode]
           ,[DefrostConfigName])

    select [DefrostConfigCode]
           ,[DefrostConfigName]
		   from [RulestreamCoilDB].dbo.DefrostConfig_lkp
GO


INSERT INTO [dbo].[CoilVendor]
           ([CoilVendorName])
     select [CoilVendorName] from  [RulestreamCoilDB].dbo.CoilVendor
GO

INSERT INTO [dbo].[CoilAccessoryID_lkp]
           ([CoilAccessoryName])
     select [CoilAccessoryName] from [RulestreamCoilDB].dbo.CoilAccessoryID_lkp
GO

INSERT INTO [dbo].[CoilFamily]
           ([CoilFamilyName]
           ,[StateID]
           ,[Version])
     select [CoilFamilyName]
           ,1
           ,1 from [RulestreamCoilDB].dbo.CoilFamily
GO

INSERT INTO [dbo].[CoilAccessoryPricing]
           ([CoilAccsID]
           ,[CoilFamilyID]
           ,[CoilVendorID]
           ,[CoilAccsPartNumber]
           ,[CoilAccsPartDescription]
           ,[CoilAccsMinBTU]
           ,[CoilAccsMaxBTU]
           ,[CoilAccsShipLooseListPrice]
           ,[CoilAccsListPrice])
		   select [CoilAccsID]
           ,[CoilFamilyID]
           ,[CoilVendorID]
           ,[CoilAccsPartNumber]
           ,[CoilAccsPartDescription]
           ,[CoilAccsMinBTU]
           ,[CoilAccsMaxBTU]
           ,[CoilAccsShipLooseListPrice]
           ,[CoilAccsListPrice]
		   from [RulestreamCoilDB].dbo.CoilAccessoryPricing
GO

INSERT INTO [dbo].[CoilBrand_lkp]
           ([CoilBrandName]
           ,[CoilSubBrandName])
     select [CoilBrandName]
           ,[CoilSubBrandName]
		   from [RulestreamCoilDB].dbo.CoilBrand_lkp
GO

INSERT INTO [dbo].[CoilMotorType_lkp]
           ([CoilMotorTypeName])
     select [CoilMotorTypeName]
	 from [RulestreamCoilDB].dbo.CoilMotorType_lkp
GO

INSERT INTO [dbo].[VoltageType_lkp]
           ([VoltageType]
           ,[VoltageName])
     select [VoltageType]
           ,[VoltageName]
		   from [RulestreamCoilDB].dbo.VoltageType_lkp
GO

DBCC CHECKIDENT ('CoilModel', RESEED, 1); 
INSERT INTO [dbo].[CoilModel]
           ([MFGID]
           ,[ModelFamilyID]
           ,[ModelNumber]
           ,[CapacityPerDegreeTDNeg20]
           ,[CapacityPerDegreeTDPos25]
           ,[NumberofFans]
           ,[Version]
           ,[LastModifiedOn]
           ,[Note]
           ,[CreatedBy]
           ,[ActionBy]
           ,[StateID]
           ,[CoilModelYear]
           ,[CoilMakeYear])
     select [MFGID]
           ,[ModelFamilyID]
           ,[ModelNumber]
           ,[CapacityPerDegreeTDNeg20]
           ,[CapacityPerDegreeTDPos25]
           ,[NumberofFans]
           ,1
           ,GETDATE()
           ,null
           ,'Test User'
           ,'Test User'
           ,1
           ,null
           ,null from
	 [RulestreamCoilDB].dbo.CoilModel
GO

DBCC CHECKIDENT ('CoilMotorAmps', RESEED, 1);
INSERT INTO [dbo].[CoilMotorAmps]
           ([ModelID]
           ,[MotorTypeID]
           ,[VoltageTypeID]
           ,[MotorAmps])
     select [ModelID]
           ,[MotorTypeID]
           ,[VoltageTypeID]
           ,[MotorAmps]
		   from [RulestreamCoilDB].dbo.CoilMotorAmps
GO

DBCC CHECKIDENT ('CoilDefrostAmps', RESEED, 1);
INSERT INTO [dbo].[CoilDefrostAmps]
           ([ModelID]
           ,[VoltageTypeID]
           ,[DefrostAmps])
     select [ModelID]
           ,[VoltageTypeID]
           ,[DefrostAmps]
		   from [RulestreamCoilDB].dbo.CoilDefrostAmps 
GO

DBCC CHECKIDENT ('CoilDrainHtrAmps', RESEED, 1);
INSERT INTO [dbo].[CoilDrainHtrAmps]
           ([ModelID]
           ,[VoltageTypeID]
           ,[DrainHtrAmps])
     select [ModelID]
           ,[VoltageTypeID]
           ,[DrainHtrAmps]
		   from [RulestreamCoilDB].dbo.CoilDrainHtrAmps
GO

DBCC CHECKIDENT ('CoilPricing', RESEED, 1);
INSERT INTO [dbo].[CoilPricing]
           ([ModelID]
           ,[DefrostConfigID]
           ,[ListPrice])
     select [ModelID]
           ,[DefrostConfigID]
           ,[ListPrice]
		   from [RulestreamCoilDB].dbo.CoilPricing
GO

DBCC CHECKIDENT ('DefrostConfigurations', RESEED, 1);
INSERT INTO [dbo].[DefrostConfigurations]
           ([ModelID]
           ,[DefrostConfigID])
     select [ModelID]
           ,[DefrostConfigID]
		   from [RulestreamCoilDB].dbo.DefrostConfigurations
GO

INSERT INTO [dbo].[CoilHeatExchgrPricing]
           ([CondUnitHP]
           ,[FreezerBTU]
           ,[CoolerBTU]
           ,[PartNumber]
           ,[LiqConnSize]
           ,[SuctConnSize]
           ,[ListPrice])
     select [CondUnitHP]
           ,[FreezerBTU]
           ,[CoolerBTU]
           ,[PartNumber]
           ,[LiqConnSize]
           ,[SuctConnSize]
           ,[ListPrice]
		   from [RulestreamCoilDB].dbo.CoilHeatExchgrPricing
GO

INSERT INTO [dbo].[CoilOptionsPricing]
           ([CoilFamilyID]
           ,[NumberFans]
           ,[PSCMotorDeduct]
           ,[PaintCost]
           ,[InsulDrainPanCost]
           ,[SSNonInsulDrainPanCost]
           ,[SSInsulDrainPanCost]
           ,[SSHousingNonInsulDrainPanCost]
           ,[SSHousingInsulDrainPanCost]
           ,[ElectroFinCost]
           ,[CopperFinCost]
           ,[ReverseAirFlowCost]
           ,[StdLongThrowAdaptersCost]
           ,[SSLongThrowAdaptersCost]
           ,[AirFilterBonnetCost])
     select [CoilFamilyID]
           ,[NumberFans]
           ,[PSCMotorDeduct]
           ,[PaintCost]
           ,[InsulDrainPanCost]
           ,[SSNonInsulDrainPanCost]
           ,[SSInsulDrainPanCost]
           ,[SSHousingNonInsulDrainPanCost]
           ,[SSHousingInsulDrainPanCost]
           ,[ElectroFinCost]
           ,[CopperFinCost]
           ,[ReverseAirFlowCost]
           ,[StdLongThrowAdaptersCost]
           ,[SSLongThrowAdaptersCost]
           ,[AirFilterBonnetCost]
		   from [RulestreamCoilDB].dbo.CoilOptionsPricing
GO

INSERT INTO [dbo].[MotorModelNames]
           ([MotorModelName])
     select [MotorModelName] from [RulestreamCoilDB].dbo.MotorModelNames
GO

INSERT INTO [dbo].[UnitConfiguration]
           ([DefrostConfigID]
           ,[DefrostAvailable]
           ,[DrainHeaterAvailable])
     select [DefrostConfigID]
           ,[DefrostAvailable]
           ,[DrainHeaterAvailable]
		   from [RulestreamCoilDB].dbo.UnitConfiguration
GO