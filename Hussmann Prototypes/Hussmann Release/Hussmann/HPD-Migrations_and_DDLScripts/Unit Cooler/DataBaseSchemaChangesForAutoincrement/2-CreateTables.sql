------------------------------- Create [dbo].[UnitConfiguration] --------------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UnitConfiguration](
	[UnitConfigID] [int] IDENTITY(1,1) NOT NULL,
	[DefrostConfigID] [nvarchar](3) NOT NULL,
	[DefrostAvailable] [bit] NOT NULL,
	[DrainHeaterAvailable] [bit] NOT NULL,
 CONSTRAINT [UnitConfiguration__pk] PRIMARY KEY CLUSTERED 
(
	[UnitConfigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

------------------------------- Create  [dbo].[MotorModelNames] ------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MotorModelNames](
	[MotorModelID] [int] IDENTITY(1,1) NOT NULL,
	[MotorModelName] [nvarchar](30) NOT NULL,
 CONSTRAINT [MotorModel__pk] PRIMARY KEY CLUSTERED 
(
	[MotorModelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--------------------------------- Create [dbo].[CoilHeatExchgrPricing] ------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CoilHeatExchgrPricing](
	[HeatExchgrPricingID] [int] IDENTITY(1,1) NOT NULL,
	[CondUnitHP] [decimal](18, 2) NOT NULL,
	[FreezerBTU] [int] NOT NULL,
	[CoolerBTU] [int] NOT NULL,
	[PartNumber] [nvarchar](15) NULL,
	[LiqConnSize] [nvarchar](15) NULL,
	[SuctConnSize] [nvarchar](15) NULL,
	[ListPrice] [decimal](18, 2) NOT NULL,
 CONSTRAINT [tbl_ModelCoilHeatExchgrPricing_pk] PRIMARY KEY CLUSTERED 
(
	[HeatExchgrPricingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

------------------------------------ Create [dbo].[CoilOptionsPricing] ------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CoilOptionsPricing](
	[CoilOptionsPricingID] int Identity(1,1) NOT NULL Primary Key, 
	[CoilFamilyID] [int] NOT NULL,
	[NumberFans] [int] NOT NULL,
	[PSCMotorDeduct] [decimal](18, 2) NOT NULL,
	[PaintCost] [decimal](18, 2) NOT NULL,
	[InsulDrainPanCost] [decimal](18, 2) NOT NULL,
	[SSNonInsulDrainPanCost] [decimal](18, 2) NOT NULL,
	[SSInsulDrainPanCost] [decimal](18, 2) NOT NULL,
	[SSHousingNonInsulDrainPanCost] [decimal](18, 2) NOT NULL,
	[SSHousingInsulDrainPanCost] [decimal](18, 2) NOT NULL,
	[ElectroFinCost] [decimal](18, 2) NOT NULL,
	[CopperFinCost] [decimal](18, 2) NOT NULL,
	[ReverseAirFlowCost] [decimal](18, 2) NOT NULL,
	[StdLongThrowAdaptersCost] [decimal](18, 2) NOT NULL,
	[SSLongThrowAdaptersCost] [decimal](18, 2) NOT NULL,
	[AirFilterBonnetCost] [decimal](18, 2) NOT NULL
) ON [PRIMARY]

GO

------------------------------ Create [dbo].[DefrostConfig_lkp] ----------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DefrostConfig_lkp](
	[DefrostConfigID] [int] IDENTITY(1,1) NOT NULL,
	[DefrostConfigCode] [nvarchar](3) NOT NULL,
	[DefrostConfigName] [nvarchar](50) NOT NULL,
 CONSTRAINT [DefrostConfig__pk] PRIMARY KEY CLUSTERED 
(
	[DefrostConfigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

------------------------------ Create [dbo].[CoilVendor] ----------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CoilVendor](
	[CoilVendorID] [int] IDENTITY(1,1) NOT NULL,
	[CoilVendorName] [nvarchar](30) NOT NULL,
 CONSTRAINT [CoilVendor_pk] PRIMARY KEY CLUSTERED 
(
	[CoilVendorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-----------------------------  Create [dbo].[CoilAccessoryID_lkp] --------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CoilAccessoryID_lkp](
	[CoilAccsID] [int] IDENTITY(1,1) NOT NULL,
	[CoilAccessoryName] [nvarchar](50) NOT NULL,
 CONSTRAINT [CoilAccessory_pk] PRIMARY KEY CLUSTERED 
(
	[CoilAccsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

---------------------------- Create [dbo].[State] ------------------------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[State](
	[StateID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[StateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

----------------------------- Create [dbo].[CoilFamily] --------------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CoilFamily](
	[CoilFamilyID] [int] IDENTITY(1,1) NOT NULL,
	[CoilFamilyName] [nvarchar](25) NOT NULL,
	[StateID] int NOT NULL FOREIGN KEY REFERENCES [State](StateID),
	[Version] int NOT NULL
 CONSTRAINT [CoilFamily__pk] PRIMARY KEY CLUSTERED 
(
	[CoilFamilyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

---------------------------------------- Create [dbo].[CoilAccessoryPricing] ------------------------- 
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CoilAccessoryPricing](
	[CoilAccessoryPricingID] [int] IDENTITY(1,1) NOT NULL,
	[CoilAccsID] [int] NULL,
	[CoilFamilyID] [int] NULL,
	[CoilVendorID] [int] NULL,
	[CoilAccsPartNumber] [nvarchar](20) NULL,
	[CoilAccsPartDescription] [nvarchar](50) NULL,
	[CoilAccsMinBTU] [int] NULL,
	[CoilAccsMaxBTU] [int] NULL,
	[CoilAccsShipLooseListPrice] [decimal](18, 2) NULL,
	[CoilAccsListPrice] [decimal](18, 2) NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CoilAccessoryPricing]  WITH CHECK ADD  CONSTRAINT [CoilAccessoryPricing_CoilAccsID_fk] FOREIGN KEY([CoilAccsID])
REFERENCES [dbo].[CoilAccessoryID_lkp] ([CoilAccsID])
GO

ALTER TABLE [dbo].[CoilAccessoryPricing] CHECK CONSTRAINT [CoilAccessoryPricing_CoilAccsID_fk]
GO

ALTER TABLE [dbo].[CoilAccessoryPricing]  WITH CHECK ADD  CONSTRAINT [CoilAccessoryPricing_CoilFamily_fk] FOREIGN KEY([CoilFamilyID])
REFERENCES [dbo].[CoilFamily] ([CoilFamilyID])
GO

ALTER TABLE [dbo].[CoilAccessoryPricing] CHECK CONSTRAINT [CoilAccessoryPricing_CoilFamily_fk]
GO

ALTER TABLE [dbo].[CoilAccessoryPricing]  WITH CHECK ADD  CONSTRAINT [CoilAccessoryPricing_CoilVendorID_fk] FOREIGN KEY([CoilVendorID])
REFERENCES [dbo].[CoilVendor] ([CoilVendorID])
GO

ALTER TABLE [dbo].[CoilAccessoryPricing] CHECK CONSTRAINT [CoilAccessoryPricing_CoilVendorID_fk]
GO

--------------------------------------- Create [dbo].[CoilBrand_lkp] ----------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CoilBrand_lkp](
	[BrandID] [int] IDENTITY(1,1) NOT NULL,
	[CoilBrandName] [nvarchar](20) NOT NULL,
	[CoilSubBrandName] [nvarchar](20) NULL,
 CONSTRAINT [CoilBrand_pk] PRIMARY KEY CLUSTERED 
(
	[BrandID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

------------------------------------ Create [dbo].[CoilMotorType_lkp] ----------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CoilMotorType_lkp](
	[Type_ID] [int] IDENTITY(1,1) NOT NULL,
	[CoilMotorTypeName] [nvarchar](20) NOT NULL,
 CONSTRAINT [CoilMotorType_pk] PRIMARY KEY CLUSTERED 
(
	[Type_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

------------------------------- Create [dbo].[VoltageType_lkp] ------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VoltageType_lkp](
	[VoltageTypeID] [int] Identity(1,1) NOT NULL,
	[VoltageType] [nvarchar](30) NOT NULL,
	[VoltageName] [nvarchar](30) NOT NULL,
 CONSTRAINT [VoltageType__pk] PRIMARY KEY CLUSTERED 
(
	[VoltageTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

---------------------------------- Create [dbo].[CoilModel] ------------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CoilModel](
	[ModelID] [int] Identity(1,1) NOT NULL,
	[MFGID] [int] NOT NULL,
	[ModelFamilyID] [int] NOT NULL,
	[ModelNumber] [nvarchar](15) NULL,
	[CapacityPerDegreeTDNeg20] [int] NULL,
	[CapacityPerDegreeTDPos25] [int] NULL,
	[NumberofFans] [int] NULL,
	[Version] [int] NOT NULL,
	[LastModifiedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[Note] [varchar](250) NULL,
	[CreatedBy] [varchar](75) NOT NULL DEFAULT ('Test User'),
	[ActionBy] [varchar](75) NOT NULL DEFAULT ('Test User'),
	[StateID] [int] NULL,
	[CoilModelYear] [varchar](4) NULL,
	[CoilMakeYear] [varchar](4) NULL,
 CONSTRAINT [tbl_Model_pk] PRIMARY KEY CLUSTERED 
(
	[ModelID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CoilModel]  WITH CHECK ADD  CONSTRAINT [CoilModel_lkp_CoilMFG_fk] FOREIGN KEY([MFGID])
REFERENCES [dbo].[CoilBrand_lkp] ([BrandID])
GO

ALTER TABLE [dbo].[CoilModel] CHECK CONSTRAINT [CoilModel_lkp_CoilMFG_fk]
GO

ALTER TABLE [dbo].[CoilModel]  WITH CHECK ADD  CONSTRAINT [fk] FOREIGN KEY([StateID])
REFERENCES [dbo].[State] ([StateID])
GO

ALTER TABLE [dbo].[CoilModel] CHECK CONSTRAINT [fk]
GO

ALTER TABLE [dbo].[CoilModel]  WITH CHECK ADD  CONSTRAINT [ModelFamily_lkp_CoilFamily_fk] FOREIGN KEY([ModelFamilyID])
REFERENCES [dbo].[CoilFamily] ([CoilFamilyID])
GO

ALTER TABLE [dbo].[CoilModel] CHECK CONSTRAINT [ModelFamily_lkp_CoilFamily_fk]
GO

----------------------------------- Create [dbo].[CoilMotorAmps] --------------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CoilMotorAmps](
	[Type_ID] [int] Identity(1,1) NOT NULL,
	[ModelID] [int] NOT NULL,
	[MotorTypeID] [int] NOT NULL,
	[VoltageTypeID] [int] NOT NULL,
	[MotorAmps] [decimal](18, 5) NULL,
 CONSTRAINT [CoilMotorAmps__pk] PRIMARY KEY CLUSTERED 
(
	[Type_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CoilMotorAmps]  WITH CHECK ADD  CONSTRAINT [CoilModel_CoilMotorAmps_fk] FOREIGN KEY([ModelID])
REFERENCES [dbo].[CoilModel] ([ModelID])
GO

ALTER TABLE [dbo].[CoilMotorAmps] CHECK CONSTRAINT [CoilModel_CoilMotorAmps_fk]
GO

ALTER TABLE [dbo].[CoilMotorAmps]  WITH CHECK ADD  CONSTRAINT [CoilMotorType_lkp_CoilMotorAmps_fk] FOREIGN KEY([MotorTypeID])
REFERENCES [dbo].[CoilMotorType_lkp] ([Type_ID])
GO

ALTER TABLE [dbo].[CoilMotorAmps] CHECK CONSTRAINT [CoilMotorType_lkp_CoilMotorAmps_fk]
GO

ALTER TABLE [dbo].[CoilMotorAmps]  WITH CHECK ADD  CONSTRAINT [VoltageType_lkp_CoilMotorAmps_fk] FOREIGN KEY([VoltageTypeID])
REFERENCES [dbo].[VoltageType_lkp] ([VoltageTypeID])
GO

ALTER TABLE [dbo].[CoilMotorAmps] CHECK CONSTRAINT [VoltageType_lkp_CoilMotorAmps_fk]
GO

---------------------------------- Create [dbo].[CoilDefrostAmps] --------------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CoilDefrostAmps](
	[Type_ID] [int] Identity(1,1) NOT NULL,
	[ModelID] [int] NOT NULL,
	[VoltageTypeID] [int] NOT NULL,
	[DefrostAmps] [decimal](18, 5) NULL,
 CONSTRAINT [CoilDefrostAmps__pk] PRIMARY KEY CLUSTERED 
(
	[Type_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CoilDefrostAmps]  WITH CHECK ADD  CONSTRAINT [CoilDefrost_CoilMotorAmps_fk] FOREIGN KEY([ModelID])
REFERENCES [dbo].[CoilModel] ([ModelID])
GO

ALTER TABLE [dbo].[CoilDefrostAmps] CHECK CONSTRAINT [CoilDefrost_CoilMotorAmps_fk]
GO

ALTER TABLE [dbo].[CoilDefrostAmps]  WITH CHECK ADD  CONSTRAINT [VoltageType_lkp_DefrostAmps_fk] FOREIGN KEY([VoltageTypeID])
REFERENCES [dbo].[VoltageType_lkp] ([VoltageTypeID])
GO

ALTER TABLE [dbo].[CoilDefrostAmps] CHECK CONSTRAINT [VoltageType_lkp_DefrostAmps_fk]
GO

----------------------------------------- Create [dbo].[CoilDrainHtrAmps] -------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CoilDrainHtrAmps](
	[Type_ID] [int] Identity(1,1) NOT NULL,
	[ModelID] [int] NOT NULL,
	[VoltageTypeID] [int] NOT NULL,
	[DrainHtrAmps] [decimal](18, 5) NULL,
 CONSTRAINT [CoilDrainHtrAmps__pk] PRIMARY KEY CLUSTERED 
(
	[Type_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CoilDrainHtrAmps]  WITH CHECK ADD  CONSTRAINT [CoilDrainHter_CoilMotorAmps_fk] FOREIGN KEY([ModelID])
REFERENCES [dbo].[CoilModel] ([ModelID])
GO

ALTER TABLE [dbo].[CoilDrainHtrAmps] CHECK CONSTRAINT [CoilDrainHter_CoilMotorAmps_fk]
GO

ALTER TABLE [dbo].[CoilDrainHtrAmps]  WITH CHECK ADD  CONSTRAINT [VoltageType_lkp_DrainHtrAmps_fk] FOREIGN KEY([VoltageTypeID])
REFERENCES [dbo].[VoltageType_lkp] ([VoltageTypeID])
GO

ALTER TABLE [dbo].[CoilDrainHtrAmps] CHECK CONSTRAINT [VoltageType_lkp_DrainHtrAmps_fk]
GO

----------------------------------- Create [dbo].[CoilModelActionHistory] -----------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CoilModelActionHistory](
	[ModelID] [int] NULL,
	[StateID] [int] NULL,
	[CreatedBy] [varchar](75) NOT NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[ActionBy] [varchar](75) NOT NULL,
	[XMLData] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CoilModelActionHistory]  WITH CHECK ADD FOREIGN KEY([ModelID])
REFERENCES [dbo].[CoilModel] ([ModelID])
GO

ALTER TABLE [dbo].[CoilModelActionHistory]  WITH CHECK ADD FOREIGN KEY([StateID])
REFERENCES [dbo].[State] ([StateID])
GO

------------------------------------ Create [dbo].[CoilPricing] -------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CoilPricing](
	[Type_ID] [int] Identity(1,1) NOT NULL,
	[ModelID] [int] NOT NULL,
	[DefrostConfigID] [int] NOT NULL,
	[ListPrice] [decimal](18, 2) NOT NULL,
 CONSTRAINT [CoilPricing__pk] PRIMARY KEY CLUSTERED 
(
	[Type_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CoilPricing]  WITH CHECK ADD  CONSTRAINT [CoilPricing_CoilModel_fk] FOREIGN KEY([ModelID])
REFERENCES [dbo].[CoilModel] ([ModelID])
GO

ALTER TABLE [dbo].[CoilPricing] CHECK CONSTRAINT [CoilPricing_CoilModel_fk]
GO

ALTER TABLE [dbo].[CoilPricing]  WITH CHECK ADD  CONSTRAINT [CoilPricing_DefrostConfigurationLKP_fk] FOREIGN KEY([DefrostConfigID])
REFERENCES [dbo].[DefrostConfig_lkp] ([DefrostConfigID])
GO

ALTER TABLE [dbo].[CoilPricing] CHECK CONSTRAINT [CoilPricing_DefrostConfigurationLKP_fk]
GO

---------------------------------Create [dbo].[DefrostConfigurations] ----------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DefrostConfigurations](
	[Type_ID] [int] Identity(1,1) NOT NULL,
	[ModelID] [int] NOT NULL,
	[DefrostConfigID] [int] NOT NULL,
 CONSTRAINT [DefrostConfigurations_pk] PRIMARY KEY CLUSTERED 
(
	[Type_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[DefrostConfigurations]  WITH CHECK ADD  CONSTRAINT [DefrostConfig_lkp_DefrostConfigurationsfk] FOREIGN KEY([DefrostConfigID])
REFERENCES [dbo].[DefrostConfig_lkp] ([DefrostConfigID])
GO

ALTER TABLE [dbo].[DefrostConfigurations] CHECK CONSTRAINT [DefrostConfig_lkp_DefrostConfigurationsfk]
GO

ALTER TABLE [dbo].[DefrostConfigurations]  WITH CHECK ADD  CONSTRAINT [DefrostConfigurations_CoilModel_fk] FOREIGN KEY([ModelID])
REFERENCES [dbo].[CoilModel] ([ModelID])
GO

ALTER TABLE [dbo].[DefrostConfigurations] CHECK CONSTRAINT [DefrostConfigurations_CoilModel_fk]
GO