

---------------------------- Drop DefrostConfigurations ----------------------------
ALTER TABLE [dbo].[DefrostConfigurations] DROP CONSTRAINT [DefrostConfigurations_CoilModel_fk]
GO

ALTER TABLE [dbo].[DefrostConfigurations] DROP CONSTRAINT [DefrostConfig_lkp_DefrostConfigurationsfk]
GO

DROP TABLE [dbo].[DefrostConfigurations]
GO

--------------------------- Drop CoilPricing ---------------------------------------------
ALTER TABLE [dbo].[CoilPricing] DROP CONSTRAINT [CoilPricing_DefrostConfigurationLKP_fk]
GO

ALTER TABLE [dbo].[CoilPricing] DROP CONSTRAINT [CoilPricing_CoilModel_fk]
GO

DROP TABLE [dbo].[CoilPricing]
GO

------------------------------ Drop [dbo].[CoilModelActionHistory] ------------------------
If Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilModelActionHistory')
Begin
ALTER TABLE [dbo].[CoilModelActionHistory] DROP CONSTRAINT [FK__CoilModel__State__4865BE2A]


ALTER TABLE [dbo].[CoilModelActionHistory] DROP CONSTRAINT [FK__CoilModel__Model__477199F1]


DROP TABLE [dbo].[CoilModelActionHistory]


End
Go


-------------------------------- Drop [dbo].[CoilDrainHtrAmps] ----------------------------
ALTER TABLE [dbo].[CoilDrainHtrAmps] DROP CONSTRAINT [VoltageType_lkp_DrainHtrAmps_fk]
GO

ALTER TABLE [dbo].[CoilDrainHtrAmps] DROP CONSTRAINT [CoilDrainHter_CoilMotorAmps_fk]
GO

/****** Object:  Table [dbo].[CoilDrainHtrAmps]    Script Date: 2/2/2018 4:04:06 PM ******/
DROP TABLE [dbo].[CoilDrainHtrAmps]
GO

------------------------------- Drop [dbo].[CoilDefrostAmps] -------------------------------
ALTER TABLE [dbo].[CoilDefrostAmps] DROP CONSTRAINT [VoltageType_lkp_DefrostAmps_fk]
GO

ALTER TABLE [dbo].[CoilDefrostAmps] DROP CONSTRAINT [CoilDefrost_CoilMotorAmps_fk]
GO

DROP TABLE [dbo].[CoilDefrostAmps]
GO

-------------------------------- Drop [dbo].[CoilMotorAmps] --------------------------------
ALTER TABLE [dbo].[CoilMotorAmps] DROP CONSTRAINT [VoltageType_lkp_CoilMotorAmps_fk]
GO

ALTER TABLE [dbo].[CoilMotorAmps] DROP CONSTRAINT [CoilMotorType_lkp_CoilMotorAmps_fk]
GO

ALTER TABLE [dbo].[CoilMotorAmps] DROP CONSTRAINT [CoilModel_CoilMotorAmps_fk]
GO

DROP TABLE [dbo].[CoilMotorAmps]
GO

-------------------------------- Drop [dbo].[CoilModel] --------------------------------------
ALTER TABLE [dbo].[CoilModel] DROP CONSTRAINT [ModelFamily_lkp_CoilFamily_fk]
GO

IF (OBJECT_ID('fk', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE [dbo].[CoilModel] DROP CONSTRAINT [fk]
END


ALTER TABLE [dbo].[CoilModel] DROP CONSTRAINT [CoilModel_lkp_CoilMFG_fk]
GO

DROP TABLE [dbo].[CoilModel]
GO

---------------------------------- Drop [dbo].[VoltageType_lkp] -------------------------------
DROP TABLE [dbo].[VoltageType_lkp]
GO

---------------------------------- Drop [dbo].[CoilMotorType_lkp] -----------------------------
DROP TABLE [dbo].[CoilMotorType_lkp]
GO

----------------------------------- Drop [dbo].[CoilBrand_lkp] -------------------------------------
DROP TABLE [dbo].[CoilBrand_lkp]
GO

----------------------------------- Drop [dbo].[CoilAccessoryPricing] ------------------------------
ALTER TABLE [dbo].[CoilAccessoryPricing] DROP CONSTRAINT [CoilAccessoryPricing_CoilVendorID_fk]
GO

ALTER TABLE [dbo].[CoilAccessoryPricing] DROP CONSTRAINT [CoilAccessoryPricing_CoilFamily_fk]
GO

ALTER TABLE [dbo].[CoilAccessoryPricing] DROP CONSTRAINT [CoilAccessoryPricing_CoilAccsID_fk]
GO

DROP TABLE [dbo].[CoilAccessoryPricing]
GO

------------------------------------ Drop [dbo].[CoilFamily] ----------------------------------------
DROP TABLE [dbo].[CoilFamily]
GO

------------------------------------ Drop [dbo].[State] --------------------------------------------
If Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'State')
Begin
		DROP TABLE [dbo].[State]

End
Go

------------------------------------ Drop [dbo].[CoilAccessoryID_lkp] -----------------------------
DROP TABLE [dbo].[CoilAccessoryID_lkp]
GO

------------------------------------ Drop [dbo].[CoilVendor] --------------------------------------
DROP TABLE [dbo].[CoilVendor]
GO

------------------------------------ Drop [dbo].[DefrostConfig_lkp] -------------------------------
DROP TABLE [dbo].[DefrostConfig_lkp]
GO

------------------------------------ Drop [dbo].[UnitConfiguration] ------------------------------
DROP TABLE [dbo].[UnitConfiguration]
GO

------------------------------------ Drop [dbo].[MotorModelNames] ---------------------------------
DROP TABLE [dbo].[MotorModelNames]
GO

------------------------------------ Drop [dbo].[CoilHeatExchgrPricing] ----------------------------
DROP TABLE [dbo].[CoilHeatExchgrPricing]
GO

------------------------------------- Drop [dbo].[CoilOptionsPricing] -------------------------------
DROP TABLE [dbo].[CoilOptionsPricing]
GO