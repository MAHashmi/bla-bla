------ Populating states in state table ---------
Insert Into [State]([Description]) values ('Draft');
Insert Into [State]([Description]) values ('Submitted');
Insert Into [State]([Description]) values ('Rejected');
Insert Into [State]([Description]) values ('Approved');
Insert Into [State]([Description]) values ('Deleted');