If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilHeatExchgrPricing'
			And	Column_Name = 'ModelID')
Begin
	ALTER TABLE CoilHeatExchgrPricing
	ADD ModelID integer CONSTRAINT fk_ModelID FOREIGN KEY (ModelID) REFERENCES CoilModel(ModelID) 
End
GO



ALTER TABLE [dbo].[CoilFamily] DROP CONSTRAINT [FK__CoilFamil__State__1FCDBCEB] --Please rename the CONSTRAINT name before running
GO


If Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilFamily'
			And	Column_Name = 'StateID')
Begin
	ALTER TABLE CoilFamily
	DROP COLUMN StateID;
End
GO

If Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilFamily'
			And	Column_Name = 'Version')
Begin
	ALTER TABLE CoilFamily
	DROP COLUMN [Version];
End
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilAccessoryPricing'
			And	Column_Name = 'ModelID')
Begin
	ALTER TABLE CoilAccessoryPricing
	ADD ModelID int  NULL;
	
	ALTER TABLE [dbo].CoilAccessoryPricing  WITH CHECK ADD  CONSTRAINT [CoilAccessoryPricing_CoilModel_fk] FOREIGN KEY([ModelID])
	REFERENCES [dbo].[CoilModel] ([ModelID])

End
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilOptionsPricing'
			And	Column_Name = 'ModelID')
Begin
	ALTER TABLE CoilOptionsPricing
	ADD ModelID int  NULL;
	
	ALTER TABLE [dbo].CoilOptionsPricing  WITH CHECK ADD  CONSTRAINT [CoilOptionsPricing_CoilModel_fk] FOREIGN KEY([ModelID])
	REFERENCES [dbo].[CoilModel] ([ModelID])

End
GO


IF Not EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'CoilAccessoryPricingUDT')
Begin
CREATE TYPE [dbo].[CoilAccessoryPricingUDT] AS TABLE(
	[ModelID] [int] NULL,
	[CoilAccsID] [int] NULL,
	[CoilAccsPartNumber] [nvarchar](20) NULL,
	[CoilVendorID] [int] NULL,
	[CoilAccsMinBTU] [int] NULL,
	[CoilAccsMaxBTU] [int] NULL,
	[CoilAccsPartDescription] [nvarchar](50) NULL,
	[CoilAccessoryPricingID] [int] NULL,
	[CoilFamilyID] [int] NULL
)
End
GO


IF Not EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'CoilOptionsPricingUDT')
Begin
CREATE TYPE [dbo].[CoilOptionsPricingUDT] AS TABLE(
	[CoilFamilyID] [int] NOT NULL,
	[NumberFans] [int] NOT NULL,
	[PSCMotorDeduct] [decimal](18, 2) NOT NULL,
	[PaintCost] [decimal](18, 2) NOT NULL,
	[InsulDrainPanCost] [decimal](18, 2) NOT NULL,
	[SSNonInsulDrainPanCost] [decimal](18, 2) NOT NULL,
	[SSInsulDrainPanCost] [decimal](18, 2) NOT NULL,
	[SSHousingNonInsulDrainPanCost] [decimal](18, 2) NOT NULL,
	[SSHousingInsulDrainPanCost] [decimal](18, 2) NOT NULL,
	[ElectroFinCost] [decimal](18, 2) NOT NULL,
	[CopperFinCost] [decimal](18, 2) NOT NULL,
	[ReverseAirFlowCost] [decimal](18, 2) NOT NULL,
	[StdLongThrowAdaptersCost] [decimal](18, 2) NOT NULL,
	[SSLongThrowAdaptersCost] [decimal](18, 2) NOT NULL,
	[AirFilterBonnetCost] [decimal](18, 2) NOT NULL,
	[ModelID] [int] NULL
)
End
GO

IF  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'CoilAccsListPriceUDT')
BEGIN

IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[spUpdateCoilOptionPricing]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[spUpdateCoilOptionPricing] 
END 

DROP TYPE [dbo].[CoilAccsListPriceUDT]


CREATE TYPE [dbo].[CoilAccsListPriceUDT] AS TABLE(
	[CoilAccessoryPricingID] [int],
	[CoilAccsListPrice] [decimal](18, 2) NULL,
	[CoilFamilyID] [int] NULL,
	[ModelID] [int] NULL,
	[CoilAccsPartDescription] [nvarchar](50) NULL
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'CoilAccsListPriceUDT')
BEGIN

CREATE TYPE [dbo].[CoilAccsListPriceUDT] AS TABLE(
	[CoilAccessoryPricingID] [int],
	[CoilAccsListPrice] [decimal](18, 2) NULL,
	[CoilFamilyID] [int] NULL,
	[ModelID] [int] NULL,
	[CoilAccsPartDescription] [nvarchar](50) NULL
)
END
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilModel'
			And	Column_Name = 'SelectedFan')
Begin
	ALTER TABLE CoilModel
	ADD SelectedFan int  NULL;

End
GO

-------------------------- CoilDefrostAmpsUDT ----------------------------

IF  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'CoilDefrostAmpsUDT')
BEGIN

IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[spInserorUpdateConfiguration]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[spInserorUpdateConfiguration] 
END 

DROP TYPE [dbo].[CoilDefrostAmpsUDT]

CREATE TYPE [dbo].[CoilDefrostAmpsUDT] AS TABLE(
	[Type_ID] [int] NULL,
	[ModelID] [int] NULL,
	[VoltageTypeID] [int] NULL,
	[DefrostAmps] [decimal](18, 5) NULL
)
END
GO

IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'CoilDefrostAmpsUDT')
BEGIN
	CREATE TYPE [dbo].[CoilDefrostAmpsUDT] AS TABLE(
	[Type_ID] [int] NULL,
	[ModelID] [int] NULL,
	[VoltageTypeID] [int] NULL,
	[DefrostAmps] [decimal](18, 5) NULL
)
END
GO

-------------------------- CoilDrainHtrAmpsUDT ----------------------------
IF  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'CoilDrainHtrAmpsUDT')
BEGIN

IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[spInserorUpdateConfiguration]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[spInserorUpdateConfiguration] 
END


DROP TYPE [dbo].[CoilDrainHtrAmpsUDT]

CREATE TYPE [dbo].[CoilDrainHtrAmpsUDT] AS TABLE(
	[Type_ID] [int] NULL,
	[ModelID] [int] NULL,
	[VoltageTypeID] [int] NULL,
	[DrainHtrAmps] [decimal](18, 5) NULL
)
END
GO
IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'CoilDrainHtrAmpsUDT')
BEGIN
CREATE TYPE [dbo].[CoilDrainHtrAmpsUDT] AS TABLE(
	[Type_ID] [int] NULL,
	[ModelID] [int] NULL,
	[VoltageTypeID] [int] NULL,
	[DrainHtrAmps] [decimal](18, 5) NULL
)
END
GO
-------------------------- CoilMotorAmpsUDT ----------------------------

IF  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'CoilMotorAmpsUDT')
BEGIN

IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[spInserorUpdateConfiguration]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[spInserorUpdateConfiguration] 
END
DROP TYPE [dbo].[CoilMotorAmpsUDT]
CREATE TYPE [dbo].[CoilMotorAmpsUDT] AS TABLE(
	[MotorTypeID] [int] NULL,
	[VoltageTypeID] [int] NULL,
	[MotorAmps] [decimal](18, 5) NULL
)
END
GO

IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'CoilMotorAmpsUDT')
BEGIN
CREATE TYPE [dbo].[CoilMotorAmpsUDT] AS TABLE(
	[MotorTypeID] [int] NULL,
	[VoltageTypeID] [int] NULL,
	[MotorAmps] [decimal](18, 5) NULL
)
END
GO
-------------------------- DefrostConfigurationsUDT ----------------------------

IF  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'DefrostConfigurationsUDT')
BEGIN
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[spInserorUpdateConfiguration]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
    DROP PROCEDURE [dbo].[spInserorUpdateConfiguration] 
END
DROP TYPE [dbo].[DefrostConfigurationsUDT]
CREATE TYPE [dbo].[DefrostConfigurationsUDT] AS TABLE(
	[DefrostConfigID] [int] NULL
)
END
GO

IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'DefrostConfigurationsUDT')
BEGIN
CREATE TYPE [dbo].[DefrostConfigurationsUDT] AS TABLE(
	[DefrostConfigID] [int] NULL
)
END
GO