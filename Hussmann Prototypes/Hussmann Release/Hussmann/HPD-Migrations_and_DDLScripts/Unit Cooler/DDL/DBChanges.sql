------------------------- For State -------------
If Not Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'State')
Begin
CREATE TABLE [dbo].[State](
	StateID int Identity(1,1) NOT NULL Primary Key,
	Description varchar(15) NOT NULL
)
End
Go

-------------------- Version for maintaing edited records --------------------------
If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilModel'
			And	Column_Name = 'Version')
Begin
	Alter Table CoilModel
	Add Version int NOT NULL DEFAULT(1) -- Set default value of 1 only at the time of column creation into exsisting rows
End
GO
-------------------- For adding StateID as a foreign key in CoilModel--------------------------------
If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilModel'
			And	Column_Name = 'StateID')
Begin
	ALTER TABLE CoilModel
	ADD StateID integer CONSTRAINT fk FOREIGN KEY (StateID) REFERENCES [State](StateID) 
End
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilModel'
			And	Column_Name = 'CreatedBy')
Begin
	Alter Table CoilModel
	Add CreatedBy varchar(75) Not NULL Default('Test User') -- Maximum legth of windows domain user is 256 but we are assuming that users do not usually set long names
End
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilModel'
			And	Column_Name = 'LastModifiedOn')
Begin
	Alter Table CoilModel
	Add LastModifiedOn Datetime Not NULL Default(GETDATE()) -- Set default value of current date only at the time of column creation into exsisting rows
End
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilModel'
			And	Column_Name = 'ActionBy')
Begin
	Alter Table CoilModel
	Add ActionBy varchar(75) Not NULL Default('Test User') -- Maximum legth of windows domain user is 256 but we are assuming that users do not usually set long names
End
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilModel'
			And	Column_Name = 'Note')
Begin
	Alter Table CoilModel
	Add Note varchar(250) NULL
End
GO



------------------------- For maitaining Action History -------------
If Not Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilModelActionHistory')
Begin
CREATE TABLE [dbo].[CoilModelActionHistory](
	ModelID int FOREIGN KEY REFERENCES CoilModel(ModelID),
	StateID int FOREIGN KEY REFERENCES [State](StateID),
	CreatedBy varchar(75) not null,-- Maximum legth of windows domain user is 256 but we are assuming that users do not usually set long names
	LastModifiedOn Datetime not null,
	ActionBy varchar(75) not null,-- Maximum legth of windows domain user is 256 but we are assuming that users do not usually set long names
	JSONData varchar(max)
)
End
Go

------------------------- For Coil Model Year -------------
If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilModel'
			And	Column_Name = 'CoilModelYear')
Begin
	Alter Table CoilModel
	Add CoilModelYear varchar(4) 
End
GO

------------------------- For mCoil Make Year -------------
If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CoilModel'
			And	Column_Name = 'CoilMakeYear')
Begin
	Alter Table CoilModel
	Add CoilMakeYear varchar(4) 
End
GO
