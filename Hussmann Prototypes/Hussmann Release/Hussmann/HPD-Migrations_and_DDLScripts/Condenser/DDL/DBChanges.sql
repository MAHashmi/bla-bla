------------------------- For State -------------
If Not Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'State')
Begin
CREATE TABLE [dbo].[State](
	StateID int Identity(1,1) NOT NULL Primary Key,
	Description varchar(15) NOT NULL
)
End
Go

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CondenserModel'
			And	Column_Name = 'Version')
Begin
	Alter Table CondenserModel
	Add Version int NOT NULL DEFAULT(1) -- Set default value of 1 only at the time of column creation into exsisting rows
End
GO
-------------------- For adding StateID as a foreign key in CondenserModel--------------------------------
If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CondenserModel'
			And	Column_Name = 'StateID')
Begin
	ALTER TABLE CondenserModel
	ADD StateID integer CONSTRAINT fk FOREIGN KEY (StateID) REFERENCES [State](StateID) 
End
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CondenserModel'
			And	Column_Name = 'CreatedBy')
Begin
	Alter Table CondenserModel
	Add CreatedBy varchar(75) Not NULL Default('System') -- Maximum legth of windows domain user is 256 but we are assuming that users do not usually set long names
End
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CondenserModel'
			And	Column_Name = 'LastModifiedOn')
Begin
	Alter Table CondenserModel
	Add LastModifiedOn Datetime Not NULL Default(GETDATE()) -- Set default value of current date only at the time of column creation into exsisting rows
End
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CondenserModel'
			And	Column_Name = 'ActionBy')
Begin
	Alter Table CondenserModel
	Add ActionBy varchar(75) Not NULL Default('System') -- Maximum legth of windows domain user is 256 but we are assuming that users do not usually set long names
End
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CondenserModel'
			And	Column_Name = 'Note')
Begin
	Alter Table CondenserModel
	Add Note varchar(250) NULL
End
GO

------------------------- For maitaining Action History -------------
If Not Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CondenserModelActionHistory')
Begin
CREATE TABLE [dbo].[CondenserModelActionHistory](
	ModelID int FOREIGN KEY REFERENCES [CondenserModel](ModelID),
	StateID int FOREIGN KEY REFERENCES [State](StateID),
	CreatedBy varchar(75) not null,-- Maximum legth of windows domain user is 256 but we are assuming that users do not usually set long names
	LastModifiedOn Datetime not null,
	ActionBy varchar(75) not null,-- Maximum legth of windows domain user is 256 but we are assuming that users do not usually set long names
	XMLData varchar(max)
)
End
Go




--- /////////************* PRICING SECTION START *******************\\\\\\\\\\\\\\\\


-- Altering Recieving Capacity Table
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='ReceiverCapacity_ReceiverType_fk')
 BEGIN 

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='Receiver_ReceiverCapacity_fk')
 BEGIN 
	ALTER TABLE ReceiverCapacity DROP CONSTRAINT Receiver_ReceiverCapacity_fk 
	ALTER TABLE [dbo].[ReceiverCapacity]  WITH CHECK ADD  CONSTRAINT ReceiverCapacity_ReceiverType_fk FOREIGN KEY([ReceiverTypeID])
	REFERENCES [dbo].[ReceiverType] ([ReceiverTypeID])
 END

END 
GO

-- Altering Reciever TABLE


IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='Receiver_Geometry_fk')
 BEGIN 
ALTER TABLE [dbo].[Receiver]  WITH CHECK ADD  CONSTRAINT [Receiver_Geometry_fk] FOREIGN KEY([GeometryID])
REFERENCES [dbo].[CondenserGeometry] ([GeometryID])
END 
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='Receiver_ReceiverType_fk')
 BEGIN 
ALTER TABLE [dbo].[Receiver]  WITH CHECK ADD  CONSTRAINT [Receiver_ReceiverType_fk] FOREIGN KEY([ReceiverTypeID])
REFERENCES [dbo].[ReceiverType] ([ReceiverTypeID])
END 
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'Receiver'
			And	Column_Name = 'ModelID')
Begin
	ALTER TABLE Receiver
	ADD ModelID integer CONSTRAINT CondenserModel_Receiver_fk FOREIGN KEY (ModelID) REFERENCES CondenserModel(ModelID) 
End
GO

If Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'Receiver'
			And	Column_Name = 'ReceiverCapacitorId')
BEGIN

ALTER TABLE Receiver
DROP COLUMN ReceiverCapacitorId

END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='Receiver_ReceiverCapacity2_fk')
 BEGIN 
ALTER TABLE Receiver DROP CONSTRAINT Receiver_ReceiverCapacity2_fk 
END 
GO

--Altering ControlPanelPrice TABLE
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='ControlPanelPrice_Geometry_fk')
 BEGIN 
ALTER TABLE [dbo].[ControlPanelPrice]  WITH CHECK ADD  CONSTRAINT [ControlPanelPrice_Geometry_fk] FOREIGN KEY([GeometryID])
REFERENCES [dbo].[CondenserGeometry] ([GeometryID])
END 
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='ControlPanelPriceXreforig_Geometry_fk')
 BEGIN 
ALTER TABLE [dbo].[ControlPanelPriceXref orig]  WITH CHECK ADD  CONSTRAINT [ControlPanelPriceXreforig_Geometry_fk] FOREIGN KEY([GeometryID])
REFERENCES [dbo].[CondenserGeometry] ([GeometryID])
END 
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='ControlPanelPriceXreforig_ControlPanelPrice_fk')
 BEGIN 
ALTER TABLE [dbo].[ControlPanelPriceXref orig]  WITH CHECK ADD  CONSTRAINT [ControlPanelPriceXreforig_ControlPanelPrice_fk] FOREIGN KEY([PriceID])
REFERENCES [dbo].[ControlPanelPrice] ([PriceID])
END 
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'ControlPanelPrice'
			And	Column_Name = 'ModelID')
Begin
	ALTER TABLE ControlPanelPrice
	ADD ModelID integer CONSTRAINT CondenserModel_ControlPanelPrice_fk FOREIGN KEY (ModelID) REFERENCES CondenserModel(ModelID) 
End
GO

--Alter CondenserPricing TABLE

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CondenserPricing'
			And	Column_Name = 'CondenserPricingID')
BEGIN

DECLARE @table NVARCHAR(512), @sql NVARCHAR(MAX);
SELECT @table = N'dbo.CondenserPricing';

SELECT @sql = 'ALTER TABLE ' + @table 
    + ' DROP CONSTRAINT ' + name + ';'
    FROM sys.key_constraints
    WHERE [type] = 'PK'
    AND [parent_object_id] = OBJECT_ID(@table);
EXEC sp_executeSQL @sql;

Alter TABLE CondenserPricing add  [CondenserPricingID] [int] IDENTITY(1,1) NOT NULL  PRIMARY KEY (CondenserPricingID)

Alter TABLE CondenserPricing  drop column [ID]
END
GO


IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='CondenserPricing_ModelSuffix_fk')
 BEGIN 
ALTER TABLE [dbo].[CondenserPricing]  WITH CHECK ADD  CONSTRAINT [CondenserPricing_ModelSuffix_fk] FOREIGN KEY([ModelSuffixID])
REFERENCES [dbo].[ModelSuffix] ([ModelSuffixID])
END 
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='CondenserPricing_CondenserType_fk')
 BEGIN 
ALTER TABLE [dbo].[CondenserPricing]  WITH CHECK ADD  CONSTRAINT [CondenserPricing_CondenserType_fk] FOREIGN KEY([CondenserTypeID])
REFERENCES [dbo].[CondenserType] ([CondenserTypeID])
END 
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'CondenserPricing'
			And	Column_Name = 'ModelID')
Begin
	ALTER TABLE CondenserPricing
	ADD ModelID integer CONSTRAINT CondenserModel_CondenserPricing_fk FOREIGN KEY (ModelID) REFERENCES CondenserModel(ModelID) 
End
GO

-- ALter leg Pricing TABLE
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='LegPrice_Geometry_fk')
 BEGIN 
ALTER TABLE [dbo].[LegPrice]  WITH CHECK ADD  CONSTRAINT [LegPrice_Geometry_fk] FOREIGN KEY([GeometryID])
REFERENCES [dbo].[CondenserGeometry] ([GeometryID])
END 
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='LegPrice_LegType_fk')
 BEGIN 
ALTER TABLE [dbo].[LegPrice]  WITH CHECK ADD  CONSTRAINT [LegPrice_LegType_fk] FOREIGN KEY([LegTypeID])
REFERENCES [dbo].[LegType] ([LegTypeID])
END 
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'LegPrice'
			And	Column_Name = 'ModelID')
Begin
	ALTER TABLE LegPrice
	ADD ModelID integer CONSTRAINT CondenserModel_LegPrice_fk FOREIGN KEY (ModelID) REFERENCES CondenserModel(ModelID) 
End
Go

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'LegPrice'
			And	Column_Name = 'IsSelected')
Begin
	ALTER TABLE LegPrice
	ADD IsSelected BIT not null default(0)
End
Go

-- Altering MiscellaneousOptionPricing TABLE

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='MiscellaneousOptionsPricing_Geometry_fk')
 BEGIN 
ALTER TABLE [dbo].[MiscellaneousOptionsPricing]  WITH CHECK ADD  CONSTRAINT [MiscellaneousOptionsPricing_Geometry_fk] FOREIGN KEY([GeometryID])
REFERENCES [dbo].[CondenserGeometry] ([GeometryID])
END 
GO


IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='MiscellaneousOptionsPricing_MiscellenousOptions_fk')
 BEGIN 
ALTER TABLE [dbo].[MiscellaneousOptionsPricing]  WITH CHECK ADD  CONSTRAINT [MiscellaneousOptionsPricing_MiscellenousOptions_fk] FOREIGN KEY([MiscellenousOptionsID])
REFERENCES [dbo].[MiscellaneousOptions] ([MiscellaneousOptionsID])
END 
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'MiscellaneousOptionsPricing'
			And	Column_Name = 'IsSelected')
Begin
	ALTER TABLE MiscellaneousOptionsPricing
	ADD IsSelected BIT not null default(0)
End
Go

--- /////////************* PRICING SECTION END *******************\\\\\\\\\\\\\\\\

--- /////////************* CONDENSER TYPE SECTION START *******************\\\\\\\\\\\\\\\\

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'MiscellaneousOptionsPricing'
			And	Column_Name = 'ModelID')
Begin
	ALTER TABLE MiscellaneousOptionsPricing
	ADD ModelID integer CONSTRAINT CondenserModel_MiscellaneousOptionsPricing_fk FOREIGN KEY (ModelID) REFERENCES CondenserModel(ModelID) 
End
Go

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME='LegAvailabilityXref_LegType_fk')
 BEGIN 
ALTER TABLE [dbo].[LegAvailabilityXref]  WITH CHECK ADD  CONSTRAINT [LegAvailabilityXref_LegType_fk] FOREIGN KEY([LegTypeID])
REFERENCES [dbo].[LegType] ([LegTypeID])
END 
GO

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'LegAvailabilityXref'
			And	Column_Name = 'ModelID')
Begin
	ALTER TABLE LegAvailabilityXref
	ADD ModelID integer CONSTRAINT CondenserModel_LegAvailabilityXref_fk FOREIGN KEY (ModelID) REFERENCES CondenserModel(ModelID) 
End
Go

--- /////////************* CONDENSER TYPE SECTION END *******************\\\\\\\\\\\\\\\\

--- /////////************* RECEIVER SECTION START *******************\\\\\\\\\\\\\\\\
If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'Receiver'
			And	Column_Name = 'IsSelected')
Begin
	Alter Table Receiver
	Add IsSelected bit NULL
End
GO

--- /////////************* RECEIVER SECTION END *******************\\\\\\\\\\\\\\\\


--------------------------------Alter RefrigerantType Table---------------------------------------------

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'RefrigerantType'
			And	Column_Name = 'Active')
Begin
	ALTER TABLE RefrigerantType
	ADD Active BIT not null default(1), Obsolete BIT not null default(0)
End
Go

--------------------------------End RefrigerantType Table---------------------------------------------


------------------------------- Alter ReceiverCapacity TABLE -----------------------------------------

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'ReceiverCapacity'
			And	Column_Name = 'ModelID')
Begin
	ALTER TABLE ReceiverCapacity
	ADD ModelID integer CONSTRAINT ReceiverCapacity_LegPrice_fk FOREIGN KEY (ModelID) REFERENCES CondenserModel(ModelID) 
End
Go

------------------------------- END ReceiverCapacity TABLE -----------------------------------------

------------------------------- Control Panel Details-----------------------------------------

If Not Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'ControlPanelDetails')
Begin
create table [dbo].[ControlPanelDetails](
ControlPanelDetailID int identity(1,1) not null primary key(ControlPanelDetailID),
ModelId int constraint Fk_ModelCondenser foreign key (ModelId) REFERENCES  CondenserModel(ModelID),
ControlID int CONSTRAINT fk_ControlID FOREIGN KEY (ControlID) REFERENCES Controls(ID),
ControlMfgID int CONSTRAINT fk_ControlMfgID FOREIGN KEY (ControlMfgID) REFERENCES ControlManufacturer(ControlManufacturerID), 
ControlAirSensorID int CONSTRAINT fk_ControlAirSensorID FOREIGN KEY (ControlAirSensorID) REFERENCES ControlPanelAirSensor(AirSensorID),
ControlFuseAndBreakerID int CONSTRAINT fk_ControlFuseAndBreakerID FOREIGN KEY (ControlFuseAndBreakerID) REFERENCES ControlFusesandBreakers(ControlFusesandBreakersID),
ControlTypeOfApplicationID int CONSTRAINT fk_ControlTypeOfApplicationID FOREIGN KEY (ControlTypeOfApplicationID) REFERENCES ControlTypeofApplication(ControlTypeofApplicationID),
ControlVoltageID int CONSTRAINT fk_ControlVoltageID FOREIGN KEY (ControlVoltageID) REFERENCES ControlVoltage(ControlVoltageID)
);
End
Go

------------------------------- Control Panel Details And Options (Junction Table)-----------------------------------------

If Not Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'ControlPanelDetailsAndOptions')
Begin
create table [dbo].[ControlPanelDetailsAndOptions](
ControlPanelDetailsAndOptionsID int identity(1,1) not null primary key(ControlPanelDetailsAndOptionsID),
ControlPanelDetailID int constraint Fk_ControlPanelDetailID foreign key (ControlPanelDetailID) references ControlPanelDetails(ControlPanelDetailID),
ControlPanelOptionsID int constraint Fk_ControlPanelOptionsID foreign key (ControlPanelOptionsID) references ControlPanelOptions(ID) 
);
End
Go

----------------------------- Rename Column ControlPanelOptionsID to ID in Table ControlPanelOptions -----------------------

If NOT Exists (Select 1 from Information_Schema.COlumns
			Where Table_Name = 'ControlPanelOptions'
			And	Column_Name = 'ControlPanelOptionsID')
Begin
EXEC sp_RENAME 'ControlPanelOptions.ControlPanelOptionID' , 'ID', 'COLUMN'
End
Go