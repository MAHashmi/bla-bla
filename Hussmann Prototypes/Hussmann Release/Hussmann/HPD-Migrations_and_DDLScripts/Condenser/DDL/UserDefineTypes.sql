

IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'LegPriceUDT')
BEGIN
CREATE TYPE [dbo].[LegPriceUDT] AS TABLE(
	[LegPriceID] [int] NULL,
	[LegPrice] [money] NULL,
	[LegTypeID] [int] NULL,
	[GeometryID] [int] NULL,
	[ModelID] [int] NULL
)
END
GO

IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'MiscellaneousOptionsPricingUDT')
BEGIN
CREATE TYPE [dbo].[MiscellaneousOptionsPricingUDT] AS TABLE(
	[MiscellaneousPricingID] [int] NULL,
	[MiscellaneousPrice] [money] NULL,
	[MiscellenousOptionsID] [int] NULL,
	[GeometryID] [int] NULL,
	[ModelID] [int] NULL
)
END
GO

IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'ControlPanelPricingUDT')
BEGIN
CREATE TYPE [dbo].[ControlPanelPricingUDT] AS TABLE(
	[PriceID] [int] NULL,
	[GeometryID] [int] NULL,
	[ListPrice] [money] NULL,
	[ModelID] [int] NULL
)
END
GO

IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'ReceiverUDT')
BEGIN
CREATE TYPE [dbo].[ReceiverUDT] AS TABLE(
	[ReceiverID] [int] NOT NULL,
	[GeometryID] [int]  NULL,
	[ReceiverTypeID] [int] NULL,
	[ReceiverPrice] [money] NULL,
	[HeatTapeInsulPrice] [money] NULL,
	[ReceiverQty] [int] NULL,
	[ModelID] [int] NULL,
	[IsSelected] [bit] NULL
)
END
GO

IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'LegAvailabilityXrefUDT')
BEGIN
CREATE TYPE [dbo].[LegAvailabilityXrefUDT] AS TABLE(
	[ID] [int] NOT NULL,
	[CondenserTypeID] [int] NULL,
	[LegTypeID] [int] NULL,
	[LegAvailable] [bit] NULL,
	[Standard] [bit] NULL,
	[ModelID] [int] NULL
)
END
GO

IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'CondenserCapacityUDT')
BEGIN
CREATE TYPE [dbo].[CondenserCapacityUDT] AS TABLE(
	[CondCapacityID_] [int]  NULL,
	[ModelID] [int]  NULL,
	[RefrigerantTypeID] [int] NULL,
	[Capacity] [decimal](18, 5) NULL
)
END
GO

IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'CondenserPropertiesUDT')
BEGIN
CREATE TYPE [dbo].[CondenserPropertiesUDT] AS TABLE(
	[CondPropID] [int]  NULL,
	[ModelID] [int] NULL,
	[VoltageTypeID] [int] NULL,
	[CondenserAmps] [decimal](18, 5) NULL,
	[MCA] [decimal](18, 5) NULL,
	[MOPD] [decimal](18, 5) NULL
)
END
GO

IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'MiscellaneousOptionsSelectionUDT')
BEGIN
CREATE TYPE [dbo].[MiscellaneousOptionsSelectionUDT] AS TABLE(
	[MiscellaneousPricingID] [int] NULL,
	[MiscellenousOptionsID] [int] NULL,
	[ModelID] [int] NULL,
	[IsSelected] [bit] NULL,
	[MiscellenousOption] [varchar] NULL
)
END
GO


IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'ReceiverCapacityUDT')
BEGIN
CREATE TYPE [dbo].[ReceiverCapacityUDT] AS TABLE(
	[RcvrCapacityID] [int] NULL,
	[ReceiverTypeID] [int] NULL,
	[RefrigerantType_ID] [int],
	[ReceiverCapacity] [decimal](18, 5) NULL,
	[ModelID] [int] NULL
)
END
GO

IF NOT  EXISTS (SELECT * FROM sys.types WHERE is_table_type = 1 AND name = 'ControlPanelOptionIds')
BEGIN
CREATE TYPE [dbo].[ControlPanelOptionIds] AS TABLE(
	[Id] [int] NULL
)
END
GO