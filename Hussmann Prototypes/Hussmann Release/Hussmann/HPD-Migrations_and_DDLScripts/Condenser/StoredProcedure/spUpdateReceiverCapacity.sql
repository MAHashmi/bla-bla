-- =============================================
-- Author:		Amirul Sunesara
-- Create date: 11-5-2018
-- Description:	Update Receiver Type Tab Data (Receiver Capacity)
-- =============================================
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spUpdateReceiverCapacity'))
BEGIN
    DROP PROCEDURE spUpdateReceiverCapacity
END

GO

CREATE PROCEDURE [dbo].[spUpdateReceiverCapacity] 
    
	 @param_ReceiverCapacityUDT ReceiverCapacityUDT Readonly
AS
BEGIN
	
		UPDATE RC
		SET 
			RC.ReceiverCapacity = RCUDT.ReceiverCapacity 
			
		FROM ReceiverCapacity RC 
		INNER JOIN @param_ReceiverCapacityUDT RCUDT 
			ON RC.ModelID = RCUDT.ModelID
			AND RC.[RcvrCapacityID ] = RCUDT.RcvrCapacityID
		

END
