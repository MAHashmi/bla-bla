-- =============================================
-- Author:		Amirul Sunesara
-- Create date: 11/5/2018
-- Description:	Get Condenser Receiver Type Tab Data
-- =============================================
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spGetCondenserReceiverTypeData'))
BEGIN
    DROP PROCEDURE spGetCondenserReceiverTypeData
END

GO

CREATE PROCEDURE [dbo].[spGetCondenserReceiverTypeData]
	@param_ModelID int
	
AS
BEGIN


DECLARE @TempTable TABLE
(
  ReceiverTypeID int,
  RefrigerantType_ID int,
  ModelID int
)

Insert Into @TempTable(
ReceiverTypeID,
RefrigerantType_ID,
ModelID
)

Select DISTINCT
RT.ReceiverTypeID,RFT.RefrigerantType_ID,@param_ModelID FROM 
(select RTT.ReceiverTypeID FROM ReceiverType RTT
JOIN Receiver R ON RTT.ReceiverTypeID=R.ReceiverTypeID
Where R.ModelID=@param_ModelID
)RT
CROSS JOIN 
(select RFTT.RefrigerantType_ID FROM RefrigerantType RFTT)RFT
 


Insert Into ReceiverCapacity(ReceiverTypeID,RefrigerantType_ID,ModelID)
select TT.ReceiverTypeID,TT.RefrigerantType_ID,TT.ModelID FROM @TempTable TT WHERE TT.RefrigerantType_ID NOT IN
(
 select B.RefrigerantType_ID FROM @TempTable A
 JOIN ReceiverCapacity B ON A.RefrigerantType_ID=B.RefrigerantType_ID
 WHERE A.ModelID=B.ModelID AND A.ReceiverTypeID=B.ReceiverTypeID
)

select DISTINCT RC.[RcvrCapacityID ],RC.ReceiverCapacity,RC.ReceiverTypeID,RC.RefrigerantType_ID,RC.ModelID,RT.RefrigerantType,R.ReceiverSize FROM ReceiverCapacity RC
INNER JOIN(select * FROM RefrigerantType RTT WHERE RTT.Active=1 AND RTT.Obsolete=0 )RT ON RT.RefrigerantType_ID=RC.RefrigerantType_ID
INNER JOIN(select RE.ReceiverTypeID,RTT.ReceiverSize FROM Receiver RE
			JOIN ReceiverType RTT ON RTT.ReceiverTypeID=RE.ReceiverTypeID
 WHERE RE.ModelID=@param_ModelID AND RE.IsSelected=1)R ON R.ReceiverTypeID=RC.ReceiverTypeID
 AND RC.ModelID=@param_ModelID





END