IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spUpdateCondenserControlPanelTabData'))
BEGIN
    DROP PROCEDURE spUpdateCondenserControlPanelTabData
END

GO


CREATE PROCEDURE [dbo].[spUpdateCondenserControlPanelTabData]  
	@param_ModelID int,
	@param_ControlID int,
	@param_ControlMfgID int,
	@param_ControlAirSensorID int,
	@param_ControlFuseAndBreakerID int,
	@param_ControlTypeOfApplicationID int,
	@param_ControlVoltageID int,
	@param_ControlPanelOptionIdsUDT [ControlPanelOptionIds] readonly
	
AS
BEGIN

IF EXISTS ( SELECT 1 FROM ControlPanelDetails WHERE ModelId=@param_ModelID )
		BEGIN
			UPDATE ControlPanelDetails
			SET 
		      ControlID=@param_ControlID,
			  ControlMfgID=@param_ControlMfgID,
			  ControlAirSensorID = @param_ControlAirSensorID,
			  ControlFuseAndBreakerID = @param_ControlAirSensorID,
			  ControlTypeOfApplicationID = @param_ControlTypeOfApplicationID,
			  ControlVoltageID = @param_ControlVoltageID
			  where ModelId = @param_ModelID

		 END
Else
	     Begin
		 insert into ControlPanelDetails(ControlID,
			  ControlMfgID,
			  ControlAirSensorID,
			  ControlFuseAndBreakerID,
			  ControlTypeOfApplicationID,
			  ControlVoltageID,
			  ModelId
			  )
			  values (@param_ControlID,@param_ControlMfgID,@param_ControlAirSensorID,@param_ControlFuseAndBreakerID,@param_ControlTypeOfApplicationID,@param_ControlVoltageID,@param_ModelID)
	     End

declare @ControlPanelDetailsId int

set @ControlPanelDetailsId = (select ControlPanelDetailID 
								from ControlPanelDetails
								 where ModelId = @param_ModelID)

delete from ControlPanelDetailsAndOptions
where ControlPanelDetailID = @ControlPanelDetailsId

insert into ControlPanelDetailsAndOptions(ControlPanelDetailID,ControlPanelOptionsID)
select @ControlPanelDetailsId,Id
from @param_ControlPanelOptionIdsUDT

END
