IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spCreateCondenserModel'))
BEGIN
    DROP PROCEDURE spCreateCondenserModel
END

GO

-- =============================================
-- Author:		Hafsah Azfar
-- Create date: 26/4/2018
-- Description:	Create Condenser Model
-- =============================================
CREATE PROCEDURE [dbo].[spCreateCondenserModel]

	@param_MFGID int,
	@param_ModelFamilyID int,
	@param_ModelNumber nvarchar(15),
	@param_CubicFeetPerMinute  int,
	@param_Title24Compliant bit,
	@param_R404SummerCharge  int,
	@param_R404WinterCharge  int,
	@param_SoundDecibals10  int,
	@param_UnitWeight  int,
	@param_Length  int,
	@param_Width  int,
	@param_Height  int,
	@param_CondenserTypeID  int,
	@param_ModelSuffixID  int,
	@param_GeometryID  int,
	@param_CondFanMotorID  int,
	@param_CreatedBy varchar(75)

	
AS
BEGIN
	
	DECLARE @Version int = 1,
			@StateID int = 1,
			@ModelID int,
			@CurrentDate datetime = GETDATE();

	Insert Into CondenserModel(
			MFGID,
			ModelFamilyID,
			ModelNumber,
			CubicFeetPerMinute,
			Title24Compliant,
			R404SummerCharge,
			R404WinterCharge,
			SoundDecibals10,
			UnitWeight,
			[Length],
			Width,
			Height,
			CondenserTypeID,
			ModelSuffixID,
			GeometryID,
			CondFanMotorID,
			[Version],
			StateID,
			LastModifiedOn,
			Note,
			CreatedBy,
			ActionBy
			)
	select
		 	
			@param_MFGID,
			@param_ModelFamilyID,
			@param_ModelNumber,
			@param_CubicFeetPerMinute,
			@param_Title24Compliant,
			@param_R404SummerCharge,
			@param_R404WinterCharge,
			@param_SoundDecibals10 ,
			@param_UnitWeight ,
			@param_Length ,
			@param_Width ,
			@param_Height ,
			@param_CondenserTypeID ,
			@param_ModelSuffixID ,
			@param_GeometryID ,
			@param_CondFanMotorID ,
		 	@Version,
			@StateID,
		 	@CurrentDate,
		 	Null ,
		 	@param_CreatedBy ,
		 	@param_CreatedBy;

		
	SET @ModelID = SCOPE_IDENTITY();

	INSERT INTO [dbo].[CondenserModelActionHistory]
           ([ModelID]
           ,[StateID]
           ,[CreatedBy]
           ,[LastModifiedOn]
           ,[ActionBy]
           ,[XMLData])
     VALUES
           (@ModelID
           ,@StateID
           ,@param_CreatedBy
           ,@CurrentDate
           ,@param_CreatedBy
           ,(Select * from CondenserModel where ModelID = @ModelID  For XML Path));
	
	

	
	IF NOT EXISTS ( SELECT 1 FROM LegPrice WHERE ModelID=@ModelID )
	BEGIN
	INSERT INTO LegPrice(LegPrice,LegTypeID,GeometryID,ModelID) Values(null,1,@param_GeometryID,@ModelID)
	INSERT INTO LegPrice(LegPrice,LegTypeID,GeometryID,ModelID) Values(null,2,@param_GeometryID,@ModelID)
	INSERT INTO LegPrice(LegPrice,LegTypeID,GeometryID,ModelID) Values(null,3,@param_GeometryID,@ModelID)
	INSERT INTO LegPrice(LegPrice,LegTypeID,GeometryID,ModelID) Values(null,4,@param_GeometryID,@ModelID)
	INSERT INTO LegPrice(LegPrice,LegTypeID,GeometryID,ModelID) Values(null,5,@param_GeometryID,@ModelID)
	INSERT INTO LegPrice(LegPrice,LegTypeID,GeometryID,ModelID) Values(null,6,@param_GeometryID,@ModelID)
	INSERT INTO LegPrice(LegPrice,LegTypeID,GeometryID,ModelID) Values(null,7,@param_GeometryID,@ModelID)
	INSERT INTO LegPrice(LegPrice,LegTypeID,GeometryID,ModelID) Values(null,8,@param_GeometryID,@ModelID)
	INSERT INTO LegPrice(LegPrice,LegTypeID,GeometryID,ModelID) Values(null,9,@param_GeometryID,@ModelID)		
	END

	IF NOT EXISTS ( SELECT 1 FROM MiscellaneousOptionsPricing WHERE ModelID=@ModelID )
	BEGIN
	INSERT INTO MiscellaneousOptionsPricing(MiscellaneousPrice,MiscellenousOptionsID,GeometryID,ModelID) Values(null,1,@param_GeometryID,@ModelID)
	INSERT INTO MiscellaneousOptionsPricing(MiscellaneousPrice,MiscellenousOptionsID,GeometryID,ModelID) Values(null,2,@param_GeometryID,@ModelID)
	INSERT INTO MiscellaneousOptionsPricing(MiscellaneousPrice,MiscellenousOptionsID,GeometryID,ModelID) Values(null,3,@param_GeometryID,@ModelID)		
	END

	IF NOT EXISTS(SELECT 1 FROM ControlPanelPrice WHERE ModelID=@ModelID)
	BEGIN
	INSERT INTO ControlPanelPrice(GeometryID,ListPrice,ModelID) Values (1,null,@ModelID)
	INSERT INTO ControlPanelPrice(GeometryID,ListPrice,ModelID) Values (2,null,@ModelID)
	INSERT INTO ControlPanelPrice(GeometryID,ListPrice,ModelID) Values (3,null,@ModelID)
	INSERT INTO ControlPanelPrice(GeometryID,ListPrice,ModelID) Values (4,null,@ModelID)
	INSERT INTO ControlPanelPrice(GeometryID,ListPrice,ModelID) Values (5,null,@ModelID)
	INSERT INTO ControlPanelPrice(GeometryID,ListPrice,ModelID) Values (6,null,@ModelID)
	INSERT INTO ControlPanelPrice(GeometryID,ListPrice,ModelID) Values (7,null,@ModelID)
	INSERT INTO ControlPanelPrice(GeometryID,ListPrice,ModelID) Values (8,null,@ModelID)
	INSERT INTO ControlPanelPrice(GeometryID,ListPrice,ModelID) Values (9,null,@ModelID)
	INSERT INTO ControlPanelPrice(GeometryID,ListPrice,ModelID) Values (10,null,@ModelID)
	INSERT INTO ControlPanelPrice(GeometryID,ListPrice,ModelID) Values (11,null,@ModelID)
	INSERT INTO ControlPanelPrice(GeometryID,ListPrice,ModelID) Values (12,null,@ModelID)
	INSERT INTO ControlPanelPrice(GeometryID,ListPrice,ModelID) Values (13,null,@ModelID)
	END

		IF NOT EXISTS(SELECT 1 FROM LegAvailabilityXref WHERE ModelID=@ModelID)
	BEGIN
	INSERT INTO LegAvailabilityXref(CondenserTypeID,LegTypeID,LegAvailable,[Standard],ModelID) Values (@param_CondenserTypeID ,1,0,0,@ModelID)
	INSERT INTO LegAvailabilityXref(CondenserTypeID,LegTypeID,LegAvailable,[Standard],ModelID) Values (@param_CondenserTypeID ,2,0,0,@ModelID)
	INSERT INTO LegAvailabilityXref(CondenserTypeID,LegTypeID,LegAvailable,[Standard],ModelID) Values (@param_CondenserTypeID ,3,0,0,@ModelID)
	INSERT INTO LegAvailabilityXref(CondenserTypeID,LegTypeID,LegAvailable,[Standard],ModelID) Values (@param_CondenserTypeID ,4,0,0,@ModelID)
	INSERT INTO LegAvailabilityXref(CondenserTypeID,LegTypeID,LegAvailable,[Standard],ModelID) Values (@param_CondenserTypeID ,5,0,0,@ModelID)
	INSERT INTO LegAvailabilityXref(CondenserTypeID,LegTypeID,LegAvailable,[Standard],ModelID) Values (@param_CondenserTypeID ,6,0,0,@ModelID)
	INSERT INTO LegAvailabilityXref(CondenserTypeID,LegTypeID,LegAvailable,[Standard],ModelID) Values (@param_CondenserTypeID ,7,0,0,@ModelID)
	INSERT INTO LegAvailabilityXref(CondenserTypeID,LegTypeID,LegAvailable,[Standard],ModelID) Values (@param_CondenserTypeID ,8,0,0,@ModelID)
	INSERT INTO LegAvailabilityXref(CondenserTypeID,LegTypeID,LegAvailable,[Standard],ModelID) Values (@param_CondenserTypeID ,9,0,0,@ModelID)
	END

	IF NOT EXISTS(SELECT 1 FROM Receiver WHERE ModelID=@ModelID)
	BEGIN
	INSERT INTO Receiver(GeometryID,ReceiverTypeID,ReceiverPrice,HeatTapeInsulPrice,ReceiverQty,ModelID,IsSelected)
	VALUES (@param_GeometryID,1,NULL,NULL,NULL,@ModelID,0)
	INSERT INTO Receiver(GeometryID,ReceiverTypeID,ReceiverPrice,HeatTapeInsulPrice,ReceiverQty,ModelID,IsSelected)
	VALUES (@param_GeometryID,2,NULL,NULL,NULL,@ModelID,0)
	INSERT INTO Receiver(GeometryID,ReceiverTypeID,ReceiverPrice,HeatTapeInsulPrice,ReceiverQty,ModelID,IsSelected)
	VALUES (@param_GeometryID,3,NULL,NULL,NULL,@ModelID,0)
	INSERT INTO Receiver(GeometryID,ReceiverTypeID,ReceiverPrice,HeatTapeInsulPrice,ReceiverQty,ModelID,IsSelected)
	VALUES (@param_GeometryID,4,NULL,NULL,NULL,@ModelID,0)
	END

		select @ModelID as ModelID

END
