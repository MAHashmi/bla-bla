IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spUpdateCondenserDetailsTabData'))
BEGIN
    DROP PROCEDURE spUpdateCondenserDetailsTabData
END

GO
-- =============================================
-- Author:		Jaffer Raza
-- Create date: 10/2/2018
-- Description:	Update Condenser Details Tab data
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateCondenserDetailsTabData]  
	@param_ModelID int,
	@param_MFGID int,
	@param_ModelFamilyID int,
	@param_ModelNumber nvarchar(15),
	@param_CubicFeetPerMinute  int,
	@param_Title24Compliant bit,
	@param_R404SummerCharge  int,
	@param_R404WinterCharge  int,
	@param_SoundDecibals10  int,
	@param_UnitWeight  int,
	@param_Length  int,
	@param_Width  int,
	@param_Height  int,
	@param_CondenserTypeID  int,
	@param_ModelSuffixID  int,
	@param_GeometryID  int,
	@param_CondFanMotorID  int,
	@param_ActionBy varchar(75)
	
AS
BEGIN



	UPDATE CondenserModel 
	SET
	 MFGID = @param_MFGID,
	 ModelFamilyID = @param_ModelFamilyID,
	 ModelNumber = @param_ModelNumber,
	 CubicFeetPerMinute = @param_CubicFeetPerMinute,
	 Title24Compliant = @param_Title24Compliant,
	 R404SummerCharge = @param_R404SummerCharge,
	 R404WinterCharge = @param_R404WinterCharge,
	 SoundDecibals10  = @param_SoundDecibals10,
	 UnitWeight = @param_UnitWeight,
	 [Length] =  @param_Length,
	 Width = @param_Width ,
	 Height = @param_Height,
	 CondenserTypeID = @param_CondenserTypeID,
	 ModelSuffixID = @param_ModelSuffixID,
	 GeometryID = @param_GeometryID,
	 CondFanMotorID  = @param_CondFanMotorID ,
	 LastModifiedOn = GETDATE(),
	 ActionBy =  @param_ActionBy
	 where ModelID = @param_ModelID

	
END
