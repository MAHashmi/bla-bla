IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spGetCondenserDasboardNewsFeed'))
BEGIN
    DROP PROCEDURE spGetCondenserDasboardNewsFeed
END

GO
-- =============================================
-- Author:		Jaffer Raza
-- Create date: 23/4/2018
-- Description:	Get top 10 latest modified data for news feed of condenser
-- =============================================
CREATE PROCEDURE [dbo].[spGetCondenserDasboardNewsFeed]
	@param_UserType varchar(20),
	@param_UserName varchar(75)
AS
BEGIN
	
	IF(@param_UserType = 'Super' OR @param_UserType = 'Approver')
	BEGIN
		SELECT TOP 10 
		CM.ModelID,
		CM.StateID,
		CM.LastModifiedOn,
		CM.CreatedBy,
		CM.ActionBy,
		C.ModelNumber
	    FROM CondenserModelActionHistory CM
		join CondenserModel C
		on CM.ModelID = C.ModelID
		ORDER BY  LastModifiedOn DESC
	END
	
	IF(@param_UserType = 'DataFeeder')
	BEGIN
		SELECT TOP 10 
		CM.ModelID,
		CM.StateID,
		CM.LastModifiedOn,
		CM.CreatedBy,
		CM.ActionBy,
		C.ModelNumber
	    FROM CondenserModelActionHistory CM
		JOIN CondenserModel C
		ON CM.ModelID = C.ModelID
		WHERE CM.StateID = 4 OR CM.CreatedBy = @param_UserName --approved model or created by same user
		ORDER BY  LastModifiedOn DESC
	END
	
	IF(@param_UserType = 'Reader')
	BEGIN
		SELECT TOP 10 
		CM.ModelID,
		CM.StateID,
		CM.LastModifiedOn,
		CM.CreatedBy,
		CM.ActionBy,
		C.ModelNumber
	    FROM CondenserModelActionHistory CM
		join CondenserModel C
		on CM.ModelID = C.ModelID
		WHERE CM.StateID = 4 -- only approved models
		ORDER BY  LastModifiedOn DESC
	END

END
GO