IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spUpdateCondenserTypeTabData'))
BEGIN
    DROP PROCEDURE spUpdateCondenserTypeTabData
END

GO
-- =============================================
-- Author:		Jaffer Raza
-- Create date: 7-05-2018
-- Description:	Condenser Type Tab Update
-- =============================================
CREATE PROCEDURE  [dbo].[spUpdateCondenserTypeTabData]
	@param_ModelID INT,

	@param_LegAvailabilityXrefUDT [LegAvailabilityXrefUDT] readonly,
	@param_ReceiverUDT [ReceiverUDT] readonly
AS
BEGIN
		-- Update LegAvailabilityXref START
		-----------------------------------
		IF EXISTS ( SELECT 1 FROM LegAvailabilityXref WHERE ModelID=@param_ModelID )
		BEGIN
			UPDATE LAX
			SET 
				LAX.LegAvailable = LAXU.LegAvailable,
				LAX.[Standard] = LAXU.[Standard]
				

			FROM LegAvailabilityXref LAX
			INNER JOIN @param_LegAvailabilityXrefUDT LAXU
			ON LAX.ID = LAXU.ID

		END
		
		IF EXISTS ( SELECT 1 FROM LegPrice WHERE ModelID=@param_ModelID )
			BEGIN
				UPDATE LP
				SET 
					LP.IsSelected = (select LAX.LegAvailable FROM LegAvailabilityXref LAX
					WHERE LAX.LegTypeID=LP.LegTypeID AND LAX.ModelID=@param_ModelID
					)

				FROM LegPrice LP
				WHERE LP.ModelID=@param_ModelID

		END
		-- Update LegAvailabilityXref END

		-- Update Receiver START
		-------------------------
		IF EXISTS ( SELECT 1 FROM Receiver WHERE ModelID=@param_ModelID )
		BEGIN
			UPDATE R
			SET 
				R.IsSelected = RU.IsSelected
				
			FROM Receiver R
			INNER JOIN @param_ReceiverUDT RU
			ON R.ReceiverID = RU.ReceiverID

		END
		-- Update Receiver END
END
GO