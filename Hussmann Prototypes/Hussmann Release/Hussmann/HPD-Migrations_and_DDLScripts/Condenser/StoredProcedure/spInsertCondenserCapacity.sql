IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spInsertCondenserCapacity'))
BEGIN
    DROP PROCEDURE spInsertCondenserCapacity
END

GO
-- =============================================
-- Author:		Jaffer Raza
-- Create date: 8-5-2018
-- Description:	Add rows in CondenserCapacity on the basis of RefregerantType
-- =============================================
CREATE PROCEDURE [dbo].[spInsertCondenserCapacity]
	@param_ModelID int
AS
BEGIN
DECLARE @RefrigerantType TABLE
(
  RefrigerantType_ID int,
  RefrigerantType nchar(2),
  RefrigerantDescr nchar(200),
  Active bit,
  Obsolete bit
)

DECLARE @CondenserCapacity TABLE
(
  [CondCapacityID ] int,
  ModelID int,
  RefrigerantTypeID int,
  Capacity decimal(18, 5)
)


Insert Into @RefrigerantType(
RefrigerantType_ID,
RefrigerantType,
RefrigerantDescr,
Active,
Obsolete
)
Select 
RefrigerantType_ID,
RefrigerantType,
RefrigerantDescr,
Active,
Obsolete
from
RefrigerantType R


Insert Into @CondenserCapacity
select
0
,@param_ModelID 
,B.RefrigerantType_ID
,0
from 
(
SELECT RefrigerantType_ID from  @RefrigerantType
)B
Left join @CondenserCapacity C
on B.RefrigerantType_ID = C.RefrigerantTypeID


-- ADD rows which are not present in CondenserCapacity against RefrigerantType
Insert Into [CondenserCapacity ](ModelID,RefrigerantTypeID,Capacity)
Select CC.ModelID,cc.RefrigerantTypeID,cc.Capacity from @CondenserCapacity cc
where cc.RefrigerantTypeID Not in (
SELECT  B.RefrigerantTypeID
FROM @CondenserCapacity A
     JOIN [CondenserCapacity ] B ON (A.RefrigerantTypeID = B.RefrigerantTypeID)
	where A.ModelID in (B.ModelID) ) 
	
END	
GO
