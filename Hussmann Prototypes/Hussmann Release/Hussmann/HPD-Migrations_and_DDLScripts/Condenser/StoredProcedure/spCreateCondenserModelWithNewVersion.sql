-- =============================================
-- Author:		Amirul Sunesara
-- Create date: 16/5/2018
-- Description:	Save Versioned data
-- =============================================
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spCreateCondenserModelWithNewVersion'))
BEGIN
    DROP PROCEDURE spCreateCondenserModelWithNewVersion
END

GO

Create PROCEDURE [dbo].[spCreateCondenserModelWithNewVersion]
	@parm_ModelId INT,
	@param_ActionBy VARCHAR(75)
AS
BEGIN
	--------------------- Declaration -----------------------
	DECLARE @VersionIncrement int = 1,
			@StateID int = 1,
			@NewModelID int = null,
			@CurrentDate datetime = GETDATE(),
			@PreviousModelCreatedBy VARCHAR(75);

	SET @PreviousModelCreatedBy = (Select CreatedBy from CondenserModel where ModelID = @parm_ModelId)

	------------- Insert Into Condenser Model --------------------
	 INSERT INTO CondenserModel(
			MFGID,
			ModelFamilyID,
			ModelNumber,
			CubicFeetPerMinute,
			Title24Compliant,
			R404SummerCharge,
			R404WinterCharge,
			SoundDecibals10,
			UnitWeight,
			[Length],
			[Width],
			[Height],
			CondenserTypeID,
			ModelSuffixID,
			GeometryID,
			CondFanMotorID,
			[Version],
			StateID,
			CreatedBy,
			LastModifiedOn,
			ActionBy,
			Note
			)

	SELECT	MFGID,
			ModelFamilyID,
			ModelNumber,
			CubicFeetPerMinute,
			Title24Compliant,
			R404SummerCharge,
			R404WinterCharge,
			SoundDecibals10,
			UnitWeight,
			[Length],
			[Width],
			[Height],
			CondenserTypeID,
			ModelSuffixID,
			GeometryID,
			CondFanMotorID,
			[Version] + @VersionIncrement,
			@StateID,
			@param_ActionBy,
			@CurrentDate,
			@param_ActionBy,
			Note

	FROM CondenserModel Where ModelID = @parm_ModelId

	SET @NewModelID = SCOPE_IDENTITY();
 
	------------- Insert Into Condenser Model Action History --------------------
	INSERT INTO [dbo].[CondenserModelActionHistory]
           ([ModelID]
           ,[StateID]
           ,[CreatedBy]
           ,[LastModifiedOn]
           ,[ActionBy]
           ,[XMLData])
     VALUES
           (@NewModelID
           ,@StateID
           ,@PreviousModelCreatedBy
           ,@CurrentDate
           ,@param_ActionBy
           ,(Select *  from CondenserModel where ModelID = @NewModelID For XML Path));
	
	------------- Insert Into Condenser Pricing--------------------
	INSERT INTO [dbo].[CondenserPricing]
           (
		   ModelSuffix,
		   EpoxyElectroFinCoatedCoil,
		   PolyesterFinCoating,
		   CopperFin,
		   ModelSuffixID,
		   BaseListPrice,
		   CondenserTypeID,
		   ModelID
		   )
     SELECT 
			ModelSuffix,
			EpoxyElectroFinCoatedCoil,
			PolyesterFinCoating,
			CopperFin,
			ModelSuffixID,
			BaseListPrice,
			CondenserTypeID,
		    @NewModelID
		   FROM CondenserPricing
		   WHERE ModelID = @parm_ModelId
    
	------------- Insert Into LegPricing--------------------
	INSERT INTO [dbo].[LegPrice]
           (
		  GeometryID,
		  IsSelected,
		  LegPrice,
		  LegTypeID,
		  ModelID
		   )
     SELECT 
			GeometryID,
			IsSelected,
			LegPrice,
			LegTypeID,
		    @NewModelID
		   FROM LegPrice
		   WHERE ModelID = @parm_ModelId

	------------- Insert Into MiscellaneousOptionsPricing--------------------
	INSERT INTO [dbo].[MiscellaneousOptionsPricing]
           (
		 GeometryID,IsSelected,MiscellaneousPrice,MiscellenousOptionsID,ModelID
			)
     SELECT 
			GeometryID,
			IsSelected,
			MiscellaneousPrice,
			MiscellenousOptionsID,
			@NewModelID
		   FROM MiscellaneousOptionsPricing
		   WHERE ModelID = @parm_ModelId

	------------- Insert Into Control Panel Pricing--------------------
	INSERT INTO [dbo].[ControlPanelPrice]
           (
		   GeometryID,ListPrice,ModelID
			)
     SELECT 
			GeometryID,
			ListPrice,
			@NewModelID
		   FROM ControlPanelPrice
		   WHERE ModelID = @parm_ModelId

	------------- Insert Into Receiver Pricing--------------------
	INSERT INTO [dbo].[Receiver]
           (
		 GeometryID,HeatTapeInsulPrice,IsSelected,ReceiverPrice,ReceiverQty,ReceiverTypeID,ModelID
			)
     SELECT 
			 GeometryID,HeatTapeInsulPrice,IsSelected,ReceiverPrice,ReceiverQty,ReceiverTypeID,
			@NewModelID
		   FROM Receiver
		   WHERE ModelID = @parm_ModelId


	------------- Insert Into ReceiverCapacity--------------------
	INSERT INTO [dbo].[ReceiverCapacity]
           (
		ModelID,ReceiverCapacity,ReceiverTypeID,RefrigerantType_ID
			)
     SELECT 
			@NewModelID,ReceiverCapacity,ReceiverTypeID,RefrigerantType_ID
		   FROM ReceiverCapacity
		   WHERE ModelID = @parm_ModelId


	------------- Insert Into LegAvailabilityXref--------------------
	INSERT INTO [dbo].[LegAvailabilityXref]
           (
		CondenserTypeID,LegAvailable,LegTypeID,[Standard],ModelID
			)
     SELECT 
		CondenserTypeID,LegAvailable,LegTypeID,[Standard],@NewModelID
		   FROM LegAvailabilityXref
		   WHERE ModelID = @parm_ModelId

------------- Insert Into CondenserCapacity--------------------
	INSERT INTO [dbo].[CondenserCapacity ]
           (
		Capacity,RefrigerantTypeID,ModelID
			)
     SELECT 
		Capacity,RefrigerantTypeID,@NewModelID
		   FROM [CondenserCapacity ]
		   WHERE ModelID = @parm_ModelId

------------- Insert Into CondenserProperties--------------------
	INSERT INTO [dbo].[CondenserProperties]
           (
	CondenserAmps,VoltageTypeID,MCA,MOPD,ModelID
			)
     SELECT 
		CondenserAmps,VoltageTypeID,MCA,MOPD,@NewModelID
		   FROM [CondenserProperties]
		   WHERE ModelID = @parm_ModelId

------------- Insert Into ControlPanelDetails--------------------
	INSERT INTO ControlPanelDetails
           (
	ControlID,ControlMfgID,ControlAirSensorID,ControlFuseAndBreakerID,ControlTypeOfApplicationID,ControlVoltageID,ModelId
			)
     SELECT 
		ControlID,ControlMfgID,ControlAirSensorID,ControlFuseAndBreakerID,ControlTypeOfApplicationID,ControlVoltageID,@NewModelID
		   FROM [ControlPanelDetails]
		   WHERE ModelID = @parm_ModelId
		   
	------------- Insert Into ControlPanelDetailsAndOptions--------------------
	INSERT INTO ControlPanelDetailsAndOptions
           (
	ControlPanelDetailID,ControlPanelOptionsID
			)
     SELECT 
		(select ControlPanelDetailID from ControlPanelDetails where ModelId = @NewModelID),ControlPanelOptionsID
		   FROM ControlPanelDetailsAndOptions
		   WHERE ControlPanelDetailID = (select ControlPanelDetailID from ControlPanelDetails where ModelId = @parm_ModelId)

	----------- Return Coil Model id of new version -----------
	  select @NewModelID as 'ModelID'
END	
