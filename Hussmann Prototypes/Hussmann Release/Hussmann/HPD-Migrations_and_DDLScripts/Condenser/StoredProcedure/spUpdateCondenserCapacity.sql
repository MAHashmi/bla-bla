IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spUpdateCondenserCapacity'))
BEGIN
    DROP PROCEDURE spUpdateCondenserCapacity
END

GO
-- =============================================
-- Author:		Jaffer
-- Create date: 9-5-2018
-- Description:	Update Data of condenserCapacity Table
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateCondenserCapacity] @parm_ModelId INT
	,@param_CondenserCapacityUDT CondenserCapacityUDT Readonly
AS
BEGIN
	--Update Condenser pricing Data
		UPDATE CC
		SET 
			CC.Capacity = UCC.Capacity
			
		FROM [CondenserCapacity ] CC
		INNER JOIN @param_CondenserCapacityUDT UCC 
			ON CC.ModelID = UCC.ModelID
			AND CC.[CondCapacityID ] = UCC.CondCapacityID_
		

END
GO
