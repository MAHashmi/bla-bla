IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spGetRowsCountForSearch'))
BEGIN
    DROP PROCEDURE spGetRowsCountForSearch
END

GO
-- =============================================
-- Author:		Jaffer
-- Create date: 25/4/2018
-- Description:	Get Data Against given filter
-- =============================================
CREATE PROCEDURE [dbo].[spGetRowsCountForSearch]
	@param_ModelNumber varchar(30) = null,
	@param_CondenserBrandId int = null,
	@param_CondenserFamilyId int = null ,
	@param_StatusId int  = null,
	@param_UserName varchar(75),
	@param_UserType varchar(20)
AS
BEGIN
select Count(*) as TotalRows from(
select ModelID
	,ModelNumber
	,ModelSuffixID
	,CONVERT(VARCHAR, LastModifiedOn, 120) as 'LastModifiedOn'
	,[Version]
	,ActionBy
	,CreatedBy
	,CondenserFamilyName
	,CondenserBrandName
	, StateDescription
	from(
SELECT ModelID
	,ModelNumber
	,ModelSuffixID
	,LastModifiedOn
	,[Version]
	,ActionBy
	,CreatedBy
	,CondenserFamilyName
	,CondenserBrandName + ' ' + CASE 
		WHEN CondenserSubBrandName IS NOT NULL
			THEN '-' + CondenserSubBrandName
		ELSE ''
		END AS CondenserBrandName
	,[Description] AS StateDescription

	
FROM (
	SELECT ModelID
		,ModelNumber
		,ModelSuffixID
		,LastModifiedOn
		,[Version]
		,ActionBy
		,CreatedBy
		,ModelFamilyID
		,MFGID
		,StateID
		,ROW_NUMBER() OVER (
			PARTITION BY ModelNumber ORDER BY [Version] DESC
			) AS rownum
	FROM CondenserModel
	WHERE StateID = 4
	) AS CM
INNER JOIN [State] St ON cm.StateID = st.StateID
INNER JOIN CondenserFamily CF ON CF.CondenserFamilyID = cm.ModelFamilyID
INNER JOIN CondenserBrand_lkp CB ON cb.BrandID = CM.MFGID
WHERE rownum = 1
	AND (
		ModelNumber LIKE '%' + @param_ModelNumber + '%'
		OR @param_ModelNumber IS NULL
		)
	AND (
		CB.BrandID = @param_CondenserBrandId
		OR @param_CondenserBrandId IS NULL
		)
	AND (
		CF.CondenserFamilyID = @param_CondenserFamilyId
		OR @param_CondenserFamilyId IS NULL
		)
	AND (
		St.StateID = @param_StatusId
		OR @param_StatusId IS NULL
		)
	AND (St.StateID = 4)
------------------ Latest Approved Versions END --------------------

UNION

SELECT ModelID
	,ModelNumber
	,ModelSuffixID
	,LastModifiedOn
	,[Version]
	,ActionBy
	,CreatedBy
	,CondenserFamilyName
	,CondenserBrandName + ' ' + CASE 
		WHEN CondenserSubBrandName IS NOT NULL
			THEN '-' + CondenserSubBrandName
		ELSE ''
		END AS CondenserBrandName
	,[Description] AS StateDescription
FROM CondenserModel CM
INNER JOIN [State] St ON cm.StateID = st.StateID
INNER JOIN CondenserFamily CF ON CF.CondenserFamilyID = cm.ModelFamilyID
INNER JOIN CondenserBrand_lkp CB ON cb.BrandID = CM.MFGID
WHERE (
		ModelNumber LIKE '%' + @param_ModelNumber + '%'
		OR @param_ModelNumber IS NULL
		)
	AND (
		CB.BrandID = @param_CondenserBrandId
		OR @param_CondenserBrandId IS NULL
		)
	AND (
		CF.CondenserFamilyID = @param_CondenserFamilyId
		OR @param_CondenserFamilyId IS NULL
		)
	AND (
		St.StateID = @param_StatusId
		OR @param_StatusId IS NULL
		)
	AND (St.StateID != 4)
	AND (
			(
			@param_UserType = 'DataFeeder'
			AND CreatedBy = @param_UserName
			)
			OR
						(
			@param_UserType = 'Reader'
			AND St.StateID = 0
			)
			OR
			(@param_UserType = 'Super' OR @param_UserType = 'Approver' )
	
		)
		)A) B
END
