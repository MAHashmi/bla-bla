IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spUpdateMiscellaneous'))
BEGIN
    DROP PROCEDURE spUpdateMiscellaneous
END

GO
-- =============================================
-- Author:		Jaffer Raza
-- Create date: 5-12-2018
-- Description:	Update Miscellaneous tab Data
-- =============================================
CREATE PROCEDURE dbo.[spUpdateMiscellaneous]
	@parm_ModelId INT
	,@param_CondenserPropertiesUDT CondenserPropertiesUDT Readonly
	,@param_MiscellaneousOptionsSelectionUDT MiscellaneousOptionsSelectionUDT readonly
AS
BEGIN
	------ START MiscellaneousOptionsPricingUDT  ------
		-----------------------------------
		IF EXISTS ( SELECT 1 FROM MiscellaneousOptionsPricing WHERE ModelID=@parm_ModelId )
		BEGIN
			UPDATE MOP
			SET 
			MOP.IsSelected = MOSU.IsSelected
				

			FROM MiscellaneousOptionsPricing MOP
			INNER JOIN @param_MiscellaneousOptionsSelectionUDT MOSU
			ON MOP.MiscellaneousPricingID = MOSU.MiscellaneousPricingID

		END
	------ END MiscellaneousOptionsPricingUDT  ------

	------ START CondenserPropertiesUDT ------
	DELETE
	FROM CondenserProperties
	WHERE [CondPropID] NOT IN (
			SELECT [CondPropID]
			FROM @param_CondenserPropertiesUDT
			WHERE ModelID IS NOT NULL
			) --List from Code
		AND ModelID = @parm_ModelId --List of IDS against Model ID

	INSERT INTO dbo.CondenserProperties (
		ModelID
		,VoltageTypeID
		,CondenserAmps
		,MCA
		,MOPD
		)
	SELECT @parm_ModelId
		,VoltageTypeID
		,CondenserAmps
		,MCA
		,MOPD
	FROM @param_CondenserPropertiesUDT AS UCP
	WHERE UCP.ModelID IS NULL
	AND VoltageTypeID IS NOT NULL
		

	UPDATE CP
	SET VoltageTypeID = UCP.VoltageTypeID
		,CondenserAmps = UCP.CondenserAmps
		,MCA = UCP.MCA
		,MOPD = UCP.MOPD
	FROM CondenserProperties CP
	INNER JOIN @param_CondenserPropertiesUDT UCP ON CP.ModelID = UCP.ModelID
		AND CP.[CondPropID] = UCP.[CondPropID]
	WHERE UCP.ModelID IS NOT NULL
		AND UCP.[CondPropID] IS NOT NULL
		------ END CondenserPropertiesUDT ------
END
GO
