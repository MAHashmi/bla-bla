IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spChangeCondenserState'))
BEGIN
    DROP PROCEDURE spChangeCondenserState
END

GO
-- =============================================
-- Author:		Jaffer Raza
-- Create date: 14-5-2018
-- Description:	Change State of Condenser
-- =============================================
CREATE PROCEDURE [dbo].[spChangeCondenserState] @param_ModelId INT
	,@param_StateId INT = NULL
	,@param_ActionBy VARCHAR(75)
AS
BEGIN
	DECLARE @stateID INT
		,@CreatedBy VARCHAR(75)

	IF @param_StateId IS NULL
	BEGIN
		SELECT @stateID = StateID
		FROM CondenserModel
		WHERE ModelID = @param_ModelId
	END

	IF @stateID = 5
		AND @param_StateId IS NULL --Check that in deleted state 
	BEGIN
		SELECT TOP 1 @stateID = StateID
		FROM --restore the second last recored if null then draft
			(
			SELECT TOP 2 *
			FROM [CondenserModelActionHistory]
			WHERE ModelID = @param_ModelId
			ORDER BY LastModifiedOn DESC
			) r
		WHERE ModelID = @param_ModelId
		ORDER BY LastModifiedOn

		IF @stateID IS NULL
			OR @stateID = 5
			SET @stateID = 1

		UPDATE CondenserModel
		SET StateID = @stateID
			,LastModifiedOn = GETDATE()
			,ActionBy = @param_ActionBy
			,@CreatedBy = CreatedBy
		WHERE ModelID = @param_ModelId
	END
	ELSE
	BEGIN
		UPDATE CondenserModel
		SET @stateID = Case when @param_StateId is Null THEN 5
						ELSE @param_StateId END	
			,StateID = Case when @param_StateId is Null THEN 5
						ELSE @param_StateId END
			,LastModifiedOn = GETDATE()
			,ActionBy = @param_ActionBy
			,@CreatedBy = CreatedBy
		WHERE ModelID = @param_ModelId
	END

	INSERT INTO [dbo].[CondenserModelActionHistory] (
		[ModelID]
		,[StateID]
		,[CreatedBy]
		,[LastModifiedOn]
		,[ActionBy]
		,[XMLData]
		)
	VALUES (
		@param_ModelId
		,@stateID
		,@CreatedBy
		,GETDATE()
		,@param_ActionBy
		,(
			SELECT *
			FROM CondenserModel
			WHERE ModelID = @param_ModelId
			FOR XML Path
			)
		)
END


