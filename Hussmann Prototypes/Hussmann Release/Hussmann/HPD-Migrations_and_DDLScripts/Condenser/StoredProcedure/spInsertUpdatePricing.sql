-- =============================================
-- Author:		Amirul Sunesara
-- Create date: 26/4/2018
-- Description:	
-- =============================================
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID('spInsertUpdatePricing'))
BEGIN
    DROP PROCEDURE spInsertUpdatePricing
END

GO

CREATE PROCEDURE [dbo].[spInsertUpdatePricing] 

@param_ModelID INT,
@param_ModelSuffix nvarchar(50),
@param_EpoxyElectroFinCoatedCoil money,
@param_PolyesterFinCoating money,
@param_CopperFin money,
@param_ModelSuffixID int,
@param_BaseListPrice money,
@param_CondenserTypeID int,
@param_CondenserPricingID int,
@param_LegPriceUDT [LegPriceUDT] readonly,

@param_MiscellaneousOptionsPricingUDT [MiscellaneousOptionsPricingUDT] readonly,
@param_ControlPanelPricingUDT [ControlPanelPricingUDT] readonly,
@param_ReceiverPricingUDT [ReceiverUDT] readonly
	
AS
BEGIN
-- START: Insert Update Condenser Pricing--
IF NOT EXISTS ( SELECT 1 FROM CondenserPricing WHERE ModelID=@param_ModelID )
BEGIN
	INSERT INTO CondenserPricing(ModelSuffix,EpoxyElectroFinCoatedCoil,PolyesterFinCoating,CopperFin,ModelSuffixID,BaseListPrice,CondenserTypeID,ModelID) VALUES(@param_ModelSuffix,@param_EpoxyElectroFinCoatedCoil,@param_PolyesterFinCoating,@param_CopperFin,@param_ModelSuffixID,@param_BaseListPrice,@param_CondenserTypeID,@param_ModelID)
END
ELSE
BEGIN
		UPDATE
		CondenserPricing
		Set ModelSuffix = @param_ModelSuffix,
		EpoxyElectroFinCoatedCoil = @param_EpoxyElectroFinCoatedCoil,
		PolyesterFinCoating = @param_PolyesterFinCoating,
		CopperFin = @param_CopperFin,
		ModelSuffixID = @param_ModelSuffixID,
		BaseListPrice = @param_BaseListPrice,
		CondenserTypeID = @param_CondenserTypeID
		where ModelID = @param_ModelID AND @param_CondenserPricingID=@param_CondenserPricingID
END
-- END: Insert Update Condenser Pricing --

-- START: Update Leg Pricing--
		IF EXISTS ( SELECT 1 FROM LegPrice WHERE ModelID=@param_ModelID )
		BEGIN
			UPDATE LP
			SET LP.LegPrice = LPUD.LegPrice,
				LP.GeometryID = LPUD.GeometryID,
				LP.LegTypeID = LPUD.LegTypeID,
				LP.ModelID = LPUD.ModelID

			FROM LegPrice LP
			INNER JOIN @param_LegPriceUDT LPUD 
			ON LP.LegTypeID = LPUD.LegTypeID
			WHERE 
				LP.ModelID = LPUD.ModelID
		END
-- END: Update Leg Pricing--		

-- Start : Update Miscellaneous Options Pricing
		IF EXISTS ( SELECT 1 FROM MiscellaneousOptionsPricing WHERE ModelID=@param_ModelID )
		BEGIN
			UPDATE MOP
			SET
			MOP.MiscellaneousPrice = MOPUDT.MiscellaneousPrice,
			MOP.MiscellenousOptionsID = MOPUDT.MiscellenousOptionsID,
			MOP.GeometryID = MOPUDT.GeometryID,
			MOP.ModelID = MOPUDT.ModelID

			FROM MiscellaneousOptionsPricing MOP
			INNER JOIN @param_MiscellaneousOptionsPricingUDT MOPUDT
			ON MOP.MiscellenousOptionsID =  MOPUDT.MiscellenousOptionsID
			WHERE 
				MOP.ModelID =  MOPUDT.ModelID
		END
-- END : Update Miscellaneous Options Pricing

-- START : Update Control Panel Pricing
		IF EXISTS ( SELECT 1 FROM ControlPanelPrice WHERE ModelID=@param_ModelID )
		BEGIN
			UPDATE CPP
			SET CPP.GeometryID=CPPUDT.GeometryID,
			CPP.ListPrice=CPPUDT.ListPrice,
			CPP.ModelID=CPPUDT.ModelID
			
			FROM ControlPanelPrice CPP
			INNER JOIN @param_ControlPanelPricingUDT CPPUDT 
			ON CPP.GeometryID = CPPUDT.GeometryID
			WHERE 
				CPP.ModelID = CPPUDT.ModelID
		END
-- END : Update Control Panel Pricing

-- START : Update Reciever Pricing

		IF EXISTS ( SELECT 1 FROM Receiver WHERE ModelID=@param_ModelID )
		BEGIN
			UPDATE R
			SET R.GeometryID = RPUDT.GeometryID,
				R.HeatTapeInsulPrice = RPUDT.HeatTapeInsulPrice,
				R.ModelID=RPUDT.ModelID,
				
				R.ReceiverPrice=RPUDT.ReceiverPrice,
				R.ReceiverQty=RPUDT.ReceiverQty,
				R.ReceiverTypeID=RPUDT.ReceiverTypeID
			

			FROM Receiver R
			INNER JOIN @param_ReceiverPricingUDT RPUDT
			ON R.ReceiverTypeID = RPUDT.ReceiverTypeID
			WHERE 
				R.ModelID = RPUDT.ModelID
		END

-- END : Update Reciever Pricing

	
END
