DROP PROCEDURE [dbo].[spChangeUnitCoolerStatus] 
go


/****** Object:  StoredProcedure [dbo].[spChangeUnitCoolerStatus]    Script Date: 2/12/2018 5:51:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  
-- =============================================    
-- Author:  Asad    
-- Create date: 2/12/2018    
-- Description: Used to active or invatice a coil model    
-- =============================================    
CREATE PROCEDURE [dbo].[spChangeUnitCoolerStatus] @param_ModelId INT    
 ,@param_StateId INT = NULL    
 ,@param_ActionBy VARCHAR(75)    
AS    
BEGIN    
 DECLARE @stateID INT    
  ,@CreatedBy VARCHAR(75)    
    
 IF @param_StateId IS NULL    
 BEGIN    
  SELECT @stateID = StateID    
  FROM CoilModel    
  WHERE ModelID = @param_ModelId    
 END    
    
 IF @stateID = 5    
  AND @param_StateId IS NULL --Check that in deleted state     
 BEGIN    
  SELECT TOP 1 @stateID = StateID    
  FROM --restore the second last recored if null then draft    
   (    
   SELECT TOP 2 *    
   FROM [CoilModelActionHistory]    
   WHERE ModelID = @param_ModelId    
   ORDER BY LastModifiedOn DESC    
   ) r    
  WHERE ModelID = @param_ModelId    
  ORDER BY LastModifiedOn    
    
  IF @stateID IS NULL    
   OR @stateID = 5    
   OR @stateID = 2    
   SET @stateID = 1    
    
  UPDATE CoilModel    
  SET StateID = @stateID    
   ,LastModifiedOn = GETDATE()    
   ,ActionBy = @param_ActionBy    
   ,@CreatedBy = CreatedBy    
  WHERE ModelID = @param_ModelId    
 END    
 ELSE    
 BEGIN    
  UPDATE CoilModel    
  SET @stateID = Case when @param_StateId is Null THEN 5    
      ELSE @param_StateId END     
   ,StateID = Case when @param_StateId is Null THEN 5    
      ELSE @param_StateId END    
   ,LastModifiedOn = GETDATE()    
   ,ActionBy = @param_ActionBy    
   ,@CreatedBy = CreatedBy    
  WHERE ModelID = @param_ModelId    
 END    
    
  ------------  
DECLARE @tempModelId INT, @tempStateId INT   
SELECT TOP 1 @tempModelId = ModelID, @tempStateId = StateID FROM CoilModelActionHistory ORDER BY LastModifiedOn DESC  
SELECT @tempModelId, @tempStateId  
  
IF NOT (@tempModelId = @param_ModelId and @tempStateId = @stateID)  
 BEGIN  
 INSERT INTO [dbo].[CoilModelActionHistory] (    
  [ModelID]    
  ,[StateID]    
  ,[CreatedBy]    
  ,[LastModifiedOn]    
  ,[ActionBy]    
  ,[XMLData]    
  )    
 VALUES (    
  @param_ModelId    
  ,@stateID    
  ,@CreatedBy    
  ,GETDATE()    
  ,@param_ActionBy    
  ,(    
   SELECT *    
   FROM CoilModel    
   WHERE ModelID = @param_ModelId    
   FOR XML Path    
   )    
  )  
  END    
END    

GO


