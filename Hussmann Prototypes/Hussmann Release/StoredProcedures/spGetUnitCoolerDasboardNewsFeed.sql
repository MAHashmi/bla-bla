DROP PROCEDURE [dbo].[spGetUnitCoolerDasboardNewsFeed]
go


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jaffer Raza
-- Create date: 18/1/2018
-- Description:	Get top 10 latest modified data for news feed
-- =============================================
CREATE PROCEDURE [dbo].[spGetUnitCoolerDasboardNewsFeed]
	@param_UserType varchar(20),
	@param_UserName varchar(75)
AS
BEGIN
	
	IF(@param_UserType = 'Super' OR @param_UserType = 'Approver')
	BEGIN
		SELECT TOP 10 
		CM.ModelID,
		CM.StateID,
		CM.LastModifiedOn,
		CM.CreatedBy,
		CM.ActionBy,
		C.ModelNumber
	    FROM CoilModelActionHistory CM
		join CoilModel C
		on CM.ModelID = C.ModelID
		ORDER BY  LastModifiedOn DESC
	END
	
	IF(@param_UserType = 'DataFeeder')
	BEGIN
		SELECT TOP 10 
		CM.ModelID,
		CM.StateID,
		CM.LastModifiedOn,
		CM.CreatedBy,
		CM.ActionBy,
		C.ModelNumber
	    FROM CoilModelActionHistory CM
		JOIN CoilModel C
		ON CM.ModelID = C.ModelID
		WHERE CM.StateID = 4 OR CM.CreatedBy = @param_UserName --approved model or created by same user
		ORDER BY  LastModifiedOn DESC
	END
	
	IF(@param_UserType = 'Reader')
	BEGIN
		SELECT TOP 10 
		CM.ModelID,
		CM.StateID,
		CM.LastModifiedOn,
		CM.CreatedBy,
		CM.ActionBy,
		C.ModelNumber
	    FROM CoilModelActionHistory CM
		join CoilModel C
		on CM.ModelID = C.ModelID
		WHERE CM.StateID = 4 -- only approved models
		ORDER BY  LastModifiedOn DESC
	END

END
GO