DROP PROCEDURE [dbo].[spInserorUpdateConfiguration] 
go


CREATE PROCEDURE [dbo].[spInserorUpdateConfiguration] @parm_ModelId INT
	,@param_CoilDrainHtrAmpsUDT CoilDrainHtrAmpsUDT Readonly --done
	,@param_CoilDefrostAmpsUDT CoilDefrostAmpsUDT Readonly --done
	,@param_CoilMotorAmpsUDT CoilMotorAmpsUDT Readonly -- done
	,@param_DefrostConfigurationsUDT DefrostConfigurationsUDT Readonly --done
AS
BEGIN
	------ Start CoilMotorAmps ------
	--Delete the existing record and save new record
	DELETE
	FROM CoilMotorAmps
	WHERE ModelID = @parm_ModelId

	INSERT INTO dbo.CoilMotorAmps (
		ModelID
		,MotorTypeID
		,VoltageTypeID
		,MotorAmps
		)
	SELECT @parm_ModelId
		,CMA.MotorTypeID
		,CMA.VoltageTypeID
		,CMA.MotorAmps
	FROM @param_CoilMotorAmpsUDT AS CMA

	------ END CoilMotorAmps ------
	------ Start CoilDrainHtrAmpsUDT ------
	DELETE
	FROM CoilDrainHtrAmps
	WHERE [Type_ID] NOT IN (
			SELECT [Type_ID]
			FROM @param_CoilDrainHtrAmpsUDT
			WHERE ModelID IS NOT NULL
			) --List from Code
		AND ModelID = @parm_ModelId --List of IDS against Model ID

	INSERT INTO dbo.CoilDrainHtrAmps (
		ModelID
		,VoltageTypeID
		,DrainHtrAmps
		)
	SELECT @parm_ModelId
		,VoltageTypeID
		,DrainHtrAmps
	FROM @param_CoilDrainHtrAmpsUDT AS ICAP
	WHERE ICAP.ModelID IS NULL
	AND VoltageTypeID IS NOT NULL
		

	UPDATE CAP
	SET VoltageTypeID = UCAP.VoltageTypeID
		,DrainHtrAmps = UCAP.DrainHtrAmps
	FROM CoilDrainHtrAmps CAP
	INNER JOIN @param_CoilDrainHtrAmpsUDT UCAP ON CAP.ModelID = UCAP.ModelID
		AND CAP.[Type_ID] = UCAP.[Type_ID]
	WHERE UCAP.ModelID IS NOT NULL
		AND UCAP.[Type_ID] IS NOT NULL
		------ END CoilDrainHtrAmpsUDT ------

		------ Start CoilDefrostAmpsUDT ------

			DELETE
	FROM CoilDefrostAmps
	WHERE [Type_ID] NOT IN (
			SELECT [Type_ID]
			FROM @param_CoilDefrostAmpsUDT
			WHERE ModelID IS NOT NULL
			) --List from Code
		AND ModelID = @parm_ModelId --List of IDS against Model ID

	INSERT INTO dbo.CoilDefrostAmps (
		ModelID
		,VoltageTypeID
		,DefrostAmps
		)
	SELECT @parm_ModelId
		,VoltageTypeID
		,DefrostAmps
	FROM @param_CoilDefrostAmpsUDT AS ICAP
	WHERE ICAP.ModelID IS NULL
	AND VoltageTypeID IS NOT NULL
		

	UPDATE CAP
	SET VoltageTypeID = UCAP.VoltageTypeID
		,DefrostAmps = UCAP.DefrostAmps
	FROM CoilDefrostAmps CAP
	INNER JOIN @param_CoilDefrostAmpsUDT UCAP ON CAP.ModelID = UCAP.ModelID
		AND CAP.[Type_ID] = UCAP.[Type_ID]
	WHERE UCAP.ModelID IS NOT NULL
		AND UCAP.[Type_ID] IS NOT NULL

		------ END CoilDefrostAmpsUDT ------


		------ Start DefrostConfigurations ------

		--Delete the existing record and save new record
	DELETE
	FROM DefrostConfigurations
	WHERE ModelID = @parm_ModelId

	INSERT INTO dbo.DefrostConfigurations (
	
ModelID
,DefrostConfigID

		)
	SELECT @parm_ModelId
		,CMA.DefrostConfigID
	FROM @param_DefrostConfigurationsUDT AS CMA

		------ END DefrostConfigurations ------

END



GO