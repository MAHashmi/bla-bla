DROP PROCEDURE [dbo].[spInserorUpdateAccessory] 
go
-- =============================================
-- Author:		Asad
-- Create date: 6/2/2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[spInserorUpdateAccessory] @parm_ModelId INT
	,@param_CoilAccessoryPricingUDT CoilAccessoryPricingUDT Readonly
AS
BEGIN


	DELETE
	FROM CoilAccessoryPricing
	WHERE CoilAccessoryPricingID NOT IN (
			SELECT CoilAccessoryPricingID
			FROM @param_CoilAccessoryPricingUDT
			WHERE ModelID IS NOT NULL
			) --List from Code
		AND ModelID = @parm_ModelId --List of IDS against Model ID


		--Get family ID from coil Model If family Id is null for insert
		declare @familyID int 
		select @familyID = ModelFamilyID
		From CoilModel
		where ModelID = @parm_ModelId


	--CoilAccessoryPricingID is not null and ModelID is null then Insert else Update
		INSERT INTO dbo.CoilAccessoryPricing (
			CoilAccsID
			,CoilFamilyID
			,CoilVendorID
			,CoilAccsPartNumber
			,CoilAccsPartDescription
			,CoilAccsMinBTU
			,CoilAccsMaxBTU
			,ModelID
			)
		SELECT CoilAccsID
			,Case when CoilFamilyID is null then  @familyID
			else CoilFamilyID END
			,CoilVendorID
			,CoilAccsPartNumber
			,CoilAccsPartDescription
			,CoilAccsMinBTU
			,CoilAccsMaxBTU
			,@parm_ModelId
		FROM @param_CoilAccessoryPricingUDT AS ICAP
		WHERE ICAP.ModelID IS NULL
		AND CoilAccsID IS NOT NULL
			


--CoilAccessoryPricingID is not null and ModelID is not null then Update
		UPDATE CAP
		SET CAP.CoilAccsID = UCAP.CoilAccsID
			,CAP.CoilFamilyID = UCAP.CoilFamilyID
			,CAP.CoilVendorID = UCAP.CoilVendorID
			,CAP.CoilAccsPartNumber = UCAP.CoilAccsPartNumber
			,CAP.CoilAccsPartDescription = UCAP.CoilAccsPartDescription
			,CAP.CoilAccsMinBTU = UCAP.CoilAccsMinBTU
			,CAP.CoilAccsMaxBTU = UCAP.CoilAccsMaxBTU
		FROM CoilAccessoryPricing CAP
		INNER JOIN @param_CoilAccessoryPricingUDT UCAP ON CAP.ModelID = UCAP.ModelID
			AND CAP.CoilAccessoryPricingID = UCAP.CoilAccessoryPricingID
		WHERE UCAP.ModelID IS NOT NULL
			AND UCAP.CoilAccessoryPricingID IS NOT NULL


	
END
GO
