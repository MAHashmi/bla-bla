DROP PROCEDURE [dbo].[spCreateCoilModelWithNewVersion] 
go
-- =============================================
-- Author:		Jaffer Raza
-- Create date: 1/March/2018
-- Description:	Create new copy of model with new version
-- =============================================
CREATE PROCEDURE [dbo].[spCreateCoilModelWithNewVersion]
	@parm_ModelId INT,
	@param_ActionBy VARCHAR(75)
AS
BEGIN
	--------------------- Declaration -----------------------
	DECLARE @LatestVersion INT,
			@VersionIncrement INT =1,
			@StateID INT = 1,
			@NewModelID INT,
			@CurrentDate DATETIME = GETDATE(),
			@PreviousModelCreatedBy VARCHAR(75);

	SET @PreviousModelCreatedBy = (Select CreatedBy from CoilModel where ModelID = @parm_ModelId);
	SET @LatestVersion = (select Top 1 [Version] from CoilModel where ModelNumber = (Select ModelNumber from CoilModel where ModelID = @parm_ModelId) ORDER BY [Version] DESC);

	------------- Insert Into Coil Model --------------------
	 INSERT INTO CoilModel(
			MFGID,
			ModelFamilyID,
			ModelNumber,
			CapacityPerDegreeTDNeg20,
			CapacityPerDegreeTDPos25,
			NumberofFans,
			[Version],
			LastModifiedOn,
			Note,
			CreatedBy,
			ActionBy,
			StateID,
			CoilModelYear,
			CoilMakeYear,
			SelectedFan)

	SELECT	MFGID,
			ModelFamilyID,
			ModelNumber,
			CapacityPerDegreeTDNeg20,
			CapacityPerDegreeTDPos25,
			NumberofFans,
			@LatestVersion + @VersionIncrement,
			@CurrentDate,
			Note,
			CreatedBy,
			@param_ActionBy,
			@StateID,
			CoilModelYear,
			CoilMakeYear,
			SelectedFan
	FROM CoilModel Where ModelID = @parm_ModelId

	SET @NewModelID = SCOPE_IDENTITY();

	------------- Insert Into Coil Model Action History --------------------
	INSERT INTO [dbo].[CoilModelActionHistory]
           ([ModelID]
           ,[StateID]
           ,[CreatedBy]
           ,[LastModifiedOn]
           ,[ActionBy]
           ,[XMLData])
     VALUES
           (@NewModelID
           ,@StateID
           ,@PreviousModelCreatedBy
           ,@CurrentDate
           ,@param_ActionBy
           ,(Select *  from CoilModel where ModelID = @NewModelID For XML Path));
	
	------------- Insert Into CoilAccessoryPricing--------------------
	INSERT INTO [dbo].[CoilAccessoryPricing]
           ([CoilAccsID]
           ,[CoilFamilyID]
           ,[CoilVendorID]
           ,[CoilAccsPartNumber]
           ,[CoilAccsPartDescription]
           ,[CoilAccsMinBTU]
           ,[CoilAccsMaxBTU]
           ,[CoilAccsShipLooseListPrice]
           ,[CoilAccsListPrice]
           ,[ModelID])
     SELECT 
			[CoilAccsID]
		   ,[CoilFamilyID]
		   ,[CoilVendorID]
		   ,[CoilAccsPartNumber]
		   ,[CoilAccsPartDescription]
		   ,[CoilAccsMinBTU]
		   ,[CoilAccsMaxBTU]
		   ,[CoilAccsShipLooseListPrice]
		   ,[CoilAccsListPrice]
		   ,@NewModelID
		   FROM CoilAccessoryPricing
		   WHERE ModelID = @parm_ModelId

	------------- Insert Into Coil Option Pticing --------------------
	INSERT INTO [dbo].[CoilOptionsPricing]
           ([CoilFamilyID]
           ,[NumberFans]
           ,[PSCMotorDeduct]
           ,[PaintCost]
           ,[InsulDrainPanCost]
           ,[SSNonInsulDrainPanCost]
           ,[SSInsulDrainPanCost]
           ,[SSHousingNonInsulDrainPanCost]
           ,[SSHousingInsulDrainPanCost]
           ,[ElectroFinCost]
           ,[CopperFinCost]
           ,[ReverseAirFlowCost]
           ,[StdLongThrowAdaptersCost]
           ,[SSLongThrowAdaptersCost]
           ,[AirFilterBonnetCost]
           ,[ModelID])

	SELECT  [CoilFamilyID]
			,[NumberFans]
			,[PSCMotorDeduct]
			,[PaintCost]
			,[InsulDrainPanCost]
			,[SSNonInsulDrainPanCost]
			,[SSInsulDrainPanCost]
			,[SSHousingNonInsulDrainPanCost]
			,[SSHousingInsulDrainPanCost]
			,[ElectroFinCost]
			,[CopperFinCost]
			,[ReverseAirFlowCost]
			,[StdLongThrowAdaptersCost]
			,[SSLongThrowAdaptersCost]
			,[AirFilterBonnetCost]
			,@NewModelID
	FROM CoilOptionsPricing
	WHERE ModelID = @parm_ModelId

	------------- Insert Into CoilDefrostAmps --------------------
	INSERT INTO [dbo].[CoilDefrostAmps]
           ([ModelID]
           ,[VoltageTypeID]
           ,[DefrostAmps])

	SELECT @NewModelID
           ,[VoltageTypeID]
           ,[DefrostAmps]
	FROM CoilDefrostAmps
	WHERE ModelID = @parm_ModelId

	------------- Insert Into CoilDrainHtrAmps --------------------
	INSERT INTO [dbo].[CoilDrainHtrAmps]
           ([ModelID]
           ,[VoltageTypeID]
           ,[DrainHtrAmps])

    SELECT @NewModelID
           ,[VoltageTypeID]
           ,[DrainHtrAmps]
	FROM CoilDrainHtrAmps
	WHERE ModelID = @parm_ModelId

	------------- Insert Into DefrostConfiguration --------------------
	INSERT INTO [dbo].[DefrostConfigurations]
           ([ModelID]
           ,[DefrostConfigID])
    SELECT @NewModelID
           ,[DefrostConfigID]
	FROM DefrostConfigurations
	WHERE ModelID = @parm_ModelId

	------------- Insert Into CoilMotorAmps --------------------
	INSERT INTO [dbo].[CoilMotorAmps]
           ([ModelID]
           ,[MotorTypeID]
           ,[VoltageTypeID]
           ,[MotorAmps])

	SELECT @NewModelID
           ,[MotorTypeID]
           ,[VoltageTypeID]
           ,[MotorAmps]
	FROM CoilMotorAmps 
	WHERE  ModelID = @parm_ModelId

	----------- Return Coil Model id of new version -----------
	select @NewModelID as 'ModelID'
END	

GO
