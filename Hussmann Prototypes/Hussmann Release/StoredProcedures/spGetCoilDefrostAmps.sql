DROP PROCEDURE [dbo].[spGetCoilDefrostAmps]

CREATE PROCEDURE [dbo].[spGetCoilDefrostAmps] 
@param_ModelId INT = NULL
AS
BEGIN
	SELECT
		CAP.Type_ID
       ,CAP.VoltageTypeID
	   ,CAP.ModelID
	   ,VA.VoltageName
	   ,CAP.DefrostAmps
		
		
	FROM CoilDefrostAmps CAP
	LEFT JOIN VoltageType_lkp VA ON CAP.VoltageTypeID = VA.VoltageTypeID
	WHERE(
			CAP.ModelID = @param_ModelId
			OR @param_ModelId is NULL
			)
END


