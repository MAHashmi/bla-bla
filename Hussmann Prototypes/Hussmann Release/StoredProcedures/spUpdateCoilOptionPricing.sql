DROP PROCEDURE [dbo].[spUpdateCoilOptionPricing] 
go
-- =============================================
-- Author:		<Jaffer Raza>
-- Create date: <24/2/2018>
-- Description:	<Update Coil Option PricingGrid Data and Accessory Pricing List>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateCoilOptionPricing] 
	@parm_ModelId INT,
	@param_FamilyId INT,
	@param_SelectedFan INT,
	@param_CoilOptionPricingUDT CoilOptionsPricingUDT Readonly,
	@param_CoilAccsListPriceUDT CoilAccsListPriceUDT Readonly

AS
BEGIN
		---------------------------------- For Update Coil Option Pricing ---------------------------- 
		UPDATE COP

		SET 
			COP.[CoilFamilyID]						= UCOP.[CoilFamilyID],					
			COP.[NumberFans]						= UCOP.[NumberFans],
			COP.[PSCMotorDeduct]					= UCOP.[PSCMotorDeduct],
			COP.[PaintCost]							= UCOP.[PaintCost],
			COP.[InsulDrainPanCost]					= UCOP.[InsulDrainPanCost],
			COP.[SSNonInsulDrainPanCost]			= UCOP.[SSNonInsulDrainPanCost],
			COP.[SSInsulDrainPanCost]				= UCOP.[SSInsulDrainPanCost],
			COP.[SSHousingNonInsulDrainPanCost]		= UCOP.[SSHousingNonInsulDrainPanCost],
			COP.[SSHousingInsulDrainPanCost]		= UCOP.[SSHousingInsulDrainPanCost],
			COP.[ElectroFinCost]					= UCOP.[ElectroFinCost],
			COP.[CopperFinCost]						= UCOP.[CopperFinCost],
			COP.[ReverseAirFlowCost]				= UCOP.[ReverseAirFlowCost],
			COP.[StdLongThrowAdaptersCost]			= UCOP.[StdLongThrowAdaptersCost],
			COP.[SSLongThrowAdaptersCost]			= UCOP.[SSLongThrowAdaptersCost],
			COP.[AirFilterBonnetCost]				= UCOP.[AirFilterBonnetCost],
			COP.[ModelID]							= UCOP.[ModelID]

		FROM CoilOptionsPricing COP
		INNER JOIN @param_CoilOptionPricingUDT UCOP 
		ON COP.NumberFans = UCOP.NumberFans
		WHERE 
			COP.ModelID = @parm_ModelId
			AND COP.CoilFamilyID = @param_FamilyId;

		------------------------------- Accessory Pricing --------------------------------
		Update
		CAP
		SET
		CAP.CoilAccsListPrice = UCAP.CoilAccsListPrice
		FROM CoilAccessoryPricing CAP
		INNER JOIN @param_CoilAccsListPriceUDT UCAP
		ON CAP.CoilAccessoryPricingID = UCAP.CoilAccessoryPricingID
		WHERE
		CAP.ModelID = UCAP.ModelID
		AND CAP.CoilFamilyID = UCAP.CoilFamilyID;

		----------------------------- CoilModel ------------------------------------------
		UPDATE
		CoilModel
		Set SelectedFan = @param_SelectedFan
		where ModelID = @parm_ModelId;
END
GO
